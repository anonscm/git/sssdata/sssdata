Copies d\'écran de l\'application
================================

#### Ecran d\'accueil (17/01/14)

![Ecran d\'accueil](./snapshots/snapshot1-small.png)  
[Taille d\'origine](./snapshots/snapshot1.png)


#### Edition des nouvelles (17/01/14)


![Edition des nouvelles](./snapshots/snapshot2-small.png)  
[Taille d\'origine](./snapshots/snapshot2.png)