package fr.sedoo.commons.server.config.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="CONFIG_PROPERTY")
public class ConfigProperty {
	
	@Id
	@Column(name="CONFIG_PROPERTY_ID")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@Column(name="CONFIG_CODE")
	private String code;
	
	@Column(name="CONFIG_PROPERTY_LABEL")
	private String label;
	
	@Column(name="CONFIG_PROPERTY_VALUE", columnDefinition="TEXT")
	private String value;
	
	@Column(name="CONFIG_PROPERTY_DEFAULT", columnDefinition="TEXT")
	private String defaultValue;
	
	@ManyToOne
	@JoinColumn(name="CONFIG_CATEG_ID")
	ConfigCateg categ;
	
	public ConfigProperty() {}

	/**
	 * Renvoi la valeur de la propriété ou, si elle est absente, sa valeur par défaut. 
	 * @param property
	 * @return
	 */
	public String getValueOrDefault(){
		if (getValue() == null || getValue().isEmpty()){
			return getDefaultValue();
		}else{
			return getValue();
		}
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}

	public String getValue() {
		return value;
	}
	
	public String getDefaultValue() {
		return defaultValue;
	}
	
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public ConfigCateg getCateg() {
		return categ;
	}

	public void setCateg(ConfigCateg categ) {
		this.categ = categ;
	}
	
}
