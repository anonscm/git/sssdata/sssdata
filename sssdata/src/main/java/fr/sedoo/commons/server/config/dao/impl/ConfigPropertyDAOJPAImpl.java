package fr.sedoo.commons.server.config.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.sedoo.commons.server.config.dao.api.ConfigPropertyDAO;
import fr.sedoo.commons.server.config.model.ConfigProperty;

@Repository
public class ConfigPropertyDAOJPAImpl implements ConfigPropertyDAO{

	protected EntityManager em;
	public EntityManager getEntityManager() {
		return em;
	}

	@PersistenceContext
	public void setEntityManager(EntityManager entityManager) {
		this.em = entityManager;
	}

	@Override
	@Transactional
	public List<ConfigProperty> findAll() {
		List<ConfigProperty> aux = em.createQuery("SELECT p FROM ConfigProperty p").getResultList();
        ArrayList<ConfigProperty> result = new ArrayList<ConfigProperty>();
        result.addAll(aux);
		return result;
	}

	@Override
	@Transactional
	public ConfigProperty find(String code) {
		try{
			Query query = em.createQuery("SELECT p FROM ConfigProperty p WHERE p.code = :arg1");
			query.setParameter("arg1",code);
			return (ConfigProperty) query.getSingleResult();
		}catch(NoResultException e){
			return null;
		}
	}

	@Override
	@Transactional
	public List<ConfigProperty> findByCateg(String categ) {
		try{
			Query query = em.createQuery("SELECT p FROM ConfigProperty p WHERE p.categ.name = :arg1");
			query.setParameter("arg1",categ);
			List<ConfigProperty> aux = query.getResultList();
			List<ConfigProperty> result = new ArrayList<ConfigProperty>();
			result.addAll(aux);
			return result;
		}catch(NoResultException e){
			return null;
		}
	}

	@Override
	@Transactional
	public void update(ConfigProperty configProperty, String newValue) {
		configProperty.setValue(newValue);
		save(configProperty);
	}

	@Override
	@Transactional
	public void save(ConfigProperty configProperty) {
		if (configProperty.getId() == null){
			getEntityManager().persist(configProperty);
		}else{
			getEntityManager().merge(configProperty);
		}
	}

	@Override
	@Transactional
	public void reset(ConfigProperty configProperty) {
		configProperty.setValue(null);
		save(configProperty);
	}

	@Override
	@Transactional
	public void resetAll() {
		for (ConfigProperty prop: findAll()){
			reset(prop);
		}
	}
	
}
