package fr.sedoo.commons.server.config.dao.api;

import java.util.List;

import fr.sedoo.commons.server.config.model.ConfigProperty;

public interface ConfigPropertyDAO {
	public final static String BEAN_NAME = "configPropertyDAO";
	List<ConfigProperty> findAll();
	ConfigProperty find(String code);
	List<ConfigProperty> findByCateg(String categ);
	
	void update (ConfigProperty configProperty, String newValue);
	
	void save(ConfigProperty configProperty);
	void reset(ConfigProperty configProperty);
	void resetAll();
}
