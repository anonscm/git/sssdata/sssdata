package fr.sedoo.commons.server.config.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="CONFIG_CATEG")
public class ConfigCateg {

	@Id
	@Column(name="CONFIG_CATEG_ID")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@Column(name="CONFIG_CATEG_NAME")
	private String name;

	@OneToMany(fetch=FetchType.EAGER)
	@JoinColumn(name="CONFIG_CATEG_ID")
	private List<ConfigProperty> properties;
	
	public ConfigCateg() {
		super();
		this.properties = new ArrayList<ConfigProperty>();
	}
	
	public ConfigCateg(String name) {
		this();
		this.id = null;
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ConfigProperty> getProperties() {
		return properties;
	}

	public void setProperties(List<ConfigProperty> properties) {
		this.properties = properties;
	}
		
}
