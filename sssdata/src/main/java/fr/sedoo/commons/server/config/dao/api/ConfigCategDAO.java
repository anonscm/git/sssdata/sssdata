package fr.sedoo.commons.server.config.dao.api;

import java.util.List;

import fr.sedoo.commons.server.config.model.ConfigCateg;

public interface ConfigCategDAO {
	public final static String BEAN_NAME = "configCategDAO";
	
	List<ConfigCateg> findAll();
	ConfigCateg find(String name);
	
	//void save(ConfigCateg configProperty);
	ConfigCateg save(String name);
			
}
