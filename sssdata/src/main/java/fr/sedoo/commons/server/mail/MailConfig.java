package fr.sedoo.commons.server.mail;

public class MailConfig {

	private String host;
	private String from;
	private String admin;
	//private String contact;
	
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getAdmin() {
		return admin;
	}
	public void setAdmin(String admin) {
		this.admin = admin;
	}
	/*public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}*/
	
}
