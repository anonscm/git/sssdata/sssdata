package fr.sedoo.commons.server.mail;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

/**
 * Authenticator pour l'user brissebr.
 * 
 * @author brissebr
 */
public class MonAuthenticator extends Authenticator {

	private final String USERNAME = "brissebr";
	private final String PASSWORD = "bri001";
	
	public PasswordAuthentication getPasswordAuthentication() {
	    return new PasswordAuthentication(USERNAME, PASSWORD);
	}
	
}
