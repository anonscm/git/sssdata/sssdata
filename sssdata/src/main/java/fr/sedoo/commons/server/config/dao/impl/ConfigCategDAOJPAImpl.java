package fr.sedoo.commons.server.config.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.sedoo.commons.server.config.dao.api.ConfigCategDAO;
import fr.sedoo.commons.server.config.model.ConfigCateg;

@Repository
public class ConfigCategDAOJPAImpl implements ConfigCategDAO {

	protected EntityManager em;
	public EntityManager getEntityManager() {
		return em;
	}

	@PersistenceContext
	public void setEntityManager(EntityManager entityManager) {
		this.em = entityManager;
	}

	@Override
	@Transactional
	public List<ConfigCateg> findAll() {
		List<ConfigCateg> aux = em.createQuery("SELECT categ FROM ConfigCateg categ").getResultList();
        ArrayList<ConfigCateg> result = new ArrayList<ConfigCateg>();
        result.addAll(aux);
		return result;
	}

	@Override
	@Transactional
	public ConfigCateg find(String name) {
		try{
			Query query = em.createQuery("SELECT categ FROM ConfigCateg categ WHERE categ.name = :arg1");
			query.setParameter("arg1",name);
			return (ConfigCateg) query.getSingleResult();
		}catch(NoResultException e){
			return null;
		}
	}

	@Transactional
	private void save(ConfigCateg configCateg) {
		if (configCateg.getId() == null){
			getEntityManager().persist(configCateg);
		}else{
			getEntityManager().merge(configCateg);
		}
		
	}

	@Override
	public ConfigCateg save(String name) {
		ConfigCateg categ = find(name);
		if (categ == null){
			save(new ConfigCateg(name));
			categ = find(name);
		}
		return categ;
	}

		
}
