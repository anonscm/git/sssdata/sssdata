package fr.sedoo.commons.server.mail;

import java.io.File;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Logger;


/**
 * Classe pour envoyer des mails avec la gestion des copies (cc), copies cachées (bcc) et pièces jointes
 * @author brissebr
 */
public class MailSimple {
	
	private Session session;
	
	private final Logger log = Logger.getLogger(this.getClass());
	
	/**
	 * Constructeur
	 * @param hostname nom du serveur SMTP à utiliser
	 */
	public MailSimple(String hostname){
		
		Properties proprietes = System.getProperties();
		proprietes.setProperty("mail.smtp.host",hostname);
						
		this.session = Session.getDefaultInstance(proprietes);//, new MonAuthenticator());
		
	}
	
	/**
	 * Constructeur
	 * @param hostname nom du serveur SMTP à utiliser
	 */
	public MailSimple(String hostname, String encoding){
		
		Properties proprietes = System.getProperties();
		proprietes.setProperty("mail.smtp.host",hostname);
		proprietes.setProperty("mail.mime.charset", encoding);
		
		this.session = Session.getDefaultInstance(proprietes);//, new MonAuthenticator());
		
	}
	
	
//	********* Sans pi�ce jointe *******************//
	
	/**
	 * Envoie un mail (texte) � plusieurs personnes (en copie)
	 * @param from adresse mail de l'exp�diteur
	 * @param to adresse mail du destinataire
	 * @param cc adresse(s) mail des personnes en copie
	 * @param bcc adresse(s) mail des personnes en copie cach�e
	 * @param topic sujet du message
	 * @param body corps du message
	 */
	public void send(String from,String to,String[] cc,String[] bcc,String topic,String body) throws MailException{
		try{
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			
			if (cc != null){
				for (int i=0;i<cc.length;i++){
					message.addRecipient(Message.RecipientType.CC, new InternetAddress(cc[i]));
				}
			}
			if (bcc != null){
				for (int i=0;i<bcc.length;i++){
					message.addRecipient(Message.RecipientType.BCC, new InternetAddress(bcc[i]));
				}
			}
			message.setSubject(topic);
			message.setText(body);

			// Send message
			Transport.send(message);
		}catch(MessagingException messEx){
			log.error(messEx);
			throw new MailException();
		}
		
	}
	
	/**
	 * Envoie un mail (texte) � plusieurs personnes (en copie)
	 * @param from adresse mail de l'exp�diteur
	 * @param to adresse mail du destinataire
	 * @param cc adresse(s) mail des personnes en copie
	 * @param bcc adresse(s) mail des personnes en copie cach�e
	 * @param replyTo r�pondre �
	 * @param topic sujet du message
	 * @param body corps du message
	 */
	public void send(String from,String to,String[] cc,String[] bcc,String replyTo, String topic,String body) throws MailException{
		try{
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			
			if (replyTo != null){
				Address[] reply = {new InternetAddress(replyTo)};
				message.setReplyTo(reply);
			}
			if (cc != null){
				for (int i=0;i<cc.length;i++){
					message.addRecipient(Message.RecipientType.CC, new InternetAddress(cc[i]));
				}
			}
			if (bcc != null){
				for (int i=0;i<bcc.length;i++){
					message.addRecipient(Message.RecipientType.BCC, new InternetAddress(bcc[i]));
				}
			}
			
			message.setSubject(topic);
			message.setText(body);

			// Send message
			Transport.send(message);
		}catch(MessagingException messEx){
			log.error(messEx);
			throw new MailException();
		}
		
	}
	
		
	/**
	 * Envoie un mail (texte) � un destinataire
	 * @param from adresse mail de l'exp�diteur
	 * @param to adresse mail du destinataire
	 * @param topic sujet du message
	 * @param body corps du message
	 */
	public void send(String from,String to,String topic,String body)throws MailException{
		try{
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			message.setSubject(topic);
			message.setText(body);

			// Send message
			Transport.send(message);
		}catch(MessagingException messEx){
			log.error(messEx);
			throw new MailException();
		}
		
	}
	
	//***************** Avec pi�ce jointe (versions simples) ************************//
	
	/**
	 * Envoie un mail (texte) � un destinataire avec un fichier joint
	 * @param from adresse mail de l'exp�diteur
	 * @param to adresse mail du destinataire
	 * @param topic sujet du message
	 * @param body corps du message
	 * @param attachFilename nom du fichier � joindre
	 */
	public void send(String from,String to,String topic,String body,String attachFilename) throws MailException{
		try{
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			message.setSubject(topic);
						
			// Create the message part 
			BodyPart messageBodyPart = new MimeBodyPart();

			// Fill the message
			messageBodyPart.setText(body);

			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);

			// Part two is attachment
			messageBodyPart = new MimeBodyPart();
			DataSource source = new FileDataSource(attachFilename);
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName(attachFilename.substring(attachFilename.lastIndexOf("/")+1));
			multipart.addBodyPart(messageBodyPart);

			// Put parts in message
			message.setContent(multipart);
									
			// Send message
			Transport.send(message);
		}catch(MessagingException messEx){
			log.error(messEx);
			throw new MailException();
		}
		
	}
	
	/**
	 * Envoie un mail (texte) � un destinataire avec un fichier joint
	 * @param from adresse mail de l'exp�diteur
	 * @param to adresse mail du destinataire
	 * @param topic sujet du message
	 * @param body corps du message
	 * @param attachFile fichier � joindre
	 */
	public void send(String from,String to,String topic,String body,File attachFile) throws MailException{
		try{
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			message.setSubject(topic);
						
			// Create the message part 
			BodyPart messageBodyPart = new MimeBodyPart();

			// Fill the message
			messageBodyPart.setText(body);

			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);

			// Part two is attachment
			messageBodyPart = new MimeBodyPart();
			DataSource source = new FileDataSource(attachFile);
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName(attachFile.getName());
			multipart.addBodyPart(messageBodyPart);

			// Put parts in message
			message.setContent(multipart);
									
			// Send message
			Transport.send(message);
		}catch(MessagingException messEx){
			log.error(messEx);
			throw new MailException();
		}
		
	}
	
	//************ Avec pi�ce jointe (versions plus compl�tes) *******************//

	/**
	 * Envoie un mail (texte) � plusieurs personnes (en copie) avec �ventuellement plusieurs fichiers joints.
	 * @param from adresse mail de l'exp�diteur
	 * @param to adresse mail du destinataire
	 * @param cc adresse(s) mail des personnes en copie
	 * @param bcc adresse(s) mail des personnes en copie cach�e
	 * @param topic sujet du message
	 * @param body corps du message
	 * @param attachFile fichiers attach�s au mail (dans un tableau)
	 */
	public void send(String from,String to,String[] cc,String[] bcc,String topic,String body,File[] attachFile) throws MailException{
		try{
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			
			if (cc != null){
				for (int i=0;i<cc.length;i++){
					message.addRecipient(Message.RecipientType.CC, new InternetAddress(cc[i]));
				}
			}
			if (bcc != null){
				for (int i=0;i<bcc.length;i++){
					message.addRecipient(Message.RecipientType.BCC, new InternetAddress(bcc[i]));
				}
			}
			
			message.setSubject(topic);
						
			// Create the message part 
			BodyPart messageBodyPart = new MimeBodyPart();

			// Fill the message
			messageBodyPart.setText(body);

			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);

			// attachments
			for (int i = 0;i < attachFile.length;i++){
				messageBodyPart = new MimeBodyPart();
				DataSource source = new FileDataSource(attachFile[i]);
				messageBodyPart.setDataHandler(new DataHandler(source));
				messageBodyPart.setFileName(attachFile[i].getName());
				multipart.addBodyPart(messageBodyPart);
			}
			// Put parts in message
			message.setContent(multipart);
									
			// Send message
			Transport.send(message);
		}catch(MessagingException messEx){
			log.error(messEx);
			throw new MailException();
		}
		
	}

	/**
	 * Envoie un mail (texte) � plusieurs personnes (en copie) avec �ventuellement plusieurs fichiers joints.
	 * @param from adresse mail de l'exp�diteur
	 * @param to adresse mail du destinataire
	 * @param cc adresse(s) mail des personnes en copie
	 * @param bcc adresse(s) mail des personnes en copie cach�e
	 * @param topic sujet du message
	 * @param body corps du message
	 * @param attachFile fichiers attach�s au mail (dans un tableau)
	 */
	public void send(String from,String to,String[] cc,String[] bcc,String topic,String body,String[] attachFile) throws MailException{
		try{
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			
			if (cc != null){
				for (int i=0;i<cc.length;i++){
					message.addRecipient(Message.RecipientType.CC, new InternetAddress(cc[i]));
				}
			}
			if (bcc != null){
				for (int i=0;i<bcc.length;i++){
					message.addRecipient(Message.RecipientType.BCC, new InternetAddress(bcc[i]));
				}
			}
			
			message.setSubject(topic);
						
			// Create the message part 
			BodyPart messageBodyPart = new MimeBodyPart();

			// Fill the message
			messageBodyPart.setText(body);

			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);

			// attachments
			for (int i = 0;i < attachFile.length;i++){
				messageBodyPart = new MimeBodyPart();
				DataSource source = new FileDataSource(attachFile[i]);
				messageBodyPart.setDataHandler(new DataHandler(source));
				messageBodyPart.setFileName(attachFile[i]);
				multipart.addBodyPart(messageBodyPart);
			}
			// Put parts in message
			message.setContent(multipart);
									
			// Send message
			Transport.send(message);
		}catch(MessagingException messEx){
			log.error(messEx);
			throw new MailException();
		}
		
	}
	
}
