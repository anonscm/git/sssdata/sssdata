package fr.sedoo.commons.server.config.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.sedoo.commons.client.config.service.ConfigService;
import fr.sedoo.commons.server.DefaultServerApplication;
import fr.sedoo.commons.server.config.dao.api.ConfigCategDAO;
import fr.sedoo.commons.server.config.dao.api.ConfigPropertyDAO;
import fr.sedoo.commons.server.config.model.ConfigCateg;
import fr.sedoo.commons.server.config.model.ConfigProperty;
import fr.sedoo.commons.shared.config.ConfigCategDTO;
import fr.sedoo.commons.shared.config.ConfigPropertyDTO;
import fr.sedoo.sssdata.client.service.ServiceException;

@Repository
public class ConfigServiceImpl extends RemoteServiceServlet implements ConfigService  {

	ConfigPropertyDAO propDao;
	ConfigCategDAO categDao;
		
	private void initDao(){
		if (propDao == null){
			propDao = (ConfigPropertyDAO)DefaultServerApplication.getSpringBeanFactory().getBeanByName(ConfigPropertyDAO.BEAN_NAME);
		}
		if (categDao == null){
			categDao = (ConfigCategDAO)DefaultServerApplication.getSpringBeanFactory().getBeanByName(ConfigCategDAO.BEAN_NAME);
		}
	}
	
	private ConfigPropertyDTO buildConfigPropertyDTO(ConfigProperty property){
		if (property == null){
			return null;
		}
		ConfigPropertyDTO propertyDTO = new ConfigPropertyDTO();
		propertyDTO.setCode(property.getCode());
		propertyDTO.setLabel(property.getLabel());
		propertyDTO.setCategory(property.getCateg().getName());
		propertyDTO.setValue(property.getValueOrDefault());	
		return propertyDTO;
	}
	
	
	
	private ConfigCategDTO buildConfigCategDTO(ConfigCateg categ){
		ConfigCategDTO configCategDTO = new ConfigCategDTO(categ.getName());
		for (ConfigProperty prop: categ.getProperties()){
			configCategDTO.getProperties().add(buildConfigPropertyDTO(prop));
		}
		return configCategDTO;
	}
	
	@Override
	public List<ConfigCategDTO> getAll() throws ServiceException {
		initDao();
		List<ConfigCateg> cats = categDao.findAll();
		List<ConfigCategDTO> result = new ArrayList<ConfigCategDTO>();
		for (ConfigCateg cat: cats){
			result.add(buildConfigCategDTO(cat));
		}
		return result;
	}

	@Override
	public List<ConfigPropertyDTO> getByCateg(String categ)	throws ServiceException {
		initDao();
		List<ConfigPropertyDTO> result = new ArrayList<ConfigPropertyDTO>();
		List<ConfigProperty> aux = propDao.findByCateg(categ);
		if (aux != null){
			for (ConfigProperty prop: aux){
				result.add(buildConfigPropertyDTO(prop));
			}
		}
		return result;
	}

	@Override
	public ConfigPropertyDTO getProperty(String code) throws ServiceException {
		initDao();
		ConfigProperty property = propDao.find(code);
		return buildConfigPropertyDTO(property);
	}

	@Override
	public ConfigPropertyDTO resetProperty(String propertyCode) throws ServiceException {
		initDao();
		ConfigProperty property = propDao.find(propertyCode);
		if (property != null){
			propDao.reset(property);
			return getProperty(propertyCode);
		}else{
			return null;
		}
	}


	@Override
	public ConfigPropertyDTO updateProperty(ConfigPropertyDTO property) throws ServiceException {
		initDao();
		ConfigProperty prop = propDao.find(property.getCode());
		if (prop != null){
			propDao.update(prop, property.getValue());
			return getProperty(property.getCode());
		}
		return property;
	}

	@Override
	public List<ConfigCategDTO> restoreDefault() throws ServiceException {
		initDao();
		propDao.resetAll();
		return getAll();
	}

	@Override
	public HashMap<String, String> getProperties() {
		initDao();
		List<ConfigProperty> properties = propDao.findAll();
		HashMap<String, String> result = new HashMap<String, String>();
		for (ConfigProperty p: properties){
			result.put(p.getCode(), p.getValueOrDefault());
		}
		return result;
	}

}
