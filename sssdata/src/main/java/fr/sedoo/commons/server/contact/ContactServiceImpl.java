package fr.sedoo.commons.server.contact;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.sedoo.commons.client.contact.service.ContactService;
import fr.sedoo.commons.client.util.ServiceException;
import fr.sedoo.commons.server.DefaultServerApplication;
import fr.sedoo.commons.server.mail.MailConfig;
import fr.sedoo.commons.server.mail.MailSimple;
import fr.sedoo.commons.shared.contact.ContactInformations;

public class ContactServiceImpl extends RemoteServiceServlet implements ContactService {

	public static final String MAIL_CONFIG_BEAN_NAME = "mailConfig";
	private MailConfig mailConfig;

	/**
	 * TODO récupérer le mail de contact dans la conf
	 */
	@Override
	public void sendMessage(ContactInformations infos, String to) throws ServiceException {

		if (mailConfig == null)	{
			mailConfig = (MailConfig) DefaultServerApplication.getSpringBeanFactory().getBeanByName(MAIL_CONFIG_BEAN_NAME);
		}
		
		MailSimple m = new MailSimple(mailConfig.getHost());
		try{
			m.send(infos.getFrom(),  to, infos.getTitle(), infos.getMessage());
		}catch(Exception e){
			throw new ServiceException("Error while sending email to " +  to + ". Cause: " + e);
		}
		
	}

}
