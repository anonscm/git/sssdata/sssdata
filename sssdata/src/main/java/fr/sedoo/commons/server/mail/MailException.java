package fr.sedoo.commons.server.mail;

/**
 * @author brissebr
 */
public class MailException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public MailException() {
		super();
	}

	/**
	 * @param message
	 */
	public MailException(String message) {
		super(message);
	}
}
