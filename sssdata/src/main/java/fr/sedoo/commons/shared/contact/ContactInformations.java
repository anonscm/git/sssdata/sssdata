package fr.sedoo.commons.shared.contact;

import java.io.Serializable;

public class ContactInformations implements Serializable {

	private String from;
	private String title;
	private String message;
			
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	
}
