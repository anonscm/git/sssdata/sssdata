package fr.sedoo.commons.shared.config;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.commons.shared.domain.HasIdentifier;

public class ConfigPropertyDTO implements Serializable, HasIdentifier{

	private String code;
	private String category;
	private String label;
	private String value;
			
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	@Override
	public String getIdentifier() {
		return code;
	}
	@Override
	public List<ValidationAlert> validate() {
		return new ArrayList<ValidationAlert>();
	}
	
}
