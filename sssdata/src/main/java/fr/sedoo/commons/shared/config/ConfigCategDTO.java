package fr.sedoo.commons.shared.config;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ConfigCategDTO implements Serializable{

	private String name;
	private List<ConfigPropertyDTO> properties;
	
	public ConfigCategDTO() {
		super();
		this.properties = new ArrayList<ConfigPropertyDTO>();
	}
	
	public ConfigCategDTO(String name) {
		this();
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<ConfigPropertyDTO> getProperties() {
		return properties;
	}
	public void setProperties(List<ConfigPropertyDTO> properties) {
		this.properties = properties;
	}
		
}
