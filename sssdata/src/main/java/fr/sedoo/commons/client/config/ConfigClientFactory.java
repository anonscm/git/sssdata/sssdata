package fr.sedoo.commons.client.config;

import fr.sedoo.commons.client.config.ui.view.ConfigView;
import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;

public interface ConfigClientFactory extends AuthenticatedClientFactory {

	ConfigView getConfigView();
	
}
