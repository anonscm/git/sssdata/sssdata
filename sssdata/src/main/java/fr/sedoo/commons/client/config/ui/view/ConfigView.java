package fr.sedoo.commons.client.config.ui.view;

import java.util.List;

import fr.sedoo.commons.client.crud.component.CrudView;
import fr.sedoo.commons.shared.config.ConfigCategDTO;
import fr.sedoo.commons.shared.config.ConfigPropertyDTO;

public interface ConfigView extends CrudView {

	void setPresenter(Presenter presenter);
	void setProperty(ConfigPropertyDTO property);
	void setProperties(List<ConfigCategDTO> properties);
		
	public interface Presenter {
		void resetAll();
		void resetProperty(ConfigPropertyDTO property);
	}
	
}
