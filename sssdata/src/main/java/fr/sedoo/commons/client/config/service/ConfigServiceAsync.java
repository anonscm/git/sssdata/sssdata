package fr.sedoo.commons.client.config.service;

import java.util.HashMap;
import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.sedoo.commons.shared.config.ConfigCategDTO;
import fr.sedoo.commons.shared.config.ConfigPropertyDTO;

public interface ConfigServiceAsync {

	void restoreDefault(AsyncCallback<List<ConfigCategDTO>> callback);

	void getAll(AsyncCallback<List<ConfigCategDTO>> callback);

	void getByCateg(String categ,
			AsyncCallback<List<ConfigPropertyDTO>> callback);

	void getProperty(String code, AsyncCallback<ConfigPropertyDTO> callback);

	void resetProperty(String property,
			AsyncCallback<ConfigPropertyDTO> callback);

	void updateProperty(ConfigPropertyDTO property,
			AsyncCallback<ConfigPropertyDTO> callback);

	void getProperties(AsyncCallback<HashMap<String, String>> callback);

}
