package fr.sedoo.commons.client.config.message;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.LocalizableResource.DefaultLocale;
import com.google.gwt.i18n.client.LocalizableResource.Generate;
import com.google.gwt.i18n.client.Messages;

@Generate(format = { "com.google.gwt.i18n.rebind.format.PropertiesFormat" }, fileName = "Message", locales = { "en" })
@DefaultLocale("en")
public interface ConfigMessages extends Messages{

	public static final ConfigMessages INSTANCE = GWT.create(ConfigMessages.class);
	
	@Key("editDialog.title")
	public String editDialogTitle();
	
	@Key("view.title")
	public String configViewTitle();
	
	@Key("reset")
	public String reset();
	
	@Key("resetAll")
	public String resetAll();
	
	@Key("categ")
	public String categ();
	
	
}
