package fr.sedoo.commons.client.contact.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class ContactPlace extends Place{

	public static ContactPlace instance;
	public ContactPlace(){}

	public static class Tokenizer implements PlaceTokenizer<ContactPlace>{
		public ContactPlace getPlace(String token) {
			if (instance == null){
				instance = new ContactPlace();
			}
			return instance;
		}
		public String getToken(ContactPlace place) {
			return "";
		}
	}
}
