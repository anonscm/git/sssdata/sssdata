package fr.sedoo.commons.client.config.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class ConfigPlace extends Place{
	public static ConfigPlace instance;

	public ConfigPlace()
	{
	}

	public static class Tokenizer implements PlaceTokenizer<ConfigPlace>
	{

		public ConfigPlace getPlace(String token) {
			if (instance == null)
			{
				instance = new ConfigPlace();
			}
			return instance;
		}

		public String getToken(ConfigPlace place) {
			return "";
		}


	}
}
