package fr.sedoo.commons.client.analytics;

import com.google.gwt.event.shared.EventHandler;

public interface GAEventHandler  extends EventHandler{
	void onGAEvent(GAEvent event);
}
