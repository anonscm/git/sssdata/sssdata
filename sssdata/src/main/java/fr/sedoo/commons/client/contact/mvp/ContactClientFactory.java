package fr.sedoo.commons.client.contact.mvp;

import fr.sedoo.commons.client.contact.ui.ContactView;
import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;

public interface ContactClientFactory extends AuthenticatedClientFactory {

	ContactView getContactView();
	
}
