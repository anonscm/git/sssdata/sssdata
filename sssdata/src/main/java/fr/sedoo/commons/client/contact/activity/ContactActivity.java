package fr.sedoo.commons.client.contact.activity;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.sedoo.commons.client.contact.mvp.ContactClientFactory;
import fr.sedoo.commons.client.contact.place.ContactPlace;
import fr.sedoo.commons.client.contact.service.ContactService;
import fr.sedoo.commons.client.contact.service.ContactServiceAsync;
import fr.sedoo.commons.client.contact.ui.ContactView;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.commons.shared.contact.ContactInformations;
import fr.sedoo.sssdata.client.event.ActionEventConstants;

public abstract class ContactActivity extends AbstractActivity implements ContactView.Presenter{

	private final static ContactServiceAsync CONTACT_SERVICE = GWT.create(ContactService.class);
	
	private ContactClientFactory clientFactory;
	private EventBus eventbus;
	protected ContactView contactView;
	
	public ContactActivity(ContactPlace place, ContactClientFactory clientFactory) {
		this.clientFactory = clientFactory;
	}

	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		this.eventbus = eventBus;
		this.contactView = clientFactory.getContactView();
		contactView.setPresenter(this);
		contactView.reset();
		containerWidget.setWidget(contactView.asWidget());
	}

	@Override
	public void sendMessage(ContactInformations infos, String to) {
		ActionStartEvent event = new ActionStartEvent("Sending email..." , ActionEventConstants.BASIC_LOADING_EVENT, true);
		eventbus.fireEvent(event);
		CONTACT_SERVICE.sendMessage(infos, to, new DefaultAbstractCallBack<Void>(event, eventbus) {
			@Override
			public void onSuccess(Void result) {
				super.onSuccess(result);
				contactView.reset();
				contactView.addMessage("Email successfully sent");
			}
		});
	}
		
	@Override
	public void getDefaultCriterion() {
		ContactInformations infos = new ContactInformations();
		infos.setMessage("");
		infos.setTitle("");
		infos.setFrom("");
		contactView.setSearchCriterion(infos);		
	}

}
