package fr.sedoo.commons.client.config.service;

import java.util.HashMap;
import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fr.sedoo.commons.shared.config.ConfigCategDTO;
import fr.sedoo.commons.shared.config.ConfigPropertyDTO;
import fr.sedoo.sssdata.client.service.ServiceException;

@RemoteServiceRelativePath("config")
public interface ConfigService extends RemoteService {
		
	HashMap<String,String> getProperties();
	
	List<ConfigCategDTO> getAll() throws ServiceException;
	List<ConfigPropertyDTO> getByCateg(String categ) throws ServiceException;
	ConfigPropertyDTO getProperty(String code) throws ServiceException;
	
	ConfigPropertyDTO updateProperty (ConfigPropertyDTO property) throws ServiceException;
	ConfigPropertyDTO resetProperty(String property) throws ServiceException;
		
	List<ConfigCategDTO> restoreDefault() throws ServiceException;

}
