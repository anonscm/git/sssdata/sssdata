package fr.sedoo.commons.client.config.ui.component;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;

import fr.sedoo.commons.client.config.message.ConfigMessages;
import fr.sedoo.commons.client.config.ui.dialog.ConfigEditDialogContent;
import fr.sedoo.commons.client.crud.widget.CreateConfirmCallBack;
import fr.sedoo.commons.client.crud.widget.CrudTable;
import fr.sedoo.commons.client.crud.widget.EditConfirmCallBack;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.widget.dialog.EditDialogContent;
import fr.sedoo.commons.shared.config.ConfigPropertyDTO;
import fr.sedoo.commons.shared.domain.HasIdentifier;

public class ConfigPropertyTable extends CrudTable{
	
	TextColumn<HasIdentifier> categColumn;
	TextColumn<HasIdentifier> nameColumn;
	TextColumn<HasIdentifier> paramColumn;
	
	
	public ConfigPropertyTable() {
		super();
		hideToolBar();
	}
	
	@Override
	protected void initColumns(){

		//table.setWidth("95%");
				
		categColumn = new TextColumn<HasIdentifier>() {
			@Override
			public String getValue(HasIdentifier aux){
				ConfigPropertyDTO property = (ConfigPropertyDTO) aux;
				return property.getCategory();
			}
		};
		
		nameColumn = new TextColumn<HasIdentifier>() {
			@Override
			public String getValue(HasIdentifier aux){
				ConfigPropertyDTO property = (ConfigPropertyDTO) aux;
				return property.getLabel();
			}
		};

		paramColumn = new TextColumn<HasIdentifier>() {
			@Override
			public String getValue(HasIdentifier aux){
				ConfigPropertyDTO property = (ConfigPropertyDTO) aux;
				return property.getValue();
			}
		};

		table.addColumn(categColumn, ConfigMessages.INSTANCE.categ());
		table.setColumnWidth(categColumn, 100.0, Unit.PX);
		table.addColumn(nameColumn, CommonMessages.INSTANCE.name());
		table.setColumnWidth(nameColumn, 100.0, Unit.PX);
		table.addColumn(paramColumn, CommonMessages.INSTANCE.value());
		table.setColumnWidth(paramColumn, 100.0, Unit.PX);
		table.addColumn(editColumn);
		table.setColumnWidth(editColumn, 30.0, Unit.PX);
				
	}
	
	
	@Override
	public String getAddItemText() {
		return null;
	}

	@Override
	public String getEditDialogTitle() {
		return ConfigMessages.INSTANCE.editDialogTitle();
	}

	@Override
	public EditDialogContent getEditDialogContent(EditConfirmCallBack callBack,	HasIdentifier hasIdentifier) {
		return new ConfigEditDialogContent(callBack, (ConfigPropertyDTO) hasIdentifier);		
	}

	@Override
	public String getCreateDialogTitle() {
		return null;
	}

	@Override
	public EditDialogContent getCreateDialogContent(CreateConfirmCallBack callBack, HasIdentifier hasIdentifier) {
		return null;	
	}

	@Override
	public HasIdentifier createNewItem() {
		return new ConfigPropertyDTO();
	}

}
