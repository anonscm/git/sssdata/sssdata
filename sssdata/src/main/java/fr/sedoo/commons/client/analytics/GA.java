package fr.sedoo.commons.client.analytics;

import com.google.gwt.user.client.Window;

public class GA {
	public static native void trackPageview(String page) /*-{
	$wnd._gaq.push(['_trackPageview', page]);
	}-*/;

	public static void trackPageview(){
		trackPageview(Window.Location.getHref());
	}

}
