package fr.sedoo.commons.client.config.ui.view;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.config.ui.component.ConfigPropertyTable;
import fr.sedoo.commons.client.crud.component.CrudViewImpl;
import fr.sedoo.commons.client.crud.widget.CrudTable;
import fr.sedoo.commons.client.widget.ConfirmCallBack;
import fr.sedoo.commons.client.widget.DialogBoxTools;
import fr.sedoo.commons.shared.config.ConfigCategDTO;
import fr.sedoo.commons.shared.config.ConfigPropertyDTO;
import fr.sedoo.sssdata.client.GlobalBundle;

public class ConfigViewImpl extends CrudViewImpl implements ConfigView{

	private static ConfigViewImplUiBinder uiBinder = GWT.create(ConfigViewImplUiBinder.class);

	interface ConfigViewImplUiBinder extends UiBinder<Widget, ConfigViewImpl> {}
	
	Presenter presenter;
	
	@UiField
	ConfigPropertyTable propertiesTable;
	
	public ConfigViewImpl() {
		GWT.<GlobalBundle>create(GlobalBundle.class).css().ensureInjected();
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
		propertiesTable.init(new ArrayList<ConfigPropertyDTO>());
	}
	
	@UiHandler("resetParameters")
	void onResetParametersButtonClicked(ClickEvent event){
		DialogBox modalConfirm = DialogBoxTools.modalConfirm("Confirm", "Do you confirm this action ?", new ConfirmCallBack() {
			@Override
			public void confirm(boolean choice) {
				if (choice){
					presenter.resetAll();
				}
			}
		});
		modalConfirm.center();
		modalConfirm.show();
	}
	
	
	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;		
	}

	@Override
	public void setProperty(ConfigPropertyDTO property) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setProperties(List<ConfigCategDTO> properties) {
		List<ConfigPropertyDTO> props = new ArrayList<ConfigPropertyDTO>();
		for (ConfigCategDTO categ: properties){
			props.addAll(categ.getProperties());
		}
		propertiesTable.init(props);
	}

	@Override
	public CrudTable getCrudTable() {
		return propertiesTable;
	}

}
