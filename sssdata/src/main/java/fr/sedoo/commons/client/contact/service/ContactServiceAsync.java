package fr.sedoo.commons.client.contact.service;

import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.sedoo.commons.shared.contact.ContactInformations;


public interface ContactServiceAsync {
	void sendMessage(ContactInformations infos, String to,
			AsyncCallback<Void> callback);	
}
