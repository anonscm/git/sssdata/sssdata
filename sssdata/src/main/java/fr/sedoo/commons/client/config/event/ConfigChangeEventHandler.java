package fr.sedoo.commons.client.config.event;

import com.google.gwt.event.shared.EventHandler;


public interface ConfigChangeEventHandler extends EventHandler {
	void onConfigChange(ConfigChangeEvent event);
}
