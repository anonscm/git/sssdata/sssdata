package fr.sedoo.commons.client.config.event;

import com.google.gwt.event.shared.GwtEvent;

import fr.sedoo.commons.shared.config.ConfigPropertyDTO;

public class ConfigChangeEvent extends GwtEvent<ConfigChangeEventHandler>{
	public static final Type<ConfigChangeEventHandler> TYPE = new Type<ConfigChangeEventHandler>();

	private ConfigPropertyDTO property;

	public ConfigChangeEvent(ConfigPropertyDTO property) {
		super();
		this.property = property;
	}

	@Override
	public Type<ConfigChangeEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(ConfigChangeEventHandler handler) {
		handler.onConfigChange(this);
	}

	public ConfigPropertyDTO getProperty() {
		return property;
	}
}
