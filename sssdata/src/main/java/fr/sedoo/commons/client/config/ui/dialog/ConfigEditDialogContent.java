package fr.sedoo.commons.client.config.ui.dialog;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.crud.widget.CrudConfirmCallBack;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.dialog.EditDialogContent;
import fr.sedoo.commons.shared.config.ConfigPropertyDTO;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.sssdata.client.ui.dialog.DialogBoxContent;

public class ConfigEditDialogContent  extends EditDialogContent implements DialogBoxContent {

	private static ConfigEditDialogContentUiBinder uiBinder = GWT.create(ConfigEditDialogContentUiBinder.class);

	interface ConfigEditDialogContentUiBinder extends UiBinder<Widget, ConfigEditDialogContent> {}
	
	String id;
	
	@UiField
	Label categ;
	
	@UiField
	Label name;

	@UiField
	TextBox value;
	
	public ConfigEditDialogContent(CrudConfirmCallBack confirmCallback, ConfigPropertyDTO property) {
		super(confirmCallback);
		initWidget(uiBinder.createAndBindUi(this));
		edit(property);
	}
	
	private void edit(ConfigPropertyDTO property){
		id = property.getCode();
		categ.setText(StringUtil.trimToEmpty(property.getCategory()));
		name.setText(StringUtil.trimToEmpty(property.getLabel()));
		value.setText(StringUtil.trimToEmpty(property.getValue()));
	}
	
	@Override
	public String getPreferredHeight() {
		return "100px";
	}

	@Override
	public HasIdentifier flush() {
		ConfigPropertyDTO property = new ConfigPropertyDTO();
		property.setCode(id);
		property.setCategory(categ.getText());
		property.setLabel(name.getText());
		property.setValue(value.getText());
		return property;
	}
	
}
