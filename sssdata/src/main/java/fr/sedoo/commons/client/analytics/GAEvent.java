package fr.sedoo.commons.client.analytics;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.place.shared.Place;

public class GAEvent extends GwtEvent<GAEventHandler>{

	public static final Type<GAEventHandler> TYPE = new Type<GAEventHandler>();
	
	private String placeName;

	public GAEvent(Place place) {
		super();
		placeName = place.getClass().getName();
		placeName = placeName.substring(placeName.lastIndexOf(".") + 1);			
	}
	
	@Override
	public Type<GAEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(GAEventHandler handler) {
		handler.onGAEvent(this);
	}
	
	public String getPlaceName() {
		return placeName;
	}
}
