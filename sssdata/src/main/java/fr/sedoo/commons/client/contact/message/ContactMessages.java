package fr.sedoo.commons.client.contact.message;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.LocalizableResource.DefaultLocale;
import com.google.gwt.i18n.client.LocalizableResource.Generate;
import com.google.gwt.i18n.client.Messages;

@Generate(
format = { "com.google.gwt.i18n.rebind.format.PropertiesFormat" }, 
fileName = "Message", 
locales = {"en"})
@DefaultLocale("en")
public interface ContactMessages extends Messages {

	public static final ContactMessages INSTANCE = GWT
			.create(ContactMessages.class);
	
	@Key("title")
	public String title();
	@Key("topic")
	public String topic();
	@Key("body")
	public String body();
	@Key("email")
	public String email();
	@Key("send")
	public String send();
}
