package fr.sedoo.commons.client.config.activity;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.sedoo.commons.client.config.ClientPropertiesList;
import fr.sedoo.commons.client.config.ConfigClientFactory;
import fr.sedoo.commons.client.config.event.ConfigChangeEvent;
import fr.sedoo.commons.client.config.place.ConfigPlace;
import fr.sedoo.commons.client.config.service.ConfigService;
import fr.sedoo.commons.client.config.service.ConfigServiceAsync;
import fr.sedoo.commons.client.config.ui.view.ConfigView;
import fr.sedoo.commons.client.config.ui.view.ConfigView.Presenter;
import fr.sedoo.commons.client.crud.activity.CrudActivity;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.commons.shared.config.ConfigCategDTO;
import fr.sedoo.commons.shared.config.ConfigPropertyDTO;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.sssdata.client.event.ActionEventConstants;
import fr.sedoo.sssdata.client.event.NotificationEvent;

public abstract class ConfigActivity extends CrudActivity implements Presenter{

	public final static ConfigServiceAsync CONFIG_SERVICE = GWT.create(ConfigService.class);
	
	protected ConfigView view;
	
	protected EventBus eventBus;
	
	public ConfigActivity(ConfigClientFactory clientFactory, ConfigPlace place) {
		super(clientFactory, place);
	}

	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		if (isValidUser() == false) {
			goToLoginPlace();
			return;
		}else{
			this.view = ((ConfigClientFactory)clientFactory).getConfigView();  
			view.setPresenter(this);
			view.setCrudPresenter(this);
			this.eventBus = eventBus;
			ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstants.BASIC_LOADING_EVENT, true);
	    	eventBus.fireEvent(e);
	    	panel.setWidget(view.asWidget());
	    	
	        CONFIG_SERVICE.getAll(new DefaultAbstractCallBack<List<ConfigCategDTO>>(e, eventBus) {
				@Override
				public void onSuccess(List<ConfigCategDTO> result) {
					super.onSuccess(result);
					view.setProperties(result);
				}
				
			});
		}
	}

    /**
     * Ask user before stopping this activity
     */
    @Override
    public String mayStop() {
    	return null;
    }

	@Override
	public void resetAll() {
		ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.saving(), ActionEventConstants.BASIC_DELETING_EVENT, true);
    	eventBus.fireEvent(e);
    	
    	CONFIG_SERVICE.restoreDefault(new DefaultAbstractCallBack<List<ConfigCategDTO>>(e, eventBus) {
    		@Override
    		public void onSuccess(List<ConfigCategDTO> result) {
    			super.onSuccess(result);
    			ClientPropertiesList.loadProperties(clientFactory);
    			view.setProperties(result);
    			clientFactory.getEventBus().fireEvent(new NotificationEvent(CommonMessages.INSTANCE.operationSuccessful()));
    		}
		});
		
	}

	@Override
	public void resetProperty(ConfigPropertyDTO property) {
		ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.saving(), ActionEventConstants.BASIC_DELETING_EVENT, true);
    	eventBus.fireEvent(e);
    	
    	CONFIG_SERVICE.resetProperty(property.getIdentifier(),  new DefaultAbstractCallBack<ConfigPropertyDTO>(e, eventBus) {
    		@Override
    		public void onSuccess(ConfigPropertyDTO result) {
    			super.onSuccess(result);
    			view.broadcastEdition(result);
    			//TODO mettre à jour une liste coté client ?
    			ClientPropertiesList.updateProperty(result.getCode(), result.getValue());
    			clientFactory.getEventBus().fireEvent(new NotificationEvent(CommonMessages.INSTANCE.operationSuccessful()));
    		}
		});
		
	}
	
	@Override
	public void edit(final HasIdentifier hasIdentifier) {
		ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.saving(), ActionEventConstants.BASIC_SAVING_EVENT, true);
    	eventBus.fireEvent(e);
    	
    	CONFIG_SERVICE.updateProperty((ConfigPropertyDTO)hasIdentifier, new DefaultAbstractCallBack<ConfigPropertyDTO>(e, eventBus) {
    		@Override
    		public void onSuccess(ConfigPropertyDTO result) {
    			super.onSuccess(result);
    			ClientPropertiesList.updateProperty(result.getCode(), result.getValue());
    			view.broadcastEdition(result);
    			eventBus.fireEvent(new NotificationEvent(CommonMessages.INSTANCE.savedElement()));
    			eventBus.fireEvent(new ConfigChangeEvent(result));
    		}
		});
		
	}

	@Override
	public void delete(HasIdentifier hasIdentifier) {
		//Not implemented		
	}

	@Override
	public void create(HasIdentifier hasIdentifier) {
		//Not implemented
	}
	
}
