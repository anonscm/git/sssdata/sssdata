package fr.sedoo.commons.client.captcha;

import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;

public class CaptchaComposite extends Composite { 
    private Grid grid = new Grid(1, 1); 
    private static final String DIV_NAME = "g-recaptcha"; 
    
    public CaptchaComposite() { 

            this.initWidget(grid); 

            Label dummyLable = new Label(); 
            grid.setWidget(0, 0, dummyLable); 
            Element divElement = dummyLable.getElement(); 
            divElement.setAttribute("id", DIV_NAME); 
    } 

    private void createChallenge() { 
            createNewChallengeJSNI(); 
    } 

    public String getChallengeField() { 
            String value = getChallengeJSNI(); 
            //PMLogger.debug("challenge=" + value); 
            return value; 
    } 
    public String getResponse() { 
            String value = getResponseJSNI(); 
            //PMLogger.debug("response=" + value); 
            return value; 
    } 


private native String getResponseJSNI() 
/*-{ 
  return $wnd.Recaptcha.get_response(); 
}-*/; 

private native String getChallengeJSNI() 
/*-{ 
  return $wnd.Recaptcha.get_challenge(); 
}-*/; 

private native void createNewChallengeJSNI() 
/*-{ 
            $wnd.Recaptcha.create("6Lf3jP4SAAAAAGhbyL5-mB7NttmyobMADCDhaYK_", 
            "g-recaptcha", { 
               theme: "red", 
               callback: $wnd.Recaptcha.focus_response_field 
            }); 
}-*/; 

private native void reloadChallengeJSNI() 
/*-{ 
            $wnd.Recaptcha.reload(); 
}-*/; 

private native void destroyJSNI() 
/*-{ 
            $wnd.Recaptcha.destroy(); 
}-*/; 

    public void createNewChallenge() { 
            reloadChallengeJSNI(); 
    } 

    public void destroyCaptcha() { 
            destroyJSNI(); 
    } 

    @Override 
    protected void onAttach() { 
            super.onAttach(); 
            createChallenge(); 
    } 

} 