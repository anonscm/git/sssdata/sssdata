package fr.sedoo.commons.client.contact.ui;

import com.google.gwt.editor.client.Editor;
import com.google.gwt.user.client.ui.IsWidget;

import fr.sedoo.commons.shared.contact.ContactInformations;

public interface ContactView extends IsWidget, Editor<ContactInformations> {

	void reset();
	void setPresenter(Presenter presenter);
	void setSearchCriterion(ContactInformations infos);
	void addMessage(String msg);
	void addErrorMessage(String msg);
	void resetMessages();
	
	void setContactEmail(String email);
	
	public interface Presenter {
		void sendMessage(ContactInformations infos, String to);
		void getDefaultCriterion();
		void getContactEmail();
	}


}
