package fr.sedoo.commons.client.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;

import fr.sedoo.commons.client.callback.LoadCallBack;
import fr.sedoo.commons.client.config.service.ConfigService;
import fr.sedoo.commons.client.config.service.ConfigServiceAsync;
import fr.sedoo.commons.client.event.ActionEventConstants;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.mvp.BasicClientFactory;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;


public class ClientPropertiesList {

	private static Map<String , String> properties = new HashMap<String , String>();
	private static boolean loaded = false;
	private static ArrayList<LoadCallBack<Map<String , String>>> callBacks = new ArrayList<LoadCallBack<Map<String , String>>>();
	private static HashMap<String, LoadCallBack<String>> parameterCallBacks = new HashMap<String, LoadCallBack<String>>();
	private final static ConfigServiceAsync CONFIG_SERVICE = GWT.create(ConfigService.class);


	public static void getProperties(LoadCallBack<Map<String , String>> callBack) {
		if (loaded){
			callBack.postLoadProcess(properties);
		}else{
			callBacks.add(callBack);
		}
	}

	public static void getProperty(String key, LoadCallBack<String> callBack){
		if (loaded){
			callBack.postLoadProcess(properties.get(key));
		}else{
			parameterCallBacks.put(key,  callBack);
		}
	}

	public static void loadProperties(BasicClientFactory clientFactory){
		final EventBus eventBus = clientFactory.getEventBus();
		ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstants.PARAMETER_LOADING_EVENT, true);
		eventBus.fireEvent(e);

		CONFIG_SERVICE.getProperties(new DefaultAbstractCallBack<HashMap<String,String>>(e, eventBus) {

			@Override
			public void onSuccess(HashMap<String, String> result) {
				super.onSuccess(result);
				properties = result;
				loaded = true;
				Iterator<LoadCallBack<Map<String,String>>> iterator = callBacks.iterator();
				while (iterator.hasNext()) {
					getProperties(iterator.next());
				}

				Iterator<String> iterator2 = parameterCallBacks.keySet().iterator();
				while (iterator2.hasNext()) {
					String parameterName = iterator2.next();
					getProperty(parameterName, parameterCallBacks.get(parameterName));
				}
				parameterCallBacks.clear();
				callBacks.clear();
			}

		});

	}

	public static void updateProperty(String key, String value) {
		if (properties != null){
			properties.put(key, value);
		}
	}
}
