package fr.sedoo.commons.client.contact.ui;

import org.gwtbootstrap3.client.ui.Alert;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.component.AbstractView;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.util.ValidationUtil;
import fr.sedoo.commons.shared.contact.ContactInformations;

public class ContactViewImpl extends AbstractView implements ContactView {

	private static ContactViewImplUiBinder uiBinder = GWT.create(ContactViewImplUiBinder.class);
	interface ContactViewImplUiBinder extends UiBinder<Widget, ContactViewImpl> {
	}

	interface ContactDriver extends SimpleBeanEditorDriver<ContactInformations, ContactViewImpl> {}
	private ContactDriver driver = GWT.create(ContactDriver.class);

	Presenter presenter;

	//@Ignore
	//@UiField(provided=false)
	//VerticalPanel messagePanel;
	@Ignore
	@UiField
	Alert infoMsg;
	@Ignore
	@UiField
	Alert errorMsg;
	
	String contactEmail;
	
	@UiField(provided=false)
	TextBox fromEditor;
	@UiField(provided=false)
	TextBox titleEditor;
	@UiField(provided=false)
	TextArea messageEditor;

	public ContactViewImpl() {
		super();
		initWidget(uiBinder.createAndBindUi(this));
		driver.initialize(this);	
		
	}
	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public void setContactEmail(String email) {
		this.contactEmail = email;
	}
	
	@Override
	public void reset() {
		presenter.getDefaultCriterion();
		presenter.getContactEmail();
		resetMessages();
		//resetTextFields();
	}

/*	private void resetTextFields(){
		fromEmailEditor.getElement().getStyle().setBackgroundColor("#FFFFFF");
		titleEditor.getElement().getStyle().setBackgroundColor("#FFFFFF");
		messageEditor.getElement().getStyle().setBackgroundColor("#FFFFFF");
	}*/

	private boolean validInfos(ContactInformations infos) {
		boolean result = true;
		resetMessages();
		//resetTextFields();

		if (StringUtil.isEmpty(infos.getFrom())){
			//fromEmailEditor.getElement().getStyle().setBackgroundColor("#FFCCCC");
			addErrorMessage("Email is required");
			result = false;
		}else{
			boolean isValidEmail = ValidationUtil.isValidEmail(infos.getFrom());
			if (!isValidEmail){
				addErrorMessage("Email is incorrect");
				result = false;
			}
		}

		if (StringUtil.isEmpty(infos.getTitle())){
			//titleEditor.getElement().getStyle().setBackgroundColor("#FFCCCC");
			addErrorMessage("Title is required");
			result = false;
		}

		if (StringUtil.isEmpty(infos.getMessage())){
			//messageEditor.getElement().getStyle().setBackgroundColor("#FFCCCC");
			addErrorMessage("Message is required");
			result = false;
		}
						
		return result;
	}

	@UiHandler("resetButton")
	void onResetButtonClicked(ClickEvent event){
		reset();
	}


	@UiHandler("sendButton")
	void onSendButtonClicked(ClickEvent event){
		if (validInfos(driver.flush()) && contactEmail != null){
			presenter.sendMessage(driver.flush(), contactEmail);
		}
	}

	@Override
	public void addMessage(String msg) {
		Label l = new Label(msg);
		//l.getElement().getStyle().setColor("green");
		//messagePanel.add(l);
		infoMsg.add(l);
		infoMsg.setVisible(true);
	}

	@Override
	public void addErrorMessage(String msg) {
		Label l = new Label(msg);
		//l.getElement().getStyle().setColor("red");
		//messagePanel.add(l);
		errorMsg.add(l);
		errorMsg.setVisible(true);
	}

	@Override
	public void resetMessages() {
		//messagePanel.clear();
		errorMsg.clear();
		infoMsg.clear();
		infoMsg.setVisible(false);
		errorMsg.setVisible(false);
	}

	@Override
	public void setSearchCriterion(ContactInformations infos) {
		driver.edit(infos);
	}

}
