package fr.sedoo.commons.client.contact.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fr.sedoo.commons.client.util.ServiceException;
import fr.sedoo.commons.shared.contact.ContactInformations;

@RemoteServiceRelativePath("contact")
public interface ContactService extends RemoteService{
	void sendMessage(ContactInformations infos, String to) throws ServiceException;
}
