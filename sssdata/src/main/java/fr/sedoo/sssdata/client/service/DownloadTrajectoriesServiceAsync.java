package fr.sedoo.sssdata.client.service;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.sedoo.sssdata.shared.FileDTO;
import fr.sedoo.sssdata.shared.SearchCriterion;

public interface DownloadTrajectoriesServiceAsync {

	void downloadTrajectory(Long requestId, String trajectoryName,
			SearchCriterion sc, String outputFormat,
			AsyncCallback<FileDTO> callback);

	void createArchive(Long requestId, List<FileDTO> fichiers,
			String compression, AsyncCallback<String> callback);

}
