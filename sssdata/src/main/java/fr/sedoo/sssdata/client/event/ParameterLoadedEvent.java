package fr.sedoo.sssdata.client.event;

import java.util.Map;

import com.google.gwt.event.shared.GwtEvent;

public class ParameterLoadedEvent extends GwtEvent<ParameterLoadedEventHandler>{

	Map<String, String> parameters;
	
    public static final Type<ParameterLoadedEventHandler> TYPE = new Type<ParameterLoadedEventHandler>();

    public ParameterLoadedEvent(Map<String, String> parameters)
    {
    	this.parameters = parameters;
    }
    
	@Override
    protected void dispatch(ParameterLoadedEventHandler handler) {
            handler.onNotification(this);
    }

    @Override
    public com.google.gwt.event.shared.GwtEvent.Type<ParameterLoadedEventHandler> getAssociatedType() {
            return TYPE;
    }
    
    public Map<String, String> getParameters() {
		return parameters;
	}

}