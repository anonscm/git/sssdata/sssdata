package fr.sedoo.sssdata.client.ui.component;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.cell.client.Cell;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.view.client.ListDataProvider;

import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.widget.table.ImagedActionCell;
import fr.sedoo.commons.client.widget.table.TableBundle;
import fr.sedoo.sssdata.client.message.ApplicationMessages;
import fr.sedoo.sssdata.client.ui.view.api.DownloadLogView.Presenter;
import fr.sedoo.sssdata.shared.DownloadDTO;
import fr.sedoo.sssdata.shared.GriddedProduct;

public class DownloadTable extends CellTable<DownloadDTO> {

	private int DISPLAY_LIMIT = 100;
	
	private TextColumn<DownloadDTO> date;
	private TextColumn<DownloadDTO> user;
	private TextColumn<DownloadDTO> email;
	private TextColumn<DownloadDTO> obj;
	private TextColumn<DownloadDTO> country;
	private TextColumn<DownloadDTO> format;
	private TextColumn<DownloadDTO> files;
	private TextColumn<DownloadDTO> volume;
	private TextColumn<DownloadDTO> labo;
	
	private ListDataProvider<DownloadDTO> dataProvider;
	
	private Image viewImage = new Image(TableBundle.INSTANCE.view());
	private Image deleteImage = new Image(TableBundle.INSTANCE.delete());
	
	Presenter presenter;
	
	private String getDisplay(String value){
		if (value.length() > DISPLAY_LIMIT){
			return value.substring(0,DISPLAY_LIMIT) + "...";
		}else{
			return value;
		}
	}
	
	public int getSize(){
		if (dataProvider.getList() != null){
			return dataProvider.getList().size();
		}else{
			return 0;
		}
	}
	
	public DownloadTable() {
		super();
		
		date = new	TextColumn<DownloadDTO>() {
			@Override
			public String getValue(DownloadDTO d) {
				return "" + d.getDate();
			}
		};
				
		user = new TextColumn<DownloadDTO>() {
			@Override
			public String getValue(DownloadDTO d) {
				return d.getUser();
			}
		};
		
		email = new TextColumn<DownloadDTO>() {
			@Override
			public String getValue(DownloadDTO d) {
				return d.getUserEmail();
			}
		};
		
		obj = new TextColumn<DownloadDTO>() {
			@Override
			public String getValue(DownloadDTO d) {
				return getDisplay(d.getObjectives());
			}
		};
		
		country = new TextColumn<DownloadDTO>() {
			@Override
			public String getValue(DownloadDTO d) {
				return getDisplay(d.getCountry());
			}
		};
		
		format = new TextColumn<DownloadDTO>() {
			@Override
			public String getValue(DownloadDTO d) {
				return d.getFormat();
			}
		};
		labo = new TextColumn<DownloadDTO>() {
			@Override
			public String getValue(DownloadDTO d) {
				return d.getLabo();
			}
		};
		files = new TextColumn<DownloadDTO>() {
			@Override
			public String getValue(DownloadDTO d) {
				if (d.getSize() == GriddedProduct.GRID_SIZE){
					return "Grid";
				}else if ( (d.getFiles().size() == 1) && d.getSize() > 1){
					return "Archive (" + d.getSize() + ")";	
				}else{
					return "" + d.getSize();
				}
			}
		};		
		volume = new TextColumn<DownloadDTO>() {
			@Override
			public String getValue(DownloadDTO d) {
				return d.getVolume();
			}
		};
		
		Cell<DownloadDTO> viewActionCell = new ImagedActionCell<DownloadDTO>(viewAction, viewImage, CommonMessages.INSTANCE.view());
		Cell<DownloadDTO> deleteActionCell = new ImagedActionCell<DownloadDTO>(deleteAction, deleteImage, CommonMessages.INSTANCE.delete());
		
		Column<DownloadDTO, DownloadDTO> viewColumn = new Column<DownloadDTO, DownloadDTO>(viewActionCell) {
			@Override
			public DownloadDTO getValue(DownloadDTO object) {
				return object;
			}
		};
		Column<DownloadDTO, DownloadDTO> deleteColumn = new Column<DownloadDTO, DownloadDTO>(deleteActionCell) {
			@Override
			public DownloadDTO getValue(DownloadDTO object) {
				return object;
			}
		};
		
		addColumn(date, "Date");
		addColumn(user, ApplicationMessages.INSTANCE.userInfoViewUserNameLabel());
		addColumn(email, ApplicationMessages.INSTANCE.userInfoViewUserEmailLabel());
		addColumn(labo, ApplicationMessages.INSTANCE.userInfoViewUserOrganismLabel());
		addColumn(obj, ApplicationMessages.INSTANCE.userInfoViewUserObjectiveLabel());
		addColumn(country, ApplicationMessages.INSTANCE.userInfoViewUserCountryLabel());
		addColumn(format, ApplicationMessages.INSTANCE.userInfoViewChooseDataFormatLabel());
		addColumn(files, "Files");
		addColumn(volume, ApplicationMessages.INSTANCE.downloadLogViewVolumeLabel());
		addColumn(viewColumn);
		addColumn(deleteColumn);
		setColumnWidth(viewColumn, 30.0, Unit.PX);
		
		dataProvider = new ListDataProvider<DownloadDTO>();
		dataProvider.addDataDisplay(this);
	}
	
	ImagedActionCell.Delegate<DownloadDTO> viewAction = new ImagedActionCell.Delegate<DownloadDTO>() {
		@Override
		public void execute(DownloadDTO dl) {
			presenter.searchDownload(dl.getId());
		}

	};
	
	ImagedActionCell.Delegate<DownloadDTO> deleteAction = new ImagedActionCell.Delegate<DownloadDTO>() {
		@Override
		public void execute(DownloadDTO dl) {
			presenter.deleteDownload(dl.getId());
		}

	};
	
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}
	
	private void addSortHandler(){
		date.setSortable(true);
		user.setSortable(true);
		files.setSortable(true);
		country.setSortable(true);
		
		ListHandler<DownloadDTO> columnSortHandler = new ListHandler<DownloadDTO>(dataProvider.getList());
		columnSortHandler.setComparator(user, new Comparator<DownloadDTO>() {
			@Override
			public int compare(DownloadDTO o1, DownloadDTO o2) {
				return o1.getUser().compareTo(o2.getUser());
			}
		});
		columnSortHandler.setComparator(date, new Comparator<DownloadDTO>() {
			@Override
			public int compare(DownloadDTO o1, DownloadDTO o2) {
				return o1.getDate().compareTo(o2.getDate());
			}
		});
		columnSortHandler.setComparator(files, new Comparator<DownloadDTO>() {
			@Override
			public int compare(DownloadDTO o1, DownloadDTO o2) {
				return o1.getSize() - o2.getSize();
			}
		});
		columnSortHandler.setComparator(country, new Comparator<DownloadDTO>() {
			@Override
			public int compare(DownloadDTO o1, DownloadDTO o2) {
				return o1.getCountry().compareTo(o2.getCountry());
			}
		});
				
		addColumnSortHandler(columnSortHandler);
	}
	
	public void setData(List<DownloadDTO> downloads) {
		dataProvider.setList(downloads);
		addSortHandler();		
	}
	
}
