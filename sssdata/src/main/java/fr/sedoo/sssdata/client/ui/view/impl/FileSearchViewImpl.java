package fr.sedoo.sssdata.client.ui.view.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.callback.LoadCallBack;
import fr.sedoo.commons.client.component.AbstractView;
import fr.sedoo.commons.client.config.ClientPropertiesList;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.panel.HorizontalCollapsingPanel;
import fr.sedoo.sssdata.client.GlobalBundle;
import fr.sedoo.sssdata.client.ui.component.UpdatesTable;
import fr.sedoo.sssdata.client.ui.component.UpdatesTablePresenter;
import fr.sedoo.sssdata.client.ui.dialog.UpdateDialogBox;
import fr.sedoo.sssdata.client.ui.view.api.FileSearchView;
import fr.sedoo.sssdata.client.ui.widget.BoatListBox;
import fr.sedoo.sssdata.client.ui.widget.date.SssDataDateBox;
import fr.sedoo.sssdata.shared.BoatDTO;
import fr.sedoo.sssdata.shared.ConfigPropertiesCode;
import fr.sedoo.sssdata.shared.FileSearchInformations;
import fr.sedoo.sssdata.shared.UpdateDTO;

public class FileSearchViewImpl extends AbstractView implements FileSearchView{

	interface FileSearchViewImplUiBinder extends UiBinder<Widget, FileSearchViewImpl> {}
	private static FileSearchViewImplUiBinder uiBinder = GWT.create(FileSearchViewImplUiBinder.class);
	
	interface FileSearchDriver extends SimpleBeanEditorDriver<FileSearchInformations, FileSearchViewImpl> {}
	private FileSearchDriver driver = GWT.create(FileSearchDriver.class);
	
	private Presenter presenter;
	
	@UiField
	HorizontalCollapsingPanel boatListPanel;
	@UiField
	BoatListBox boatListSelection;
	@UiField
	Button selectAllBoats;
		
	@UiField
	HorizontalCollapsingPanel datePanel;
	@UiField(provided=true)
	SssDataDateBox beginDateEditor;
	@UiField(provided=true)
	SssDataDateBox endDateEditor;
	
	@Ignore
	@UiField(provided=false)
	VerticalPanel messagePanel;
	
	@UiField
	UpdatesTable table;
	
	UpdateDialogBox box;
	
	public FileSearchViewImpl() {
		super();
		GWT.<GlobalBundle>create(GlobalBundle.class).css().ensureInjected();
		
		beginDateEditor = new SssDataDateBox("yyyy-MM-dd");
		endDateEditor = new SssDataDateBox("yyyy-MM-dd");

		ClientPropertiesList.getProperty(ConfigPropertiesCode.DATE_MIN, new LoadCallBack<String>() {
			@Override
			public void postLoadProcess(String result){
				DateTimeFormat format = DateTimeFormat.getFormat("dd/MM/yyyy");
				beginDateEditor.setMinDate(format.parse(StringUtil.trimToEmpty(result)));
				endDateEditor.setMinDate(format.parse(StringUtil.trimToEmpty(result)));
			}
		});
		
		ClientPropertiesList.getProperty(ConfigPropertiesCode.DATE_MAX,
				new LoadCallBack<String>() {
					@Override
					public void postLoadProcess(String result) {
						if (result == null || result.isEmpty()){
							beginDateEditor.setMaxDate(new Date());
							endDateEditor.setMaxDate(new Date());
						}else{
							DateTimeFormat format = DateTimeFormat
									.getFormat("dd/MM/yyyy");
							beginDateEditor.setMaxDate(format.parse(StringUtil
									.trimToEmpty(result)));
							endDateEditor.setMaxDate(format.parse(StringUtil
									.trimToEmpty(result)));
						}
					}
				});

		initWidget(uiBinder.createAndBindUi(this));
		driver.initialize(this);
		applyCommonStyle();		

		box = new UpdateDialogBox();
		boatListSelection.setVisibleItemCount(10);
	}
	
	@Override
	public void displayUpdate(UpdateDTO u) {
		box.setData(u);
		box.center();
	}
	
	@UiHandler("resetButton")
	void onResetButtonClicked(ClickEvent event){
		reset();
	}
	
	private boolean validInfos(FileSearchInformations infos) {
		boolean result = true;
		resetMessages();
		if (infos.getBoatListSelection().isEmpty()){
			addErrorMessage("Ship is required.");
			result = false;
		}
		return result;
	}
	
	@UiHandler("searchButton")
	void onSendButtonClicked(ClickEvent event){
		search();
	}
	
	@Override
	public void search() {
		if (validInfos(driver.flush())){
			presenter.search(driver.flush());
		}
	}
	
	@UiHandler("selectAllBoats")
	void onselectAllBoatsClicked(ClickEvent event){
		this.boatListSelection.setAllItemSelected();
	}
	
	@UiHandler("unselectAllBoats")
	void onunselectAllBoatsClicked(ClickEvent event){
		this.boatListSelection.reset();
	}
	
	@Override
	public void reset() {
		presenter.getDefaultCriterion();
		boatListSelection.reset();
		resetMessages();
		resetResult();
	}
	
	@Override
	public void resetResult() {
		table.reset();
	}

	@Override
	public void addMessage(String msg) {
		Label l = new Label(msg);
		l.getElement().getStyle().setColor("green");
		messagePanel.add(l);
	}

	@Override
	public void addErrorMessage(String msg) {
		Label l = new Label(msg);
		l.getElement().getStyle().setColor("red");
		messagePanel.add(l);
	}

	@Override
	public void resetMessages() {
		messagePanel.clear();
	}
	
	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public void setBoatList(ArrayList<BoatDTO> boats) {
		boatListSelection.clear();
		for (BoatDTO b : boats){
			boatListSelection.addItem(b.getName());
		}	
	}

	@Override
	public void setSearchCriterion(FileSearchInformations infos) {
		driver.edit(infos);
	}

	@Override
	public void setResult(List<UpdateDTO> result){
		table.setRowData(result);
	}

	@Override
	public void setUpdatesTablePresenter(UpdatesTablePresenter presenter) {
		table.setPresenter(presenter);
	}
	
}
