package fr.sedoo.sssdata.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.sedoo.sssdata.shared.metadata.MetadataDTO;

public interface MetadataServiceAsync {
	
	void getMetadata(String doi, AsyncCallback<MetadataDTO> callback);

	void updateMetadata(String doi, AsyncCallback<MetadataDTO> callback);

}
