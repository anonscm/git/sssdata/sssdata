package fr.sedoo.sssdata.client.ui.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.dom.client.Style.Display;
import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;


public class ViewsPopUp extends Composite{

	  interface Binder extends UiBinder<HTMLPanel, ViewsPopUp> {
	  }

	  private static final Binder BINDER = GWT.create(Binder.class);
	  
	  private int positionYCorrection;

	  @UiField
	  HTMLPanel container;
	  
	  @UiField()
	  DivElement borderElement;

	  @UiField
	  DivElement heightMeasure;

	  @UiField()
	  SpanElement notificationText;

	  public ViewsPopUp() {
	    initWidget(BINDER.createAndBindUi(this));
	    positionYCorrection = 50;
	  }

	  /**
	   * Hides the notification.
	   */
	  public void hide() {
		  notificationText.setInnerText("");
		  this.getElement().getStyle().setDisplay(Display.NONE);
		  borderElement.getStyle().setDisplay(Display.NONE);
		  borderElement.getStyle().setProperty("zIndex", "0");
	  }


	  /**
	   * Sets the message text to be displayed.
	   * 
	   * @param message the text to be displayed.
	   */
	  public void setMessage(String message) {
	    notificationText.setInnerText(message);
	  }
	  
	  /**
	   * Set the message text and then display the notification.
	   * at the decide absolute position
	   *  @param message the text to be displayed.
	   *  @param top = top css position double value in percentage 
	   *  @param right = right css position double value in percentage
	   */
	  public void show(String message,int top, int right) {
	    setMessage(message);
	    show(top, right);
	  }
	  
	  /**
	   * Display the notification with the existing message.
	   */
	  public void show(int top, int left) {
		  this.getElement().getStyle().setDisplay(Display.BLOCK);
		  borderElement.getStyle().setDisplay(Display.BLOCK);
		  borderElement.getStyle().setWidth(notificationText.getOffsetWidth(), Unit.PX); 
		  borderElement.getStyle().setPosition(Position.ABSOLUTE);
		  borderElement.getStyle().setTop((top - positionYCorrection), Unit.PX);
		  borderElement.getStyle().setLeft(left, Unit.PX);
		  borderElement.getStyle().setProperty("zIndex", "9998");
	  }

	  

}