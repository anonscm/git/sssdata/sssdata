package fr.sedoo.sssdata.client.ui.view.api;

import com.google.gwt.user.client.ui.IsWidget;

public interface GriddedProductView extends IsWidget {

	void setContent(String content);
	void setPageTitle(String title);
	void setPresenter(Presenter p);
	
	public interface Presenter{
		void toMetadata();
		void toDownloadPage();
	}
}
