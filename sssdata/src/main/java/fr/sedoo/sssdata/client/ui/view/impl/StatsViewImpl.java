package fr.sedoo.sssdata.client.ui.view.impl;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.visualization.client.AbstractDataTable.ColumnType;
import com.google.gwt.visualization.client.DataTable;
import com.google.gwt.visualization.client.visualizations.LineChart;

import fr.sedoo.commons.client.component.AbstractView;
import fr.sedoo.commons.client.date.DateUtil;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.panel.HorizontalCollapsingPanel;
import fr.sedoo.sssdata.client.GlobalBundle;
import fr.sedoo.sssdata.client.ui.view.api.StatsView;
import fr.sedoo.sssdata.shared.MonthyReport;
import fr.sedoo.sssdata.shared.StatsReport;

public class StatsViewImpl extends AbstractView implements StatsView {

	private static StatsViewImplUiBinder uiBinder = GWT
			.create(StatsViewImplUiBinder.class);

	interface StatsViewImplUiBinder extends UiBinder<Widget, StatsViewImpl> {
	}

	interface StatsDriver extends
			SimpleBeanEditorDriver<StatsReport, StatsViewImpl> {
	}

	private StatsDriver driver = GWT.create(StatsDriver.class);

	@UiField
	Label downloadNumberEditor;

	@UiField
	Label userNumberEditor;

	@UiField
	HorizontalPanel graphPanel;
	
	@Editor.Ignore
	@UiField
	HorizontalCollapsingPanel globalResultPanel;
	
	@Editor.Ignore
	@UiField
	HorizontalCollapsingPanel detailPanel;
	
	@Editor.Ignore
	@UiField
	InlineLabel previousYear;
	
	@Editor.Ignore
	@UiField
	InlineLabel nextYear;
	
	@UiField
	Image rightArrow;
	
	
	Integer year;
		private Presenter presenter;
	
	private Widget centerWidget; 

	public StatsViewImpl() {
		super();
		
		GWT.<GlobalBundle> create(GlobalBundle.class).css().ensureInjected();
		initWidget(uiBinder.createAndBindUi(this));

		driver.initialize(this);
		globalResultPanel.setMilliseconds(2000);
		detailPanel.setMilliseconds(2000);
		reset();
		applyCommonStyle();

	}

	@Override
	public void reset() {
		year=null;
//		StatsReport sc = new StatsReport();
//		if (presenter != null) {
//			sc = presenter.getEmptyReport();
//		}
//		display(sc);

	}

	private void edit(StatsReport sc) {

		driver.edit(sc);
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;

	}

	@Override
	public void display(StatsReport report) {
		edit(report);
		if (StringUtil.isEmpty(report.getYear()))
		{
			globalResultPanel.setTitle("Global results");
			nextYear.setText("");
			previousYear.setText("");
		}
		else
		{
			year = new Integer(report.getYear());
			globalResultPanel.setTitle(""+year+ " global results");
			previousYear.setText(""+(year-1));
			if (DateUtil.getCurrentYearAsString().compareTo(""+year)==0)
			{
				//L'année affichée est l'année en cours
				nextYear.setText("");
				rightArrow.setVisible(false);
			}
			else
			{
				nextYear.setText(""+(year+1));
				rightArrow.setVisible(true);
			}
		}
		
		if (report.getMonthlyDetails().isEmpty() == false)
		{
			DataTable linechartDataTable = DataTable.create();
			linechartDataTable.addColumn(ColumnType.STRING, "Month");
			linechartDataTable.addColumn(ColumnType.NUMBER, "Number of Downloads");
			linechartDataTable.addColumn(ColumnType.NUMBER,"Number of Users"); 
			int rowIndex = 0;
			for (MonthyReport g : report.getMonthlyDetails()) {
				// build datatable for each value
				linechartDataTable.addRow();
				linechartDataTable.setValue(rowIndex, 0, g.getMonth());
				linechartDataTable.setValue(rowIndex, 1, g.getDownloadsNumber());
				linechartDataTable.setValue(rowIndex, 2, g.getUsers().size()); 
				rowIndex++;

			}		
			LineChart.Options options = createLineOptions(report);
			graphPanel.clear();
			graphPanel.setWidth("100%");
			graphPanel.setHeight("100%");
			graphPanel.add(new LineChart(linechartDataTable, options));
		}
	}


	private LineChart.Options createLineOptions(StatsReport report) {
		LineChart.Options options = LineChart.Options.create();
		options.setHeight(400);
		options.setWidth(600);
		options.setMin(0);
		options.setMax(report.getMaxDownloads());
		return options;
	}
	
	@UiHandler(value={"leftArrow", "previousYear"})
	void onPreviousYearClicked(ClickEvent event)
	{
		presenter.displayYear(previousYear.getText());
	}
	
	@UiHandler(value={"rightArrow", "nextYear"})
	void onNextYearClicked(ClickEvent event)
	{
		presenter.displayYear(nextYear.getText());
	}

}
