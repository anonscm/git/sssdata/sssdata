package fr.sedoo.sssdata.client.ui.view.impl;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.resources.client.CssResource;

import fr.sedoo.commons.client.component.CssResourceProvider;
import fr.sedoo.commons.client.image.CommonBundle;
import fr.sedoo.commons.client.style.SedooStyleBundle;
import fr.sedoo.sssdata.client.GlobalBundle;

public class SSSCssResourceProvider implements CssResourceProvider{

	@Override
	public List<CssResource> getCssResources() {
		
		ArrayList<CssResource> result = new ArrayList<CssResource>();
		result.add(GlobalBundle.INSTANCE.css());
		result.add(GlobalBundle.INSTANCE.bootstrapCss());
		result.add(SedooStyleBundle.INSTANCE.sedooStyle());
		result.add(CommonBundle.INSTANCE.css());
		return result;
	}

}
