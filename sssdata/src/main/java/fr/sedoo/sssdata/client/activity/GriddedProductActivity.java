package fr.sedoo.sssdata.client.activity;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.sedoo.commons.client.callback.LoadCallBack;
import fr.sedoo.commons.client.cms.ClientScreenList;
import fr.sedoo.commons.client.event.BreadCrumbChangeEvent;
import fr.sedoo.commons.client.event.PlaceNavigationEvent;
import fr.sedoo.commons.client.util.LocaleUtil;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;
import fr.sedoo.commons.shared.domain.cms.TitledScreen;
import fr.sedoo.sssdata.client.ClientFactory;
import fr.sedoo.sssdata.client.place.GriddedProductMetadataPlace;
import fr.sedoo.sssdata.client.place.GriddedProductPlace;
import fr.sedoo.sssdata.client.place.UserInfoPlace;
import fr.sedoo.sssdata.client.ui.component.ShortcutFactory;
import fr.sedoo.sssdata.client.ui.view.api.GriddedProductView;
import fr.sedoo.sssdata.client.ui.view.api.GriddedProductView.Presenter;


public class GriddedProductActivity extends AbstractActivity implements LoadCallBack<TitledScreen>, Presenter  {

	private ClientFactory clientFactory;
	private GriddedProductView view;
	private EventBus eventBus;
	
	private GriddedProductPlace place;
	
	public GriddedProductActivity(GriddedProductPlace place, ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
		this.place = place;
	}

	/**
	 * Invoked by the ActivityManager to start a new Activity
	 */
	@Override
	public void start(AcceptsOneWidget containerWidget, final EventBus eventBus) {
		
		view = clientFactory.getGriddedProductView();
		view.setPresenter(this);
		this.eventBus = eventBus;
		containerWidget.setWidget(view.asWidget());
		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		shortcuts.add(ShortcutFactory.getWelcomeShortcut());
		shortcuts.add(ShortcutFactory.getGridShortcut(place.getGrid()));
		eventBus.fireEvent(new BreadCrumbChangeEvent(shortcuts));
		ClientScreenList.getConsultContentByName(place.getGrid(), LocaleUtil.getClientLocaleLanguage(clientFactory.getDefaultLanguage()), clientFactory.getLanguages(), this);

		
		view.setPageTitle(place.getTitle());
	}

		
	@Override
	public void postLoadProcess(TitledScreen resultScreen) {
		String result = resultScreen.getContent();
		view.setContent(StringUtil.trimToEmpty(result));
	}

	@Override
	public void toMetadata() {
		clientFactory.getPlaceController().goTo(new GriddedProductMetadataPlace(place.getGrid()));		
	}

	@Override
	public void toDownloadPage() {
		UserInfoPlace userInfoPlace = new UserInfoPlace();
		userInfoPlace.setPretreatedArchiveType(place.getGrid());
		this.eventBus.fireEvent(new PlaceNavigationEvent(userInfoPlace));
	}
	
}