package fr.sedoo.sssdata.client.activity;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.sedoo.commons.client.event.BreadCrumbChangeEvent;
import fr.sedoo.commons.client.faq.activity.FaqConsultActivity;
import fr.sedoo.commons.client.faq.place.FaqConsultPlace;
import fr.sedoo.commons.client.mvp.CommonClientFactory;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;
import fr.sedoo.sssdata.client.ui.component.ShortcutFactory;

public class SSSFaqConsultActivity extends FaqConsultActivity {

	public SSSFaqConsultActivity(FaqConsultPlace place,	CommonClientFactory clientFactory) {
		super(place, clientFactory);
	}
	
	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		super.start(containerWidget, eventBus);
		
		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		shortcuts.add(ShortcutFactory.getWelcomeShortcut());
		shortcuts.add(ShortcutFactory.getFaqShortcut());
		eventBus.fireEvent(new BreadCrumbChangeEvent(shortcuts));
				
	}

}
