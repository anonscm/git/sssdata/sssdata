package fr.sedoo.sssdata.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface NoFeatureIsSelectedHandler extends EventHandler {
    void onNoFeatureIsSelected(NoFeatureIsSelectedEvent event);
}

