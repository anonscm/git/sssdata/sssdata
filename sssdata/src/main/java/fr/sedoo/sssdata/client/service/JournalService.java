package fr.sedoo.sssdata.client.service;

import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fr.sedoo.commons.client.util.ServiceException;
import fr.sedoo.sssdata.shared.DBStatusDTO;
import fr.sedoo.sssdata.shared.DownloadDTO;
import fr.sedoo.sssdata.shared.FileSearchInformations;
import fr.sedoo.sssdata.shared.UpdateDTO;

@RemoteServiceRelativePath("journal")
public interface JournalService extends RemoteService {

	List<DownloadDTO> getAllDownloads() throws ServiceException;
	DownloadDTO getDownload(int id) throws ServiceException;
	void delete(int id) throws ServiceException;
	
	List<UpdateDTO> getLastUpdates(int nb) throws ServiceException;
	List<UpdateDTO> getAllUpdates() throws ServiceException;
	
	Integer getUpdateHits() throws ServiceException;
	List<UpdateDTO> getUpdatesByPage(int first, int pageSize) throws ServiceException;
	
	List<UpdateDTO> searchFiles(FileSearchInformations infos) throws ServiceException;
	void deleteFile(UpdateDTO u) throws ServiceException;
	
	DBStatusDTO getDBStatus() throws ServiceException;
}
