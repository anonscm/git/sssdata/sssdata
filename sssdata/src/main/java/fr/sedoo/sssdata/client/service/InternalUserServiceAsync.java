package fr.sedoo.sssdata.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.sedoo.commons.client.user.AuthenticatedUser;

public interface InternalUserServiceAsync {

	void internalLogin(String login, String password,
			AsyncCallback<AuthenticatedUser> callback);

}
