package fr.sedoo.sssdata.client.ui.view.impl;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.component.AbstractView;
import fr.sedoo.commons.client.news.widget.CarrouselBlock;
import fr.sedoo.commons.client.news.widget.NewsListPresenter;
import fr.sedoo.commons.shared.domain.New;
import fr.sedoo.sssdata.client.ui.view.api.WelcomeView;
import fr.sedoo.sssdata.shared.DBStatusDTO;

public class WelcomeViewImpl extends AbstractView implements WelcomeView {

	@UiField
	CarrouselBlock newsBlock;
	
	private static WelcomeViewImplUiBinder uiBinder = GWT
			.create(WelcomeViewImplUiBinder.class);

	interface WelcomeViewImplUiBinder extends UiBinder<Widget, WelcomeViewImpl> {
	}
	
	@UiField
	DivElement welcomeMessageContainer;

	private boolean loaded=false;

	@UiField
	Label dbUpdate;
	@UiField
	Label fileUpdate;
	@UiField
	Label filesNb;
	@UiField
	Label shipsNb;
	@UiField
	Label dbPeriod;
	
	
	//@UiField
	//LastUpdatesBlock updatesBlock;
	
	public WelcomeViewImpl() {
		super();
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
	}

	@Override
	public void setWelcomeMessage(String welcomeMessage) {
		welcomeMessageContainer.setInnerHTML(welcomeMessage);
	}

	@Override
	public void setNews(ArrayList<? extends New> carrouselNews, ArrayList<? extends New> news) {
		newsBlock.setNews(carrouselNews, news);
		loaded = true;
	}

	@Override
	public void reset(){
		newsBlock.reset();
		loaded = false;
	}
	
	/*@Override
	public void setUpdates(List<UpdateDTO> updates) {
		updatesBlock.setData(updates);
	}*/
	
	@Override
	public boolean areNewsLoaded() {
		return loaded;
	}

	@Override
	public void setNewsPresenter(NewsListPresenter presenter) {
		newsBlock.setPresenter(presenter);		
	}
	
	/*@Override
	public void setUpdatesPresenter(UpdatesTablePresenter presenter) {
		updatesBlock.setPresenter(presenter);		
	}*/

	@Override
	public void setDbStatus(DBStatusDTO status) {
		if (status.getLastDbUpdateDate() != null){
			dbUpdate.setText(status.getLastDbUpdateDate());
		}else{
			dbUpdate.setText("NA");
		}
		if (status.getLastModifiedFileDate() != null){
			fileUpdate.setText(status.getLastModifiedFileDate());
		}else{
			fileUpdate.setText("NA");
		}
		if (status.getStartDate() != null && status.getEndDate() != null){
			dbPeriod.setText(status.getStartDate() + " - " + status.getEndDate());
		}else{
			dbPeriod.setText("NA");
		}
		shipsNb.setText("" + status.getShipNb());
		filesNb.setText("" + status.getInsertedFilesNb());
	}
	
}
