package fr.sedoo.sssdata.client.activity;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.sedoo.commons.client.event.BreadCrumbChangeEvent;
import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;
import fr.sedoo.commons.client.news.activity.MessageEditActivity;
import fr.sedoo.commons.client.news.place.MessageEditPlace;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;
import fr.sedoo.sssdata.client.ui.component.ShortcutFactory;

public class SSSMessageEditActivity extends MessageEditActivity{

	public SSSMessageEditActivity(MessageEditPlace place,
			AuthenticatedClientFactory clientFactory) {
		super(place, clientFactory);
	}

		
	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		super.start(containerWidget, eventBus);
		
		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		shortcuts.add(ShortcutFactory.getWelcomeShortcut());
		shortcuts.add(ShortcutFactory.getAdminShortcut());
		shortcuts.add(ShortcutFactory.getMessageManagementShortcut());
		shortcuts.add(ShortcutFactory.getMessageEditionShortcut());
		eventBus.fireEvent(new BreadCrumbChangeEvent(shortcuts));
		
	}
	
	@Override
	protected boolean isValidUser() {
		return isLoggedUser();
	}

	

	
}
