package fr.sedoo.sssdata.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class DownloadStatsPlace extends Place{
	public static DownloadStatsPlace instance;
	
	public DownloadStatsPlace(){}

	public static class Tokenizer implements PlaceTokenizer<DownloadStatsPlace>	{
		
		public DownloadStatsPlace getPlace(String token) {
			if (instance == null){
				instance = new DownloadStatsPlace();
			}
			return instance;
		}

		public String getToken(DownloadStatsPlace place) {
			return "";
		}

		
	}
}