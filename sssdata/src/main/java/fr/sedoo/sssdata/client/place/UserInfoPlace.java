package fr.sedoo.sssdata.client.place;

import java.util.List;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

import fr.sedoo.sssdata.shared.SearchCriterion;

public class UserInfoPlace extends Place
{
	public static UserInfoPlace instance;
	
	private SearchCriterion searchCriterion;
	private List<String> allSelectedTrajectories;
	private String pretreatedArchiveType;
	
	public SearchCriterion getSearchCriterion() {
		return searchCriterion;
	}

	public void setSearchCriterion(SearchCriterion searchCriterion) {
		this.searchCriterion = searchCriterion;
	}

	public List<String> getAllSelectedTrajectories() {
		return allSelectedTrajectories;
	}

	public void setAllSelectedTrajectories(
			List<String> allSelectedTrajectories) {
		this.allSelectedTrajectories = allSelectedTrajectories;
	}

	public String getPretreatedArchiveType() {
		return pretreatedArchiveType;
	}

	public void setPretreatedArchiveType(String pretreatedArchiveType) {
		this.pretreatedArchiveType = pretreatedArchiveType;
	}

	public UserInfoPlace()
	{
	}

	public static class Tokenizer implements PlaceTokenizer<UserInfoPlace>
	{
		
		public UserInfoPlace getPlace(String token) {
			if (instance == null)
			{
				instance = new UserInfoPlace();
			}
			return instance;
		}

		public String getToken(UserInfoPlace place) {
			return "";
		}

		
	}
}