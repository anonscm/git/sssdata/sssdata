package fr.sedoo.sssdata.client.ui.component;

import fr.sedoo.sssdata.shared.UpdateDTO;

public interface UpdatesTablePresenter {
		
	void delete(UpdateDTO u);
	void view(UpdateDTO u);
}
