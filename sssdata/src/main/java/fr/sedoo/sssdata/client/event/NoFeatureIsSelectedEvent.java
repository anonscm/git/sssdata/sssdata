package fr.sedoo.sssdata.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class NoFeatureIsSelectedEvent extends GwtEvent<NoFeatureIsSelectedHandler>{
	
	public static final Type<NoFeatureIsSelectedHandler> TYPE = new Type<NoFeatureIsSelectedHandler>();

	
	public NoFeatureIsSelectedEvent()
	{
		
	}
	
	 @Override
	    protected void dispatch(NoFeatureIsSelectedHandler handler) {
	        handler.onNoFeatureIsSelected(this);
	    }

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<NoFeatureIsSelectedHandler> getAssociatedType() {
		return TYPE;
	}

}
