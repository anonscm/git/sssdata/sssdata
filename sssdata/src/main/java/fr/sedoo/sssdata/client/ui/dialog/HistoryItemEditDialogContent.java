package fr.sedoo.sssdata.client.ui.dialog;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.crud.widget.CrudConfirmCallBack;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.dialog.EditDialogContent;
import fr.sedoo.sssdata.shared.metadata.HistoryItem;

public class HistoryItemEditDialogContent extends EditDialogContent implements DialogBoxContent {

	private static HistoryItemEditDialogContentUiBinder uiBinder = GWT
			.create(HistoryItemEditDialogContentUiBinder.class);

	interface HistoryItemEditDialogContentUiBinder extends
	UiBinder<Widget, HistoryItemEditDialogContent> {
	}

	Integer id;
	
	@UiField
	TextArea comment;

	@UiField
	TextBox date;

	@UiField
	TextBox version;
	
	public HistoryItemEditDialogContent(CrudConfirmCallBack confirmCallback, HistoryItem item) {
		super(confirmCallback);
		initWidget(uiBinder.createAndBindUi(this));
		edit(item);
		date.setEnabled(true);
		version.setEnabled(true);
	}

	
	private void edit(HistoryItem item){
		id = item.getId();
		date.setText(StringUtil.trimToEmpty(item.getDate()));
		version.setText(StringUtil.trimToEmpty(item.getVersion()));
		comment.setText(StringUtil.trimToEmpty(item.getComment()));
	}
	

	@Override
	public HistoryItem flush(){
		HistoryItem aux = new HistoryItem();
		aux.setId(id);
		aux.setDate(date.getText());
		aux.setComment(comment.getText());
		aux.setVersion(version.getText());
		return aux;
	}


	@Override
	public String getPreferredHeight() {
		return "200px";
	}


	
	

}
