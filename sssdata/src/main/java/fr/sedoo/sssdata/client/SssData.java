package fr.sedoo.sssdata.client;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gwt.activity.shared.ActivityManager;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.place.shared.PlaceHistoryHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Event.NativePreviewEvent;
import com.google.gwt.user.client.Event.NativePreviewHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.google.gwt.user.client.ui.RootPanel;

import fr.sedoo.commons.client.DefaultClientApplication;
import fr.sedoo.commons.client.analytics.GAEvent;
import fr.sedoo.commons.client.callback.LoadCallBack;
import fr.sedoo.commons.client.config.ClientPropertiesList;
import fr.sedoo.commons.client.event.BreadCrumbChangeEvent;
import fr.sedoo.commons.client.event.PlaceNavigationEvent;
import fr.sedoo.commons.client.event.UserLoginEvent;
import fr.sedoo.commons.client.event.UserLoginEventHandler;
import fr.sedoo.commons.client.event.UserLogoutEvent;
import fr.sedoo.commons.client.event.UserLogoutEventHandler;
import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;
import fr.sedoo.commons.client.user.InternalUser;
import fr.sedoo.commons.client.widget.DialogBoxTools;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;
import fr.sedoo.sssdata.client.event.HidePopUpEvent;
import fr.sedoo.sssdata.client.event.HidePopUpHandler;
import fr.sedoo.sssdata.client.event.NotificationEvent;
import fr.sedoo.sssdata.client.event.NotificationHandler;
import fr.sedoo.sssdata.client.event.ParameterLoadedEvent;
import fr.sedoo.sssdata.client.event.ShowPopUpEvent;
import fr.sedoo.sssdata.client.event.ShowPopUpHandler;
import fr.sedoo.sssdata.client.misc.ClientBoatList;
import fr.sedoo.sssdata.client.misc.ClientSystemParametersList;
import fr.sedoo.sssdata.client.mvp.AppActivityMapper;
import fr.sedoo.sssdata.client.mvp.AppPlaceHistoryMapper;
import fr.sedoo.sssdata.client.place.WelcomePlace;
import fr.sedoo.sssdata.client.ui.component.ShortcutFactory;
import fr.sedoo.sssdata.client.ui.component.impl.menu.HorizontalMenuImpl;
import fr.sedoo.sssdata.client.ui.widget.LocalNotificationMole;
import fr.sedoo.sssdata.client.ui.widget.ViewsPopUp;
import fr.sedoo.sssdata.client.ui.widget.skeleton.TopSideMenuSkeleton;
import fr.sedoo.sssdata.shared.ParameterConstants;


/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class SssData extends DefaultClientApplication implements EntryPoint, NotificationHandler, ShowPopUpHandler , HidePopUpHandler, LoadCallBack<Map<String, String>>, UserLogoutEventHandler, UserLoginEventHandler{

	public static boolean TEST_MODE = false;
	
	private Place defaultPlace = new WelcomePlace();
	
	private LocalNotificationMole notificationMole;
	private ViewsPopUp viewsPopUp;
	
	private int mouseX;
	private int mouseY;

	//private LeftSideMenuSkeleton skeleton;
	private TopSideMenuSkeleton skeleton;

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {

		super.onModuleLoad();
		GWT.<GlobalBundle>create(GlobalBundle.class).css().ensureInjected();
		
		/*DefaultAuthenticatedUser fakeAdmin = new DefaultAuthenticatedUser();
		fakeAdmin.setAdmin(true);
		fakeAdmin.setName("Fake admin");
		clientFactory.getUserManager().setUser(fakeAdmin);*/
		
		getClientFactory().getEventBus().addHandler(UserLogoutEvent.TYPE, this);
		getClientFactory().getEventBus().addHandler(UserLoginEvent.TYPE, this);
		
		guiInit((ClientFactory) getClientFactory());
		final EventBus eventBus = getClientFactory().getEventBus();
		
		
		eventBus.addHandler(NotificationEvent.TYPE, this);
		eventBus.addHandler(ShowPopUpEvent.TYPE, this);
		eventBus.addHandler(HidePopUpEvent.TYPE, this);

		
		Event.addNativePreviewHandler(new NativePreviewHandler() {
			
			@Override
			public void onPreviewNativeEvent(NativePreviewEvent event) {
				 if (event.getNativeEvent().getType().equals("mousemove")) {
                     mouseX = event.getNativeEvent().getClientX();
                     mouseY = event.getNativeEvent().getClientY();
                 }
			}
		});

		PlaceController placeController = getClientFactory().getPlaceController();

		RootPanel loadingMessage = RootPanel.get("loadingMessage");
		if (loadingMessage != null)
		{
			DOM.setInnerHTML(loadingMessage.getElement(), "");
		}

		// Start ActivityManager for the main widget with our ActivityMapper
		ActivityMapper activityMapper = new AppActivityMapper((ClientFactory) getClientFactory());
		ActivityManager activityManager = new ActivityManager(activityMapper, eventBus);
		activityManager.setDisplay(skeleton.getContentPanel());

		// Start PlaceHistoryHandler with our PlaceHistoryMapper
		AppPlaceHistoryMapper historyMapper= GWT.create(AppPlaceHistoryMapper.class);
		PlaceHistoryHandler historyHandler = new PlaceHistoryHandler(historyMapper);
		historyHandler.register(placeController, eventBus, defaultPlace);
		

		notificationMole = new LocalNotificationMole();
		viewsPopUp = new ViewsPopUp();
		RootLayoutPanel.get().add(notificationMole);
		RootLayoutPanel.get().add(viewsPopUp);
		RootLayoutPanel.get().add(skeleton);
		Window.enableScrolling(false);
		Window.setMargin("0px");
		// Goes to the place represented on URL else default place
		historyHandler.handleCurrentHistory();
		
		//Chargement des variables globales
		ClientPropertiesList.loadProperties(getClientFactory());
		//ClientPropertiesList.getProperties(this);
		ClientSystemParametersList.loadParameters();
		ClientSystemParametersList.getParameters(this);
		ClientBoatList.loadBoats();
	}

	private void guiInit(ClientFactory clientFactory) 
	{
		skeleton = new TopSideMenuSkeleton(clientFactory.getEventBus());
		skeleton.guiInit(clientFactory.getHeaderView(), clientFactory.getStatusBar(), new HorizontalMenuImpl(clientFactory.getEventBus()), clientFactory.getNavigationBar());
		
		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		shortcuts.add(ShortcutFactory.getWelcomeShortcut());
		
		clientFactory.getEventBus().fireEvent(new BreadCrumbChangeEvent(shortcuts));
	}


	private void showMole(String message) {


		notificationMole.setAnimationDuration(400);
		notificationMole.addStyleName("notif");
		notificationMole.getElement().getStyle().setProperty("zIndex", ""+DialogBoxTools.getHigherZIndex());
		notificationMole.getElement().getStyle().setProperty("position", "absolute");
		notificationMole.getElement().getStyle().setProperty("left", "0");
		notificationMole.getElement().getStyle().setProperty("top", "0");
		notificationMole.setWidth("200px");

		notificationMole.show(message);
		Timer timer = new Timer() {
			@Override
			public void run() {
				notificationMole.hide();
			}
		};
		timer.schedule(2500);

	}


	@Override
	public void onNotification(NotificationEvent event) 
	{
		showMole(event.getMessage());
	}
	
	@Override
	public void onShowPopUp(ShowPopUpEvent event) 
	{
//		this.getElement().getStyle().setProperty("zIndex", "9999");
		
//		viewsPopUp.show(event.getMessage(),event.getTop(), event.getRight());
		viewsPopUp.show(event.getMessage(), mouseY + 10, mouseX + 10);
	}

	@Override
	public void onHidePopUp(HidePopUpEvent event) {
		viewsPopUp.hide();
	}

	@Override
	public void postLoadProcess(Map<String, String> result) {
		getClientFactory().getEventBus().fireEvent(new ParameterLoadedEvent(result));	
		
		String testMode = result.get(ParameterConstants.TEST_MODE);
		TEST_MODE = Boolean.parseBoolean(testMode);
		
		if (TEST_MODE){
			setTestUser();
		}else{
			getClientFactory().getEventBus().addHandler(GAEvent.TYPE, (ClientFactory)getClientFactory());
		}
	}

	private void setTestUser(){
		InternalUser u = new InternalUser();
		u.setName("TEST USER");
		u.setAdmin(true);
		((ClientFactory) getClientFactory()).getUserManager().setUser(u);
		getClientFactory().getEventBus().fireEvent(new UserLoginEvent(u));
	}
	
	@Override
	public void onNotification(UserLoginEvent event) {
		((AuthenticatedClientFactory) getClientFactory()).getUserManager().setUser(event.getUser());
		getClientFactory().getEventBus().fireEvent(new NotificationEvent("User logged in"));
	}

	@Override
	public void onNotification(UserLogoutEvent event) {
		((AuthenticatedClientFactory) getClientFactory()).getUserManager().clearCurrentUser();
		getClientFactory().getEventBus().fireEvent(new PlaceNavigationEvent(defaultPlace));
	}


}