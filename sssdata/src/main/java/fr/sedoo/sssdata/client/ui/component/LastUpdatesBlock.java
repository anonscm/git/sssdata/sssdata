package fr.sedoo.sssdata.client.ui.component;

import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.place.shared.PlaceChangeEvent;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

import fr.sedoo.commons.client.DefaultClientApplication;
import fr.sedoo.commons.client.image.CommonBundle;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.sssdata.client.place.UpdateListPlace;
import fr.sedoo.sssdata.shared.UpdateDTO;

public class LastUpdatesBlock extends VerticalPanel {

	private UpdatesTable table;
	
	public LastUpdatesBlock() {
		super();
		CommonBundle.INSTANCE.commonStyle().ensureInjected();
		setWidth("100%");
		setHeight("100%");
		table = new UpdatesTable(true);
		add(table);
		//add(getCompleteListLink());
	}
	
	private HorizontalPanel getCompleteListLink(){
		HorizontalPanel secondaryPanel = new HorizontalPanel();
		Image image = new Image(CommonBundle.INSTANCE.menuDot());
		image.setStyleName(CommonBundle.INSTANCE.commonStyle().leftMargin5px());
		secondaryPanel.add(image);
		Label label = new Label(CommonMessages.INSTANCE.completeList());
		label.setStyleName(CommonBundle.INSTANCE.commonStyle().leftMargin5px());
		label.addStyleName(CommonBundle.INSTANCE.commonStyle().clickable());
		label.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				UpdateListPlace place = new UpdateListPlace();
				DefaultClientApplication.getClientFactory().getEventBus().fireEvent(new PlaceChangeEvent(place));
			}
		});
		secondaryPanel.add(label);
		return secondaryPanel;
	}
	
	public void setData(List<UpdateDTO> updates) {
		table.setRowData(updates);
	}

	public void reset() {
		table.reset();
	}

	public void setPresenter(UpdatesTablePresenter presenter) {
		table.setPresenter(presenter);
	}
	
}
