package fr.sedoo.sssdata.client.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fr.sedoo.sssdata.shared.BoatDTO;

@RemoteServiceRelativePath("boats")

public interface BoatService extends RemoteService {
        ArrayList<BoatDTO> findAll() throws ServiceException;
    	//void add (BoatDTO b) throws ServiceException;
    	//BoatDTO findByName(String name)throws ServiceException;
}
