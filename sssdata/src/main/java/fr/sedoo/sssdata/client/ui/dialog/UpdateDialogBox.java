package fr.sedoo.sssdata.client.ui.dialog;

import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.ScrollPanel;

import fr.sedoo.sssdata.shared.UpdateDTO;

public class UpdateDialogBox extends DialogBox {

	private UpdateDialogBoxContent content;
		
	public UpdateDialogBox() {
		super(true);
		setGlassEnabled(true);
		
		ScrollPanel scrollPanel = new ScrollPanel();
		setWidth("400px");
		setHeight("300px");
		content = new UpdateDialogBoxContent();
		scrollPanel.add(content);
		setWidget(scrollPanel);
	}
	
	public void setData(UpdateDTO u){
		content.setData(u);
	}	
	
}
