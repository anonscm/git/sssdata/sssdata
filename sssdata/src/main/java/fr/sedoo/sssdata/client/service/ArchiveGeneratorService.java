package fr.sedoo.sssdata.client.service;

import java.text.ParseException;
import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fr.sedoo.sssdata.shared.ArchiveGeneratorRequest;
import fr.sedoo.sssdata.shared.BoatDTO;
import fr.sedoo.sssdata.shared.FileDTO;

@RemoteServiceRelativePath("archivegenerator")
public interface ArchiveGeneratorService extends RemoteService {

	List<ArchiveGeneratorRequest> initRequests() throws Exception;

	List<FileDTO> getArchiveFilesByBoat(ArchiveGeneratorRequest request, BoatDTO boat) throws Exception;

	String createArchive(ArchiveGeneratorRequest request, List<FileDTO> temporaryFiles) throws Exception;

}
