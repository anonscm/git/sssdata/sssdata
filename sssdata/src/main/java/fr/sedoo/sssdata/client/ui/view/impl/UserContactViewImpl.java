/**
 * 
 */
package fr.sedoo.sssdata.client.ui.view.impl;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.callback.LoadCallBack;
import fr.sedoo.commons.client.component.AbstractView;
import fr.sedoo.commons.client.config.ClientPropertiesList;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.panel.HorizontalCollapsingPanel;
import fr.sedoo.sssdata.client.GlobalBundle;
import fr.sedoo.sssdata.client.ui.view.api.UserContactView;
import fr.sedoo.sssdata.client.ui.widget.BoatListBox;
import fr.sedoo.sssdata.client.ui.widget.date.SssDataDateBox;
import fr.sedoo.sssdata.shared.BoatDTO;
import fr.sedoo.sssdata.shared.ConfigPropertiesCode;
import fr.sedoo.sssdata.shared.UserContactInformations;

/**
 * @author brissebr
 */
public class UserContactViewImpl extends AbstractView implements UserContactView {

	interface UserContactViewImplUiBinder extends UiBinder<Widget, UserContactViewImpl> {}
	private static UserContactViewImplUiBinder uiBinder = GWT.create(UserContactViewImplUiBinder.class);
	
	interface UserContactDriver extends SimpleBeanEditorDriver<UserContactInformations, UserContactViewImpl> {}
	private UserContactDriver driver = GWT.create(UserContactDriver.class);
	
	private Presenter presenter;
	
	
	@UiField
	HorizontalCollapsingPanel boatListPanel;
	@UiField
	BoatListBox boatListSelection;
	@UiField
	Button selectAllBoats;
		
	@UiField
	HorizontalCollapsingPanel datePanel;
	@UiField(provided=true)
	SssDataDateBox beginDateEditor;
	@UiField(provided=true)
	SssDataDateBox endDateEditor;
	
	
	@Ignore
	@UiField(provided=false)
	VerticalPanel messagePanel;
		
	@UiField(provided=false)
	TextBox titleEditor;
	
	@UiField(provided=false)
	TextArea messageEditor;
	
	@UiField(provided=false)
	CheckBox addNewsEditor;
	
	@UiField(provided=false)
	Label subjectPrefixEditor;	
	
	@UiField(provided=false)
	Label greetingEditor;	
	
	@UiField(provided=false)
	Label salutationEditor;	
	
	@UiField(provided=false)
	Label signatureEditor;	
	
	@UiField(provided=false)
	Label fromEmailEditor;	
	
	public UserContactViewImpl() {
		super();
		GWT.<GlobalBundle>create(GlobalBundle.class).css().ensureInjected();

		beginDateEditor = new SssDataDateBox("yyyy-MM-dd");
		endDateEditor = new SssDataDateBox("yyyy-MM-dd");

		ClientPropertiesList.getProperty(ConfigPropertiesCode.DATE_MIN, new LoadCallBack<String>() {
			@Override
			public void postLoadProcess(String result){
				DateTimeFormat format = DateTimeFormat.getFormat("dd/MM/yyyy");
				beginDateEditor.setMinDate(format.parse(StringUtil.trimToEmpty(result)));
				endDateEditor.setMinDate(format.parse(StringUtil.trimToEmpty(result)));
			}
		});
		
		ClientPropertiesList.getProperty(ConfigPropertiesCode.DATE_MAX,
				new LoadCallBack<String>() {
					@Override
					public void postLoadProcess(String result) {
						DateTimeFormat format = DateTimeFormat
								.getFormat("dd/MM/yyyy");
						beginDateEditor.setMaxDate(format.parse(StringUtil
								.trimToEmpty(result)));
						endDateEditor.setMaxDate(format.parse(StringUtil
								.trimToEmpty(result)));
					}
				});

		initWidget(uiBinder.createAndBindUi(this));
		driver.initialize(this);
		applyCommonStyle();		
		
		//boatListPanel.showPanel();
		//datePanel.showPanel();
		boatListSelection.setVisibleItemCount(10);
		
	}

	@Override
	public void setBoatList(ArrayList<BoatDTO> boats) {
		boatListSelection.clear();
		for (BoatDTO b : boats){
			boatListSelection.addItem(b.getName());//,b.getId().toString());
		}	
	}
	
	@Override
	public void reset() {
		presenter.getDefaultCriterion();
		boatListSelection.reset();
		resetMessages();
		resetTextFields();
	}

	
	private void resetTextFields(){
		titleEditor.getElement().getStyle().setBackgroundColor("#FFFFFF");
		messageEditor.getElement().getStyle().setBackgroundColor("#FFFFFF");
	}
	
	private boolean validInfos(UserContactInformations infos) {
		boolean result = true;
		resetMessages();
		resetTextFields();
		if (StringUtil.isEmpty(infos.getTitle())){
			titleEditor.getElement().getStyle().setBackgroundColor("#FFCCCC");
			addErrorMessage("Title is required");
			result = false;
		}
		
		if (StringUtil.isEmpty(infos.getMessage())){
			messageEditor.getElement().getStyle().setBackgroundColor("#FFCCCC");
			addErrorMessage("Message is required");
			result = false;
		}
		return result;
	}
		
	
	@UiHandler("resetButton")
	void onResetButtonClicked(ClickEvent event){
		reset();
	}
	
	
	@UiHandler("sendButton")
	void onSendButtonClicked(ClickEvent event){
		if (validInfos(driver.flush())){
			presenter.sendMessage(driver.flush());
		}
	}
	
	@UiHandler("selectAllBoats")
	void onselectAllBoatsClicked(ClickEvent event){
		this.boatListSelection.setAllItemSelected();
	}
	
	@UiHandler("unselectAllBoats")
	void onunselectAllBoatsClicked(ClickEvent event){
		this.boatListSelection.reset();
	}
	
	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public void addMessage(String msg) {
		Label l = new Label(msg);
		l.getElement().getStyle().setColor("green");
		messagePanel.add(l);
	}

	@Override
	public void addErrorMessage(String msg) {
		Label l = new Label(msg);
		l.getElement().getStyle().setColor("red");
		messagePanel.add(l);
	}

	@Override
	public void resetMessages() {
		messagePanel.clear();
	}

	@Override
	public void setSearchCriterion(UserContactInformations infos) {
		driver.edit(infos);
	}
	
}
