package fr.sedoo.sssdata.client.activity;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.sedoo.commons.client.cms.activity.CMSEditActivity;
import fr.sedoo.commons.client.cms.place.CMSEditPlace;
import fr.sedoo.commons.client.event.BreadCrumbChangeEvent;
import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;
import fr.sedoo.sssdata.client.ui.component.ShortcutFactory;

public class SSSCMSEditActivity extends CMSEditActivity {

	Place place;
	
	public SSSCMSEditActivity(CMSEditPlace place, AuthenticatedClientFactory clientFactory) {
		super(place, clientFactory);
		this.place = place;
	}
		
	
	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		super.start(containerWidget, eventBus);
		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		shortcuts.add(ShortcutFactory.getWelcomeShortcut());
		shortcuts.add(ShortcutFactory.getAdminShortcut());
		eventBus.fireEvent(new BreadCrumbChangeEvent(shortcuts));
		
		
	}
	
	@Override
	protected boolean isValidUser() {
		return  isLoggedUser();
	}


	@Override
	public Place getPlace() {
		return place;
	}

}
