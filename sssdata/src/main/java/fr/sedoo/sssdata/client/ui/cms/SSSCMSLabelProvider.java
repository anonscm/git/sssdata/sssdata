package fr.sedoo.sssdata.client.ui.cms;

import fr.sedoo.commons.client.cms.ui.CMSLabelProvider;
import fr.sedoo.sssdata.client.message.ApplicationMessages;
import fr.sedoo.sssdata.client.ui.component.impl.menu.ScreenNames;

public class SSSCMSLabelProvider implements CMSLabelProvider {

	@Override
	public String getLabelByScreenName(String screenName) {
		if (screenName.compareTo(ScreenNames.HOME) == 0) {
			return ApplicationMessages.INSTANCE.welcome();
		}else if (screenName.compareTo(ScreenNames.DATA_POLICY) == 0) {
			return  ApplicationMessages.INSTANCE.policyViewTitle();
		}else{
			return "";
		}
	}

}
