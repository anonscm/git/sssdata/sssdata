package fr.sedoo.sssdata.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.sedoo.sssdata.shared.metadata.DoiInfos;
import fr.sedoo.sssdata.shared.metadata.HistoryItem;

public interface DoiHistoryServiceAsync {

	void create(HistoryItem item, String doiSuffix, AsyncCallback<HistoryItem> callback);

	void delete(HistoryItem item, AsyncCallback<Void> callback);

	void edit(HistoryItem item, String doiSuffix, AsyncCallback<HistoryItem> callback);

	//void findAll(AsyncCallback<ArrayList<HistoryItem>> callback);

	void getDoiInfos(String doiSuffix, AsyncCallback<DoiInfos> callback);

}
