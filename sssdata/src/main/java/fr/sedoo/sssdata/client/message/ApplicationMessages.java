package fr.sedoo.sssdata.client.message;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.LocalizableResource.DefaultLocale;
import com.google.gwt.i18n.client.LocalizableResource.Generate;

import com.google.gwt.i18n.client.Messages;

@Generate(format = { "com.google.gwt.i18n.rebind.format.PropertiesFormat" }, fileName = "Message", locales = { "en" })
@DefaultLocale("en")
public interface ApplicationMessages extends Messages {

	public static final ApplicationMessages INSTANCE = GWT
			.create(ApplicationMessages.class);

	@Key("welcome")
	public String welcome();
	
	@Key("welcome.title")
	public String welcomeTitle();
		
	@Key("about")
	public String about();
	
	@Key("refresh")
	public String refresh();
	
	@Key("date")
	public String date();
	
	@Key("version")
	public String version();
	
	@Key("comment")
	public String comment();
	
	@Key("welcome.updates")
	public String lastUpdates();
	
	@Key("welcome.update")
	public String lastUpdate();
	
	@Key("welcome.dbStatus")
	public String dbStatus();
	
	@Key("updatesView.title")
	public String updatesViewTitle();
		
	@Key("temperatureMeasurementUnit")
	public String temperatureMeasurementUnit();
	
	@Key("salinityMeasurementUnit")
	public String salinityMeasurementUnit();

	@Key("systemView.title")
	public String systemViewTitle();

	@Key("systemView.applicationVersion")
	public String systemViewApplicationVersion();

	@Key("systemView.javaVersion")
	public String systemViewJavaVersion();

	@Key("systemView.testMode")
	public String systemViewTestMode();
	
	@Key("systemView.doi")
	public String systemViewDoi();
	
	@Key("administrationMenu.title")
	public String administrationMenuTitle();

	@Key("managementMenu.title")
	public String managementMenuTitle();
	
	@Key("importMenu.title")
	public String importMenuTitle();

	@Key("doiMenu.title")
	public String doiMenuTitle();
	
	@Key("historyView.title")
	public String doiHistoryViewTitle();
		
	@Key("doiHistoryManagementView.title")
	public String doiHistoryManagementViewTitle();

	@Key("metadata.title")
	public String metadataTitle();
	
	@Key("gridMenu.title")
	public String gridMenuTitle();
	
	@Key("grid.metadata.title")
	public String gridMetadataTitle();
	
	@Key("grid.atlantic")
	public String gridAtlantic();
	@Key("grid.pacific")
	public String gridPacific();
	@Key("grid.nasg")
	public String gridNasg();
	@Key("grid.naspg")
	public String gridNaspg();
	
	@Key("grid.nasg.menu")
	public String gridNasgMenu();
	@Key("grid.nasg.menu.nasg")
	public String gridNasgSubMenu();
	@Key("grid.nasg.menu.naspg")
	public String gridNaspgSubMenu();
	
	@Key("grid.atlantic.title")
	public String gridAtlanticTitle();
	@Key("grid.atlantic.bin.title")
	public String gridBinAtlanticTitle();
	@Key("grid.pacific.title")
	public String gridPacificTitle();
	@Key("grid.nasg.title")
	public String gridNasgTitle();
	@Key("grid.naspg.title")
	public String gridNaspgTitle();
	
	@Key("grid.hist")
	public String gridHist();
	@Key("grid.hist.title")
	public String gridHistTitle();
	
	@Key("administratorManagementMenu.title")
	public String administratorManagementMenuitle();
	
	@Key("delayedModeData.title")
	public String delayedModeDataTitle();
	
	@Key("faqManagementMenu.title")
	public String faqManagementMenuTitle();
	
	@Key("administratorManagementMenu.title")
	public String administratorManagementMenuTitle();
	
	@Key("administratorManagementView.title")
	public String administratorManagementViewTitle();

	@Key("administratorManagementView.addAdministrator")
	public String administratorManagementViewAddAdministrator();

	@Key("administratorManagementView.editAdministrator")
	public String administratorManagementViewEditAdministrator();
	
	@Key("doiHistoryManagementView.addItem")
	public String doiHistoryManagementViewAddItem();

	@Key("doiHistoryManagementView.editItem")
	public String doiHistoryManagementViewEditItem();

	@Key("messageManagementView.editMessage")
	public String messageManagementViewEditMessage();

	@Key("messageManagementView.addMessage")
	public String messageManagementViewAddMessage();

	@Key("messageEditView.title")
	public String messageEditViewTitle();
	
	
	@Key("messageManagementMenu.title")
	public String messageManagementMenuTitle();
	
	@Key("messageManagementView.title")
	public String messageManagementViewTitle();
	
	@Key("message.content")
	public String messageContent();
	
	@Key("message.date")
	public String messageDate();

	@Key("searchView.searchMessage")
	public String searchViewSearchMessage();
	
	@Key("searchView.beginDateLabel")
	public String searchViewBeginDateLabel();
	
	@Key("searchView.endDateLabel")
	public String searchViewEndDateLabel();
	
	@Key("searchView.searchButtonLabel")
	public String searchViewSearchButtonLabel();
	
	@Key("searchView.resetButtonLabel")
	public String searchViewResetButtonLabel();
	
	@Key("searchView.selectAllButtonLabel")
	public String searchViewSelectAllButtonLabel();
	
	@Key("searchView.unselectAllButtonLabel")
	public String searchViewUnselectAllButtonLabel();
		
	@Key("searchView.allArchive")
	public String searchViewAllArchive();
	
	@Key("searchView.allGoodArchive")
	public String searchViewAllGoodArchive();	
		
	@Key("searchView.title")
	public String searchViewTitle();
	
	@Key("searchMenu.title")
	public String searchMenuTitle();
	
	@Key("dataMenu.title")
	public String dataMenuTitle();
	
	@Key("policyMenu.title")
	public String policyMenuTitle();
	
	@Key("policyView.title")
	public String policyViewTitle();
	
	@Key("searchView.temperatureFieldLabel")
	public String searchViewTemperatureFieldLabel();
	
	@Key("searchView.salinityFieldLabel")
	public String searchViewSalinityFieldLabel();
	
	@Key("searchView.dateFieldLabel")
	public String searchViewDateFieldLabel();
	
	@Key("searchView.pre2003DataFieldLabel")
	public String searchViewpre2003DataFieldLabel();
	
	@Key("searchView.DataRangeFieldLabel")
	public String searchViewDataRangeFieldLabel();
	
	@Key("searchView.boatListFieldLabel")
	public String searchViewBoatListFieldLabel();
	
	@Key("importView.dirListFieldLabel")
	public String importViewDirListFieldLabel();

	@Key("searchView.qualityThresholdFieldLabel")
	public String searchViewQualityThresholdFieldLabel();
	
	@Key("searchView.qualityThresholdExplainLabel")
	public String searchViewQualityThresholdExplainLabel();
	
	@Key("searchView.qualityThresholdZeroTooltip")
	public String searchViewQualityThresholdZeroTooltip();
	
	@Key("searchView.qualityThresholdOneTooltip")
	public String searchViewQualityThresholdOneTooltip();
	
	@Key("searchView.qualityThresholdTwoTooltip")
	public String searchViewQualityThresholdTwoTooltip();
	
	@Key("searchView.qualityThresholdThreeTooltip")
	public String searchViewQualityThresholdThreeTooltip();
	
	@Key("searchView.qualityThresholdFourTooltip")
	public String searchViewQualityThresholdFourTooltip();
	
	@Key("searchView.qualityThresholdFiveTooltip")
	public String searchViewQualityThresholdFiveTooltip();
	
	@Key("userInfoView.title")
	public String userInfoViewTitle();
	
	@Key("userInfoView.topInstruction")
	public String userInfoViewTopInstruction();
	
	@Key("userInfoView.topInstructionExplanation")
	public String userInfoViewTopInstructionExplanation();
	
	@Key("userInfoView.adBottomInstruction")
	public String userInfoViewAdBottomInstruction();
	
	@Key("userInfoView.userNameLabel")
	public String userInfoViewUserNameLabel();
	
	@Key("userInfoView.userOrganismLabel")
	public String userInfoViewUserOrganismLabel();
	
	
	@Key("userInfoView.userCountryLabel")
	public String userInfoViewUserCountryLabel();	

	@Key("userInfoView.userObjectiveLabel")
	public String userInfoViewUserObjectiveLabel();
	
	@Key("userInfoView.userDataUsageLabel")
	public String userInfoViewUserDataUsageLabel();
	
	@Key("userInfoView.userEmailLabel")
	public String userInfoViewUserEmailLabel();
	
	@Key("userInfoView.downloadButtonLabel")
	public String userInfoViewDownloadButtonLabel();
	
	@Key("userInfoView.retrieveInfoButtonLabel")
	public String userInfoViewRetrieveInfoButtonLabel();
	
	@Key("userInfoView.retrieveInfoButtonTooltip")
	public String userInfoViewRetrieveInfoButtonTooltip();
	
	@Key("userInfoView.backButtonLabel")
	public String userInfoViewBackButtonLabel();
	
	@Key("userInfoView.chooseDataFormatLabel")
	public String userInfoViewChooseDataFormatLabel();
	
	@Key("userInfoView.chooseDataCompressionLabel")
	public String userInfoViewChooseDataCompressionLabel();
		
	@Key("userInfoView.errorsLabel")
	public String userInfoViewErrorsLabel();
			
	@Key("header.laboTitle")
	public String headerLaboTitle();
	
	@Key("editableParameterView.addParameter")
	public String editableParameterViewAddParameter();

	@Key("editableParameterView.editParameter")
	public String editableParameterViewEditParameter();
	
	@Key("statsMenu.title")
	public String statsMenuTitle();
	
	@Key("userStatsMenu.title")
	public String userStatsMenuTitle();
		
	@Key("editableParameterMenu.title")
	public String editableParameterMenuTitle();
	
	@Key("editableParameterView.title")
	public String editableParameterViewTitle();

	@Key("statsView.title")
	public String statsViewTitle();
	
	@Key("statsView.statsMessage")
	public String statsViewMessage();
	
	@Key("userStatsView.title")
	public String userStatsViewTitle();
	
	@Key("statsView.downloadStatsFieldLabel")
	public String downloadStatsFieldLabel();
	
	@Key("importView.title")
	public String importViewTitle();

	@Key("importView.importData")
	public String importData();
		
	@Key("welcomeManagementView.Title")
	public String welcomeManagementViewTitle();

	@Key("aboutManagementView.Title")
	public String aboutManagementViewTitle();	
	
	@Key("screenManagement")
	public String screenManagement();
	
	@Key("resetDefaultParameters")
	public String resetDefaultParameters();
	
	@Key("fileSearchView.title")
	public String fileSearchViewTitle();
	
	@Key("userContactView.title")
	public String userContactViewTitle();
		
	@Key("userContactView.subjectLabel")
	public String userContactViewSubjectLabel();
	
	@Key("userContactView.fromLabel")
	public String userContactViewFromLabel();
		
	@Key("userContactView.bodyLabel")
	public String userContactViewBodyLabel();
			
	@Key("userContactView.addNews")
	public String userContactViewAddNews();
	
	@Key("downloadLogView.title")
	public String downloadLogViewTitle();
	
	@Key("downloadLogView.label.volume")
	public String downloadLogViewVolumeLabel();
	
	@Key("downloadLogMenu.title")
	public String downloadLogMenuTitle();
	
	@Key("dataDownloadsMenu.title")
	public String dataDownloadsMenuTitle();
		
		
}
