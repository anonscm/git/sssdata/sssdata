package fr.sedoo.sssdata.client.ui.view.impl;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.AbstractImagePrototype;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.component.AbstractView;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.sssdata.client.GlobalBundle;
import fr.sedoo.sssdata.client.ui.view.api.MetadataView;
import fr.sedoo.sssdata.shared.metadata.CreatorDTO;
import fr.sedoo.sssdata.shared.metadata.HistoryItem;
import fr.sedoo.sssdata.shared.metadata.MetadataDTO;
import fr.sedoo.sssdata.shared.metadata.RelatedIdentifierDTO;

public class MetadataViewImpl extends AbstractView implements MetadataView{

	Presenter presenter;

	@UiField
	Label pageTitle;
	
	@UiField
	VerticalPanel metadataPanel;

	@UiField
	Button xmlButton;
	
	@UiField
	Button dataButton;
	
	@UiField
	Image refreshImage;
	
	@UiField
	DivElement messageContainer;
	
	FlexTable table;

	private static MetadataViewImplUiBinder uiBinder = GWT
			.create(MetadataViewImplUiBinder.class);

	interface MetadataViewImplUiBinder extends UiBinder<Widget, MetadataViewImpl> {
	}


	public MetadataViewImpl() {
		super();
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();

		table = new FlexTable();

		table.setWidth("100%");
		table.getColumnFormatter().setWidth(0, "20%");
		table.getColumnFormatter().setWidth(1, "80%");
				
		metadataPanel.add(table);
	}

	@Override
	public void setOptionalContent(String content) {
		messageContainer.setInnerHTML(content);
	}
	
	@Override
	public void displayMetadata(MetadataDTO metadata) {
		if (metadata != null){
			addSection("General information");
			addLabelValue("Title", metadata.getTitle());
			addLabelValue("DOI", metadata.getDoi());
						
			if (StringUtil.isEmpty(metadata.getDoi())){
				xmlButton.asWidget().setVisible(false);
			}
			
			addLabelValue("Version", metadata.getVersion());
			addLabelValue("Temporal coverage", metadata.getDateBegin() + " - " + metadata.getDateEnd());
			addLabelValue("Publisher", metadata.getPublisher());
			addLabelValue("Publication year", metadata.getPublicationYear());
			//addLabelValue("Abstract", metadata.getSummary());
			
			/*List<IsWidget> creators = new ArrayList<IsWidget>();
			for(CreatorDTO c: metadata.getCreators()){
				HTML v = new HTML(c.getName());
				v.setStyleName("datasetValue");
				creators.add(v);
				
			}*/
			//addLabelValue("Creators", creators);
			
			
			
			addSection("Creators");
			for(CreatorDTO c: metadata.getCreators()){
				int row = table.getRowCount();
				//table.setWidget(row,0,createValue(c.getName()));
				table.setWidget(row, 0, createCreatorLine(c));
				table.getFlexCellFormatter().setColSpan(row, 0, 2);
			}
			
			addSection("Abstract");
			int r = table.getRowCount();
			table.setWidget(r,0,createValue(metadata.getSummary()));
			table.getFlexCellFormatter().setColSpan(r, 0, 2);
		
			addSection("Data use information");
			if (metadata.getCitation() != null){
				addLabelValue("Citation", metadata.getCitation());
			}
			addLabelValue("Use constraints", metadata.getRights());
			
			List<IsWidget> formats = new ArrayList<IsWidget>();
			for(String f: metadata.getDataFormats()){
				HTML v = new HTML(f);
				v.setStyleName("datasetValue");
				formats.add(v);
			}
			addLabelValue("Data format(s)", formats);
						
			if (!metadata.getHistory().isEmpty()){
				addSection("History");
				for(HistoryItem h: metadata.getHistory()){
					addLabelValue(h.getDate() + ", " + h.getVersion(), h.getComment());
				}
			}
			
			addSection("Contributors");
			for(String c: metadata.getContributors()){
				int row = table.getRowCount();
				table.setWidget(row,0,createValue(c));
				table.getFlexCellFormatter().setColSpan(row, 0, 2);
			}
			
			addSection("Keywords");
			for(String k: metadata.getKeywords()){
				int row = table.getRowCount();
				table.setWidget(row,0,createValue(k));
				table.getFlexCellFormatter().setColSpan(row, 0, 2);
			}
			
			addSection("Related identifiers");
			for(RelatedIdentifierDTO dto: metadata.getRelatedIdentifiers()){
				addLabelAnchor(dto.getType(), dto.getIdentifier());
			}
		}
	}

	private SafeHtml orcidAnchor(String orcid){
		Image orcidLogo = AbstractImagePrototype.create(GlobalBundle.INSTANCE.orcid()).createImage();
		orcidLogo.setTitle("ORCID");
		orcidLogo.addStyleName("orcid");
		Anchor orcidAnchor = new Anchor(SafeHtmlUtils.fromTrustedString(orcidLogo.toString()), "https://orcid.org/" + orcid);
		orcidAnchor.setTarget("_blank");
		return SafeHtmlUtils.fromTrustedString(orcidAnchor.toString());
	}
	private HTML createCreatorLine(CreatorDTO c) {
		SafeHtmlBuilder builder = new SafeHtmlBuilder();
		HTML l = new HTML();
		l.setStyleName("datasetValue");
		if (c.getOrcid() != null){
			builder.append(orcidAnchor(c.getOrcid()));
		}else{
			l.addStyleName("noOrcid");
		}
		l.setHTML(builder.appendEscapedLines(c.getName()).toSafeHtml());
		return l;
	}

	private void addSection(String titre){
		int row = table.getRowCount();
		Label t = new Label(titre);
		t.setStyleName("datasetTitreSection");
		table.getFlexCellFormatter().setColSpan(row, 0, 2);
		table.setWidget(row, 0, t);
	}

	private void addLabelValue(String label, String value){
		if (StringUtil.isNotEmpty(value)){
			addLabelValue(label, createValue(value));
		}
	}
	
	private void addLabelAnchor(String label, String value){
		if (StringUtil.isNotEmpty(value)){
			addLabelValue(label, createAnchor(value));
		}
	}
	
	private void addLabelValue(String label, IsWidget value){
		int row = table.getRowCount();
		table.setWidget(row, 0, createLabel(label));
		table.getFlexCellFormatter().setVerticalAlignment(row, 0, HasVerticalAlignment.ALIGN_TOP);
		table.setWidget(row, 1, value);
	}
	private void addLabelValue(String label, List<IsWidget> values){
		if (values != null && !values.isEmpty()){
			int row = table.getRowCount();
			VerticalPanel p = new VerticalPanel();
			for (IsWidget w: values){
				p.add(w);
			}
			table.setWidget(row, 0, createLabel(label));
			table.getFlexCellFormatter().setVerticalAlignment(row, 0, HasVerticalAlignment.ALIGN_TOP);
			table.setWidget(row, 1, p);
		}
	}

	private Label createLabel(String s) {
		Label l = new Label(s);
		l.setStyleName("datasetLabel");
		return l;
	}

	private HTML createValue(String s) {
		HTML l = new HTML(new SafeHtmlBuilder().appendEscapedLines(s).toSafeHtml());
		l.setStyleName("datasetValue");
		return l;
	}
	
	private Anchor createAnchor(String s) {
		Anchor a = new Anchor(new SafeHtmlBuilder().appendEscapedLines(s).toSafeHtml(), s);
		//a.setStyleName("datasetValue");
		a.setTarget("_blank");
		return a;
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;		
	}

	@Override
	public void reset() {
		table.clear();		
	}

	@UiHandler("xmlButton")
	public void export(ClickEvent event) {
		presenter.exportMetadata();
	}
	@UiHandler("dataButton")
	public void onDataButtonClicked(ClickEvent event) {
		presenter.toDataPage();
	}

	@UiHandler("refreshImage")
	public void refresh(ClickEvent event) {
		presenter.refreshMetadata();		
	}

	public void setPageTitle(String title) {
		this.pageTitle.setText(title);
	}
}
