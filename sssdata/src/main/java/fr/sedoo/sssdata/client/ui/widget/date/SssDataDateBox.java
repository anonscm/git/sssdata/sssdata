package fr.sedoo.sssdata.client.ui.widget.date;

import java.util.Date;

import com.google.gwt.event.logical.shared.ShowRangeEvent;
import com.google.gwt.event.logical.shared.ShowRangeHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;

import fr.sedoo.commons.client.widget.datepicker.FormatedDateBox;

public class SssDataDateBox extends FormatedDateBox implements ShowRangeHandler<Date>, ValueChangeHandler<Date>
{
	private Date minDate;
	private Date maxDate;
	
	public SssDataDateBox(String format) {
		super(format);
		getDatePicker().addShowRangeHandler(this);
		addValueChangeHandler(this);
		getTextBox().setReadOnly(true);
	}

	@Override
	public void onShowRange(ShowRangeEvent<Date> dateShowRangeEvent) 
	{
		Date d = zeroTime(dateShowRangeEvent.getStart());
		final long endTime = dateShowRangeEvent.getEnd().getTime();
		while (d.getTime() <= endTime)
		{
			if (minDate != null)
			{
				if (d.before(minDate))
				{
					getDatePicker().setTransientEnabledOnDates(false, d);
				}
			}

			if (maxDate != null)
			{
				if (d.after(maxDate))
				{
					getDatePicker().setTransientEnabledOnDates(false, d);
				}
			}

			nextDay(d);
		}

	}

	/** this is important to get rid of the time portion, including ms */
	private static Date zeroTime(final Date date)
	{
		return DateTimeFormat.getFormat("yyyyMMdd").parse(DateTimeFormat.getFormat("yyyyMMdd").format(date));
	}

	private static void nextDay(final Date d)
	{
		com.google.gwt.user.datepicker.client.CalendarUtil.addDaysToDate(d, 1);
	}

	public Date getMaxDate() {
		return maxDate;
	}

	public void setMaxDate(Date maxDate) {
		this.maxDate= zeroTime(maxDate);
	}

	public Date getMinDate() {
		return minDate;
	}

	public void setMinDate(Date minDate) {
		this.minDate = zeroTime(minDate);
	}

	@Override
	public void onValueChange(ValueChangeEvent<Date> dateValueChangeEvent) 
	{
		if (dateValueChangeEvent.getValue() !=  null)
		{
			if (minDate != null)
			{
				if (dateValueChangeEvent.getValue().before(minDate))
				{
					setValue(minDate, false);
				}
			}

			if (maxDate != null)
			{
				if (dateValueChangeEvent.getValue().after(maxDate))
				{
					setValue(maxDate, false);
				}
			}
		}
	}




}
