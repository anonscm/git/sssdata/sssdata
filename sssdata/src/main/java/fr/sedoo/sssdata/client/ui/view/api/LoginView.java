package fr.sedoo.sssdata.client.ui.view.api;

import com.google.gwt.user.client.ui.IsWidget;

public interface LoginView extends IsWidget {

	void setPresenter(Presenter presenter);
	void reset();
	void onLoginFailed();
	
	void addErrorMessage(String message);
	void addMessage(String message);
	void resetMessages();
	
	public interface Presenter {
	        void login(String login, String password);
	 }

	
	
}
