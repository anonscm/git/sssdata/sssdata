package fr.sedoo.sssdata.client.ui.component;

import java.util.Date;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.TextColumn;

import fr.sedoo.commons.client.crud.widget.CreateConfirmCallBack;
import fr.sedoo.commons.client.crud.widget.CrudTable;
import fr.sedoo.commons.client.crud.widget.EditConfirmCallBack;
import fr.sedoo.commons.client.widget.dialog.EditDialogContent;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.sssdata.client.message.ApplicationMessages;
import fr.sedoo.sssdata.client.ui.dialog.HistoryItemEditDialogContent;
import fr.sedoo.sssdata.shared.metadata.HistoryItem;

public class DoiHistoryTable extends CrudTable{

	private String currentVersion;
	
	public DoiHistoryTable() {
		super();
		setAddButtonEnabled(true);
	}

	@Override
	public String getAddItemText() {
		return ApplicationMessages.INSTANCE.doiHistoryManagementViewAddItem();
	}

	protected void initColumns() {
		TextColumn<HasIdentifier> dateColumn = new TextColumn<HasIdentifier>() {
			@Override
			public String getValue(HasIdentifier aux) {
				HistoryItem item = (HistoryItem) aux;
				return item.getDate();
			}
		};
		
		TextColumn<HasIdentifier> versionColumn = new TextColumn<HasIdentifier>() {
			@Override
			public String getValue(HasIdentifier aux) {
				HistoryItem item = (HistoryItem) aux;
				return item.getVersion();
			}
		};

		TextColumn<HasIdentifier> commentColumn = new TextColumn<HasIdentifier>() {
			@Override
			public String getValue(HasIdentifier aux) {
				HistoryItem item = (HistoryItem) aux;
				return item.getComment();
			}
		};
		
		table.addColumn(dateColumn, ApplicationMessages.INSTANCE.date());
		table.setColumnWidth(dateColumn, 100.0, Unit.PX);
		table.addColumn(versionColumn, ApplicationMessages.INSTANCE.version());
		table.setColumnWidth(versionColumn, 50.0, Unit.PX);
		table.addColumn(commentColumn, ApplicationMessages.INSTANCE.comment());
		table.setColumnWidth(commentColumn, 200.0, Unit.PX);
		table.addColumn(editColumn);
		table.addColumn(deleteColumn);
		table.setColumnWidth(editColumn, 30.0, Unit.PX);
		table.setColumnWidth(deleteColumn, 30.0, Unit.PX);
	}

	@Override
	public String getEditDialogTitle() {
		return ApplicationMessages.INSTANCE.doiHistoryManagementViewEditItem();
	}

	@Override
	public String getCreateDialogTitle() {
		return ApplicationMessages.INSTANCE.doiHistoryManagementViewAddItem();
	}
	
	@Override
	public EditDialogContent getEditDialogContent(EditConfirmCallBack callBack, HasIdentifier hasIdentifier) {
		return new HistoryItemEditDialogContent(callBack, (HistoryItem) hasIdentifier);
	}

	@Override
	public EditDialogContent getCreateDialogContent(CreateConfirmCallBack callBack, HasIdentifier hasIdentifier) {
		return new HistoryItemEditDialogContent(callBack, (HistoryItem) hasIdentifier);
	}

	@Override
	public HasIdentifier createNewItem() {
		HistoryItem newItem = new HistoryItem();
		newItem.setDate(DateTimeFormat.getFormat("yyyy-MM-dd").format(new Date()));
		newItem.setVersion(currentVersion);
		return newItem;
	}
	
	public void setCurrentVersion(String currentVersion) {
		this.currentVersion = currentVersion;
	}
	public String getCurrentVersion() {
		return currentVersion;
	}
}
