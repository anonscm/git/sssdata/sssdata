package fr.sedoo.sssdata.client.activity;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.event.BreadCrumbChangeEvent;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.sssdata.client.ClientFactory;
import fr.sedoo.sssdata.client.event.ActionEventConstants;
import fr.sedoo.sssdata.client.event.NotificationEvent;
import fr.sedoo.sssdata.client.message.CommonMessages;
import fr.sedoo.sssdata.client.place.AdministratorManagementPlace;
import fr.sedoo.sssdata.client.service.AdministratorService;
import fr.sedoo.sssdata.client.service.AdministratorServiceAsync;
import fr.sedoo.sssdata.client.ui.component.ShortcutFactory;
import fr.sedoo.sssdata.client.ui.view.api.AdministratorManagementView;
import fr.sedoo.sssdata.shared.Administrator;

public class AdministratorManagementActivity extends SSSCrudActivity  {

    public final static AdministratorServiceAsync ADMINISTRATOR_SERVICE = GWT.create(AdministratorService.class);

	private AdministratorManagementView view;

    public AdministratorManagementActivity(AdministratorManagementPlace place, ClientFactory clientFactory) {
    	super(clientFactory, place);
    }

    /**
     * Invoked by the ActivityManager to start a new Activity
     */
    @Override
    public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
    	if (isValidUser() == false) {
			goToLoginPlace();
			return;
		}
    	
        view = ((ClientFactory) clientFactory).getAdministratorManagementView();
        containerWidget.setWidget(view.asWidget());
        view.setCrudPresenter(this);
        List<Shortcut> shortcuts = new ArrayList<Shortcut>();
        shortcuts.add(ShortcutFactory.getWelcomeShortcut());
        shortcuts.add(ShortcutFactory.getAdminShortcut());
        eventBus.fireEvent(new BreadCrumbChangeEvent(shortcuts));
        ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstants.BASIC_LOADING_EVENT, true);
    	eventBus.fireEvent(e);
        
        ADMINISTRATOR_SERVICE.findAll(new DefaultAbstractCallBack<ArrayList<Administrator>>(e, eventBus) {

			@Override
			public void onSuccess(ArrayList<Administrator> result) {
				super.onSuccess(result);
				view.setAdministrator(result);
			}
			
		});
        
    }

    /**
     * Ask user before stopping this activity
     */
    @Override
    public String mayStop() {
    	return null;
    }

	@Override
	public void delete(final HasIdentifier hasIdentifier) 
	{
		EventBus eventBus = clientFactory.getEventBus();
		ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstants.BASIC_DELETING_EVENT, true);
    	eventBus.fireEvent(e);
		ADMINISTRATOR_SERVICE.delete((Administrator) hasIdentifier, new DefaultAbstractCallBack<Void>(e, eventBus) {

			@Override
			public void onSuccess(Void result) {
				super.onSuccess(result);
				view.broadcastDeletion(hasIdentifier);
				clientFactory.getEventBus().fireEvent(new NotificationEvent(CommonMessages.INSTANCE.deletedElement()));
			}
		});
		
	}

	@Override
	public void create(HasIdentifier hasIdentifier) {
		EventBus eventBus = clientFactory.getEventBus();
		ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.saving(), ActionEventConstants.BASIC_SAVING_EVENT, true);
    	eventBus.fireEvent(e);
    	ADMINISTRATOR_SERVICE.create((Administrator) hasIdentifier, new DefaultAbstractCallBack<Administrator>(e, eventBus) {

			@Override
			public void onSuccess(Administrator savedAdministrator) {
				super.onSuccess(savedAdministrator);
				view.broadcastCreation(savedAdministrator);
				clientFactory.getEventBus().fireEvent(new NotificationEvent(CommonMessages.INSTANCE.savedElement()));
			}
		});	}

	@Override
	public void edit(HasIdentifier hasIdentifier) {
		EventBus eventBus = clientFactory.getEventBus();
		ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.saving(), ActionEventConstants.BASIC_SAVING_EVENT, true);
    	eventBus.fireEvent(e);
    	ADMINISTRATOR_SERVICE.edit((Administrator) hasIdentifier, new DefaultAbstractCallBack<Administrator>(e, eventBus) {

			@Override
			public void onSuccess(Administrator savedAdministrator) {
				super.onSuccess(savedAdministrator);
				view.broadcastEdition(savedAdministrator);
				clientFactory.getEventBus().fireEvent(new NotificationEvent(CommonMessages.INSTANCE.savedElement()));
			}
		});	
	
	}

}