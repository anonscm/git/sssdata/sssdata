package fr.sedoo.sssdata.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class HidePopUpEvent extends GwtEvent<HidePopUpHandler>{
	
	public static final Type<HidePopUpHandler> TYPE = new Type<HidePopUpHandler>();

	
	public HidePopUpEvent()
	{
		
	}
	
	 @Override
	    protected void dispatch(HidePopUpHandler handler) {
	        handler.onHidePopUp(this);
	    }

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<HidePopUpHandler> getAssociatedType() {
		return TYPE;
	}

}
