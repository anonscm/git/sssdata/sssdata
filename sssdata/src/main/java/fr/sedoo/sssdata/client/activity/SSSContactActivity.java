package fr.sedoo.sssdata.client.activity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.sedoo.commons.client.callback.LoadCallBack;
import fr.sedoo.commons.client.config.ClientPropertiesList;
import fr.sedoo.commons.client.contact.activity.ContactActivity;
import fr.sedoo.commons.client.contact.mvp.ContactClientFactory;
import fr.sedoo.commons.client.contact.place.ContactPlace;
import fr.sedoo.commons.client.event.BreadCrumbChangeEvent;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;
import fr.sedoo.sssdata.client.ui.component.ShortcutFactory;
import fr.sedoo.sssdata.shared.ConfigPropertiesCode;

public class SSSContactActivity extends ContactActivity {

	public SSSContactActivity(ContactPlace place, ContactClientFactory clientFactory) {
		super(place, clientFactory);
	}

	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		super.start(containerWidget, eventBus);
		
		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		shortcuts.add(ShortcutFactory.getWelcomeShortcut());
		shortcuts.add(ShortcutFactory.getContactShortcut());
		eventBus.fireEvent(new BreadCrumbChangeEvent(shortcuts));
	}
	
	@Override
	public void getContactEmail() {
		ClientPropertiesList.getProperties(new LoadCallBack<Map<String, String>>() {
			@Override
			public void postLoadProcess(Map<String, String> result) {
				String contact = StringUtil.trimToEmpty(result.get(ConfigPropertiesCode.CONTACT_EMAIL));
				contactView.setContactEmail(contact);
			}
		});
		
	}
	
}
