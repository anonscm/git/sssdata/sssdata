package fr.sedoo.sssdata.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface FeatureIsSelectedHandler extends EventHandler {
    void onFeatureIsSelected(FeatureIsSelectedEvent event);
}

