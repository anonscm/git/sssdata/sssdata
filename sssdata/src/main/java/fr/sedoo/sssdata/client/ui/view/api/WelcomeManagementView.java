package fr.sedoo.sssdata.client.ui.view.api;

import com.google.gwt.user.client.ui.IsWidget;

import fr.sedoo.commons.client.back.BackPresenter;

public interface WelcomeManagementView extends IsWidget {
	
	void reset();
	void setWelcomeMessage(String welcomeMessage);
	void setPresenter(Presenter presenter);
	void setBackPresenter(BackPresenter presenter);
	
	public interface Presenter 
	{
		void save(String welcomeMessage);
	}

}
