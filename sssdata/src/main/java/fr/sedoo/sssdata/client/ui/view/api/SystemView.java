package fr.sedoo.sssdata.client.ui.view.api;

import com.google.gwt.user.client.ui.IsWidget;

public interface SystemView extends IsWidget {

	void setJavaVersion(String javaVersion);
	void setApplicationVersion(String applicationVersion);
	void setTestMode(String testMode);
}
