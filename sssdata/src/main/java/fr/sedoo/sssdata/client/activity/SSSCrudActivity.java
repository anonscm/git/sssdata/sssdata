package fr.sedoo.sssdata.client.activity;

import com.google.gwt.place.shared.Place;

import fr.sedoo.commons.client.crud.activity.CrudActivity;
import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;

public abstract class SSSCrudActivity extends CrudActivity {
	public SSSCrudActivity(AuthenticatedClientFactory clientFactory,
			Place place) {
		super(clientFactory, place);
	}

	@Override
	protected boolean isValidUser() {
		return isLoggedUser();
	}
}
