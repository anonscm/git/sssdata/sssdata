package fr.sedoo.sssdata.client.service;

import java.io.IOException;
import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fr.sedoo.sssdata.shared.FileDTO;
import fr.sedoo.sssdata.shared.SearchCriterion;

@RemoteServiceRelativePath("extract")
public interface DownloadTrajectoriesService extends RemoteService{

	FileDTO downloadTrajectory(Long requestId, String trajectoryName, SearchCriterion sc, String outputFormat);
	
	String createArchive(Long requestId, List<FileDTO> fichiers, String compression) throws IOException;
}
