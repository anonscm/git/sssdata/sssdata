package fr.sedoo.sssdata.client.ui.widget;

import com.google.gwt.user.client.ui.IsWidget;

public interface PreferredWidthWidget extends IsWidget {

	double getPreferredWidth();
}
