package fr.sedoo.sssdata.client.activity;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.sedoo.commons.client.callback.LoadCallBack;
import fr.sedoo.commons.client.cms.ClientScreenList;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.event.BreadCrumbChangeEvent;
import fr.sedoo.commons.client.news.place.NewDisplayPlace;
import fr.sedoo.commons.client.news.service.MessageService;
import fr.sedoo.commons.client.news.service.MessageServiceAsync;
import fr.sedoo.commons.client.news.widget.NewsListPresenter;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.commons.client.util.LocaleUtil;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;
import fr.sedoo.commons.shared.domain.cms.TitledScreen;
import fr.sedoo.commons.shared.domain.message.TitledMessage;
import fr.sedoo.sssdata.client.ClientFactory;
import fr.sedoo.sssdata.client.event.ActionEventConstants;
import fr.sedoo.sssdata.client.message.CommonMessages;
import fr.sedoo.sssdata.client.place.WelcomePlace;
import fr.sedoo.sssdata.client.service.JournalService;
import fr.sedoo.sssdata.client.service.JournalServiceAsync;
import fr.sedoo.sssdata.client.ui.component.ShortcutFactory;
import fr.sedoo.sssdata.client.ui.component.impl.menu.ScreenNames;
import fr.sedoo.sssdata.client.ui.view.api.WelcomeView;
import fr.sedoo.sssdata.shared.DBStatusDTO;

public class WelcomeActivity extends AbstractActivity implements LoadCallBack<TitledScreen>, NewsListPresenter {//, UpdatesTablePresenter  {

	private ClientFactory clientFactory;
	private WelcomeView welcomeView;
	private EventBus eventBus;
	public final static MessageServiceAsync MESSAGE_SERVICE = GWT.create(MessageService.class);
	public final static JournalServiceAsync JOURNAL_SERVICE = GWT.create(JournalService.class);

	public WelcomeActivity(WelcomePlace place, ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
	}

	/**
	 * Invoked by the ActivityManager to start a new Activity
	 */
	@Override
	public void start(AcceptsOneWidget containerWidget, final EventBus eventBus) {
		
		welcomeView = clientFactory.getWelcomeView();
		welcomeView.setNewsPresenter(this);
		this.eventBus = eventBus;
		containerWidget.setWidget(welcomeView.asWidget());
		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		shortcuts.add(ShortcutFactory.getWelcomeShortcut());
		eventBus.fireEvent(new BreadCrumbChangeEvent(shortcuts));
		ClientScreenList.getConsultContentByName(ScreenNames.HOME, LocaleUtil.getClientLocaleLanguage(clientFactory.getDefaultLanguage()), clientFactory.getLanguages(), this);
		
		//if (welcomeView.areNewsLoaded()== false){
			final ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstants.NEWS_LOADING_EVENT, true);
			eventBus.fireEvent(e);
			welcomeView.reset();

			MESSAGE_SERVICE.getLastTitledMessage(LocaleUtil.getClientLocaleLanguage(clientFactory.getDefaultLanguage()), clientFactory.getLanguages(), new DefaultAbstractCallBack<ArrayList<TitledMessage>>(e, eventBus) {
				@Override
				public void onSuccess(final ArrayList<TitledMessage> otherNews) {
					MESSAGE_SERVICE.getCarouselNews(LocaleUtil.getClientLocaleLanguage(clientFactory.getDefaultLanguage()), clientFactory.getLanguages(), new DefaultAbstractCallBack<ArrayList<TitledMessage>>(e, eventBus) 
							{
						@Override
						public void onSuccess(ArrayList<TitledMessage> carouselNews) {
							super.onSuccess(otherNews);
							welcomeView.setNews(carouselNews, otherNews);
						}
							});
				}
			});

			// }

	}

	private void setDbStatus(){
		final ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstants.NEWS_LOADING_EVENT, true);
		eventBus.fireEvent(e);

		JOURNAL_SERVICE.getDBStatus(new DefaultAbstractCallBack<DBStatusDTO>(e, eventBus) {
			@Override
			public void onSuccess(DBStatusDTO result) {
				super.onSuccess(result);
				if (result != null){
					welcomeView.setDbStatus(result);
				}
			}
						
			public void onFailure(Throwable caught) {
				super.onFailure(caught);
				welcomeView.setDbStatus(new DBStatusDTO());
			}
		});
		
	}
	
	@Override
	public void postLoadProcess(TitledScreen resultScreen) {
		String result = resultScreen.getContent();
		welcomeView.setWelcomeMessage(StringUtil.trimToEmpty(result));
		setDbStatus();
	}

	@Override
	public void displayNew(String newsUuid) {
		clientFactory.getPlaceController().goTo(new NewDisplayPlace(newsUuid));
	}

	@Override
	public void loadPageEntries(int pageNumber) {
	}

	

}