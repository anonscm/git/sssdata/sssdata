package fr.sedoo.sssdata.client.ui.view.impl;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.crud.component.CrudViewImpl;
import fr.sedoo.commons.client.crud.widget.CrudTable;
import fr.sedoo.sssdata.client.GlobalBundle;
import fr.sedoo.sssdata.client.ui.component.AdministratorTable;
import fr.sedoo.sssdata.client.ui.view.api.AdministratorManagementView;
import fr.sedoo.sssdata.shared.Administrator;

public class AdministratorManagementViewImpl extends CrudViewImpl implements AdministratorManagementView {

	@UiField
	AdministratorTable administratorTable;
	
	private static WelcomeViewImplUiBinder uiBinder = GWT
			.create(WelcomeViewImplUiBinder.class);

	interface WelcomeViewImplUiBinder extends UiBinder<Widget, AdministratorManagementViewImpl> {
	}

	public AdministratorManagementViewImpl() {
		super();
		GWT.<GlobalBundle>create(GlobalBundle.class).css().ensureInjected();
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
		administratorTable.init(new ArrayList<Administrator>());
	}

	@Override
	public void setAdministrator(ArrayList<Administrator> result) 
	{
		administratorTable.init(result);
    }

	@Override
	public CrudTable getCrudTable() {
		return administratorTable;
	}

}
