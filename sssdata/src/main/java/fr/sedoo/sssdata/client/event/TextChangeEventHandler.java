package fr.sedoo.sssdata.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface TextChangeEventHandler extends EventHandler {
	  void onTextChange(TextChangeEvent event);
}