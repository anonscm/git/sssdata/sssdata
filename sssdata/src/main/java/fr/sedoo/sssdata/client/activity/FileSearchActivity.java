package fr.sedoo.sssdata.client.activity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.sedoo.commons.client.callback.LoadCallBack;
import fr.sedoo.commons.client.config.ClientPropertiesList;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.sssdata.client.ClientFactory;
import fr.sedoo.sssdata.client.event.ActionEventConstants;
import fr.sedoo.sssdata.client.message.CommonMessages;
import fr.sedoo.sssdata.client.misc.ClientBoatList;
import fr.sedoo.sssdata.client.place.FileSearchPlace;
import fr.sedoo.sssdata.client.service.JournalService;
import fr.sedoo.sssdata.client.service.JournalServiceAsync;
import fr.sedoo.sssdata.client.ui.component.UpdatesTablePresenter;
import fr.sedoo.sssdata.client.ui.view.api.FileSearchView;
import fr.sedoo.sssdata.shared.BoatDTO;
import fr.sedoo.sssdata.shared.ConfigPropertiesCode;
import fr.sedoo.sssdata.shared.FileSearchInformations;
import fr.sedoo.sssdata.shared.UpdateDTO;

public class FileSearchActivity extends SSSAdministrationActivity implements FileSearchView.Presenter, LoadCallBack<ArrayList<BoatDTO>>, UpdatesTablePresenter  {

	public final static JournalServiceAsync JOURNAL_SERVICE = GWT.create(JournalService.class);
		
	private FileSearchView view;
	
	public FileSearchActivity(FileSearchPlace place, AuthenticatedClientFactory clientFactory) {
		super(clientFactory, place);
	}
	
	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		super.start(panel, eventBus);

		this.view = ((ClientFactory) clientFactory).getFileSearchView();
		view.setPresenter(this);
		view.setUpdatesTablePresenter(this);
		view.reset();
		panel.setWidget(view.asWidget());
        
        ClientBoatList.getBoats(this);
	}

	/**
     * Ask user before stopping this activity
     */
    @Override
    public String mayStop() {
    	return null;
    }

    @Override
	public void search(FileSearchInformations infos) {
    	ActionStartEvent event = new ActionStartEvent(CommonMessages.INSTANCE.loading() , ActionEventConstants.BASIC_LOADING_EVENT, true);
		eventbus.fireEvent(event);
		JOURNAL_SERVICE.searchFiles(infos, new DefaultAbstractCallBack<List<UpdateDTO>>(event, eventbus) {
			@Override
			public void onSuccess(List<UpdateDTO> result) {
				super.onSuccess(result);
				if (result.isEmpty()){
					view.addErrorMessage("No file found.");
					view.resetResult();
				}else{
					view.addMessage(result.size() + " file(s) found.");
					view.setResult(result);
				}
			}
		});
		
	}
    
	@Override
	public void postLoadProcess(ArrayList<BoatDTO> result) {
		view.setBoatList((ArrayList<BoatDTO>) result);
	}

	@Override
	public void getDefaultCriterion() {
		ClientPropertiesList.getProperties(new LoadCallBack<Map<String, String>>() {
			@Override
			public void postLoadProcess(Map<String, String> result) {
				FileSearchInformations infos = new FileSearchInformations();
				infos.setBoatListSelection(new ArrayList<BoatDTO>());
				DateTimeFormat format = DateTimeFormat.getFormat("dd/MM/yyyy");
				infos.setBeginDate(format.parse(StringUtil.trimToEmpty(result.get(ConfigPropertiesCode.DATE_MIN))));
				
				if (StringUtil.isEmpty(result.get(ConfigPropertiesCode.DATE_MAX))){
					infos.setEndDate(new Date());	
				}else{
					infos.setEndDate(format.parse(result.get(ConfigPropertiesCode.DATE_MAX)));	
				}
				
				view.setSearchCriterion(infos);
			}
		});
		
	}

	@Override
	public void delete(final UpdateDTO u) {
		//view.addMessage("Delete " + toto);		
		ActionStartEvent event = new ActionStartEvent(CommonMessages.INSTANCE.loading() , ActionEventConstants.BASIC_LOADING_EVENT, true);
		eventbus.fireEvent(event);
		JOURNAL_SERVICE.deleteFile(u, new DefaultAbstractCallBack<Void>(event, eventbus) {
			@Override
			public void onSuccess(Void result) {
				super.onSuccess(result);
				view.addMessage(u.getFileName() + " successfully deleted");
				view.search();
			}
		});
	}
	
	@Override
	public void view(UpdateDTO u) {
		view.displayUpdate(u);
	}
}
