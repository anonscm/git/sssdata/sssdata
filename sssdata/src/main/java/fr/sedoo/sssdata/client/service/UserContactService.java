package fr.sedoo.sssdata.client.service;

import java.util.Set;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fr.sedoo.commons.client.util.ServiceException;
import fr.sedoo.sssdata.shared.UserContactInformations;

@RemoteServiceRelativePath("usercontact")
public interface UserContactService extends RemoteService {

	Set<String> getUsers(UserContactInformations infos) throws ServiceException;
	void sendMessage(Set<String> emails, String subject, String body, String from) throws ServiceException;
	
}
