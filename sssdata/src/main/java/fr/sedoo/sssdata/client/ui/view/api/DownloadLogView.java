package fr.sedoo.sssdata.client.ui.view.api;

import java.util.List;

import com.google.gwt.user.client.ui.IsWidget;

import fr.sedoo.sssdata.shared.DownloadDTO;

public interface DownloadLogView extends IsWidget {

	void setPresenter(Presenter presenter);
	void setDownloads(List<DownloadDTO> downloads);

	void displayDownload(DownloadDTO download);
	
	public interface Presenter {
		void searchDownload(int id);
		void deleteDownload(int id);
	}
	
}
