package fr.sedoo.sssdata.client.ui.dialog;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.sssdata.shared.UpdateDTO;

public class UpdateDialogBoxContent extends Composite {

	private static UpdateDialogBoxContentUiBinder uiBinder = GWT.create(UpdateDialogBoxContentUiBinder.class);

	interface UpdateDialogBoxContentUiBinder extends UiBinder<Widget, UpdateDialogBoxContent> {}
	
	@UiField
	Label date;
	@UiField
	Label boatName;
	@UiField
	Label fileName;
	@UiField
	Label period;
	@UiField
	VerticalPanel journal;
	
	public UpdateDialogBoxContent() {
		super();
		initWidget(uiBinder.createAndBindUi(this));
		setHeight("300px");
	}
	
	public void setData(UpdateDTO u) {
		date.setText(u.getDate());
		period.setText(u.getStartDate() + " - " + u.getEndDate());
		boatName.setText(u.getBoatName());
		fileName.setText(u.getFileName());
		journal.clear();
		for (String j: u.getJournal()){
			journal.add(new Label(j));
		}
	}
	
}
