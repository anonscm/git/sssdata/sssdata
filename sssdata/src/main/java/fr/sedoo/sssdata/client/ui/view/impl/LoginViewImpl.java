package fr.sedoo.sssdata.client.ui.view.impl;

import org.gwtbootstrap3.client.ui.Alert;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.component.AbstractView;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.sssdata.client.ui.view.api.LoginView;

public class LoginViewImpl extends AbstractView implements LoginView {

	private static LoginViewImplUiBinder uiBinder = GWT.create(LoginViewImplUiBinder.class);
	
	interface LoginViewImplUiBinder extends UiBinder<Widget, LoginViewImpl> {
	}

	Presenter presenter;
	
	@UiField TextBox login;
	@UiField PasswordTextBox password;
	@UiField Button connectButton;
		
	@UiField
	Alert errorMsg;
	@UiField
	Alert infoMsg;
	
	
	public LoginViewImpl() {
		super();
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
	}
	
	
	@UiHandler("connectButton")
	void onSignInClicked(ClickEvent event) {
		resetMessages();
		boolean isValid = true;
		if (StringUtil.isEmpty(login.getText())){
			addErrorMessage("Login is required");
			isValid = false;
		}
		if (StringUtil.isEmpty(password.getText())){
			addErrorMessage("Password is required");
			isValid = false;
		}		
		if (isValid){
			presenter.login(login.getText(), password.getText());			
		}
	}
	
	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}
	
	@Override
	public void reset() {
		resetMessages();
	}

	@Override
	public void onLoginFailed() {
		resetMessages();
		addErrorMessage(CommonMessages.INSTANCE.incorrectLoginOrPassword());
	}
	
	@Override
	public void addErrorMessage(String msg) {
		Label l = new Label(msg);
		errorMsg.add(l);
		errorMsg.setVisible(true);
	}
	@Override
	public void addMessage(String msg) {
		Label l = new Label(msg);
		infoMsg.add(l);
		infoMsg.setVisible(true);
	}
	@Override
	public void resetMessages() {
		errorMsg.clear();
		infoMsg.clear();
		infoMsg.setVisible(false);
		errorMsg.setVisible(false);
	}
		
}
