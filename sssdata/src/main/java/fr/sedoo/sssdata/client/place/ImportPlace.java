package fr.sedoo.sssdata.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class ImportPlace extends Place
{
	public static ImportPlace instance;
	
	public ImportPlace()
	{
	}

	public static class Tokenizer implements PlaceTokenizer<ImportPlace>
	{
		
		public ImportPlace getPlace(String token) {
			if (instance == null)
			{
				instance = new ImportPlace();
			}
			return instance;
		}

		public String getToken(ImportPlace place) {
			return "";
		}

		
	}
}