package fr.sedoo.sssdata.client.ui.widget;

import org.gwtbootstrap3.client.ui.TextBox;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.HasAllFocusHandlers;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.event.shared.HasHandlers;
import com.google.gwt.user.client.Event;

import fr.sedoo.sssdata.client.event.TextChangeEvent;
import fr.sedoo.sssdata.client.event.TextChangeEventHandler;

public class TextListenerTextBox extends TextBox implements HasHandlers, HasAllFocusHandlers {

	private HandlerManager handlerManager;

	public TextListenerTextBox() {
		super();

		handlerManager = new HandlerManager(this);

		// For all browsers - catch onKeyUp
		sinkEvents(Event.ONKEYUP);

		// For IE and Firefox - catch onPaste
		sinkEvents(Event.ONPASTE);
		
		sinkEvents(Event.ONFOCUS);

		// For Opera - catch onInput
		// sinkEvents(Event.ONINPUT);
	}

	@Override
	public void onBrowserEvent(Event event) {
		super.onBrowserEvent(event);

		switch (event.getTypeInt()) {
			case Event.ONKEYUP:
			case Event.ONPASTE:
			// case Event.ONINPUT:
			{
				// Scheduler needed so pasted data shows up in TextBox before we
				// fire event
				Scheduler.get().scheduleDeferred(new ScheduledCommand() {
	
					@Override
					public void execute() {
						fireEvent(new TextChangeEvent());
					}
				});
				break;
			}
			case Event.ONFOCUS:
			{Scheduler.get().scheduleDeferred(new ScheduledCommand() {
				
				@Override
				public void execute() {
					FocusEvent e = GWT.create(FocusEvent.class);
					fireEvent(e);
				}
			});
			break;
				
			}
			default:
				// Do nothing
		}
	}

	@Override
	public void fireEvent(GwtEvent<?> event) {
		handlerManager.fireEvent(event);
	}

	public HandlerRegistration addTextChangeEventHandler(
			TextChangeEventHandler handler) {
		return handlerManager.addHandler(TextChangeEvent.TYPE, handler);
	}
	
	@Override
	public HandlerRegistration addFocusHandler(
			FocusHandler handler) {
		return handlerManager.addHandler(FocusEvent.getType(), handler);
	}
	
}