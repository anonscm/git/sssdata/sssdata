package fr.sedoo.sssdata.client.mvp;

import com.google.gwt.place.shared.PlaceHistoryMapper;
import com.google.gwt.place.shared.WithTokenizers;

import fr.sedoo.commons.client.cms.place.CMSConsultPlace;
import fr.sedoo.commons.client.cms.place.CMSEditPlace;
import fr.sedoo.commons.client.cms.place.CMSListPlace;
import fr.sedoo.commons.client.config.place.ConfigPlace;
import fr.sedoo.commons.client.contact.place.ContactPlace;
import fr.sedoo.commons.client.faq.place.FaqConsultPlace;
import fr.sedoo.commons.client.faq.place.FaqEditPlace;
import fr.sedoo.commons.client.faq.place.FaqManagePlace;
import fr.sedoo.commons.client.mvp.place.LoginPlace;
import fr.sedoo.commons.client.news.place.MessageEditPlace;
import fr.sedoo.commons.client.news.place.MessageManagePlace;
import fr.sedoo.commons.client.news.place.NewDisplayPlace;
import fr.sedoo.commons.client.news.place.NewsArchivePlace;
import fr.sedoo.sssdata.client.place.AdministrationPlace;
import fr.sedoo.sssdata.client.place.AdministratorManagementPlace;
import fr.sedoo.sssdata.client.place.DoiHistoryPlace;
import fr.sedoo.sssdata.client.place.DownloadLogPlace;
import fr.sedoo.sssdata.client.place.DownloadStatsPlace;
import fr.sedoo.sssdata.client.place.FileSearchPlace;
import fr.sedoo.sssdata.client.place.GriddedProductMetadataPlace;
import fr.sedoo.sssdata.client.place.GriddedProductPlace;
import fr.sedoo.sssdata.client.place.HelpPlace;
import fr.sedoo.sssdata.client.place.ImportPlace;
import fr.sedoo.sssdata.client.place.MetadataPlace;
import fr.sedoo.sssdata.client.place.SearchPlace;
import fr.sedoo.sssdata.client.place.StatsPlace;
import fr.sedoo.sssdata.client.place.SystemPlace;
import fr.sedoo.sssdata.client.place.UpdateListPlace;
import fr.sedoo.sssdata.client.place.UserContactPlace;
import fr.sedoo.sssdata.client.place.UserInfoPlace;
import fr.sedoo.sssdata.client.place.UserStatsPlace;
import fr.sedoo.sssdata.client.place.WelcomePlace;

/**
 * PlaceHistoryMapper interface is used to attach all places which the
 * PlaceHistoryHandler should be aware of. This is done via the @WithTokenizers
 * annotation or by extending PlaceHistoryMapperWithFactory and creating a
 * separate TokenizerFactory.
 */
@WithTokenizers( { LoginPlace.Tokenizer.class, ContactPlace.Tokenizer.class, MetadataPlace.Tokenizer.class,
	UserStatsPlace.Tokenizer.class, DownloadStatsPlace.Tokenizer.class, ConfigPlace.Tokenizer.class,
	UpdateListPlace.Tokenizer.class, HelpPlace.Tokenizer.class, AdministrationPlace.Tokenizer.class, 
	FaqConsultPlace.Tokenizer.class, FaqManagePlace.Tokenizer.class, FaqEditPlace.Tokenizer.class, 
	CMSConsultPlace.Tokenizer.class, CMSEditPlace.Tokenizer.class, CMSListPlace.Tokenizer.class,
	NewsArchivePlace.Tokenizer.class, NewDisplayPlace.Tokenizer.class, 
	GriddedProductPlace.Tokenizer.class, GriddedProductMetadataPlace.Tokenizer.class,
	DownloadLogPlace.Tokenizer.class, UserContactPlace.Tokenizer.class, FileSearchPlace.Tokenizer.class,
	WelcomePlace.Tokenizer.class, SystemPlace.Tokenizer.class, DoiHistoryPlace.Tokenizer.class,
	AdministratorManagementPlace.Tokenizer.class, MessageManagePlace.Tokenizer.class, MessageEditPlace.Tokenizer.class, 
	SearchPlace.Tokenizer.class, StatsPlace.Tokenizer.class, UserInfoPlace.Tokenizer.class, ImportPlace.Tokenizer.class })
public interface AppPlaceHistoryMapper extends PlaceHistoryMapper {
}
