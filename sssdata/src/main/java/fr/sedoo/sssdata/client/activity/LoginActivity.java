package fr.sedoo.sssdata.client.activity;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceChangeEvent;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.sedoo.commons.client.event.ActionEventConstants;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.event.ActivityStartEvent;
import fr.sedoo.commons.client.event.BreadCrumbChangeEvent;
import fr.sedoo.commons.client.event.UserLoginEvent;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;
import fr.sedoo.commons.client.mvp.activity.PlaceActivity;
import fr.sedoo.commons.client.mvp.place.LoginPlace;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.commons.client.user.AuthenticatedUser;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;
import fr.sedoo.sssdata.client.ClientFactory;
import fr.sedoo.sssdata.client.place.WelcomePlace;
import fr.sedoo.sssdata.client.service.InternalUserService;
import fr.sedoo.sssdata.client.service.InternalUserServiceAsync;
import fr.sedoo.sssdata.client.ui.component.ShortcutFactory;
import fr.sedoo.sssdata.client.ui.view.api.LoginView;
import fr.sedoo.sssdata.client.ui.view.api.LoginView.Presenter;

public class LoginActivity extends AbstractActivity implements PlaceActivity, Presenter{

	private static final InternalUserServiceAsync INTERNAL_USER_SERVICE = GWT.create(InternalUserService.class);
	
	private LoginView loginView;
	private AuthenticatedClientFactory clientFactory;
	private Place destinationPlace;
	
	private Place place;
	
	public LoginActivity(LoginPlace place, AuthenticatedClientFactory clientFactory) {
		this.clientFactory = clientFactory;
		this.place = place;
		if (place.getWishedPlace() != null)	{
			destinationPlace = place.getWishedPlace();
		}else{
			destinationPlace = new WelcomePlace();
		}
	}
	
	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		
		loginView = ((ClientFactory) clientFactory).getLoginView();
		panel.setWidget(loginView.asWidget());
		eventBus.fireEvent(new ActivityStartEvent(this));

		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		shortcuts.add(ShortcutFactory.getWelcomeShortcut());
		shortcuts.add(ShortcutFactory.getLoginShortcut()); 
		eventBus.fireEvent(new BreadCrumbChangeEvent(shortcuts));
						
		loginView.setPresenter(this);
		loginView.reset();
	}

	@Override
	public void login(String login, String password) {
		final ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.connecting(), ActionEventConstants.USER_LOGIN_EVENT, true);
		clientFactory.getEventBus().fireEvent(e);
		INTERNAL_USER_SERVICE.internalLogin(login, password, new DefaultAbstractCallBack<AuthenticatedUser>(e, clientFactory.getEventBus()) 
				{
					@Override
					public void onSuccess(AuthenticatedUser user) {
						super.onSuccess(user);
						if (user == null){
							loginView.onLoginFailed();
						}else{
							//broadcastLogin(user);
							clientFactory.getEventBus().fireEvent(new UserLoginEvent(user));
							clientFactory.getEventBus().fireEvent(new PlaceChangeEvent(destinationPlace));
						}
					}
					
				});		
	}

	@Override
	public Place getPlace() {
		return place;
	}
	
	/*private void broadcastLogin(AuthenticatedUser user) 
	{
		clientFactory.getUserManager().setUser(user);
		loginView.updateSucces();
		clientFactory.getEventBus().fireEvent(new UserLoginEvent(user));
//		Logger.addLog(user, LogConstant.SESSION_CATEGORY, LogConstant.CONNEXION_ACTION, "User "+user.getName()+" has logged in.");
		Timer t = new Timer() {
			public void run() {
				loginView.reset();
				clientFactory.getEventBus().fireEvent(new PlaceChangeEvent(destinationPlace));
			}
		};
		t.schedule(2000);
	}
*/
}
