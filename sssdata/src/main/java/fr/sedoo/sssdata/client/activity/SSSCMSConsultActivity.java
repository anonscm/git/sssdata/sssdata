package fr.sedoo.sssdata.client.activity;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.sedoo.commons.client.cms.activity.CMSConsultActivity;
import fr.sedoo.commons.client.cms.place.CMSConsultPlace;
import fr.sedoo.commons.client.event.BreadCrumbChangeEvent;
import fr.sedoo.commons.client.mvp.CMSClientFactory;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;
import fr.sedoo.sssdata.client.ui.component.ShortcutFactory;
import fr.sedoo.sssdata.client.ui.component.impl.menu.ScreenNames;

public class SSSCMSConsultActivity extends CMSConsultActivity {
	
	public SSSCMSConsultActivity(CMSConsultPlace place,	CMSClientFactory clientFactory) {
		super(place, clientFactory);

		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		shortcuts.add(ShortcutFactory.getWelcomeShortcut());
		if (place.getScreenName().compareToIgnoreCase(ScreenNames.DATA_POLICY) == 0) {
			shortcuts.add(ShortcutFactory.getDataPolicyShortcut());
		}
		clientFactory.getEventBus().fireEvent(new BreadCrumbChangeEvent(shortcuts));
	}

	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		super.start(containerWidget, eventBus);

	}

	@Override
	protected boolean isEditor() {
		// TODO Auto-generated method stub
		return false;
	}

}
