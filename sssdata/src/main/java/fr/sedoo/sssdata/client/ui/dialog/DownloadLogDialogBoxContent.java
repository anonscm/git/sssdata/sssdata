package fr.sedoo.sssdata.client.ui.dialog;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.sssdata.shared.DownloadDTO;

public class DownloadLogDialogBoxContent extends Composite {

	private static DownloadLogDialogBoxContentUiBinder uiBinder = GWT.create(DownloadLogDialogBoxContentUiBinder.class);

	interface DownloadLogDialogBoxContentUiBinder extends UiBinder<Widget, DownloadLogDialogBoxContent> {
	}
	
	@UiField
	Label date;
	@UiField
	Label user;
	@UiField
	Label labo;
	@UiField
	Label goal;
	@UiField
	Label country;
	@UiField
	Label format;
	@UiField
	Label email;
	@UiField
	Label volume;
	@UiField
	VerticalPanel files;
	
	public DownloadLogDialogBoxContent() {
		super();
		initWidget(uiBinder.createAndBindUi(this));
		//setWidth("95%");
		setHeight("600px");
	}
	
	public void setData(DownloadDTO dl) {
		date.setText(dl.getDate());
		user.setText(dl.getUser());
		labo.setText(dl.getLabo());
		goal.setText(dl.getObjectives());
		country.setText(dl.getCountry());
		format.setText(dl.getFormat());
		email.setText(dl.getUserEmail());
		volume.setText(dl.getVolume());
		files.clear();
		for (String f: dl.getFiles()){
			files.add(new Label(f));
		}
	}
	
}
