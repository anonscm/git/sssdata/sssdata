package fr.sedoo.sssdata.client.ui.view.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.gwtbootstrap3.client.ui.Alert;
import org.gwtopenmaps.openlayers.client.Bounds;
import org.gwtopenmaps.openlayers.client.LonLat;
import org.gwtopenmaps.openlayers.client.Projection;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.SplitLayoutPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.widgetideas.client.ProgressBar;

import fr.sedoo.commons.client.callback.LoadCallBack;
import fr.sedoo.commons.client.component.AbstractView;
import fr.sedoo.commons.client.config.ClientPropertiesList;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.map.impl.AreaSelectorWidget;
import fr.sedoo.commons.client.widget.map.impl.MapConstants;
import fr.sedoo.commons.client.widget.map.impl.SingleAreaSelectorWidget;
import fr.sedoo.commons.client.widget.panel.HorizontalCollapsingPanel;
import fr.sedoo.sssdata.client.message.ApplicationMessages;
import fr.sedoo.sssdata.client.ui.view.api.SearchView;
import fr.sedoo.sssdata.client.ui.widget.BoatListBox;
import fr.sedoo.sssdata.client.ui.widget.date.SssDataDateBox;
import fr.sedoo.sssdata.client.util.TrajectoryHelper;
import fr.sedoo.sssdata.shared.ArchiveGeneratorRequest;
import fr.sedoo.sssdata.shared.BoatDTO;
import fr.sedoo.sssdata.shared.Boundings;
import fr.sedoo.sssdata.shared.ConfigPropertiesCode;
import fr.sedoo.sssdata.shared.SearchCriterion;
import fr.sedoo.sssdata.shared.Trajectory;

public class SearchViewImpl extends AbstractView implements SearchView {

	private static final int VISIBLE_BOAT_NUMBER = 8;

	//public static final String BASE_LAYER = AreaSelectorWidget.MAPBOX_LAYER;
	public static final String MAPBOX_API_KEY = "pk.eyJ1Ijoic3NzZGF0YSIsImEiOiJjaXF3MXV2cGswMDB4aTVubmw2cGN2eG1oIn0.4QqZ6RUKcsBZorpOCI884g";
			
	public static final double MAP_CENTER_LAT = 0;
	public static final double MAP_CENTER_LON = -150;

	private static SearchViewImplUiBinder uiBinder = GWT
			.create(SearchViewImplUiBinder.class);

	interface SearchViewImplUiBinder extends UiBinder<Widget, SearchViewImpl> {
	}

	interface SearchDriver extends SimpleBeanEditorDriver<SearchCriterion, SearchViewImpl> {}

	private SearchDriver driver = GWT.create(SearchDriver.class);

	@UiField
	HorizontalCollapsingPanel boatListPanel;

	@UiField
	HorizontalCollapsingPanel temperaturePanel;

	@UiField
	HorizontalCollapsingPanel salinityPanel;

	@UiField
	HorizontalCollapsingPanel qualityPanel;

	@UiField
	HorizontalCollapsingPanel datePanel;

	@UiField(provided=true)
	SingleAreaSelectorWidget boundingBox;

	@UiField
	BoatListBox boatListSelection;

	@UiField
	CheckBox uncontrolledDataEditor;

	@UiField
	CheckBox goodDataEditor;

	@UiField
	CheckBox probablyGoodDataEditor;

	@UiField
	CheckBox probablyBadDataEditor;

	@UiField
	CheckBox badDataEditor;

	@UiField
	CheckBox harbourDataEditor;

	@UiField
	DoubleBox temperatureMinEditor;

	@UiField
	DoubleBox temperatureMaxEditor;

	@UiField
	DoubleBox salinityMinEditor;

	@UiField
	DoubleBox salinityMaxEditor;

	@UiField(provided=true)
	SssDataDateBox beginDateEditor;

	@UiField(provided=true)
	SssDataDateBox endDateEditor;

	@UiField
	DockLayoutPanel buttonsPanel;

	@UiField
	ScrollPanel scroll;

	@UiField
	Button resetButton;

	@UiField
	Button searchButton;

	@UiField
	HTMLPanel selectedTrajectoriesButtons;

	@UiField
	Button selectAllButton;

	@UiField
	Button unselectAllButton;

	@UiField
	HTMLPanel downloadPanel;

	@UiField
	Button selectAllBoats;

	@UiField
	Button getGoodDataArchive;

	@UiField
	Button getAllDataArchive;

	@UiField(provided=true)
	SplitLayoutPanel split;

	@UiField
	ProgressBar progress;
	@UiField
	Button stopButton;

	@UiField
	Alert errorMsg;

	private TrajectoryHelper trajectoryHelper;

	private Presenter presenter;

	public SearchViewImpl() {
		super();
		split = new SplitLayoutPanel(){
			@Override
			public void onResize() {
				super.onResize();
				boundingBox.onResize();
			}
		};
		boundingBox = new SingleAreaSelectorWidget(AreaSelectorWidget.MAPBOX_STREETS_LAYER, MAPBOX_API_KEY);
		boundingBox.getAreaSelector().addMapBoxLayer(MapConstants.MAPBOX_SATELLITE_LAYER, MAPBOX_API_KEY);		
		boundingBox.getAreaSelector().enableLayerSwitcherControl();
		boundingBox.getAreaSelector().setAutoswitchToDragControl(true);

		beginDateEditor = new SssDataDateBox("yyyy-MM-dd");
		endDateEditor = new SssDataDateBox("yyyy-MM-dd");

		ClientPropertiesList.getProperty(ConfigPropertiesCode.DATE_MIN, new LoadCallBack<String>() {

			@Override
			public void postLoadProcess(String result) 
			{
				DateTimeFormat format = DateTimeFormat.getFormat("dd/MM/yyyy");
				beginDateEditor.setMinDate(format.parse(StringUtil.trimToEmpty(result)));
				endDateEditor.setMinDate(format.parse(StringUtil.trimToEmpty(result)));
			}
		});

		ClientPropertiesList.getProperty(ConfigPropertiesCode.DATE_MAX,
				new LoadCallBack<String>() {
					@Override
					public void postLoadProcess(String result) {
						if (result == null || result.isEmpty()){
							beginDateEditor.setMaxDate(new Date());
							endDateEditor.setMaxDate(new Date());
						}else{
							DateTimeFormat format = DateTimeFormat
									.getFormat("dd/MM/yyyy");
							beginDateEditor.setMaxDate(format.parse(StringUtil
									.trimToEmpty(result)));
							endDateEditor.setMaxDate(format.parse(StringUtil
									.trimToEmpty(result)));
						}
					}
				});

		initWidget(uiBinder.createAndBindUi(this));
		driver.initialize(this);
		init();
		applyCommonStyle();



		if (presenter != null){
			reset();
		}

		salinityPanel.hidePanel();
		datePanel.hidePanel();
		temperaturePanel.hidePanel();

		progress.setTextFormatter(new ProgressBar.TextFormatter() {

			@Override
			protected String getText(ProgressBar bar, double curProgress) {
				if (bar.getProgress() == bar.getMaxProgress()){
					return "Computing complete";
				}
				NumberFormat formatter = NumberFormat.getFormat("##");
				return "Computing trajectories for boat " + formatter.format(bar.getProgress()) + " / " + formatter.format(bar.getMaxProgress() - 1) + ". Please wait...";
			}
		});

	}



	public void reset() {
		presenter.getDefaultCriterion();
		resetProgressBar();
		resetErrorMessages();
		boundingBox.reset();

		//On centre la carte sur le pacifique
		Projection defaultProj = new Projection("EPSG:4326");
		LonLat centreSss = new LonLat(MAP_CENTER_LON, MAP_CENTER_LAT);
		centreSss.transform(defaultProj.getProjectionCode(), boundingBox.getMap().getProjection());
		boundingBox.getMap().setCenter(centreSss);
		boundingBox.getAreaSelector().center();

		boatListSelection.reset();
		if (trajectoryHelper != null){
			trajectoryHelper.destroyOldTrajectoryLayer(boundingBox.getAreaSelector().getMap());
		}

		hideSelectTrajectoriesButtons();
		hideDowloadButton();

		boundingBox.getAreaSelector().getMap().zoomToMaxExtent();
		boundingBox.onResize();

		boundingBox.getAreaSelector().graphicalUpdate();

		initDownloadArchiveButton();
	}

	private void init(){
		selectedTrajectoriesButtons.setVisible(false);
		downloadPanel.setVisible(false);
		boatListSelection.setVisibleItemCount(VISIBLE_BOAT_NUMBER);
	}

	private void initDownloadArchiveButton() {
		// Disable/Enable download archive button
		getAllDataArchive.setEnabled(true);
		getGoodDataArchive.setEnabled(true);
	/*	ClientPropertiesList.getProperty(
				ConfigPropertiesCode.DOWNLOAD_ARCHIVE_GOOD_ASCII,
				new LoadCallBack<String>() {

					@Override
					public void postLoadProcess(String result) {
						if (result == null) {
							getGoodDataArchive.setEnabled(false);
						}
					}
				});
		ClientPropertiesList.getProperty(
				ConfigPropertiesCode.DOWNLOAD_ARCHIVE_GOOD_NETCDF,
				new LoadCallBack<String>() {

					@Override
					public void postLoadProcess(String result) {
						if (result == null) {
							getGoodDataArchive.setEnabled(false);
						}
					}
				});
		ClientPropertiesList.getProperty(
				ConfigPropertiesCode.DOWNLOAD_ARCHIVE_ALL_ASCII,
				new LoadCallBack<String>() {

					@Override
					public void postLoadProcess(String result) {
						if (result == null) {
							getAllDataArchive.setEnabled(false);
						}
					}
				});
		ClientPropertiesList.getProperty(
				ConfigPropertiesCode.DOWNLOAD_ARCHIVE_ALL_NETCDF,
				new LoadCallBack<String>() {

					@Override
					public void postLoadProcess(String result) {
						if (result == null) {
							getAllDataArchive.setEnabled(false);
						}
					}
				});*/
	}

	@Override
	public void resetProgressBar() {
		this.progress.setVisible(false);
		this.stopButton.setVisible(false);
	}
	@Override
	public void initProgressBar(int maxProgress) {
		this.progress.setMaxProgress(maxProgress);
		this.progress.setMinProgress(0);
		this.progress.setProgress(0);
		this.progress.setVisible(true);
		this.stopButton.setVisible(true);
		this.stopButton.setEnabled(true);
	}
	@Override
	public void updateProgressBar() {
		double p = this.progress.getProgress();
		this.progress.setProgress(p+1);
		if (progress.getProgress() == progress.getMaxProgress()){
			//this.stopButton.setEnabled(false);
			resetProgressBar();
		}
	}
	@UiHandler("stopButton")
	void onStopButtonClicked(ClickEvent event){
		presenter.stop();
	}

	@Override
	public void setSearchMessage(String searchMessage) {

	}



	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;

	}

	@UiHandler("resetButton")
	void onResetButtonClicked(ClickEvent event){
		reset();

	}

	@UiHandler("searchButton")
	void onSearchButtonClicked(ClickEvent event){
		search();
	}

	@UiHandler("selectAllBoats")
	void onselectAllBoatsClicked(ClickEvent event)
	{
		this.boatListSelection.setAllItemSelected();
	}


	void edit(SearchCriterion searchCriterion)
	{
		driver.edit(searchCriterion);

	}

	private boolean validateSearchCriterion(SearchCriterion sc){
		boolean result = true;
		resetErrorMessages();
		if (sc.getBoatListSelection().isEmpty()){
			addErrorMessage("You must select at least 1 ship.");
			result = false;
		}

		if (sc.getBeginDate() == null || sc.getEndDate() == null){
			addErrorMessage("Date range is required.");
			result = false;
		}else if(sc.getBeginDate().after(sc.getEndDate())){
			addErrorMessage("Incorrect date range.");
			result = false;
		}

		if ( sc.getTemperatureMin() == null || sc.getTemperatureMax() == null){
			addErrorMessage("Temperature range is required.");
			result = false;
		}else if ( sc.getTemperatureMin().compareTo(sc.getTemperatureMax()) > 0 ){
			addErrorMessage("Temperature max must be greater than temperature min.");
			result = false;
		}

		if ( sc.getSalinityMin() == null || sc.getSalinityMax() == null ){
			addErrorMessage("Salinity range is required.");
			result = false;
		}else if ( sc.getSalinityMin().compareTo(sc.getSalinityMax()) > 0 ){
			addErrorMessage("Salinity max must be greater than salinity min.");
			result = false;
		}

		if (sc.isDataQualityEmpty()){
			addErrorMessage("You must select at least 1 data quality flag.");
			result = false;
		}

		return result;
	}

	public void search(){
		int zoomLevel = boundingBox.getAreaSelector().getMap().getZoom();
		Bounds extent = boundingBox.getAreaSelector().getMap().getExtent();
		Projection etalon = new Projection("EPSG:4326");
		LonLat lowerLeft = new LonLat(extent.getLowerLeftX(), extent.getLowerLeftY());
		LonLat upperRight = new LonLat(extent.getUpperRightX(), extent.getUpperRightY());
		lowerLeft.transform(boundingBox.getAreaSelector().getMap().getProjection(), etalon.getProjectionCode());
		upperRight.transform(boundingBox.getAreaSelector().getMap().getProjection(), etalon.getProjectionCode());

		/*System.out.println("Zoom: " + zoomLevel);
		System.out.println("lowerLeft: " + lowerLeft.lat() + ", " + lowerLeft.lon());
		System.out.println("upperRight: " + upperRight.lat() + ", " + upperRight.lon());*/


		SearchCriterion sc = driver.flush();

		if (validateSearchCriterion(sc)){
			sc.setZoomLevel(zoomLevel);
			sc.setMapExtent(new Boundings(upperRight.lat(), upperRight.lon(), lowerLeft.lat(), lowerLeft.lon()));

			presenter.search(sc);
		}
	}

	@UiHandler("selectAllButton")
	void onSelectAlltButtonClicked(ClickEvent event)
	{
		this.trajectoryHelper.selectAllFeatures();
	}

	@UiHandler("unselectAllButton")
	void onUnselectAllButtonClicked(ClickEvent event)
	{
		this.trajectoryHelper.unselectAllFeatures();
	}

	@UiHandler("downloadButton")
	void onDownloadButtonClicked(ClickEvent event)
	{
		this.presenter.downloadFromSearchCriterions(driver.flush(), this.trajectoryHelper.getAllSelectedTrajectories());
	}

	@UiHandler("getGoodDataArchive")
	void onGetGoodArchiveClicked(ClickEvent event) {
		this.presenter.downloadPretreatedArchive(ArchiveGeneratorRequest.ARCHIVE_FLAG_GOOD);
	}

	@UiHandler("getAllDataArchive")
	void onGetAllArchiveClicked(ClickEvent event) {
		this.presenter.downloadPretreatedArchive(ArchiveGeneratorRequest.ARCHIVE_FLAG_ALL);
	}

	@Override
	public void setBoatList(ArrayList<BoatDTO> boats) {
		boatListSelection.clear();
		for (BoatDTO b : boats){
			boatListSelection.addItem(b.getName());
		}	
	}


	@Override
	public void initFeatureLayerOnMap(EventBus eventBus){
		trajectoryHelper = new TrajectoryHelper(eventBus);
		trajectoryHelper.resetTrajectories(boundingBox.getAreaSelector());
	}


	@Override
	public void addFeatureOnMap(BoatDTO b, List<Trajectory> trajectoriesToPrint) {
		trajectoryHelper.addTrajectories(b, trajectoriesToPrint, boundingBox.getAreaSelector());		
	}

	public void showSelectTrajectoriesButtons(){
		this.selectedTrajectoriesButtons.setVisible(true);
	}

	public void hideSelectTrajectoriesButtons(){
		this.selectedTrajectoriesButtons.setVisible(false);
	}

	public void showDowloadButton(){
		this.downloadPanel.setVisible(true);
	}

	public void hideDowloadButton(){
		this.downloadPanel.setVisible(false);
	}

	@Override
	public void setSearchCriterion(SearchCriterion searchCriterion) {
		edit(searchCriterion);		
	}

	@Override
	public void onResize() {
		super.onResize();

	}

	@Override
	public void addErrorMessage(String msg) {
		Label l = new Label("> " + msg);
		errorMsg.add(l);
		errorMsg.setVisible(true);
	}

	@Override
	public void resetErrorMessages() {
		errorMsg.clear();
		errorMsg.setVisible(false);
	}
}
