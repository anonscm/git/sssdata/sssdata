package fr.sedoo.sssdata.client.service;

import java.util.Collection;

import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.sedoo.sssdata.shared.dataimport.FilePostImportInfo;
import fr.sedoo.sssdata.shared.dataimport.FilePreImportInfo;

public interface ImportDataServiceAsync {

	/*void importData(AsyncCallback<Void> callback);

	void getDistantBoatNames(String racine,
			AsyncCallback<ArrayList<String>> callback);

	void getBoatPreImportInfo(String racine, String boatName,
			AsyncCallback<BoatPreImportInfo> callback);

	void getFilePreImportInfo(String racine, String boatName, String fileName,
			AsyncCallback<FilePreImportInfo> callback);

	void importFile(String racine, String boatName, String fileName,
			boolean deleteFirst, AsyncCallback<FilePostImportInfo> callback);

	void getRacines(AsyncCallback<String[]> callback);*/

	void getFiles(AsyncCallback<Collection<String>> callback);

	void getFilePreImportInfo(String file,
			AsyncCallback<FilePreImportInfo> callback);

	void importFile(String file, boolean deleteFirst,
			AsyncCallback<FilePostImportInfo> callback);

	void getDirs(AsyncCallback<Collection<String>> callback);

	void getFiles(Collection<String> dirs,
			AsyncCallback<Collection<String>> callback);

	//void launchArchiveGenerator(AsyncCallback<Void> defaultAbstractCallBack);

}
