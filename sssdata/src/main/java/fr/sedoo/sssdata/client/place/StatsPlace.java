package fr.sedoo.sssdata.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class StatsPlace extends Place
{
	public static StatsPlace instance;
	private String year;
	
	public StatsPlace(String year)	
	{
		this.setYear(year);
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public static class Tokenizer implements PlaceTokenizer<StatsPlace>
	{
		
		public StatsPlace getPlace(String token) {
			if (instance == null)
			{
				instance = new StatsPlace(token);
			}
			return instance;
		}

		public String getToken(StatsPlace place) {
			return place.getYear();
		}

		
	}
}