package fr.sedoo.sssdata.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class FeatureIsSelectedEvent extends GwtEvent<FeatureIsSelectedHandler>{
	
	public static final Type<FeatureIsSelectedHandler> TYPE = new Type<FeatureIsSelectedHandler>();

	
	public FeatureIsSelectedEvent()
	{
		
	}
	
	 @Override
	    protected void dispatch(FeatureIsSelectedHandler handler) {
	        handler.onFeatureIsSelected(this);
	    }

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<FeatureIsSelectedHandler> getAssociatedType() {
		return TYPE;
	}

}
