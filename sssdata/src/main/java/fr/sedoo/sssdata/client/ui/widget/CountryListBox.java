package fr.sedoo.sssdata.client.ui.widget;

import org.gwtbootstrap3.client.ui.ListBox;

import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.i18n.client.DefaultLocalizedNames;

public class CountryListBox extends ListBox implements LeafValueEditor<String>{
	private static final String DEFAULT_COUNTRY = "FR";
	private int defaultIndex = 0;
	
	public CountryListBox() {
		//Liste des pays
		DefaultLocalizedNames cNames = new DefaultLocalizedNames();
		int i = 0;
		for(String countryCode: cNames.getSortedRegionCodes()){
			addItem(cNames.getRegionName(countryCode),countryCode);
			if (DEFAULT_COUNTRY.equals(countryCode)){
				defaultIndex = i;
			}
			i++;
		}
	}

	public void reset(){
		setSelectedIndex(defaultIndex);
	}
	
	@Override
	public void setValue(String value) {
		for (int i = 0; i < getItemCount();i++){
			if (getItemText(i).equals(value)){
				setSelectedIndex(i);
			}
		}
	}

	@Override
	public String getValue() {
		return getItemText(this.getSelectedIndex());
	}

}
