package fr.sedoo.sssdata.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.sedoo.sssdata.shared.StatsDTO;
import fr.sedoo.sssdata.shared.StatsReport;
import fr.sedoo.sssdata.shared.UserStatsDTO;

public interface StatsServiceAsync {

	void getGlobalStats(int year, AsyncCallback<StatsReport> callback);

	void getUserStats(AsyncCallback<UserStatsDTO> callback);

	void getDownloadStats(AsyncCallback<StatsDTO> callback);


}
