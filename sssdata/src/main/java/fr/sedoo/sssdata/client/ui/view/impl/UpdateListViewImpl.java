package fr.sedoo.sssdata.client.ui.view.impl;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.component.AbstractView;
import fr.sedoo.sssdata.client.ui.component.PagedUpdatesTable;
import fr.sedoo.sssdata.client.ui.component.PagedUpdatesTablePresenter;
import fr.sedoo.sssdata.client.ui.dialog.UpdateDialogBox;
import fr.sedoo.sssdata.client.ui.view.api.UpdateListView;
import fr.sedoo.sssdata.shared.UpdateDTO;

public class UpdateListViewImpl extends AbstractView  implements UpdateListView {

	interface UpdateListViewImplUiBinder extends UiBinder<Widget, UpdateListViewImpl> {}
	private static UpdateListViewImplUiBinder uiBinder = GWT.create(UpdateListViewImplUiBinder.class);
				
	@UiField
	PagedUpdatesTable table;
				
	UpdateDialogBox box;
	
	public UpdateListViewImpl() {
		super();
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
		
		box = new UpdateDialogBox();
	}
		
	@Override
	public void setUpdatesTablePresenter(PagedUpdatesTablePresenter presenter) {
		table.setPresenter(presenter);

	}

	@Override
	public void displayUpdate(UpdateDTO u) {
		box.setData(u);
		box.center();
	}
	
	@Override
	public void reset() {
		table.reset();	
	}
	
	@Override
	public int getStartIndex() {
		return table.getStartIndex();
	}

	@Override
	public void setByRange(int i, List<UpdateDTO> items) {
		table.setByRange(i, items);
	}
			
	@Override
	public void setHits(int hits) {
		table.setHits(hits);
	}
		
}
