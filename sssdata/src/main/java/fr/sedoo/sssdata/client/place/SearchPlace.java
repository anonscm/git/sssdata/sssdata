package fr.sedoo.sssdata.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class SearchPlace extends Place
{
	public static SearchPlace instance;
	
	public SearchPlace(){}

	public static class Tokenizer implements PlaceTokenizer<SearchPlace>{
		
		public SearchPlace getPlace(String token) {
			if (instance == null){
				instance = new SearchPlace();
			}
			return instance;
		}

		public String getToken(SearchPlace place) {
			return "";
		}

		
	}
}