package fr.sedoo.sssdata.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.sedoo.sssdata.shared.FileDTO;
import fr.sedoo.sssdata.shared.UserInformations;

public interface UserInfoServiceAsync {

	//void getUser(String email, AsyncCallback<User> callback);

	/*void insertDataFromUserInformation(UserInformations userinfo,
			AsyncCallback<Void> callback);*/

	void insertDownload(UserInformations userInfos, AsyncCallback<Long> callback);

	void getUserInfos(String email, AsyncCallback<UserInformations> callback);

	void updateDownload(Long id, FileDTO file, AsyncCallback<Void> callback);

	void getArchiveName(String flag, String format, AsyncCallback<String> callback);

	void updateDownloadArchive(Long id, String archiveName, AsyncCallback<Void> callback);

}
