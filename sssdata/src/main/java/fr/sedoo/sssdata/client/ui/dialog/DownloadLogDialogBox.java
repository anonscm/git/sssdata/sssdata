package fr.sedoo.sssdata.client.ui.dialog;

import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.ScrollPanel;

import fr.sedoo.sssdata.shared.DownloadDTO;

public class DownloadLogDialogBox extends DialogBox {

	private DownloadLogDialogBoxContent container;
	
	public DownloadLogDialogBox() {
		super(true);
		setGlassEnabled(true);
		
		ScrollPanel scrollPanel = new ScrollPanel();
		setWidth("800px");
		setHeight("600px");
		container = new DownloadLogDialogBoxContent();
		scrollPanel.add(container);
		setWidget(scrollPanel);
	}
	
	public void setData(DownloadDTO dl){
		container.setData(dl);
	}	
}
