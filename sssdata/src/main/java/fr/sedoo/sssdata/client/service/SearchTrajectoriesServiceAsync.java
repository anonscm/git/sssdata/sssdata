package fr.sedoo.sssdata.client.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.sedoo.sssdata.shared.BoatDTO;
import fr.sedoo.sssdata.shared.SearchCriterion;
import fr.sedoo.sssdata.shared.Trajectory;

public interface SearchTrajectoriesServiceAsync {

	/*void searchTrajectories(SearchCriterion sc,
			AsyncCallback<HashMap<Boat, List<Trajectory>>> callback);
*/
	void searchTrajectories(BoatDTO b, SearchCriterion sc,
			AsyncCallback<List<Trajectory>> callback);

}
