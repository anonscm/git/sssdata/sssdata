package fr.sedoo.sssdata.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.sssdata.shared.metadata.DoiConstants;

public class MetadataPlace extends Place implements DOIPlace {
	public static MetadataPlace instance;
	
	private String doiSuffix;
	
	
	public MetadataPlace(){
		this(null);
	}
	public MetadataPlace(String doiSuffix){
		if (StringUtil.isEmpty(doiSuffix)){
			this.doiSuffix = DoiConstants.SSS_DATA;
		}else{
			this.doiSuffix = doiSuffix;
		}
	}
	
	public String getDoiSuffix() {
		return doiSuffix;
	}
	public void setDoiSuffix(String doiSuffix) {
		this.doiSuffix = doiSuffix;
	}
	
	public static class Tokenizer implements PlaceTokenizer<MetadataPlace>{
		
		public MetadataPlace getPlace(String token) {
			if (instance == null){
				instance = new MetadataPlace(token);
			}
			return instance;
		}

		public String getToken(MetadataPlace place) {
			return place.getDoiSuffix();
		}

		
	}
}