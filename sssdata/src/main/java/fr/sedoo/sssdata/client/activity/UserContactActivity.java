package fr.sedoo.sssdata.client.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.sedoo.commons.client.callback.LoadCallBack;
import fr.sedoo.commons.client.config.ClientPropertiesList;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.event.NotificationEvent;
import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;
import fr.sedoo.commons.client.news.service.MessageService;
import fr.sedoo.commons.client.news.service.MessageServiceAsync;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.shared.domain.Message;
import fr.sedoo.commons.shared.domain.message.TitledMessage;
import fr.sedoo.sssdata.client.ClientFactory;
import fr.sedoo.sssdata.client.event.ActionEventConstants;
import fr.sedoo.sssdata.client.message.CommonMessages;
import fr.sedoo.sssdata.client.misc.ClientBoatList;
import fr.sedoo.sssdata.client.place.UserContactPlace;
import fr.sedoo.sssdata.client.service.UserContactService;
import fr.sedoo.sssdata.client.service.UserContactServiceAsync;
import fr.sedoo.sssdata.client.ui.view.api.UserContactView;
import fr.sedoo.sssdata.shared.BoatDTO;
import fr.sedoo.sssdata.shared.ConfigPropertiesCode;
import fr.sedoo.sssdata.shared.UserContactInformations;

public class UserContactActivity extends SSSAdministrationActivity implements UserContactView.Presenter, LoadCallBack<ArrayList<BoatDTO>>  {

	private final static UserContactServiceAsync USER_CONTACT_SERVICE = GWT.create(UserContactService.class);
	private static final MessageServiceAsync MESSAGE_SERVICE = GWT.create(MessageService.class);
	
	private UserContactView view;
	
	public UserContactActivity(UserContactPlace place, AuthenticatedClientFactory clientFactory) {
		super(clientFactory, place);
	}

	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		super.start(panel, eventBus);

		this.view = ((ClientFactory) clientFactory).getUserContactView();
		view.setPresenter(this);
		view.reset();
		panel.setWidget(view.asWidget());
        
        ClientBoatList.getBoats(this);
	}

	/**
     * Ask user before stopping this activity
     */
    @Override
    public String mayStop() {
    	return null;
    }
    
	@Override
	public void sendMessage(final UserContactInformations infos) {
		ActionStartEvent event = new ActionStartEvent(CommonMessages.INSTANCE.loading() , ActionEventConstants.BASIC_LOADING_EVENT, true);
		eventbus.fireEvent(event);
		USER_CONTACT_SERVICE.getUsers(infos, new DefaultAbstractCallBack<Set<String>>(event, eventbus) {
			@Override
			public void onSuccess(Set<String> result) {
				super.onSuccess(result);
				if (result.isEmpty()){
					view.addErrorMessage("No user downloaded the corresponding data.");
				}else{
					//view.addMessage(result.size() + " user(s) downloaded the corresponding data.");
					send(result, infos);
				}
			}
		});
		
	}

	private void send(final Set<String> emails, final UserContactInformations infos){
		ActionStartEvent event = new ActionStartEvent("Sending emails..." , ActionEventConstants.BASIC_LOADING_EVENT, true);
		eventbus.fireEvent(event);
		USER_CONTACT_SERVICE.sendMessage(emails, infos.getSubject(), infos.getBody(), infos.getFromEmail(), new DefaultAbstractCallBack<Void>(event, eventbus) {
			@Override
			public void onSuccess(Void result) {
				super.onSuccess(result);
				view.reset();
				view.addMessage("Update successfully sent to " + emails.size() + " user(s).");
				if (infos.isAddNews()){
					addNews(infos.getTitle(), infos.getMessage());
				}
			}
		});
	}
	
	@Override
	public void addNews(String title, String body) {
		if (getLoggedUser() != null){
			ActionStartEvent event = new ActionStartEvent("Adding a news..." , ActionEventConstants.BASIC_LOADING_EVENT, true);
			eventbus.fireEvent(event);
			String author = getLoggedUser().getName();
			TitledMessage message = new TitledMessage();
			message.setTitle(title);
			message.setContent(body);
			HashMap<String, TitledMessage> content = new HashMap<String, TitledMessage>();
			content.put(getLocale(), message);
			
			MESSAGE_SERVICE.saveMessage("", author, content, null, Message.NO_IMAGE, null, false, new DefaultAbstractCallBack<String>(event, eventbus) {
				@Override
				public void onSuccess(String uuid) {
					super.onSuccess(uuid);
					clientFactory.getEventBus().fireEvent(new NotificationEvent(CommonMessages.INSTANCE.savedElement()));
				}
			});	
		}
	}

	@Override
	public void getDefaultCriterion() {
		ClientPropertiesList.getProperties(new LoadCallBack<Map<String, String>>() {
			@Override
			public void postLoadProcess(Map<String, String> result) {
				UserContactInformations infos = new UserContactInformations();
				infos.setMessage("");
				infos.setTitle("");
				
				infos.setSubjectPrefix(StringUtil.trimToEmpty(result.get(ConfigPropertiesCode.EMAIL_PREFIX)));
				infos.setGreeting(StringUtil.trimToEmpty(result.get(ConfigPropertiesCode.EMAIL_GREETING)));
				infos.setSignature(StringUtil.trimToEmpty(result.get(ConfigPropertiesCode.EMAIL_SIGNATURE)));
				infos.setSalutation(StringUtil.trimToEmpty(result.get(ConfigPropertiesCode.EMAIL_SALUTATION)));
				infos.setFromEmail(StringUtil.trimToEmpty(result.get(ConfigPropertiesCode.CONTACT_EMAIL)));
				infos.setAddNews(false);
				
				infos.setBoatListSelection(new ArrayList<BoatDTO>());
				DateTimeFormat format = DateTimeFormat.getFormat("dd/MM/yyyy");
				infos.setBeginDate(format.parse(StringUtil.trimToEmpty(result.get(ConfigPropertiesCode.DATE_MIN))));
						infos.setEndDate(format.parse(result
								.get(ConfigPropertiesCode.DATE_MAX)));
				view.setSearchCriterion(infos);
			}
		});
		
		
	}

	@Override
	public void postLoadProcess(ArrayList<BoatDTO> result) {
		view.setBoatList((ArrayList<BoatDTO>) result);
	}
	
	

}
