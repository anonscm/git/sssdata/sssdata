package fr.sedoo.sssdata.client.ui.component;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.view.client.AbstractDataProvider;
import com.google.gwt.view.client.AsyncDataProvider;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.Range;

import fr.sedoo.sssdata.shared.UpdateDTO;

public class PagedUpdatesTable extends VerticalPanel {

	public static final int PAGE_SIZE = 25; 
	
	private UpdatesTable table;
	private SimplePager pager;
	private PagedUpdatesTablePresenter presenter;
			
	public PagedUpdatesTable() {
		super();
		setWidth("100%");
		table = new UpdatesTable();
		pager = new SimplePager();
		pager.setDisplay(table);
		reset();
		getDataProvider().addDataDisplay(table);
		add(pager);
		add(table);
	}
	
	public void reset() {
		pager.setPageSize(PAGE_SIZE);
		pager.setPageStart(0);
		table.setRowData(0, new ArrayList<UpdateDTO>());
		table.setRowCount(0);
	}
	
	public int getStartIndex(){
		return pager.getDisplay().getVisibleRange().getStart();
	}
	
	public void setByRange(int i, List<UpdateDTO> items) {
		table.setRowData(i, items);
	}
		
	public void setHits(int hits) {
		table.setRowCount(hits);
	}
	
	protected AbstractDataProvider<UpdateDTO> getDataProvider() {
		AsyncDataProvider<UpdateDTO> dataProvider = new AsyncDataProvider<UpdateDTO>() {

			@Override
			protected void onRangeChanged(HasData<UpdateDTO> display) {

				final Range range = display.getVisibleRange();
				int start = range.getStart();
				if (presenter != null) {
					presenter.loadPageEntries(start + 1);
				}
			}
		};

		return dataProvider;
	}
	
	public PagedUpdatesTablePresenter getPresenter() {
		return presenter;
	}
	
	public void setPresenter(PagedUpdatesTablePresenter presenter) {
		this.presenter = presenter;
		table.setPresenter(presenter);
	}
	
}
