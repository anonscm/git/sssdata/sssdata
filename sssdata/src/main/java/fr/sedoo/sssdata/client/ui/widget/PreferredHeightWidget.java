package fr.sedoo.sssdata.client.ui.widget;

import com.google.gwt.user.client.ui.IsWidget;

public interface PreferredHeightWidget extends IsWidget {

	double getPreferredHeight();
}
