package fr.sedoo.sssdata.client.ui.component.impl.menu;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.MenuItem;

import fr.sedoo.commons.client.cms.place.CMSEditPlace;
import fr.sedoo.commons.client.component.impl.menu.PlaceCommand;
import fr.sedoo.commons.client.config.message.ConfigMessages;
import fr.sedoo.commons.client.config.place.ConfigPlace;
import fr.sedoo.commons.client.faq.place.FaqManagePlace;
import fr.sedoo.commons.client.news.place.MessageManagePlace;
import fr.sedoo.sssdata.client.message.ApplicationMessages;
import fr.sedoo.sssdata.client.place.AdministratorManagementPlace;
import fr.sedoo.sssdata.client.place.DoiHistoryPlace;
import fr.sedoo.sssdata.client.place.DownloadLogPlace;
import fr.sedoo.sssdata.client.place.DownloadStatsPlace;
import fr.sedoo.sssdata.client.place.FileSearchPlace;
import fr.sedoo.sssdata.client.place.ImportPlace;
import fr.sedoo.sssdata.client.place.SystemPlace;
import fr.sedoo.sssdata.client.place.UserContactPlace;
import fr.sedoo.sssdata.client.place.UserStatsPlace;
import fr.sedoo.sssdata.shared.metadata.DoiConstants;

public class HorizontalAdministrationMenu extends MenuBar{


	private EventBus eventBus;
	
	public HorizontalAdministrationMenu(EventBus eventBus){
		super(true);
		this.eventBus = eventBus;
		initContent();
		
	}

	private void initContent() {
		
		setAutoOpen(true);
		
		MenuBar accessBar = new MenuBar(true);
		accessBar.addItem(ApplicationMessages.INSTANCE.downloadLogMenuTitle(), new PlaceCommand(eventBus, new DownloadLogPlace()));
		//accessBar.addItem(ApplicationMessages.INSTANCE.statsMenuTitle(), new PlaceCommand(eventBus, new StatsPlace(DateUtil.getCurrentYearAsString())));
		accessBar.addItem(ApplicationMessages.INSTANCE.statsMenuTitle(), new PlaceCommand(eventBus, new DownloadStatsPlace()));
		accessBar.addItem(ApplicationMessages.INSTANCE.userStatsMenuTitle(), new PlaceCommand(eventBus, new UserStatsPlace()));
		accessBar.addItem(ApplicationMessages.INSTANCE.userContactViewTitle(), new PlaceCommand(eventBus, new UserContactPlace()));
		
		MenuItem accessSubMenu = new MenuItem(ApplicationMessages.INSTANCE.dataDownloadsMenuTitle(), accessBar);
		addItem(accessSubMenu);
		
		MenuBar managementBar = new MenuBar(true);
		//managementBar.addItem(ApplicationMessages.INSTANCE.welcomeManagementViewTitle(), new PlaceCommand(eventBus, new WelcomeManagementPlace()));
		//managementBar.addItem(ApplicationMessages.INSTANCE.policyMenuTitle(), new PlaceCommand(eventBus, new DataPolicyManagementPlace()));
		managementBar.addItem(ApplicationMessages.INSTANCE.welcomeManagementViewTitle(), new PlaceCommand(eventBus, new CMSEditPlace(ScreenNames.HOME)));
		managementBar.addItem(ApplicationMessages.INSTANCE.policyMenuTitle(), new PlaceCommand(eventBus, new CMSEditPlace(ScreenNames.DATA_POLICY)));
		
		managementBar.addItem(ApplicationMessages.INSTANCE.gridAtlantic(), new PlaceCommand(eventBus, new CMSEditPlace(ScreenNames.GRID_ATL)));
		managementBar.addItem(ApplicationMessages.INSTANCE.gridPacific(), new PlaceCommand(eventBus, new CMSEditPlace(ScreenNames.GRID_PAC)));
		managementBar.addItem(ApplicationMessages.INSTANCE.gridNasg(), new PlaceCommand(eventBus, new CMSEditPlace(ScreenNames.GRID_NASG)));
		managementBar.addItem(ApplicationMessages.INSTANCE.gridNaspg(), new PlaceCommand(eventBus, new CMSEditPlace(ScreenNames.GRID_NASPG)));
		managementBar.addItem(ApplicationMessages.INSTANCE.gridBinAtlanticTitle(), new PlaceCommand(eventBus, new CMSEditPlace(ScreenNames.GRID_BIN_ATL)));
		managementBar.addItem(ApplicationMessages.INSTANCE.gridHist(), new PlaceCommand(eventBus, new CMSEditPlace(ScreenNames.SSS_HIST)));
		
		managementBar.addItem(ApplicationMessages.INSTANCE.messageManagementMenuTitle(), new PlaceCommand(eventBus, new MessageManagePlace()));
		managementBar.addItem(ApplicationMessages.INSTANCE.faqManagementMenuTitle(), new PlaceCommand(eventBus, new FaqManagePlace()));
		managementBar.addItem(ApplicationMessages.INSTANCE.administratorManagementMenuTitle(), new PlaceCommand(eventBus, new AdministratorManagementPlace()));
		managementBar.addItem(ConfigMessages.INSTANCE.configViewTitle(), new PlaceCommand(eventBus, new ConfigPlace()));
		
		MenuItem managementSubMenu = new MenuItem(ApplicationMessages.INSTANCE.managementMenuTitle(), managementBar);
		addItem(managementSubMenu);
		
		MenuBar importBar = new MenuBar(true);
		importBar.addItem(ApplicationMessages.INSTANCE.importViewTitle(), new PlaceCommand(eventBus, new ImportPlace()));
		importBar.addItem(ApplicationMessages.INSTANCE.fileSearchViewTitle(), new PlaceCommand(eventBus, new FileSearchPlace()));
		//importBar.addItem(ApplicationMessages.INSTANCE.updatesViewTitle(), new PlaceCommand(eventBus, new UpdateListPlace()));
		
		MenuItem importSubMenu = new MenuItem(ApplicationMessages.INSTANCE.importMenuTitle(), importBar);
		addItem(importSubMenu);
		
		
		//Sous-menu DOI
		MenuBar doiBar = new MenuBar(true);
		//doiBar.addItem(ApplicationMessages.INSTANCE.doiHistoryViewTitle(), new PlaceCommand(eventBus, new DoiHistoryPlace()));
		doiBar.addItem(DoiConstants.SSS_DATA, new PlaceCommand(eventBus, new DoiHistoryPlace(DoiConstants.SSS_DATA)));
		doiBar.addItem(DoiConstants.SSS_HIST, new PlaceCommand(eventBus, new DoiHistoryPlace(DoiConstants.SSS_HIST)));
		doiBar.addItem(DoiConstants.GRID_ATL, new PlaceCommand(eventBus, new DoiHistoryPlace(DoiConstants.GRID_ATL)));
		doiBar.addItem(DoiConstants.GRID_PAC, new PlaceCommand(eventBus, new DoiHistoryPlace(DoiConstants.GRID_PAC)));
		doiBar.addItem(DoiConstants.SSS_BIN_NASG, new PlaceCommand(eventBus, new DoiHistoryPlace(DoiConstants.SSS_BIN_NASG)));
		doiBar.addItem(DoiConstants.TSD_BINS_NASPG, new PlaceCommand(eventBus, new DoiHistoryPlace(DoiConstants.TSD_BINS_NASPG)));
		doiBar.addItem(DoiConstants.SSS_BIN_ATL, new PlaceCommand(eventBus, new DoiHistoryPlace(DoiConstants.SSS_BIN_ATL)));
		
		MenuItem doiSubMenu = new MenuItem(ApplicationMessages.INSTANCE.doiMenuTitle(), doiBar);
		addItem(doiSubMenu);
		
		/*
		//Sous-menu de gestion des écrans
		MenuBar screenManagementBar = new MenuBar(true);
		screenManagementBar.addItem(ApplicationMessages.INSTANCE.welcomeManagementViewTitle(), new PlaceCommand(eventBus, new WelcomeManagementPlace()));
		//screenManagementBar.addItem(ApplicationMessages.INSTANCE.aboutManagementViewTitle(), new PlaceCommand(eventBus, new AboutManagementPlace()));
		
		MenuItem screenManagementSubMenu = new MenuItem(ApplicationMessages.INSTANCE.screenManagement(), screenManagementBar);
		addItem(screenManagementSubMenu);
		*/
						
		addItem(ApplicationMessages.INSTANCE.systemViewTitle(), new PlaceCommand(eventBus, new SystemPlace()));
	}
	
	
}
