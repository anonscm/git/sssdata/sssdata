package fr.sedoo.sssdata.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface ShowPopUpHandler extends EventHandler {
    void onShowPopUp(ShowPopUpEvent event);
}

