package fr.sedoo.sssdata.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.sssdata.shared.metadata.DoiConstants;

public class DoiHistoryPlace extends Place implements DOIPlace {
	public static DoiHistoryPlace instance;

	private String doiSuffix;
	
	public DoiHistoryPlace(){
		this(null);
	}

	
	public DoiHistoryPlace(String doiSuffix){
		if (StringUtil.isEmpty(doiSuffix)){
			this.doiSuffix = DoiConstants.SSS_DATA;
		}else{
			this.doiSuffix = doiSuffix;
		}
	}
	
	public static class Tokenizer implements PlaceTokenizer<DoiHistoryPlace>{

		public DoiHistoryPlace getPlace(String token) {
			if (instance == null){
				instance = new DoiHistoryPlace(token);
			}
			return instance;
		}

		public String getToken(DoiHistoryPlace place) {
			return place.getDoiSuffix();
		}
		
	}

	public String getDoiSuffix() {
		return doiSuffix;
	}
	public void setDoiSuffix(String doiSuffix) {
		this.doiSuffix = doiSuffix;
	}
}
