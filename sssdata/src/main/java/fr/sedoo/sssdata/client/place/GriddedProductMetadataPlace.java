package fr.sedoo.sssdata.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

import fr.sedoo.sssdata.shared.GriddedProduct;
import fr.sedoo.sssdata.shared.metadata.DoiConstants;

public class GriddedProductMetadataPlace extends Place implements DOIPlace{
		
	public static GriddedProductMetadataPlace instance;
	
	private String grid;
		
	public GriddedProductMetadataPlace(String grid){
		this.grid = grid;
	}
	
	public String getGrid() {
		return grid;
	}
	public void setGrid(String grid) {
		this.grid = grid;
	}
	
	@Override
	public String getDoiSuffix() {
		if (GriddedProduct.GRID_ATLANTIC.equalsIgnoreCase(grid)){
			return DoiConstants.GRID_ATL;
		}
		if (GriddedProduct.GRID_PACIFIC.equalsIgnoreCase(grid)){
			return DoiConstants.GRID_PAC;
		}
		if (GriddedProduct.GRID_NASG.equalsIgnoreCase(grid)){
			return DoiConstants.SSS_BIN_NASG;
		}
		if (GriddedProduct.SSS_HIST.equalsIgnoreCase(grid)){
			return DoiConstants.SSS_HIST;
		}
		if (GriddedProduct.GRID_NASPG.equalsIgnoreCase(grid)){
			return DoiConstants.TSD_BINS_NASPG;
		}
		if (GriddedProduct.GRID_BIN_ATL.equalsIgnoreCase(grid)){
			return DoiConstants.SSS_BIN_ATL;
		}
		return null;
	}
	
	public static class Tokenizer implements PlaceTokenizer<GriddedProductMetadataPlace>
	{
		
		public GriddedProductMetadataPlace getPlace(String token) {
			if (instance == null){
				instance = new GriddedProductMetadataPlace(token);
			}
			return instance;
		}

		public String getToken(GriddedProductMetadataPlace place) {
			return place.getGrid();
		}

		
	}

	
}