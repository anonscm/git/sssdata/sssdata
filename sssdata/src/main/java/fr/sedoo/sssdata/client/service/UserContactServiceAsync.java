package fr.sedoo.sssdata.client.service;

import java.util.Set;

import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.sedoo.sssdata.shared.UserContactInformations;

public interface UserContactServiceAsync {

	void sendMessage(Set<String> emails, String subject, String body,String from,
			AsyncCallback<Void> callback);

	void getUsers(UserContactInformations infos,
			AsyncCallback<Set<String>> callback);

}
