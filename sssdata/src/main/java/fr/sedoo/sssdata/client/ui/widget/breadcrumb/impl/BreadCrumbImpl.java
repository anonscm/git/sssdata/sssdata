package fr.sedoo.sssdata.client.ui.widget.breadcrumb.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.event.PlaceNavigationEvent;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;
import fr.sedoo.sssdata.client.ClientFactory;
import fr.sedoo.sssdata.client.message.CommonMessages;
import fr.sedoo.sssdata.client.place.WelcomePlace;
import fr.sedoo.sssdata.client.ui.widget.PreferredHeightWidget;
import fr.sedoo.sssdata.client.ui.widget.breadcrumb.api.BreadCrumb;

public class BreadCrumbImpl extends Composite implements PreferredHeightWidget, BreadCrumb {
	
	 private static BreadCrumbUiBinder uiBinder = GWT.create(BreadCrumbUiBinder.class);
	 private EventBus eventBus;

     @UiField
     HorizontalPanel contentPanel;

     private ClientFactory clientFactory;
     
     List<Shortcut> currentShortcuts = new ArrayList<Shortcut>();

     interface BreadCrumbUiBinder extends UiBinder<Widget, BreadCrumbImpl> {
     }

     public BreadCrumbImpl()  {
             initWidget(uiBinder.createAndBindUi(this));
     }


     @Override
     public void setShortcuts(List<Shortcut> shortcuts)
     {
             contentPanel.clear();
             InlineLabel inlineLabel = new InlineLabel(CommonMessages.INSTANCE.breadCrumbIntroductionText());
             inlineLabel.setStyleName("breadCrumbIntroductionText");
             contentPanel.add(inlineLabel);
             Iterator<Shortcut> iterator = shortcuts.iterator();
             while (iterator.hasNext()) {
                     final Shortcut shortcut = iterator.next();
                     if (iterator.hasNext())
                     {
                             Anchor aux = new Anchor();
                             aux.setText(shortcut.getLabel());
                             aux.setStyleName("breadCrumbInnerToken");
                             aux.addClickHandler(new ClickHandler() {

                                     @Override
                                     public void onClick(ClickEvent event) {
                                    	 getEventBus().fireEvent(new PlaceNavigationEvent(shortcut.getPlace()));
                                     }
                             });
                             contentPanel.add(aux);
                             Label separator = new Label("");
                             separator.setStyleName("breadCrumbSeparator");
                             contentPanel.add(separator);
                     }
                     else
                     {
                             Label aux = new Label();
                             aux.setText(shortcut.getLabel());
                             aux.setStyleName("breadCrumbLastToken");
                             contentPanel.add(aux);
                     }

             }
             currentShortcuts = shortcuts;
     }

     public ClientFactory getClientFactory() {
             return clientFactory;
     }


     public void setClientFactory(ClientFactory clientFactory) {
             this.clientFactory = clientFactory;
     }


     @Override
     public void addShortcut(Shortcut shortcut) {
             currentShortcuts.add(shortcut);
             setShortcuts(currentShortcuts);
     }


     @Override
     public List<Shortcut> getShortcuts() 
     {
             return currentShortcuts;
     }


	@Override
	public void setEventBus(EventBus eventBus) {
		this.eventBus = eventBus;
	}
	
	public EventBus getEventBus() {
		return eventBus;
	}


	@Override
	public double getPreferredHeight() {
		return 15;
	}

	@Override
	public Place getBackPlace() {
		if (currentShortcuts.size()<2)
		{
			return new WelcomePlace();
		}
		else
		{
			Shortcut shortcut = currentShortcuts.get(currentShortcuts.size()-2);
			return shortcut.getPlace();
		}
	}

}
