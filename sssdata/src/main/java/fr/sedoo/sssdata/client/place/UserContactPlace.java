package fr.sedoo.sssdata.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class UserContactPlace extends Place {
	
	public static UserContactPlace instance;

	public UserContactPlace(){}

	public static class Tokenizer implements PlaceTokenizer<UserContactPlace>{

		public UserContactPlace getPlace(String token) {
			if (instance == null)
			{
				instance = new UserContactPlace();
			}
			return instance;
		}

		public String getToken(UserContactPlace place) {
			return "";
		}
	}
	
}
