package fr.sedoo.sssdata.client.ui.component;

import java.util.List;

import org.gwtbootstrap3.client.ui.ListBox;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

import fr.sedoo.sssdata.client.ui.view.api.DownloadLogView.Presenter;
import fr.sedoo.sssdata.shared.DownloadDTO;

public class DownloadTablePanel extends VerticalPanel {
	
	public static final int PAGE_SIZE = 25;
	
	public static final String PAGE_DISPLAY_ALL = "All";
		
	private ListBox sizeList;
	private DownloadTable table;
	private SimplePager pager;
	
	public DownloadTablePanel() {
		super();
		table = new DownloadTable();
		
		pager = new SimplePager();
		pager.setDisplay(table);
		pager.setPageSize(PAGE_SIZE);	
		pager.addStyleName("downloadPager");
		
		sizeList = new ListBox();
		sizeList.addItem("10");
		sizeList.addItem("25");
		sizeList.addItem("50");
		sizeList.addItem("100");
		
		sizeList.setSelectedIndex(1);
		sizeList.setWidth("80px");
		sizeList.setHeight("30px");
		sizeList.addStyleName("downloadPagerList");
		sizeList.setTitle("Number of rows to display");
		
		sizeList.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				String size = sizeList.getSelectedValue();
				if (PAGE_DISPLAY_ALL.equals(size)){
					pager.setPageSize(table.getSize());
				}else{
					pager.setPageSize(Integer.valueOf(size));
				}
			}
		});
		
		HorizontalPanel pagerPanel = new HorizontalPanel();
		pagerPanel.setWidth("100%");
		pagerPanel.addStyleName("downloadPagerPanel");
		pagerPanel.add(pager);
		
		pagerPanel.add(pager);
		pagerPanel.add(sizeList);
		
		add(pagerPanel);
		add(table);
	}
		
	public void setData(List<DownloadDTO> downloads) {
		table.setData(downloads);
		sizeList.addItem(PAGE_DISPLAY_ALL);
	}
	
	public void setPresenter(Presenter presenter) {
		table.setPresenter(presenter);
	}
		
}
