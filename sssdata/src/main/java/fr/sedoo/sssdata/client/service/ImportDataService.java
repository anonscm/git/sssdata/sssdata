package fr.sedoo.sssdata.client.service;

import java.util.Collection;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fr.sedoo.sssdata.shared.dataimport.FilePostImportInfo;
import fr.sedoo.sssdata.shared.dataimport.FilePreImportInfo;

@RemoteServiceRelativePath("importdata")

public interface ImportDataService extends RemoteService {
      /*  void importData();
        ArrayList<String> getDistantBoatNames(String racine) throws Exception;
        BoatPreImportInfo getBoatPreImportInfo(String racine, String boatName) throws Exception;
        FilePreImportInfo getFilePreImportInfo(String racine, String boatName, String fileName) throws Exception;
        FilePostImportInfo importFile(String racine, String boatName, String fileName, boolean deleteFirst) throws Exception;
		String[] getRacines() throws Exception;*/
		Collection<String> getFiles() throws Exception;
		FilePreImportInfo getFilePreImportInfo(String file) throws Exception;
		FilePostImportInfo importFile(String file, boolean deleteFirst)	throws Exception;
		Collection<String> getDirs() throws Exception;
		Collection<String> getFiles(Collection<String> dirs) throws Exception;

	//void launchArchiveGenerator();
}
