package fr.sedoo.sssdata.client.ui.view.api;

import com.google.gwt.editor.client.Editor;
import com.google.gwt.user.client.ui.IsWidget;

import fr.sedoo.sssdata.shared.StatsReport;

public interface StatsView extends IsWidget, Editor<StatsReport>{

	void reset();
	void display(StatsReport report);	
	void setPresenter(Presenter presenter);

	public interface Presenter 
	{
		void displayYear(String year);
		StatsReport getEmptyReport();
	}

}