package fr.sedoo.sssdata.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class FileSearchPlace extends Place {
	
	public static FileSearchPlace instance;

	public FileSearchPlace(){}

	public static class Tokenizer implements PlaceTokenizer<FileSearchPlace>{

		public FileSearchPlace getPlace(String token) {
			if (instance == null)
			{
				instance = new FileSearchPlace();
			}
			return instance;
		}

		public String getToken(FileSearchPlace place) {
			return "";
		}
	}
	

}
