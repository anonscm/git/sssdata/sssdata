package fr.sedoo.sssdata.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fr.sedoo.sssdata.shared.StatsDTO;
import fr.sedoo.sssdata.shared.StatsReport;
import fr.sedoo.sssdata.shared.UserStatsDTO;

@RemoteServiceRelativePath("stats")
public interface StatsService extends RemoteService{

	StatsReport getGlobalStats(int year) throws ServiceException;
	
	UserStatsDTO getUserStats() throws ServiceException;
	StatsDTO getDownloadStats() throws ServiceException;
	
}


