package fr.sedoo.sssdata.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface MessageModificationEventHandler extends EventHandler {
    void onNotification(MessageModificationEvent event);
}
