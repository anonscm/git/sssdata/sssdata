package fr.sedoo.sssdata.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface HidePopUpHandler extends EventHandler {
    void onHidePopUp(HidePopUpEvent event);
}

