package fr.sedoo.sssdata.client.activity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.sedoo.commons.client.event.ActionEventConstants;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.sssdata.client.ClientFactory;
import fr.sedoo.sssdata.client.event.NotificationEvent;
import fr.sedoo.sssdata.client.place.ImportPlace;
import fr.sedoo.sssdata.client.service.ArchiveGeneratorService;
import fr.sedoo.sssdata.client.service.ArchiveGeneratorServiceAsync;
import fr.sedoo.sssdata.client.service.ImportDataService;
import fr.sedoo.sssdata.client.service.ImportDataServiceAsync;
import fr.sedoo.sssdata.client.ui.view.api.ImportView;
import fr.sedoo.sssdata.shared.ArchiveGeneratorRequest;
import fr.sedoo.sssdata.shared.BoatDTO;
import fr.sedoo.sssdata.shared.FileDTO;
import fr.sedoo.sssdata.shared.dataimport.FilePostImportInfo;
import fr.sedoo.sssdata.shared.dataimport.FilePreImportInfo;

public class ImportActivity extends SSSAdministrationActivity implements ImportView.Presenter {

	public final static ImportDataServiceAsync IMPORT_DATA_SERVICE = GWT.create(ImportDataService.class);
	public final static ArchiveGeneratorServiceAsync ARCHIVE_SERVICE = GWT.create(ArchiveGeneratorService.class);
	
	//private ArrayList<String> boatList;
	//private ArrayList<String> currentFileNames;
	//private String currentBoat="";
	private ArrayList<String> fileList;
	private String currentFile="";
	//private int boatSize=0;
	private int fileSize=0;
	private boolean stop = false;
	private boolean importInProgress=false;
	private boolean paused=false;
	private ArrayList<String> report = new ArrayList<String>();

	public ImportActivity(ImportPlace place, ClientFactory clientFactory) {
		super(clientFactory, place);
	}

	private ImportView importView;
	protected boolean fileHasBeenImportedSuccesfully = false;

	/**
	 * Invoked by the ActivityManager to start a new Activity
	 */
	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		super.start(containerWidget, eventBus);
		/*if (isValidUser() == false) {
			goToLoginPlace();
			return;
		}*/
		importView = ((ClientFactory) clientFactory).getImportView();
		importView.setPresenter(this);
		containerWidget.setWidget(importView.asWidget());
		/*List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		shortcuts.add(ShortcutFactory.getWelcomeShortcut());
		shortcuts.add(ShortcutFactory.getImportShortcut());
		eventBus.fireEvent(new BreadCrumbChangeEvent(shortcuts));*/
		
		ActionStartEvent event = new ActionStartEvent("Looking for boats", ActionEventConstants.BASIC_LOADING_EVENT, true);
		clientFactory.getEventBus().fireEvent(event);
		IMPORT_DATA_SERVICE.getDirs(new DefaultAbstractCallBack<Collection<String>>(event, clientFactory.getEventBus()) {
			@Override
			public void onSuccess(Collection<String> result){
				super.onSuccess(result);
				importView.setRacines(result);
			}
			@Override
			public void onFailure(Throwable caught) {
				super.onFailure(caught);
			}
		});	
		
	}



	/**
	 * Ask user before stopping this activity
	 */
	@Override
	public String mayStop() {
		if (importInProgress)
		{
			return "A import is currently in progress. You must first stop it to quit this page. Click on \"No\".";
		}
		else
		{
			return null;
		}
	}


	@Override
	public void launchImport() {
		currentFile="";
		stop=false;
		importInProgress=true;
		report = new ArrayList<String>();
		ActionStartEvent event = new ActionStartEvent("Looking for boats", ActionEventConstants.BASIC_LOADING_EVENT, true);
		clientFactory.getEventBus().fireEvent(event);
		IMPORT_DATA_SERVICE.getFiles(importView.getSelectedDirs(),
				new DefaultAbstractCallBack<Collection<String>>(event,
						clientFactory.getEventBus()) {
					@Override
					public void onSuccess(Collection<String> result) {
						super.onSuccess(result);
						fileList = new ArrayList<String>(result);
						fileSize = result.size();
						importView.logNormal(fileSize + " files found");
						if (result.size() > 0) {
							importNextFile();
						} else {
							importView.logKO("No file found");
							writeReport();
						}
					}

					@Override
					public void onFailure(Throwable caught) {
						super.onFailure(caught);
						caught.printStackTrace();
						importView
								.logKO("Unable to get files - End of process");
						writeReport();
					}
				});
	}


	/*public void launchImportOld() {
		boatList= null;
		currentFileNames=null;
		currentBoat="";
		currentFile="";
		stop=false;
		importInProgress=true;
		report = new ArrayList<String>();
		String racine = "";
		ActionStartEvent event = new ActionStartEvent("Looking for boats", ActionEventConstants.BASIC_LOADING_EVENT, true);
		clientFactory.getEventBus().fireEvent(event);
		IMPORT_DATA_SERVICE.getDistantBoatNames(racine, new DefaultAbstractCallBack<ArrayList<String>>(event, clientFactory.getEventBus()) {
			@Override
			public void onSuccess(ArrayList<String> result){
				super.onSuccess(result);
				boatList = result;
				boatSize = result.size();
				importView.logNormal(boatSize +" boats found");
				if (result.size()>0)
				{
					importNextBoat();
				}
				else
				{
					importView.logKO("No boat found");
					writeReport();
				}
			}
		});	
	}*/

	/*
	public void importNextBoat()
	{
		if ((boatList.size() == 0) || (stop))
		{
			importView.logOK("End of import");
			writeReport();
		}
		else
		{
			currentBoat = boatList.remove(0);
			int restingBoat = boatSize-boatList.size();
			importView.logNormal("Analysing datas for boat : "+currentBoat +" (boat "+restingBoat+" out of "+boatSize+")");
			ActionStartEvent event = new ActionStartEvent("Analysing data for boat "+currentBoat, ActionEventConstants.BASIC_LOADING_EVENT, true);
			clientFactory.getEventBus().fireEvent(event);
			IMPORT_DATA_SERVICE.getBoatPreImportInfo(currentBoat, new DefaultAbstractCallBack<BoatPreImportInfo>(event, clientFactory.getEventBus()) {;

			@Override
			public void onSuccess(BoatPreImportInfo result){
				super.onSuccess(result);
				if (result.isAlreadyPresent())
				{
					importView.logNormal("The boat is already present in the database");	
				}
				else
				{
					importView.logOK("The boat has been added in the database");
				}
				currentFileNames = result.getFileNames();
				fileSize = currentFileNames.size();
				importNextFile();
			}
			});
		}
	}
	 */
	private void writeReport() {
		importInProgress = false;
		importView.logInfo("------------------------------------------");
		importView.logInfo(report.size()+" file(s) added or updated");
		Iterator<String> iterator = report.iterator();
		while (iterator.hasNext()) {
			String reportLine = (String) iterator.next();
			importView.logInfo(reportLine);
		}
		importView.logInfo("------------------------------------------");
		importView.broadcastImportEnd();
	}

	public void importNextFile(){
		if (paused == true){
			clientFactory.getEventBus().fireEvent(new NotificationEvent("The process is now paused."));
			importView.logInfo("Paused");
			return;
		}
		if ((fileList.size() == 0) || (stop)){
			importView.logOK("End of import");
			writeReport();
			if (fileHasBeenImportedSuccesfully) {
				importView.enableArchiveGeneration(true);
			}
		}else{
			currentFile = fileList.remove(0);
			int fileNum = fileSize - fileList.size();
			importView.logNormal("Reading file " + currentFile +" (file " + fileNum + " out of " + fileSize + ")");
			ActionStartEvent event = new ActionStartEvent("Reading file " + currentFile, ActionEventConstants.BASIC_LOADING_EVENT, true);
			clientFactory.getEventBus().fireEvent(event);
			IMPORT_DATA_SERVICE.getFilePreImportInfo(currentFile, new DefaultAbstractCallBack<FilePreImportInfo>(event, clientFactory.getEventBus()) {;
				@Override
				public void onSuccess(FilePreImportInfo result){
					super.onSuccess(result);
					if (result.isAlreadyPresent()){
						importView.logNormal("The file is already present in the database");
						if (result.isNeedUpdate() == false){
							importView.logNormal("The file is already up to date in the database");
							importNextFile();
						}else{
							importView.logNormal("The file needs to be updated");
							importCurrentFile(true);
						}
					}else{
						importView.logOK("The file needs to be added in the database");
						importCurrentFile(false);
					}
				}			
			});
		}
	}

	
	private ArrayList<ArchiveGeneratorRequest> archivesRequestsList;
	private ArchiveGeneratorRequest currentRequest;
	private ArrayList<BoatDTO> currentRequestBoatList;
	private BoatDTO currentBoat;
	List<FileDTO> temporaryFiles = null;
	
	private void createArchiveFilesNextBoat(){
		if ((currentRequestBoatList.size() == 0)){
			createArchive();
		}else{
			
			currentBoat = currentRequestBoatList.remove(0);
			importView.logNormal("Creating files for ship " + currentBoat.getName() + "...");
			
			ActionStartEvent event = new ActionStartEvent("Processing ship " + currentBoat.getName() + "...", ActionEventConstants.BASIC_LOADING_EVENT, true);
			clientFactory.getEventBus().fireEvent(event);
			ARCHIVE_SERVICE.getArchiveFilesByBoat(currentRequest, currentBoat , new DefaultAbstractCallBack<List<FileDTO>>(event, clientFactory.getEventBus()) {
				@Override
				public void onSuccess(List<FileDTO> result) {
					super.onSuccess(result);
					importView.logOK(result.size() + " files were successfully created.");
					temporaryFiles.addAll(result);
					createArchiveFilesNextBoat();
				}
			});
		}
	}
	
	private void createArchive(){
		ActionStartEvent event = new ActionStartEvent("Reading file " + currentFile, ActionEventConstants.BASIC_LOADING_EVENT, true);
		clientFactory.getEventBus().fireEvent(event);
		ARCHIVE_SERVICE.createArchive(currentRequest, temporaryFiles, new DefaultAbstractCallBack<String>(event, clientFactory.getEventBus()) {
			@Override
			public void onSuccess(String message) {
				super.onSuccess(message);
				importView.logOK(message);
				createNextArchive();
			}
		});
	}
	
	private void createNextArchive(){
		if ((archivesRequestsList.size() == 0)){
			importView.logOK("End of archive generation");
			importView.enableArchiveGeneration(true);
			importView.enableImport(true);
			importInProgress=false;
		}else{
			currentRequest = archivesRequestsList.remove(0);
			currentRequestBoatList = new ArrayList<BoatDTO>(currentRequest.getSearchCriterion().getBoatListSelection());
			//.subList(0, 2)
			importView.logNormal("Creating archive " + currentRequest.getRequestId() + "...");
			temporaryFiles = new ArrayList<FileDTO>();
			createArchiveFilesNextBoat();
		}
	}
	
	public void launchArchiveGeneration() {
		stop=false;
		importInProgress=true;
		ActionStartEvent event = new ActionStartEvent("Generating archive... ",
				ActionEventConstants.BASIC_LOADING_EVENT, true);
		clientFactory.getEventBus().fireEvent(event);
		ARCHIVE_SERVICE.initRequests(new DefaultAbstractCallBack<List<ArchiveGeneratorRequest>>(event, clientFactory.getEventBus()) {
			@Override
			public void onSuccess(List<ArchiveGeneratorRequest> result) {
				super.onSuccess(result);
				importView.logNormal(result.size() + " archives to create.");
				importView.enableArchiveGeneration(false);
				archivesRequestsList = new ArrayList<ArchiveGeneratorRequest>(result);
				if (result.size() > 0) {
					createNextArchive();
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				super.onFailure(caught);
				fileHasBeenImportedSuccesfully = false;
				importView.enableArchiveGeneration(false);
				importView.logKO("Archive generation failed");
			}
			
		});
	}
	/*
	public void launchArchiveGeneration() {
		ActionStartEvent event = new ActionStartEvent("Generating archive... ",
				ActionEventConstants.BASIC_LOADING_EVENT, true);
		clientFactory.getEventBus().fireEvent(event);
		importView.logNormal("Generating archive... ");
		IMPORT_DATA_SERVICE
				.launchArchiveGenerator(new DefaultAbstractCallBack<Void>(
						event, clientFactory.getEventBus()) {
					@Override
					public void onSuccess(Void result) {
						super.onSuccess(result);
						fileHasBeenImportedSuccesfully = false;
						importView.enableArchiveGeneration(false);
						importView.logOK("End of archive generation");
					}

					@Override
					public void onFailure(Throwable caught) {
						super.onFailure(caught);
						fileHasBeenImportedSuccesfully = false;
						importView.enableArchiveGeneration(false);
						importView.logKO("Archive generation failed");
					}
				});
	}
	*/
		

	/*public void importNextFile()
	{
		if (paused == true)
		{
			clientFactory.getEventBus().fireEvent(new NotificationEvent("The process is now paused."));
			importView.logInfo("Paused");
			return;
		}
		if ((currentFileNames.size() == 0) || (stop))
		{
			importNextBoat();
		}
		else
		{
			currentFile = currentFileNames.remove(0);
			int restingFiles = fileSize-currentFileNames.size();
			String progressMessage = "Processing file "+currentFile+" (file "+restingFiles+" out of "+fileSize+")";
			importView.logNormal(progressMessage);

			ActionStartEvent event = new ActionStartEvent(progressMessage, ActionEventConstants.BASIC_LOADING_EVENT, true);
			clientFactory.getEventBus().fireEvent(event);
			IMPORT_DATA_SERVICE.getFilePreImportInfo(currentBoat, currentFile, new DefaultAbstractCallBack<FilePreImportInfo>(event, clientFactory.getEventBus()) {;

			@Override
			public void onSuccess(FilePreImportInfo result){
				super.onSuccess(result);
				if (result.isAlreadyPresent())
				{
					importView.logNormal("The file is already present in the database");
					if (result.isNeedUpdate() == false)
					{
						importView.logNormal("The file is already up to date in the database");
						importNextFile();
					}
					else
					{
						importView.logNormal("The file needs to be updated");
						importCurrentFile(true);
					}
				}
				else
				{
					importView.logOK("The file needs to be added in the database");
					importCurrentFile(false);
				}

			}


			});
		}
	}*/

	private void importCurrentFile(boolean deleteFirst) {
		ActionStartEvent event = new ActionStartEvent("Importing file " + currentFile, ActionEventConstants.BASIC_LOADING_EVENT, true);
		clientFactory.getEventBus().fireEvent(event);
		IMPORT_DATA_SERVICE.importFile(currentFile, deleteFirst, new DefaultAbstractCallBack<FilePostImportInfo>(event, clientFactory.getEventBus()) {

			@Override
			public void onSuccess(FilePostImportInfo result){
				super.onSuccess(result);
				if (result.isSuccess() == true){
					/*if (result.isAdjustedSspsMissing()){
						importView.logInfo("WARNING: Pas de correction appliquée sur la SSS");
					}*/
					for (String msg: result.getMessages()){
						importView.logInfo(msg);
					}
					importView.logOK("Import successfull ("+result.getLines()+" lines - duration : "+result.getDuration()+")");
					report.add(currentFile+" : "+result.getLines()+" lines - duration : "+result.getDuration());
							fileHasBeenImportedSuccesfully = true;
					importNextFile();
				}else{
					importView.logKO("Import failed - End of process");
					writeReport();
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				super.onFailure(caught);
				caught.printStackTrace();
				importView.logKO("Import failed - End of process");
				writeReport();
			}
		});

	}


	@Override
	public void stop() {
		stop=true;
		importInProgress=false;
		clientFactory.getEventBus().fireEvent(new NotificationEvent("The process will be stopped after the end of the current file import."));
	}



	@Override
	public void pause() {
		paused = true;
		clientFactory.getEventBus().fireEvent(new NotificationEvent("The process will be paused after the end of the current file import."));
	}



	@Override
	public void resume() {
		paused=false;
		importView.logInfo("Resumed");
		importNextFile();
	}

}