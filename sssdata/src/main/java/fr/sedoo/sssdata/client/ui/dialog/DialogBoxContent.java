package fr.sedoo.sssdata.client.ui.dialog;

import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.IsWidget;

public interface DialogBoxContent extends IsWidget{
	
	void setDialogBox(DialogBox dialog);

}
