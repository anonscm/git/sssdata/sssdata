package fr.sedoo.sssdata.client.activity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.sedoo.commons.client.callback.LoadCallBack;
import fr.sedoo.commons.client.config.ClientPropertiesList;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.event.BackEvent;
import fr.sedoo.commons.client.event.BreadCrumbChangeEvent;
import fr.sedoo.commons.client.event.PlaceNavigationEvent;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;
import fr.sedoo.sssdata.client.ClientFactory;
import fr.sedoo.sssdata.client.event.ActionEventConstants;
import fr.sedoo.sssdata.client.event.NotificationEvent;
import fr.sedoo.sssdata.client.message.CommonMessages;
import fr.sedoo.sssdata.client.place.SearchPlace;
import fr.sedoo.sssdata.client.place.UserInfoPlace;
import fr.sedoo.sssdata.client.service.DownloadTrajectoriesService;
import fr.sedoo.sssdata.client.service.DownloadTrajectoriesServiceAsync;
import fr.sedoo.sssdata.client.service.UserInfoService;
import fr.sedoo.sssdata.client.service.UserInfoServiceAsync;
import fr.sedoo.sssdata.client.ui.component.ShortcutFactory;
import fr.sedoo.sssdata.client.ui.view.api.UserInfoView;
import fr.sedoo.sssdata.shared.ArchiveGeneratorRequest;
import fr.sedoo.sssdata.shared.ConfigPropertiesCode;
import fr.sedoo.sssdata.shared.FileDTO;
import fr.sedoo.sssdata.shared.GriddedProduct;
import fr.sedoo.sssdata.shared.SearchCriterion;
import fr.sedoo.sssdata.shared.UserInformations;

public class UserInfoActivity extends AbstractActivity implements UserInfoView.Presenter {

	private fr.sedoo.sssdata.client.ClientFactory clientFactory;

	private boolean isForPreteatedArchive;
	private String archiveType;

	public final static UserInfoServiceAsync USER_INFO_SERVICE = GWT.create(UserInfoService.class);
	public final static DownloadTrajectoriesServiceAsync DOWNLOAD_TRAJECTORIES_SERVICE = GWT.create(DownloadTrajectoriesService.class);

	public UserInfoActivity(UserInfoPlace place, ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
		this.eventbus = clientFactory.getEventBus();

		// TODO => VERIFY IF IT'S OK
		this.searchCriterion = place.getSearchCriterion();
		this.allSelectedTrajectories = place.getAllSelectedTrajectories();
		this.archiveType = place.getPretreatedArchiveType();
		if (this.archiveType == null) {
			this.isForPreteatedArchive = false;
		} else {
			this.isForPreteatedArchive = (
					this.archiveType.equals(ArchiveGeneratorRequest.ARCHIVE_FLAG_ALL)
					|| this.archiveType.equals(ArchiveGeneratorRequest.ARCHIVE_FLAG_GOOD)
					|| this.archiveType.equals(GriddedProduct.GRID_ATLANTIC)
					|| this.archiveType.equals(GriddedProduct.GRID_NASG)
					|| this.archiveType.equals(GriddedProduct.SSS_HIST)
					|| this.archiveType.equals(GriddedProduct.GRID_NASPG)
					|| this.archiveType.equals(GriddedProduct.GRID_BIN_ATL)
					|| this.archiveType.equals(GriddedProduct.GRID_PACIFIC) ) ? true : false;
		}
	}

	private UserInfoView userInfoView;
	private EventBus eventbus;

	private UserInformations userInfoCriterion;
	private SearchCriterion searchCriterion;
	private List<String> allSelectedTrajectories;
	private List<FileDTO> generatedFiles;
	boolean stopDownload = false;
	private boolean downloadInProgress=false;


	private Long requestId;

	int cpt = 0;

	/**
	 * Invoked by the ActivityManager to start a new Activity
	 */
	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		if (noSearchBefore()){
			eventBus.fireEvent(new PlaceNavigationEvent(new SearchPlace()));
			return;
		}

		userInfoView = clientFactory.getUserInfoView();
		userInfoView.setPresenter(this);
		userInfoView.reset();
		containerWidget.setWidget(userInfoView.asWidget());
		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		shortcuts.add(ShortcutFactory.getWelcomeShortcut());
		shortcuts.add(ShortcutFactory.getDataAccessShortcut());
		shortcuts.add(ShortcutFactory.getUserInfoShortcut());
		this.eventbus = eventBus;
		eventbus.fireEvent(new BreadCrumbChangeEvent(shortcuts));
		downloadInProgress = false;
		userInfoView.downloadArchiveMode(isForPreteatedArchive);
	}

	private boolean noSearchBefore() {
		return (searchCriterion == null || allSelectedTrajectories == null)
				&& !isForPreteatedArchive;
	}

	/**
	 * Ask user before stopping this activity
	 */
	@Override
	public String mayStop() {
		if (downloadInProgress){
			return "A processing is currently in progress. Are you sure you want to stop it and quit this page ?";
		}else{
			return null;
		}
	}

	@Override
	public void stopDownload() {
		this.stopDownload = true;
		clientFactory.getEventBus().fireEvent(new NotificationEvent("The download will be stopped after the current file."));
	}

	@Override
	public void download(final UserInformations userinfo) {
		this.userInfoCriterion = userinfo;
		this.stopDownload = false;		
		this.generatedFiles = new ArrayList<FileDTO>();

		System.out.println("Download by " + userinfo.getUserEmail());
		if (!isForPreteatedArchive) {
			System.out.println("Tractories: " + allSelectedTrajectories.size());
			userInfoView.initProgressBar(allSelectedTrajectories.size());
			cpt = 0;
			downloadInProgress = true;
		} else {
			userinfo.setDataCompression(FileDTO.COMPRESSION_ZIP);
		}
		saveRequest();
	}

	
	private void updateArchiveDownload(final String archiveName){
		ActionStartEvent event = new ActionStartEvent("Updating request", ActionEventConstants.BASIC_LOADING_EVENT, true);
		eventbus.fireEvent(event);
		USER_INFO_SERVICE.updateDownloadArchive(requestId, archiveName, new DefaultAbstractCallBack<Void>(event,eventbus) {
			@Override
			public void onSuccess(Void result) {
				super.onSuccess(result);
				userInfoView.downloadArchive(FileDTO.ARCHIVE_FOLDER	+ "/" + archiveName);
				userInfoView.downloadArchiveMode(isForPreteatedArchive);
			}
			@Override
			public void onFailure(Throwable caught) {
				super.onFailure(caught);
			}
		});
				
	}
	private void getArchive(String format){
		ActionStartEvent event = new ActionStartEvent("Loading archive..." , ActionEventConstants.BASIC_LOADING_EVENT, true);
		eventbus.fireEvent(event);

		USER_INFO_SERVICE.getArchiveName(archiveType, format, new DefaultAbstractCallBack<String>(event, eventbus) {
			@Override
			public void onSuccess(String result) {
				super.onSuccess(result);
				//Update download table
				updateArchiveDownload(result);
			}
			@Override
			public void onFailure(Throwable caught) {
				super.onFailure(caught);
			}

		});
	}

	private void saveRequest(){
		ActionStartEvent event = new ActionStartEvent("Initializing request..." , ActionEventConstants.BASIC_LOADING_EVENT, true);
		eventbus.fireEvent(event);
		//Insérer la requete dans la base
		USER_INFO_SERVICE.insertDownload(userInfoCriterion,
				new DefaultAbstractCallBack<Long>(event, eventbus) {
			@Override
			public void onSuccess(Long result) {
				super.onSuccess(result);
				System.out.println("Save request with id " + result);
				requestId = result;

				if (isForPreteatedArchive) {
					getArchive(userInfoCriterion.getDataFormat());
				} else {
					downloadNextTrajectory();
				}
			}
		});
	}

	private void updateRequest(final FileDTO file) {
		//Ajoute le dernier fichier traité fichier à la requete
		ActionStartEvent event = new ActionStartEvent("Updating request", ActionEventConstants.BASIC_LOADING_EVENT, true);
		eventbus.fireEvent(event);
		USER_INFO_SERVICE.updateDownload(requestId, file, new DefaultAbstractCallBack<Void>(event,eventbus) {
			@Override
			public void onSuccess(Void result) {
				super.onSuccess(result);
				System.out.println("File added to request " + requestId + ": " + file.getSssFileName());
				downloadNextTrajectory();
			}
		});
	}

	private void createArchive() {
		ActionStartEvent event = new ActionStartEvent("Creating " + userInfoCriterion.getDataCompression() + " archive." , ActionEventConstants.BASIC_LOADING_EVENT, true);
		eventbus.fireEvent(event);
		DOWNLOAD_TRAJECTORIES_SERVICE.createArchive(requestId, generatedFiles, userInfoCriterion.getDataCompression(), new DefaultAbstractCallBack<String>(event,eventbus) {

			@Override
			public void onSuccess(String result) {
				super.onSuccess(result);
				userInfoView.updateProgressBar();
				System.out.println("Archive: " + result);
				userInfoView.downloadArchive(result);
				downloadInProgress = false;
			}

		});		

	}

	private void downloadNextTrajectory() {
		if (stopDownload || cpt >= allSelectedTrajectories.size()){
			//userInfoView.updateProgressBar();
			/*if (stopDownload){
				userInfoView.resetProgressBar();
			}*/
			//TODO générer une archive ?
			createArchive();
			//TODO téléchargement du résultat

		}else{
			String trajectoryName =  allSelectedTrajectories.get(cpt++);

			ActionStartEvent event = new ActionStartEvent("Processing trajectory " + trajectoryName, ActionEventConstants.BASIC_LOADING_EVENT, true);
			eventbus.fireEvent(event);
			DOWNLOAD_TRAJECTORIES_SERVICE.downloadTrajectory(requestId, trajectoryName, searchCriterion, userInfoCriterion.getDataFormat(), new DefaultAbstractCallBack<FileDTO>(event,eventbus) {

				@Override
				public void onSuccess(FileDTO result) {
					super.onSuccess(result);
					try{
						System.out.println("File: " + result.getGeneratedFileName());
						generatedFiles.add(result);
						userInfoView.updateProgressBar();
						updateRequest(result);
					}catch(Exception e){
						System.out.println("Erreur: " + e);
					}
				}
			});
		}
	}

	public SearchCriterion getSearchCriterion() {
		return searchCriterion;
	}

	public void setSearchCriterion(SearchCriterion searchCriterion) {
		this.searchCriterion = searchCriterion;
	}

	public Collection<String> getAllSelectedTrajectories() {
		return allSelectedTrajectories;
	}

	public void setAllSelectedTrajectories(List<String> allSelectedTrajectories) {
		this.allSelectedTrajectories = allSelectedTrajectories;
	}

	@Override
	public void getUserFromEmail(String email) {
		ActionStartEvent event = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstants.BASIC_LOADING_EVENT, true);
		eventbus.fireEvent(event);
		USER_INFO_SERVICE.getUserInfos(email, new DefaultAbstractCallBack<UserInformations>(event,eventbus) {

			@Override
			public void onSuccess(UserInformations result) {
				super.onSuccess(result);
				if (result != null){
					userInfoView.setKnownUserInfo(result);
					//TODO message ?
				}else{
					userInfoView.addMessage("Unknown user in database. Please fill in the form.");
				}
			}

		});
	}

	@Override
	public void back() {
		clientFactory.getEventBus().fireEvent(new BackEvent());
	}

	@Override
	public void getInstructions() {
		ClientPropertiesList.getProperties(new LoadCallBack<Map<String, String>>() {
			@Override
			public void postLoadProcess(Map<String, String> result) {
				String intro1 = StringUtil.trimToEmpty(result.get(ConfigPropertiesCode.DOWNLOAD_INTRO));
				String intro2 = StringUtil.trimToEmpty(result.get(ConfigPropertiesCode.DOWNLOAD_INTRO2));
				String policy = StringUtil.trimToEmpty(result.get(ConfigPropertiesCode.DOWNLOAD_POLICY));
				userInfoView.setInstructions(intro1, intro2, policy);
			}
		});
	}


}