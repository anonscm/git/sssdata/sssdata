package fr.sedoo.sssdata.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

import fr.sedoo.sssdata.client.message.ApplicationMessages;
import fr.sedoo.sssdata.shared.GriddedProduct;

public class GriddedProductPlace extends Place {
			
	public static GriddedProductPlace instance;
	
	private String grid;
		
	public GriddedProductPlace(String grid){
		this.grid = grid;
	}

	public String getGrid() {
		return grid;
	}
	public void setGrid(String grid) {
		this.grid = grid;
	}
		
	public String getTitle() {
		if (GriddedProduct.GRID_ATLANTIC.equalsIgnoreCase(grid)){
			return ApplicationMessages.INSTANCE.gridAtlanticTitle();
		}
		if (GriddedProduct.GRID_PACIFIC.equalsIgnoreCase(grid)){
			return ApplicationMessages.INSTANCE.gridPacificTitle();
		}
		if (GriddedProduct.GRID_NASG.equalsIgnoreCase(grid)){
			return ApplicationMessages.INSTANCE.gridNasgTitle();
		}
		if (GriddedProduct.SSS_HIST.equalsIgnoreCase(grid)){
			return ApplicationMessages.INSTANCE.gridHistTitle();
		}
		if (GriddedProduct.GRID_NASPG.equalsIgnoreCase(grid)){
			return ApplicationMessages.INSTANCE.gridNaspgTitle();
		}
		if (GriddedProduct.GRID_BIN_ATL.equalsIgnoreCase(grid)){
			return ApplicationMessages.INSTANCE.gridBinAtlanticTitle();
		}
		return null;
	}
	
	public static class Tokenizer implements PlaceTokenizer<GriddedProductPlace>
	{
		
		public GriddedProductPlace getPlace(String token) {
			if (instance == null){
				instance = new GriddedProductPlace(token);
			}
			return instance;
		}

		public String getToken(GriddedProductPlace place) {
			return place.getGrid();
		}

		
	}

	
}