package fr.sedoo.sssdata.client.ui.view.api;

import java.util.Collection;

import com.google.gwt.user.client.ui.IsWidget;

public interface ImportView extends IsWidget{
	
	void setPresenter(Presenter presenter);
	
	public interface Presenter 
	{
		void launchImport();

		void stop();
		void pause();
		void resume();

		void launchArchiveGeneration();
	}

	void setRacines(Collection<String> racines);
	
	void logNormal(String string);
	void logOK(String string);
	void logKO(String string);
	void logInfo(String string);
	void broadcastImportEnd();

	Collection<String> getSelectedDirs();

	void enableArchiveGeneration(boolean b);

	void enableImport(boolean b);
}
