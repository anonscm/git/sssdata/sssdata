package fr.sedoo.sssdata.client;

import java.util.HashMap;

import org.gwtbootstrap3.client.ui.Alert;
import org.gwtbootstrap3.client.ui.constants.AlertType;

import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.VerticalPanel;

import fr.sedoo.commons.client.analytics.GA;
import fr.sedoo.commons.client.analytics.GAEvent;
import fr.sedoo.commons.client.cms.ui.CMSConsultView;
import fr.sedoo.commons.client.cms.ui.CMSConsultViewImpl;
import fr.sedoo.commons.client.cms.ui.CMSEditView;
import fr.sedoo.commons.client.cms.ui.CMSEditViewImpl;
import fr.sedoo.commons.client.cms.ui.CMSLabelProvider;
import fr.sedoo.commons.client.cms.ui.CMSListView;
import fr.sedoo.commons.client.cms.ui.CMSListViewImpl;
import fr.sedoo.commons.client.config.ClientPropertiesList;
import fr.sedoo.commons.client.config.event.ConfigChangeEvent;
import fr.sedoo.commons.client.config.ui.view.ConfigView;
import fr.sedoo.commons.client.config.ui.view.ConfigViewImpl;
import fr.sedoo.commons.client.contact.ui.ContactView;
import fr.sedoo.commons.client.contact.ui.ContactViewImpl;
import fr.sedoo.commons.client.event.ActionEndEvent;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.event.BackEvent;
import fr.sedoo.commons.client.event.BreadCrumbChangeEvent;
import fr.sedoo.commons.client.event.MaximizeEvent;
import fr.sedoo.commons.client.event.MinimizeEvent;
import fr.sedoo.commons.client.faq.ui.FaqConsultView;
import fr.sedoo.commons.client.faq.ui.FaqConsultViewImpl;
import fr.sedoo.commons.client.faq.ui.FaqEditView;
import fr.sedoo.commons.client.faq.ui.FaqEditViewImpl;
import fr.sedoo.commons.client.faq.ui.FaqManageView;
import fr.sedoo.commons.client.faq.ui.FaqManageViewImpl;
import fr.sedoo.commons.client.language.place.LanguageSwitchPlace;
import fr.sedoo.commons.client.language.ui.LanguageSwitchingView;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.mvp.CommonClientFactoryImpl;
import fr.sedoo.commons.client.mvp.place.LoginPlace;
import fr.sedoo.commons.client.news.ui.MessageEditView;
import fr.sedoo.commons.client.news.ui.MessageEditViewImpl;
import fr.sedoo.commons.client.news.ui.MessageManageView;
import fr.sedoo.commons.client.news.ui.MessageManageViewImpl;
import fr.sedoo.commons.client.news.widget.NewDisplayView;
import fr.sedoo.commons.client.news.widget.NewDisplayViewImpl;
import fr.sedoo.commons.client.news.widget.NewsArchiveView;
import fr.sedoo.commons.client.news.widget.NewsArchiveViewImpl;
import fr.sedoo.commons.client.util.LocaleUtil;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;
import fr.sedoo.sssdata.client.event.ParameterLoadedEvent;
import fr.sedoo.sssdata.client.print.SSSPrintStyleProvider;
import fr.sedoo.sssdata.client.ui.cms.SSSCMSLabelProvider;
import fr.sedoo.sssdata.client.ui.component.ShortcutFactory;
import fr.sedoo.sssdata.client.ui.component.api.NavigationBar;
import fr.sedoo.sssdata.client.ui.component.api.StatusBar;
import fr.sedoo.sssdata.client.ui.component.impl.NavigationBarImpl;
import fr.sedoo.sssdata.client.ui.component.impl.StatusBarImpl;
import fr.sedoo.sssdata.client.ui.component.impl.menu.ScreenNames;
import fr.sedoo.sssdata.client.ui.view.api.AdministratorManagementView;
import fr.sedoo.sssdata.client.ui.view.api.DoiHistoryManagementView;
import fr.sedoo.sssdata.client.ui.view.api.DownloadLogView;
import fr.sedoo.sssdata.client.ui.view.api.DownloadStatsView;
import fr.sedoo.sssdata.client.ui.view.api.FileSearchView;
import fr.sedoo.sssdata.client.ui.view.api.GriddedProductView;
import fr.sedoo.sssdata.client.ui.view.api.HeaderView;
import fr.sedoo.sssdata.client.ui.view.api.ImportView;
import fr.sedoo.sssdata.client.ui.view.api.LoginView;
import fr.sedoo.sssdata.client.ui.view.api.MetadataView;
import fr.sedoo.sssdata.client.ui.view.api.SearchView;
import fr.sedoo.sssdata.client.ui.view.api.StatsView;
import fr.sedoo.sssdata.client.ui.view.api.SystemView;
import fr.sedoo.sssdata.client.ui.view.api.UpdateListView;
import fr.sedoo.sssdata.client.ui.view.api.UserContactView;
import fr.sedoo.sssdata.client.ui.view.api.UserInfoView;
import fr.sedoo.sssdata.client.ui.view.api.UserStatsView;
import fr.sedoo.sssdata.client.ui.view.api.WelcomeView;
import fr.sedoo.sssdata.client.ui.view.impl.AdministratorManagementViewImpl;
import fr.sedoo.sssdata.client.ui.view.impl.DoiHistoryManagementViewImpl;
import fr.sedoo.sssdata.client.ui.view.impl.DownloadLogViewImpl;
import fr.sedoo.sssdata.client.ui.view.impl.DownloadStatsViewImpl;
import fr.sedoo.sssdata.client.ui.view.impl.FileSearchViewImpl;
import fr.sedoo.sssdata.client.ui.view.impl.GriddedProductViewImpl;
import fr.sedoo.sssdata.client.ui.view.impl.HeaderViewImpl;
import fr.sedoo.sssdata.client.ui.view.impl.ImportViewImpl;
import fr.sedoo.sssdata.client.ui.view.impl.LoginViewImpl;
import fr.sedoo.sssdata.client.ui.view.impl.MetadataViewImpl;
import fr.sedoo.sssdata.client.ui.view.impl.SearchViewImpl;
import fr.sedoo.sssdata.client.ui.view.impl.StatsViewImpl;
import fr.sedoo.sssdata.client.ui.view.impl.SystemViewImpl;
import fr.sedoo.sssdata.client.ui.view.impl.UpdateListViewImpl;
import fr.sedoo.sssdata.client.ui.view.impl.UserContactViewImpl;
import fr.sedoo.sssdata.client.ui.view.impl.UserInfoViewImpl;
import fr.sedoo.sssdata.client.ui.view.impl.UserStatsViewImpl;
import fr.sedoo.sssdata.client.ui.view.impl.WelcomeViewImpl;
import fr.sedoo.sssdata.shared.ConfigPropertiesCode;
 
public class ClientFactoryImpl extends CommonClientFactoryImpl implements ClientFactory{

	public static WelcomeView welcomeView;
	public static SystemView systemView;
	public static AdministratorManagementView administratorManagementView;
	public static SearchView searchView;
	public static HeaderView headerView;
	public static StatusBar statusBar;
	public static NavigationBar navigationBar;
	public static UserInfoView userInfoView;
	public static StatsView statsView;
	public static UserStatsView userStatsView;
	public static DownloadStatsView downloadStatsView;
	public static ImportView importView;
	public static MessageManageView messageManageView;
	public static MessageEditView messageEditView;
	public static NewDisplayView newDisplayView;
	public static NewsArchiveView newsArchiveView;
	public static FaqManageView faqManageView;
	public static FaqConsultView faqConsultView;
	public static FaqEditView faqEditView;
	public static ContactView contactView;
	public static UpdateListView updateListView;
	public static ConfigView configView;
	public static CMSConsultView cmsConsultView;
    public static CMSEditView cmsEditView;
     
    public static CMSListView cmsListView;
    public static CMSLabelProvider labelProvider = new SSSCMSLabelProvider();

    public static MetadataView metadataView;
    public static DoiHistoryManagementView doiHistoryManagementView;
    
    public static GriddedProductView griddedProductView;
    
	private HashMap<String, String> cmsScreens;
	
	 public ClientFactoryImpl(){
		super();
	}
	
	@Override
	public WelcomeView getWelcomeView() {
		if (welcomeView == null)
		{
			welcomeView = new WelcomeViewImpl();
		}
		return welcomeView;
	}
			
	@Override
	public AdministratorManagementView getAdministratorManagementView() {
		if (administratorManagementView == null)
		{
			administratorManagementView = new AdministratorManagementViewImpl();
		}
		return administratorManagementView;
	}
	
	
	
	@Override
	public SystemView getSystemView() {
		if (systemView == null)
		{
			systemView = new SystemViewImpl();
		}
		return systemView;
	}
	
	@Override
	public SearchView getSearchView() {
		if (searchView == null)
		{
			searchView = new SearchViewImpl();
		}
		return searchView;
	}

	@Override
	public HeaderView getHeaderView() {
		if (headerView == null){
			headerView = new HeaderViewImpl();
			ClientPropertiesList.getProperty(ConfigPropertiesCode.WEBSITE_TITLE, headerView);
			getEventBus().addHandler(MaximizeEvent.TYPE, headerView);
			getEventBus().addHandler(MinimizeEvent.TYPE, headerView);
			getEventBus().addHandler(ConfigChangeEvent.TYPE, headerView);
		}
		return headerView;	
	}

	@Override
	public StatusBar getStatusBar() {
		if (statusBar == null){
			statusBar = new StatusBarImpl();
			getEventBus().addHandler(ParameterLoadedEvent.TYPE, statusBar);
			getEventBus().addHandler(ActionStartEvent.TYPE, statusBar);
			getEventBus().addHandler(ActionEndEvent.TYPE, statusBar);
		}
		return statusBar;	
	}

	@Override
	public NavigationBar getNavigationBar() {
		if (navigationBar == null)
		{
			navigationBar = new NavigationBarImpl(getEventBus());
			getEventBus().addHandler(BreadCrumbChangeEvent.TYPE, navigationBar);
			getEventBus().addHandler(BackEvent.TYPE, navigationBar);
		}
		return navigationBar;
	}
	
	@Override
	public StatsView getStatsView() {
		if (statsView == null)
		{
			statsView = new StatsViewImpl();
		}
		return statsView;
	}
	
	public ImportView getImportView() {
		if (importView == null)
		{
			importView = new ImportViewImpl();
		}
		return importView;
	}

	@Override
	public UserInfoView getUserInfoView() {
		if (userInfoView == null)
		{
			userInfoView = new UserInfoViewImpl();
		}
		return userInfoView;
	}

	
	@Override
	public LoginPlace getLoginPlace() {
		return new LoginPlace();
	}

	@Override
	public MessageManageView getMessageManageView() {
		if (messageManageView == null)
		{
			messageManageView = new MessageManageViewImpl();
		}
		return messageManageView;
	}

	@Override
	public MessageEditView getMessageEditView() {
		if (messageEditView == null)
		{
			messageEditView = new MessageEditViewImpl(LocaleUtil.getCurrentLanguage(this));
		}
		return messageEditView;
	}

	@Override
	public LanguageSwitchPlace getLanguageSwitchPlace(String language) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LanguageSwitchingView getLanguageSwitchingView() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LoginView getLoginView() {
		return new LoginViewImpl();
	}

	@Override
	public UserContactView getUserContactView() {
		return new UserContactViewImpl();
	}
	
	@Override
	public FileSearchView getFileSearchView() {
		return new FileSearchViewImpl();	
	};
		

	@Override
	public DownloadLogView getDownloadLogView() {
		return new DownloadLogViewImpl();
	}

	@Override
	public IsWidget getUnAuthorizedUserView() {
		
		VerticalPanel panel = new VerticalPanel();
		panel.setWidth("100%");
		panel.setSpacing(5);
		Alert alert = new Alert(CommonMessages.INSTANCE.insufficientPrivileges(),AlertType.DANGER);
		panel.add(alert);
		return panel;
	}

	@Override
	public NewDisplayView getNewDisplayView() {
		if (newDisplayView == null)
		{
			newDisplayView = new NewDisplayViewImpl(new SSSPrintStyleProvider());
		}
		return newDisplayView;
	}

	@Override
	public NewsArchiveView getNewsArchiveView() {
		if (newsArchiveView == null)
		{
			newsArchiveView = new NewsArchiveViewImpl();
		}
		return newsArchiveView;
	}

	@Override
	public FaqEditView getFaqEditView() {
		if (faqEditView == null)
		{
			faqEditView = new FaqEditViewImpl(LocaleUtil.getCurrentLanguage(this));
		}
		return faqEditView;
	}

	@Override
	public FaqConsultView getFaqConsultView() {
		if (faqConsultView == null)
		{
			faqConsultView = new FaqConsultViewImpl();
		}
		return faqConsultView;
	}

	@Override
	public FaqManageView getFaqManageView() {
		if (faqManageView == null)
		{
			faqManageView = new FaqManageViewImpl();
		}
		return faqManageView;
	}

	@Override
	public ContactView getContactView() {
		if (contactView == null){
			contactView = new ContactViewImpl();
		}
		return contactView;
	}

	@Override
	public UpdateListView getUpdateListView() {
		if (updateListView == null){
			updateListView = new UpdateListViewImpl();
		}
		return updateListView;
	}

	@Override
	public UserStatsView getUserStatsView() {
		if (userStatsView == null){
			userStatsView = new UserStatsViewImpl();
		}
		return userStatsView;
	}

	@Override
	public DownloadStatsView getDownloadStatsView() {
		if (downloadStatsView == null){
			downloadStatsView = new DownloadStatsViewImpl();
		}
		return downloadStatsView;
	}

	@Override
	public ConfigView getConfigView() {
		if (configView == null){
			configView = new ConfigViewImpl();
		}
		return configView;
	}

	@Override
	public void onGAEvent(GAEvent event) {
		GA.trackPageview(event.getPlaceName());		
	}

	@Override
	public HashMap<String, String> getScreenNames() {
		 if (cmsScreens == null) {
             cmsScreens = new HashMap<String, String>();
             cmsScreens.put(ScreenNames.HOME, 
            		 getCMSLabelProvider().getLabelByScreenName(ScreenNames.HOME));
             cmsScreens.put(ScreenNames.DATA_POLICY,
                             getCMSLabelProvider().getLabelByScreenName(ScreenNames.DATA_POLICY));
		 }
		return cmsScreens;
	}

	@Override
	public CMSConsultView getCmsConsultView() {
		if (cmsConsultView == null) {
			cmsConsultView = new CMSConsultViewImpl(
					new SSSPrintStyleProvider(), true);
		}
		return cmsConsultView;
	}

	@Override
	public CMSEditView getCmsEditView() {
		 if (cmsEditView == null) {
             cmsEditView = new CMSEditViewImpl(
                             LocaleUtil.getCurrentLanguage(this), true);
             /*((CMSEditViewImpl) cmsEditView).getSaveButton().removeStyleName(
                             "btn-primary");*/
     }
     return cmsEditView;
	}

	@Override
	public CMSListView getCmsListView() {
		if (cmsListView == null) {
			cmsListView = new CMSListViewImpl();
		}
		return cmsListView;
	}

	@Override
	public CMSLabelProvider getCMSLabelProvider() {
		return labelProvider;
	}

	@Override
	public Shortcut getWelcomeShortcut() {
		return ShortcutFactory.getWelcomeShortcut();
		//return new Shortcut(CommonMessages.INSTANCE.home(), new WelcomePlace());
	}

	@Override
	public MetadataView getMetadataView() {
		if (metadataView == null) {
			metadataView = new MetadataViewImpl();
		}
		return metadataView;
	}
	
	@Override
	public DoiHistoryManagementView getDoiHistoryManagementView() {
		if (doiHistoryManagementView == null) {
			doiHistoryManagementView = new DoiHistoryManagementViewImpl();
		}
		return doiHistoryManagementView;
	}

	@Override
	public GriddedProductView getGriddedProductView() {
		if (griddedProductView == null) {
			griddedProductView = new GriddedProductViewImpl();
		}
		return griddedProductView;
	}
	
}
