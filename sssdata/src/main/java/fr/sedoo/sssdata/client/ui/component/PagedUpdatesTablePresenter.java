package fr.sedoo.sssdata.client.ui.component;

public interface PagedUpdatesTablePresenter extends UpdatesTablePresenter {
	void loadPageEntries(int pageNumber);
}
