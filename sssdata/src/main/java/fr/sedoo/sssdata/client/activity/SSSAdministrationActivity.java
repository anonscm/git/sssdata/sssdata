package fr.sedoo.sssdata.client.activity;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.sedoo.commons.client.event.BreadCrumbChangeEvent;
import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;
import fr.sedoo.commons.client.mvp.activity.AdministrationActivity;
import fr.sedoo.commons.client.user.AuthenticatedUser;
import fr.sedoo.commons.client.util.LocaleUtil;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;
import fr.sedoo.sssdata.client.ui.component.ShortcutFactory;

public abstract class SSSAdministrationActivity extends AdministrationActivity{
	
	protected EventBus eventbus;
	
	public SSSAdministrationActivity(AuthenticatedClientFactory clientFactory, Place place) {
		super(clientFactory, place);
	}

	
	
	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		if (isValidUser() == false) {
			goToLoginPlace();
			return;
		}
		this.eventbus = eventBus;
		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		shortcuts.add(ShortcutFactory.getWelcomeShortcut());
		shortcuts.add(ShortcutFactory.getAdminShortcut());
		eventBus.fireEvent(new BreadCrumbChangeEvent(shortcuts));
	}
	
	@Override
	protected boolean isValidUser() {
		return isLoggedUser();
	}
	
	protected AuthenticatedUser getLoggedUser(){
		return ((AuthenticatedClientFactory) clientFactory).getUserManager().getUser();
	}
	
	protected String getLocale(){
		return LocaleUtil.getClientLocaleLanguage(clientFactory.getDefaultLanguage());
	}
}
