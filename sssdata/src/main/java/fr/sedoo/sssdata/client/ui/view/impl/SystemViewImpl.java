package fr.sedoo.sssdata.client.ui.view.impl;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.component.AbstractView;
import fr.sedoo.sssdata.client.GlobalBundle;
import fr.sedoo.sssdata.client.ui.view.api.SystemView;

public class SystemViewImpl extends AbstractView implements SystemView {

	private static WelcomeViewImplUiBinder uiBinder = GWT
			.create(WelcomeViewImplUiBinder.class);

	interface WelcomeViewImplUiBinder extends UiBinder<Widget, SystemViewImpl> {
	}

	@UiField 
	InlineLabel applicationVersion;
	
	@UiField 
	InlineLabel javaVersion;
	
	@UiField 
	InlineLabel testMode;
	
	@UiField 
	InlineLabel gwtModuleName;
	
	@UiField 
	InlineLabel gwtBaseUrl;
	
	public SystemViewImpl() {
		super();
		GWT.<GlobalBundle>create(GlobalBundle.class).css().ensureInjected();
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
		reset();
		
		this.gwtBaseUrl.setText(GWT.getModuleBaseURL());
		this.gwtModuleName.setText(GWT.getModuleName());
	}

	private void reset() {
		setJavaVersion("");
		setApplicationVersion("");
	}

	@Override
	public void setJavaVersion(String javaVersion) {
		this.javaVersion.setText(javaVersion);
	}

	@Override
	public void setApplicationVersion(String applicationVersion) {
		this.applicationVersion.setText(applicationVersion);
		
	}

	@Override
	public void setTestMode(String testMode) {
		this.testMode.setText(""+Boolean.parseBoolean(testMode));
	}

}
