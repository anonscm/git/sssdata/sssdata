package fr.sedoo.sssdata.client.ui.widget.skeleton;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.SimpleLayoutPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.event.MaximizeEvent;
import fr.sedoo.commons.client.event.MaximizeEventHandler;
import fr.sedoo.commons.client.event.MinimizeEvent;
import fr.sedoo.commons.client.event.MinimizeEventHandler;
import fr.sedoo.sssdata.client.ui.component.api.NavigationBar;
import fr.sedoo.sssdata.client.ui.widget.PreferredHeightWidget;

public class TopSideMenuSkeleton extends Skeleton implements MinimizeEventHandler, MaximizeEventHandler{

	private DockLayoutPanel mainPanel;
	
	private Widget north;
	private Widget south;
	
	private double topPanelSmallSize;
	private double topPanelLargeSize;
	
	private static final String MINIMIZED_SIZE="960px";
	private static final String MAXIMIZED_SIZE="100%";
	
	private final int ANIMATION_DURATION = 500;
	
	private SimpleLayoutPanel contentPanel =  new SimpleLayoutPanel();

	private DockLayoutPanel topPanel;

	public TopSideMenuSkeleton(EventBus eventBus)
	{
		super(eventBus);
	}
	
	public void guiInit(PreferredHeightWidget header, PreferredHeightWidget footer, PreferredHeightWidget topMenu, NavigationBar navigationBar)
	{
		north = header.asWidget();
		south = footer.asWidget();
		eventBus.addHandler(MaximizeEvent.TYPE, this);
		eventBus.addHandler(MinimizeEvent.TYPE, this);
		init(header, footer, topMenu, navigationBar);
	}

	private void init(PreferredHeightWidget header, PreferredHeightWidget footer, PreferredHeightWidget menu, NavigationBar navigationBar) {
		mainPanel = new DockLayoutPanel(Unit.PX);
		mainPanel.getElement().getStyle().setProperty("border","white solid 0px");
		mainPanel.getElement().getStyle().setProperty("boxShadow","1px 1px 5px 1px rgba(0, 0, 0, 0.7)");
		mainPanel.setWidth(MINIMIZED_SIZE);
		mainPanel.getElement().getStyle().setProperty("margin", "auto");	
		
		topPanel = new DockLayoutPanel(Unit.PX);
		topPanel.addNorth(header, header.getPreferredHeight());
		DockLayoutPanel aux = new DockLayoutPanel(Unit.PX);
		aux.addNorth(menu, menu.getPreferredHeight());
		aux.addSouth(navigationBar, navigationBar.getPreferredHeight());
		topPanel.add(aux);
		
		topPanelLargeSize = header.getPreferredHeight()+menu.getPreferredHeight()+navigationBar.getPreferredHeight();
		topPanelSmallSize = menu.getPreferredHeight()+navigationBar.getPreferredHeight();
		
		mainPanel.addNorth(topPanel, topPanelLargeSize);
		mainPanel.addSouth(footer, footer.getPreferredHeight());
		
		mainPanel.add(contentPanel);
		add(mainPanel);
	}

	
	public AcceptsOneWidget getContentPanel() {
		return contentPanel;
	}
	
	@Override
	public void onNotification(MaximizeEvent event) {
		topPanel.setWidgetHidden(north, true);
		mainPanel.setWidgetSize(topPanel, topPanelSmallSize);
		mainPanel.setWidth(MAXIMIZED_SIZE);
		animate();
	}

	@Override
	public void onNotification(MinimizeEvent event) {
		topPanel.setWidgetHidden(north, false);
		mainPanel.setWidgetSize(topPanel, topPanelLargeSize);
		mainPanel.setWidth(MINIMIZED_SIZE);
		animate();
	}
	
	private void animate()
	{
		topPanel.animate(ANIMATION_DURATION);
		mainPanel.animate(ANIMATION_DURATION);
	}

}
