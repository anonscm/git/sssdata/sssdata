package fr.sedoo.sssdata.client.service;

import java.util.ArrayList;
import java.util.List;

import java.util.HashMap;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fr.sedoo.sssdata.shared.BoatDTO;
import fr.sedoo.sssdata.shared.SearchCriterion;
import fr.sedoo.sssdata.shared.Trajectory;

@RemoteServiceRelativePath("trajectories")

public interface SearchTrajectoriesService extends RemoteService{

	//HashMap<Boat,List<Trajectory>> searchTrajectories(SearchCriterion sc) throws ServiceException;
	List<Trajectory> searchTrajectories(BoatDTO b, SearchCriterion sc) throws ServiceException;
	
}
