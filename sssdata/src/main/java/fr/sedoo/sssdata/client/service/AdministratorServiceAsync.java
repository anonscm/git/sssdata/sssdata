package fr.sedoo.sssdata.client.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.sedoo.sssdata.shared.Administrator;

public interface AdministratorServiceAsync {

	void findAll(AsyncCallback<ArrayList<Administrator>> callback);

	void delete(Administrator administrator, AsyncCallback<Void> callback);

	void edit(Administrator administrator, AsyncCallback<Administrator> callback);

	void create(Administrator administrator,
			AsyncCallback<Administrator> callback);


}
