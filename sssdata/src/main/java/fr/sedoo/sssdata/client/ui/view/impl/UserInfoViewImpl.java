package fr.sedoo.sssdata.client.ui.view.impl;

import java.util.List;

import org.gwtbootstrap3.client.ui.Alert;
import org.gwtbootstrap3.client.ui.TextArea;
import org.gwtbootstrap3.client.ui.TextBox;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.widgetideas.client.ProgressBar;

import fr.sedoo.commons.client.component.AbstractView;
import fr.sedoo.commons.client.util.ValidationUtil;
import fr.sedoo.sssdata.client.event.TextChangeEvent;
import fr.sedoo.sssdata.client.ui.view.api.UserInfoView;
import fr.sedoo.sssdata.client.ui.widget.CountryListBox;
import fr.sedoo.sssdata.client.ui.widget.DataFormatListBox;
import fr.sedoo.sssdata.client.ui.widget.TextListenerTextBox;
import fr.sedoo.sssdata.shared.FileDTO;
import fr.sedoo.sssdata.shared.SearchCriterion;
import fr.sedoo.sssdata.shared.UserInformations;

public class UserInfoViewImpl extends AbstractView implements UserInfoView {
	
	
	private static UserInfoViewImplUiBinder uiBinder = GWT
			.create(UserInfoViewImplUiBinder.class);

	interface UserInfoViewImplUiBinder extends UiBinder<Widget, UserInfoViewImpl> {
	}

	interface UserInfoDriver extends SimpleBeanEditorDriver<UserInformations, UserInfoViewImpl> {}
	
	private UserInfoDriver driver = GWT.create(UserInfoDriver.class);
	
	private SearchCriterion searchCriterion;
	private List<String> allSelectedTrajectories;
	
	private Presenter presenter;
	
	@Ignore
	@UiField(provided=false)
	Label intro1;
	@Ignore
	@UiField(provided=false)
	Label intro2;
	@Ignore
	@UiField(provided=false)
	Label policy;
	
	@UiField(provided=false)
	TextBox userNameEditor;
	
	@UiField(provided=false)
	TextBox userOrganismEditor;
	
	@UiField(provided=false)
	TextArea userObjectiveEditor;

	
	@UiField(provided=false)
	TextListenerTextBox userEmailEditor;
	
	@UiField(provided=true)
	DataFormatListBox dataFormat;
	
	@Ignore
	@UiField
	Label dataCompressionLabel;

	@UiField(provided=true)
	DataFormatListBox dataCompression;
	
	@Ignore
	@UiField(provided=false)
	Alert errorMsg;
		
	@Ignore
	@UiField(provided=false)
	Alert infoMsg;
	
	@UiField
	Button retrieveInfoButton;
	
	@UiField
	ProgressBar progress;
	@UiField
	Button stopButton;
	
	@UiField(provided=false)
	Anchor downloadLabel;
	
	@UiField(provided=false)
	CountryListBox country;
	
	
	public SearchCriterion getSearchCriterion() {
		return searchCriterion;
	}


	public List<String> getAllSelectedTrajectories() {
		return allSelectedTrajectories;
	}

	public UserInfoViewImpl() {
		super();
		initDataFormats();
		initWidget(uiBinder.createAndBindUi(this));
		driver.initialize(this);
		applyCommonStyle();
		
		/*if (presenter != null){
			reset();
		}*/
		
		progress.setTextFormatter(new ProgressBar.TextFormatter() {
			
			@Override
			protected String getText(ProgressBar bar, double curProgress) {
				if (bar.getProgress() == bar.getMaxProgress() - 1 ){
					return "Creating archive. Please wait...";
				}else if (bar.getProgress() == bar.getMaxProgress()){
					return "Processing complete.";
				}
				NumberFormat formatter = NumberFormat.getFormat("####");
				return "Computing file " + formatter.format(bar.getProgress() + 1) + " / " + formatter.format(bar.getMaxProgress() - 1) + ". Please wait...";
			}
		});
		
		

		//reset();
				
	}
	
	
	@Override
	public void resetProgressBar() {
		this.progress.setVisible(false);
		this.stopButton.setVisible(false);
	}
	@Override
	public void initProgressBar(int fileNumber) {
		//+1 pour la génération du zip
		this.progress.setMaxProgress(fileNumber + 1);
		this.progress.setMinProgress(0);
		this.progress.setProgress(0);
		this.progress.setVisible(true);
		this.stopButton.setVisible(true);
		this.stopButton.setEnabled(true);
	}
	
	@Override
	public void updateProgressBar() {
		double p = this.progress.getProgress();
		this.progress.setProgress(p+1);
		
		if (progress.getProgress() == progress.getMaxProgress()){
			resetProgressBar();
		}
	}
	@UiHandler("stopButton")
	void onStopButtonClicked(ClickEvent event){
		presenter.stopDownload();
		stopButton.setEnabled(false);
		progress.setMaxProgress(progress.getProgress() + 2);
	}
	
	@Override
	public void downloadArchive(String filename){
		String href = "/sssdata/download?file=" + filename;
		
		downloadLabel.setVisible(true);
		downloadLabel.setHref(href);
		resetProgressBar();
		
		Window.open(href, "_blank", "");
	}

	private void initDataFormats() {
		dataFormat = new DataFormatListBox();
		dataFormat.addItem(FileDTO.FORMAT_NETCDF);
		dataFormat.addItem(FileDTO.FORMAT_ASCII);
		
		dataCompression = new DataFormatListBox();
		dataCompression.addItem(FileDTO.COMPRESSION_ZIP);
		dataCompression.addItem(FileDTO.COMPRESSION_GZ);
	}

	@UiHandler("downloadButton")
	void onDownloadButtonClicked(ClickEvent event){
		if (validUserInfos(driver.flush())){
			this.presenter.download(driver.flush());
		}
	}
	
	@UiHandler("userEmailEditor")
	public void onValueTextChange(TextChangeEvent textChangeEvent) {
		if (userEmailEditor.getText()==null || userEmailEditor.getText().equals("")){
			retrieveInfoButton.removeStyleName("btn-info");
			retrieveInfoButton.setEnabled(false);
		}else{
			retrieveInfoButton.setEnabled(true);
			retrieveInfoButton.addStyleName("btn-info");
		
		}
	}
	
	@UiHandler("resetButton")
	void onResetButtonClicked(ClickEvent event){
		reset();
	}
	
	@UiHandler("backButton")
	void onBackButtonClicked(ClickEvent event){
		presenter.back();
	}
	
	@UiHandler("retrieveInfoButton")
	void onRetrieveInfoButtonClicked(ClickEvent event){
		resetMessages();
		if (validEmail(driver.flush().getUserEmail())){
			presenter.getUserFromEmail(driver.flush().getUserEmail());
		}
	}
	
	private boolean validUserInfos(UserInformations userInfos) {
		boolean result = true;
		resetMessages();
		if (userInfos.getUserEmail()==null){
			addErrorMessage("Email is required");
			result = false;
		}else{
			result = validEmail(userInfos.getUserEmail());
		}
		if (userInfos.getUserName()==null){
			addErrorMessage("Name is required");
			result = false;
		}else if (userInfos.getUserName().length() > 255){
			addErrorMessage("Name is too long (255 characters max)");
			result = false;
		}
		if (userInfos.getUserOrganism()==null){
			addErrorMessage("Organism is required");
			result = false;
		}else if (userInfos.getUserOrganism().length() > 255){
			addErrorMessage("Organism is too long (255 characters max)");
			result = false;
		}
		if (userInfos.getUserObjective()==null){
			addErrorMessage("Purpose / Objectives is required");
			result = false;
		}
		if (userInfos.getCountry()==null){
			addErrorMessage("Country is required");
			result = false;
		}
		
		return result;
	}

	private boolean validEmail(String email){
		boolean result = ValidationUtil.isValidEmail(email);
		if (! result){
			addErrorMessage("Email is incorrect");
		}
		return result;
	}

	@Override
	public void reset() {
		UserInformations sc = new UserInformations();
		driver.edit(sc);
		resetMessages();
		resetForm();
		resetRetrieveInfoButton();
		resetProgressBar();
		presenter.getInstructions();
		downloadLabel.setVisible(false);
	}

	private void resetForm() {
		userNameEditor.setText(null);
		userOrganismEditor.setText(null);
		userObjectiveEditor.setText(null);
		country.reset();
		userEmailEditor.setText(null);
	}

	private void resetRetrieveInfoButton(){
		retrieveInfoButton.removeStyleName("btn-info");
		retrieveInfoButton.setEnabled(false);
	}
	
	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}


	@Override
	public void setKnownUserInfo(UserInformations userInfomations) {
		driver.edit(userInfomations);
	}

	@Override
	public void addErrorMessage(String msg) {
		Label l = new Label(msg);
		errorMsg.add(l);
		errorMsg.setVisible(true);
	}
	@Override
	public void addMessage(String msg) {
		Label l = new Label(msg);
		infoMsg.add(l);
		infoMsg.setVisible(true);
	}
	@Override
	public void resetMessages() {
		errorMsg.clear();
		infoMsg.clear();
		infoMsg.setVisible(false);
		errorMsg.setVisible(false);
	}


	@Override
	public void setInstructions(String intro1, String intro2, String policy) {
		this.intro1.setText(intro1);
		this.intro2.setText(intro2);
		this.policy.setText(policy);
	}

	@Override
	public void downloadArchiveMode(boolean isArchiveMode) {
		if (isArchiveMode) {
			dataCompressionLabel.setVisible(false);
			dataCompression.setVisible(false);
			downloadLabel.setVisible(false);
		} else {
			dataCompressionLabel.setVisible(true);
			dataCompression.setVisible(true);
		}

	}

}
