package fr.sedoo.sssdata.client.service;

import com.google.gwt.junit.FakeCssMaker;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("monfake")

public interface MonFakeService extends RemoteService {
        FakeCssMaker ouech() throws ServiceException;
}
