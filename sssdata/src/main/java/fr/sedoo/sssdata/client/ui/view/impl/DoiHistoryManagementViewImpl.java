package fr.sedoo.sssdata.client.ui.view.impl;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.crud.component.CrudViewImpl;
import fr.sedoo.commons.client.crud.widget.CrudTable;
import fr.sedoo.sssdata.client.GlobalBundle;
import fr.sedoo.sssdata.client.ui.component.DoiHistoryTable;
import fr.sedoo.sssdata.client.ui.view.api.DoiHistoryManagementView;
import fr.sedoo.sssdata.shared.metadata.HistoryItem;

public class DoiHistoryManagementViewImpl extends CrudViewImpl implements DoiHistoryManagementView{

	@UiField
	DoiHistoryTable doiHistoryTable;
	
	@UiField
	Label doi;
	
	private static DoiHistoryManagementViewUiBinder uiBinder = GWT
			.create(DoiHistoryManagementViewUiBinder.class);

	interface DoiHistoryManagementViewUiBinder extends UiBinder<Widget, DoiHistoryManagementViewImpl> {
	}

	public DoiHistoryManagementViewImpl() {
		super();
		GWT.<GlobalBundle>create(GlobalBundle.class).css().ensureInjected();
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
		doiHistoryTable.init(new ArrayList<HistoryItem>());
	}
	
	
	@Override
	public void setHistoryItems(ArrayList<HistoryItem> items) {
		doiHistoryTable.init(items);
	}

	@Override
	public CrudTable getCrudTable() {
		return doiHistoryTable;
	}


	@Override
	public void setCurrentVersion(String version) {
		doiHistoryTable.setCurrentVersion(version);		
	}


	@Override
	public void setDOI(String doi) {
		this.doi.setText(doi);
	}

}
