package fr.sedoo.sssdata.client.service;

import java.util.Map;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface ParameterServiceAsync {

	void getSystemParameters(AsyncCallback<Map<String, String>> callback);
	
}
