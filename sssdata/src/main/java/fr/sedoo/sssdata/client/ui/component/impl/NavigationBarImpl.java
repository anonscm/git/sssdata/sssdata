package fr.sedoo.sssdata.client.ui.component.impl;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.ResizeComposite;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.event.BackEvent;
import fr.sedoo.commons.client.event.BreadCrumbChangeEvent;
import fr.sedoo.commons.client.event.MaximizeEvent;
import fr.sedoo.commons.client.event.MinimizeEvent;
import fr.sedoo.commons.client.event.PlaceNavigationEvent;
import fr.sedoo.sssdata.client.GlobalBundle;
import fr.sedoo.sssdata.client.ui.component.api.NavigationBar;
import fr.sedoo.sssdata.client.ui.widget.breadcrumb.impl.BreadCrumbImpl;

public class NavigationBarImpl extends ResizeComposite implements NavigationBar {

	private EventBus eventBus;

	@UiField
	BreadCrumbImpl breadCrumb;
	
	@UiField
	HTMLPanel content;
	
	 @UiField 
     Image minimizeImage;

     @UiField 
     Image maximizeImage;

	private static NavigationBarImplUiBinder uiBinder = GWT
			.create(NavigationBarImplUiBinder.class);

	interface NavigationBarImplUiBinder extends UiBinder<Widget, NavigationBarImpl> {
	}

	public NavigationBarImpl(EventBus eventBus) {
		this.eventBus = eventBus;
		GWT.<GlobalBundle>create(GlobalBundle.class).css().ensureInjected();
		initWidget(uiBinder.createAndBindUi(this));
		breadCrumb.setEventBus(eventBus);
		reset();
	}

	private void reset() {
		minimizeImage.setVisible(false);
		maximizeImage.setVisible(true);
	}

	public double getPreferredHeight() {
		return 25;
	}

	@UiHandler("maximizeImage")
	void onMaximizeImageClicked(ClickEvent event) {
		minimizeImage.setVisible(true);
		maximizeImage.setVisible(false);
		eventBus.fireEvent(new MaximizeEvent());
	}

	@UiHandler("minimizeImage")
	void onMinimizeImageClicked(ClickEvent event) {
		minimizeImage.setVisible(false);
		maximizeImage.setVisible(true);
		eventBus.fireEvent(new MinimizeEvent());
	}

	@Override
	public void onNotification(BreadCrumbChangeEvent event) 
	{
		breadCrumb.setShortcuts(event.getShortcuts());
	}

	@Override
	public void onNotification(BackEvent event) {
		eventBus.fireEvent(new PlaceNavigationEvent(breadCrumb.getBackPlace()));
	}
	
}
