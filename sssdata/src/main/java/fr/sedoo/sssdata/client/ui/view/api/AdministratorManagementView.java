package fr.sedoo.sssdata.client.ui.view.api;

import java.util.ArrayList;

import fr.sedoo.commons.client.crud.component.CrudView;
import fr.sedoo.sssdata.shared.Administrator;

public interface AdministratorManagementView extends CrudView {

	void setAdministrator(ArrayList<Administrator> result);

	

}
