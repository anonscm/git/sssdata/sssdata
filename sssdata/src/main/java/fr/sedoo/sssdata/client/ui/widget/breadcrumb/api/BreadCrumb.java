package fr.sedoo.sssdata.client.ui.widget.breadcrumb.api;

import java.util.List;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;

import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;
import fr.sedoo.sssdata.client.ui.widget.PreferredHeightWidget;

public interface BreadCrumb extends PreferredHeightWidget {

     void addShortcut(Shortcut shortcut);
     List<Shortcut> getShortcuts();
	void setEventBus(EventBus eventBus);
	void setShortcuts(List<Shortcut> shortcuts);
	Place getBackPlace();
}
