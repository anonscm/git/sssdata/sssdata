package fr.sedoo.sssdata.client.ui.view.api;

import java.util.List;

import com.google.gwt.user.client.ui.IsWidget;

import fr.sedoo.sssdata.client.ui.component.PagedUpdatesTablePresenter;
import fr.sedoo.sssdata.shared.UpdateDTO;

public interface UpdateListView extends IsWidget {
	
	//void setPresenter(Presenter presenter);
	
	//void setList(List<UpdateDTO> updates);
	
	/*public interface Presenter{
		
	}*/

	void setUpdatesTablePresenter(PagedUpdatesTablePresenter presenter);
	
	void reset();
	
	int getStartIndex();
	
	void setByRange(int i, List<UpdateDTO> items);
	void setHits(int hits);

	void displayUpdate(UpdateDTO u);

}
