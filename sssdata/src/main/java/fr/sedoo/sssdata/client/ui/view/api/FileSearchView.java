package fr.sedoo.sssdata.client.ui.view.api;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.editor.client.Editor;
import com.google.gwt.user.client.ui.IsWidget;

import fr.sedoo.sssdata.client.ui.component.UpdatesTablePresenter;
import fr.sedoo.sssdata.shared.BoatDTO;
import fr.sedoo.sssdata.shared.FileSearchInformations;
import fr.sedoo.sssdata.shared.UpdateDTO;

public interface FileSearchView extends IsWidget, Editor<FileSearchInformations>{

	void reset();
	void setPresenter(Presenter presenter);
	void setBoatList(ArrayList<BoatDTO> boats);
	void setSearchCriterion(FileSearchInformations infos);
	
	void search();
	
	void addMessage(String msg);
	void addErrorMessage(String msg);
	void resetMessages();

	void setUpdatesTablePresenter(UpdatesTablePresenter presenter);
	void setResult(List<UpdateDTO> result);
		
	void displayUpdate(UpdateDTO u);
	
	public interface Presenter{
		void search(FileSearchInformations infos);
		void getDefaultCriterion();
	}

	void resetResult();
	
}
