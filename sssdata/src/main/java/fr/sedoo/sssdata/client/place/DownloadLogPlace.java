package fr.sedoo.sssdata.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class DownloadLogPlace extends Place {
	public static DownloadLogPlace instance;

	public DownloadLogPlace(){}

	public static class Tokenizer implements PlaceTokenizer<DownloadLogPlace>{

		public DownloadLogPlace getPlace(String token) {
			if (instance == null){
				instance = new DownloadLogPlace();
			}
			return instance;
		}

		public String getToken(DownloadLogPlace place) {
			return "";
		}
	}
}
