package fr.sedoo.sssdata.client.ui.component;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;

import fr.sedoo.sssdata.shared.UpdateDTO;

public class UpdateCell extends AbstractCell<UpdateDTO> {

	boolean compact;
	
	public UpdateCell(boolean compact) {
		super();
		this.compact = compact;
	}
	
	@Override
	public void render(com.google.gwt.cell.client.Cell.Context context,	UpdateDTO update, SafeHtmlBuilder sb) {
		if (compact){
			sb.appendHtmlConstant(update.getFileName() + "<br/>Boat:&nbsp;" + update.getBoatName()
				+ "<br/>" + update.getStartDate() + " - " + update.getEndDate());
		}else{
			sb.appendHtmlConstant(update.getFileName() + ":&nbsp;" + update.getBoatName()
					+ ",&nbsp;" + update.getStartDate() + "&nbsp;-&nbsp;" + update.getEndDate());
		}
	}

}
