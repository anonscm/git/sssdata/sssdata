package fr.sedoo.sssdata.client.ui.widget;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

import fr.sedoo.commons.client.widget.ConfirmCallBack;
import fr.sedoo.sssdata.client.message.CommonMessages;
import fr.sedoo.sssdata.client.ui.dialog.DialogBoxContent;


public class DialogBoxTools 
{

	public static void modalAlert(final String header, final String content) {
        final DialogBox box = new DialogBox();
        box.getElement().getStyle().setProperty("zIndex", "9000");
        final VerticalPanel panel = new VerticalPanel();
        box.setText(header);
        panel.add(new Label(content));
        final Button buttonClose = new Button("Close",new ClickHandler() {
            @Override
            public void onClick(final ClickEvent event) {
                box.hide();
            }
        });
        // few empty labels to make widget larger
        final Label emptyLabel = new Label("");
        emptyLabel.setSize("auto","25px");
        panel.add(emptyLabel);
        panel.add(emptyLabel);
        buttonClose.setWidth("90px");
        panel.add(buttonClose);
        panel.setCellHorizontalAlignment(buttonClose, HasAlignment.ALIGN_RIGHT);
        box.add(panel);
        box.center();
        box.show();
    }
	
public static DialogBox modalConfirm(final String header, final String content, final ConfirmCallBack callback) {
		
		
        final DialogBox box = new DialogBox();
        box.getElement().getStyle().setProperty("zIndex", "9999");
        final VerticalPanel panel = new VerticalPanel();
        box.setText(header);
        panel.add(new Label(content));
        final Button buttonYes = new Button(CommonMessages.INSTANCE.yes(),new ClickHandler() {
            @Override
            public void onClick(final ClickEvent event) {
                box.hide();
                callback.confirm(true);
            }
        });
        
        final Button buttonNo = new Button(CommonMessages.INSTANCE.no(),new ClickHandler() {
            @Override
            public void onClick(final ClickEvent event) {
                box.hide();
                callback.confirm(false);
            }
        });
        // few empty labels to make widget larger
        final Label emptyLabel = new Label("");
        emptyLabel.setSize("auto","25px");
        panel.add(emptyLabel);
        panel.add(emptyLabel);
        panel.setWidth("100%");
        buttonYes.setWidth("90px");
        buttonNo.setWidth("90px");
        HorizontalPanel horizontalPanel = new HorizontalPanel();
        horizontalPanel.setSpacing(5);
        horizontalPanel.setHorizontalAlignment(HasAlignment.ALIGN_CENTER);
        horizontalPanel.add(buttonYes);
        horizontalPanel.add(buttonNo);
        panel.add(horizontalPanel);
        box.setGlassEnabled(true);
	    box.setAutoHideEnabled(false);
        panel.setCellHorizontalAlignment(horizontalPanel, HasAlignment.ALIGN_CENTER);
        box.add(panel);
        return box;
    }
	

public static void popUp(String title, DialogBoxContent content) 
{
	popUp(title, content, null, null);
}

public static void popUp(String title, DialogBoxContent content, String width, String height) 
{
	final DialogBox dialogBox = new DialogBox();
	if ((width != null) && (height != null))
	{
		((Composite)content).setSize(width, height);
	}
    dialogBox.setText(title);
    dialogBox.getElement().getStyle().setProperty("zIndex", "9999");
    dialogBox.setWidget(content);
    content.setDialogBox(dialogBox);
    dialogBox.setGlassEnabled(true);
    dialogBox.setAutoHideEnabled(true);
    dialogBox.center();
    dialogBox.show();
}

	
	
	

	
}