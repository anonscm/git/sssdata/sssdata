package fr.sedoo.sssdata.client.ui.view.impl;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.visualization.client.AbstractDataTable.ColumnType;
import com.google.gwt.visualization.client.DataTable;
import com.google.gwt.visualization.client.VisualizationUtils;
import com.google.gwt.visualization.client.visualizations.corechart.AxisOptions;
import com.google.gwt.visualization.client.visualizations.corechart.ColumnChart;
import com.google.gwt.visualization.client.visualizations.corechart.ComboChart.Options;

import fr.sedoo.commons.client.component.AbstractView;
import fr.sedoo.commons.client.date.DateUtil;
import fr.sedoo.commons.client.widget.panel.HorizontalCollapsingPanel;
import fr.sedoo.sssdata.client.ui.view.api.DownloadStatsView;
import fr.sedoo.sssdata.shared.StatsDTO;

public class DownloadStatsViewImpl extends AbstractView implements DownloadStatsView {

	private static DownloadStatsViewImplUiBinder uiBinder = GWT.create(DownloadStatsViewImplUiBinder.class);

	interface DownloadStatsViewImplUiBinder extends UiBinder<Widget, DownloadStatsViewImpl> {
	}
	
	Presenter presenter;
	StatsDTO stats;
	
	@UiField
	HorizontalCollapsingPanel yearStatsPanel;
	@UiField
	HorizontalCollapsingPanel monthStatsPanel;
	
	@UiField
	InlineLabel previousYear;
	@UiField
	InlineLabel nextYear;
	@UiField
	Image rightArrow;
	@UiField
	Image leftArrow;
	
	public DownloadStatsViewImpl() {
		super();
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();		
				
	}
	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public void setStats(StatsDTO stats) {
		this.stats = stats;	
	}

	private void displayYearStats(){
		if (stats != null && stats.getDownloadsByYear() != null
				&& stats.getUsersByYear() != null){
			
			DataTable data = DataTable.create();
			data.addColumn(ColumnType.STRING, "Year");
			data.addColumn(ColumnType.NUMBER, "Downloads");
			data.addColumn(ColumnType.NUMBER, "Users");
			data.addColumn(ColumnType.NUMBER, "Volume (Go)");
			data.addRows(stats.getDownloadsByYear().size());
			int rowIndex = 0;
			for (Integer year: stats.getDownloadsByYear().keySet()){
				data.setValue(rowIndex, 0, "" + year);
				data.setValue(rowIndex, 1, stats.getDownloadsByYear().get(year));
				data.setValue(rowIndex, 2, stats.getUsersByYear().get(year));
				data.setValue(rowIndex, 3, stats.getVolumeByYear().get(year));
				rowIndex++;
			}
			
			Options chartOptions = Options.create();
			chartOptions.setWidth(700);
			chartOptions.setHeight(300);
			chartOptions.setTitle("");
						
			AxisOptions vAxisOptions = AxisOptions.create();
			vAxisOptions.setMinValue(0);
			//vAxisOptions.setMaxValue(stats.getMaxMonthDownloads());
									
			chartOptions.setVAxisOptions(vAxisOptions);
						
			ColumnChart chart = new ColumnChart(data, chartOptions);
			yearStatsPanel.clear();
			yearStatsPanel.add(chart);
		}
	}
	
	private ColumnChart getYearChart(int year){
		if (stats != null && stats.getDownloadsByMonth() != null
				&& stats.getUsersByMonth() != null){
			
			if (stats.getDownloadsByMonth().containsKey(year) 
					&& stats.getUsersByMonth().containsKey(year)){
				
				DataTable data = DataTable.create();
				data.addColumn(ColumnType.STRING, "Month");
				data.addColumn(ColumnType.NUMBER, "Downloads");
				data.addColumn(ColumnType.NUMBER, "Users");
				data.addColumn(ColumnType.NUMBER, "Volume (Go)");
				data.addRows(stats.getDownloadsByMonth().get(year).size());
				int rowIndex = 0;
				for (Integer month: stats.getDownloadsByMonth().get(year).keySet()){
					data.setValue(rowIndex, 0, "" + month);
					data.setValue(rowIndex, 1, stats.getDownloadsByMonth().get(year).get(month));
					data.setValue(rowIndex, 2, stats.getUsersByMonth().get(year).get(month));
					data.setValue(rowIndex, 3, stats.getVolumeByMonth().get(year).get(month));
					rowIndex++;
				}
				
				Options chartOptions = Options.create();
				chartOptions.setWidth(700);
				chartOptions.setHeight(300);
				chartOptions.setTitle("");
				AxisOptions vAxisOptions = AxisOptions.create();
				vAxisOptions.setMinValue(0);
				vAxisOptions.setMaxValue(stats.getMaxMonthDownloads());
				AxisOptions hAxisOptions = AxisOptions.create();
				hAxisOptions.setTitle("" + year);
				
				chartOptions.setVAxisOptions(vAxisOptions);
				chartOptions.setHAxisOptions(hAxisOptions);
			
				return new ColumnChart(data, chartOptions);
			}
		}
		return null;
	}
	
	private void displayMonthStats(int year){
		updateLabels(year);
		ColumnChart chart = getYearChart(year);
		monthStatsPanel.setTitle(year + " downloads");
		monthStatsPanel.clear();
		monthStatsPanel.add(chart);
	
	}
	
	private void displayMonthStats(String year){
		displayMonthStats(Integer.parseInt(year));
	}
	
	private void updateLabels(int year){
		if (DateUtil.getCurrentYearAsString().compareTo("" + year) == 0){
			//L'année affichée est l'année en cours
			nextYear.setText("");
			rightArrow.setVisible(false);
		}else{
			nextYear.setText("" + (year+1));
			rightArrow.setVisible(true);
		}
		if (StatsDTO.MIN_YEAR == year){
			//L'année affichée est la première année
			previousYear.setText("");
			leftArrow.setVisible(false);
		}else{
			previousYear.setText("" + (year-1));
			leftArrow.setVisible(true);
		}
	}
	
	@UiHandler(value={"leftArrow", "previousYear"})
	void onPreviousYearClicked(ClickEvent event){
		displayMonthStats(previousYear.getText());
	}
	
	@UiHandler(value={"rightArrow", "nextYear"})
	void onNextYearClicked(ClickEvent event){
		displayMonthStats(nextYear.getText());
	}
	
	@Override
	public void display() {
		VisualizationUtils.loadVisualizationApi(new Runnable() {
			public void run() {
				displayYearStats();
				displayMonthStats(DateUtil.getCurrentYearAsString());
			}}, ColumnChart.PACKAGE);
	}

}
