package fr.sedoo.sssdata.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class UserStatsPlace extends Place{
	public static UserStatsPlace instance;
	
	public UserStatsPlace(){}

	public static class Tokenizer implements PlaceTokenizer<UserStatsPlace>	{
		
		public UserStatsPlace getPlace(String token) {
			if (instance == null){
				instance = new UserStatsPlace();
			}
			return instance;
		}

		public String getToken(UserStatsPlace place) {
			return "";
		}

		
	}
}