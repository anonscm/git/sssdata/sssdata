package fr.sedoo.sssdata.client.misc;

import java.util.ArrayList;
import java.util.Iterator;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;

import fr.sedoo.commons.client.callback.LoadCallBack;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.sssdata.client.SssData;
import fr.sedoo.sssdata.client.event.ActionEventConstants;
import fr.sedoo.sssdata.client.message.CommonMessages;
import fr.sedoo.sssdata.client.service.BoatService;
import fr.sedoo.sssdata.client.service.BoatServiceAsync;
import fr.sedoo.sssdata.shared.BoatDTO;

public class ClientBoatList {
	
	private static ArrayList<BoatDTO> boats = new ArrayList<BoatDTO>();
	private static boolean loaded=false;
	private static ArrayList<LoadCallBack<ArrayList<BoatDTO>>> callBacks = new ArrayList<LoadCallBack<ArrayList<BoatDTO>>>();
	private final static BoatServiceAsync BOAT_SERVICE = GWT.create(BoatService.class);
	
	
	public static void getBoats(LoadCallBack<ArrayList<BoatDTO>> callBack) {
		if (loaded)
		{
			callBack.postLoadProcess(boats);
		}
		else
		{
			callBacks.add(callBack);
		}
	}
	
	public static void loadBoats()
	{
		EventBus eventBus = SssData.getClientFactory().getEventBus();
		ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstants.BASIC_LOADING_EVENT, true);
		eventBus.fireEvent(e);
		BOAT_SERVICE.findAll(new DefaultAbstractCallBack<ArrayList<BoatDTO>>(e, eventBus){
	      	
	      	@Override
				public void onSuccess(ArrayList<BoatDTO> boats) {
					super.onSuccess(boats);
					ClientBoatList.boats = boats;
					Iterator<LoadCallBack<ArrayList<BoatDTO>>> iterator = callBacks.iterator();
					while (iterator.hasNext()) {
						LoadCallBack<ArrayList<BoatDTO>> loadCallBack = (LoadCallBack<ArrayList<BoatDTO>>) iterator.next();
						loadCallBack.postLoadProcess(boats);
					}
					loaded = true;
					callBacks.clear();
				}
	      	
	      });	
	}
	  
	

}
