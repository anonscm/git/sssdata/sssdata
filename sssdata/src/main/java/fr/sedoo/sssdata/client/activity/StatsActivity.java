package fr.sedoo.sssdata.client.activity;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.visualization.client.VisualizationUtils;
import com.google.gwt.visualization.client.visualizations.BarChart;
import com.google.gwt.visualization.client.visualizations.LineChart;

import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.event.PlaceNavigationEvent;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.sssdata.client.ClientFactory;
import fr.sedoo.sssdata.client.event.ActionEventConstants;
import fr.sedoo.sssdata.client.message.CommonMessages;
import fr.sedoo.sssdata.client.place.StatsPlace;
import fr.sedoo.sssdata.client.service.StatsService;
import fr.sedoo.sssdata.client.service.StatsServiceAsync;
import fr.sedoo.sssdata.client.ui.view.api.StatsView;
import fr.sedoo.sssdata.shared.StatsReport;


public class StatsActivity extends SSSAdministrationActivity implements StatsView.Presenter {

    
    public final static StatsServiceAsync STATS_SERVICE = GWT.create(StatsService.class);
    //public final static DataTableLinechartServiceAsync LINECHART_SERVICE = GWT.create(DataTableLinechartService.class);

    public StatsActivity(StatsPlace place, ClientFactory clientFactory) {
    	super(clientFactory, place);
    	year = place.getYear();
    }
    
    private StatsView statsView;
    //private EventBus eventbus;
    private static boolean isVizualisationLoaded = false;
	private String year;

    /**
     * Invoked by the ActivityManager to start a new Activity
     */
    @Override
    public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
    	super.start(containerWidget, eventBus);
    	
    	statsView = ((ClientFactory) clientFactory).getStatsView();
    	statsView.setPresenter(this);
    	statsView.reset();
           	
    	if (isVizualisationLoaded == false)
    	{
    		VisualizationUtils.loadVisualizationApi(new PostVisualizationLoadingProcess(), LineChart.PACKAGE, BarChart.PACKAGE);
    		isVizualisationLoaded = true;
    	}
    	else
    	{
    		new PostVisualizationLoadingProcess().run();
    	}
   
       
    }
	/**
     * Ask user before stopping this activity
     */
    @Override
    public String mayStop() {
    	return null;
    }
	
	@Override
	public StatsReport getEmptyReport() {		
		final StatsReport report = new StatsReport();	
		report.setYear("");
	    return report;
		
	}
	
	class PostVisualizationLoadingProcess implements Runnable 
	{
		public void run() {
			   final ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstants.BASIC_LOADING_EVENT, true);
		        eventbus.fireEvent(e);
			 STATS_SERVICE.getGlobalStats(new Integer(year),new DefaultAbstractCallBack<StatsReport>(e, eventbus){
		           	
		           	@Override
		   			public void onSuccess(StatsReport result) {
		   				super.onSuccess(result);
		   				statsView.display(result);
		           	}
			 });
		}
	}

	@Override
	public void displayYear(String year) {
		clientFactory.getEventBus().fireEvent(new PlaceNavigationEvent(new StatsPlace(year)));
		
	}

		
	}





	
