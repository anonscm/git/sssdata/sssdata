package fr.sedoo.sssdata.client.ui.dialog;


public interface OkCancelContent extends DialogBoxContent{
	
	public void okClicked();
	
	public void cancelClicked();

}
