package fr.sedoo.sssdata.client.service;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.sedoo.sssdata.shared.ArchiveGeneratorRequest;
import fr.sedoo.sssdata.shared.BoatDTO;
import fr.sedoo.sssdata.shared.FileDTO;

public interface ArchiveGeneratorServiceAsync {

	void initRequests(AsyncCallback<List<ArchiveGeneratorRequest>> callback);

	void createArchive(ArchiveGeneratorRequest request, List<FileDTO> temporaryFiles, AsyncCallback<String> callback);

	void getArchiveFilesByBoat(ArchiveGeneratorRequest request, BoatDTO boat, AsyncCallback<List<FileDTO>> callback);

}
