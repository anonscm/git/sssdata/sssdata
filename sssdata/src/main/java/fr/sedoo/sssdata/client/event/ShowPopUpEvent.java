package fr.sedoo.sssdata.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class ShowPopUpEvent extends GwtEvent<ShowPopUpHandler>{
	
	public static final Type<ShowPopUpHandler> TYPE = new Type<ShowPopUpHandler>();

	private String message;
	
	public ShowPopUpEvent(String message)
	{
		this.setMessage(message);
	}
	
	 @Override
	    protected void dispatch(ShowPopUpHandler handler) {
	        handler.onShowPopUp(this);
	    }

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<ShowPopUpHandler> getAssociatedType() {
		return TYPE;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
