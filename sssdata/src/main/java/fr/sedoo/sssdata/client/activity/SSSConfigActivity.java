package fr.sedoo.sssdata.client.activity;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.sedoo.commons.client.config.ConfigClientFactory;
import fr.sedoo.commons.client.config.activity.ConfigActivity;
import fr.sedoo.commons.client.config.place.ConfigPlace;
import fr.sedoo.commons.client.event.BreadCrumbChangeEvent;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;
import fr.sedoo.sssdata.client.ui.component.ShortcutFactory;

public class SSSConfigActivity extends ConfigActivity {

	public SSSConfigActivity(ConfigPlace place, ConfigClientFactory clientFactory) {
		super(clientFactory, place);
	}
	
	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		super.start(panel, eventBus);
		
		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		shortcuts.add(ShortcutFactory.getWelcomeShortcut());
		shortcuts.add(ShortcutFactory.getAdminShortcut());
		eventBus.fireEvent(new BreadCrumbChangeEvent(shortcuts));		
	}
	
	@Override
	protected boolean isValidUser() {
		return isLoggedUser();
	}

}
