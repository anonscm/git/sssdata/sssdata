package fr.sedoo.sssdata.client.ui.view.api;

import com.google.gwt.editor.client.Editor;
import com.google.gwt.user.client.ui.IsWidget;

import fr.sedoo.sssdata.shared.UserInformations;

public interface UserInfoView extends IsWidget, Editor<UserInformations> {
	
	void reset();
	void setPresenter(Presenter presenter);
	void setKnownUserInfo(UserInformations userInfomations);
	
	void addErrorMessage(String message);
	void addMessage(String message);
	void resetMessages();
	
	void setInstructions(String intro1, String intro2, String policy);
	
	public interface Presenter 
	{
		void download(UserInformations flush);
		void getUserFromEmail(String email);
		void back();
		
		void stopDownload();
		
		void getInstructions();
	}

	void updateProgressBar();
	void initProgressBar(int maxProgress);
	void resetProgressBar();
	void downloadArchive(String filename);

	void downloadArchiveMode(boolean isArchiveMode);

}
