package fr.sedoo.sssdata.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class HelpPlace extends Place
{
	public static HelpPlace instance;
	
	public HelpPlace()
	{
	}

	public static class Tokenizer implements PlaceTokenizer<HelpPlace>
	{
		
		public HelpPlace getPlace(String token) {
			if (instance == null)
			{
				instance = new HelpPlace();
			}
			return instance;
		}

		public String getToken(HelpPlace place) {
			return "";
		}

		
	}
}