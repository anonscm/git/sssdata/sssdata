package fr.sedoo.sssdata.client.activity;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.Window;

import fr.sedoo.commons.client.cms.ClientScreenList;
import fr.sedoo.commons.client.event.BreadCrumbChangeEvent;
import fr.sedoo.commons.client.event.PlaceNavigationEvent;
import fr.sedoo.commons.client.util.LocaleUtil;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;
import fr.sedoo.sssdata.client.ClientFactory;
import fr.sedoo.sssdata.client.message.ApplicationMessages;
import fr.sedoo.sssdata.client.place.GriddedProductMetadataPlace;
import fr.sedoo.sssdata.client.place.UserInfoPlace;
import fr.sedoo.sssdata.client.ui.component.ShortcutFactory;
import fr.sedoo.sssdata.shared.GriddedProduct;

public class GriddedProductMetadataActivity extends MetadataActivity {

	private String grid;
		
	public GriddedProductMetadataActivity(GriddedProductMetadataPlace place, ClientFactory clientFactory) {
		super(place, clientFactory);
		this.grid = place.getGrid(); 
	}

	@Override
	protected void setShortcut(EventBus eventBus) {
		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		shortcuts.add(ShortcutFactory.getWelcomeShortcut());
		shortcuts.add(ShortcutFactory.getGridShortcut(grid));
		shortcuts.add(ShortcutFactory.getMetadataShortcut());
		eventBus.fireEvent(new BreadCrumbChangeEvent(shortcuts));
	}
	
	@Override
	protected void setPageTitle() {
		setPageTitle(ApplicationMessages.INSTANCE.gridMetadataTitle());
	}
	
	@Override
	public void toDataPage() {
		//TODO gérer les fichiers NASG dans l'appli
		/*if (grid.equals(GriddedProduct.GRID_NASG)){
			String url = "ftp://ftp.legos.obs-mip.fr/pub/soa/salinite/sss_products/gridded/north_atlantic_ocean/v2.0/";
			Window.open(url, "_blank", null);
		}else{*/
			UserInfoPlace userInfoPlace = new UserInfoPlace();
			userInfoPlace.setPretreatedArchiveType(grid);
			this.eventBus.fireEvent(new PlaceNavigationEvent(userInfoPlace));
		//}
	}
	
	@Override
	protected void setOptionalContent() {
		ClientScreenList.getConsultContentByName(grid, LocaleUtil.getClientLocaleLanguage(clientFactory.getDefaultLanguage()), clientFactory.getLanguages(), this);
	}
}
