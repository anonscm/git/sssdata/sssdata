package fr.sedoo.sssdata.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class AdministratorManagementPlace extends Place
{
	public static AdministratorManagementPlace instance;
	
	public AdministratorManagementPlace()
	{
	}

	public static class Tokenizer implements PlaceTokenizer<AdministratorManagementPlace>
	{
		
		public AdministratorManagementPlace getPlace(String token) {
			if (instance == null)
			{
				instance = new AdministratorManagementPlace();
			}
			return instance;
		}

		public String getToken(AdministratorManagementPlace place) {
			return "";
		}

		
	}
}