package fr.sedoo.sssdata.client.activity;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.sssdata.client.ClientFactory;
import fr.sedoo.sssdata.client.event.ActionEventConstants;
import fr.sedoo.sssdata.client.message.CommonMessages;
import fr.sedoo.sssdata.client.place.DownloadLogPlace;
import fr.sedoo.sssdata.client.service.JournalService;
import fr.sedoo.sssdata.client.service.JournalServiceAsync;
import fr.sedoo.sssdata.client.ui.view.api.DownloadLogView;
import fr.sedoo.sssdata.shared.DownloadDTO;

public class DownloadLogActivity extends SSSAdministrationActivity implements DownloadLogView.Presenter{

	public final static JournalServiceAsync JOURNAL_SERVICE = GWT.create(JournalService.class);


	private DownloadLogView view;
	private EventBus eventbus;

	public DownloadLogActivity(DownloadLogPlace place, AuthenticatedClientFactory clientFactory) {
		super(clientFactory, place);
	}

	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		super.start(panel, eventBus);

		this.eventbus = eventBus;
		this.view = ((ClientFactory) clientFactory).getDownloadLogView();
		view.setPresenter(this);

		panel.setWidget(view.asWidget());

		//Charger la liste des téléchargements
		load();

	}

	private void load(){
		ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstants.BASIC_LOADING_EVENT, true);
		eventbus.fireEvent(e);
		JOURNAL_SERVICE.getAllDownloads(new DefaultAbstractCallBack<List<DownloadDTO>>(e, eventbus) {
			@Override
			public void onSuccess(List<DownloadDTO> result) {
				super.onSuccess(result);
				view.setDownloads(result);
			}
		});
	}

	@Override
	public void searchDownload(int id) {
		ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstants.BASIC_LOADING_EVENT, true);
		this.eventbus.fireEvent(e);
		JOURNAL_SERVICE.getDownload(id, new DefaultAbstractCallBack<DownloadDTO>(e, eventbus) {
			@Override
			public void onSuccess(DownloadDTO result) {
				super.onSuccess(result);
				if (result != null){
					view.displayDownload(result);
				}
			}
		});

	}

	@Override
	public void deleteDownload(int id) {
		ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstants.BASIC_DELETING_EVENT, true);
		this.eventbus.fireEvent(e);
		JOURNAL_SERVICE.delete(id, new DefaultAbstractCallBack<Void>(e, eventbus) {
			@Override
			public void onSuccess(Void v) {
				super.onSuccess(v);
				load();
			}
		});

	}

}
