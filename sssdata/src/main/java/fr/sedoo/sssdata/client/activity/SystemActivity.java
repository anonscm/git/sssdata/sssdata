package fr.sedoo.sssdata.client.activity;

import java.util.Map;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.sedoo.commons.client.callback.LoadCallBack;
import fr.sedoo.sssdata.client.ClientFactory;
import fr.sedoo.sssdata.client.misc.ClientSystemParametersList;
import fr.sedoo.sssdata.client.place.SystemPlace;
import fr.sedoo.sssdata.client.ui.view.api.SystemView;
import fr.sedoo.sssdata.shared.ParameterConstants;

public class SystemActivity extends SSSAdministrationActivity implements LoadCallBack<Map<String, String>>  {


    public SystemActivity(SystemPlace place, ClientFactory clientFactory) {
    	super(clientFactory, place);
    }
    
    private SystemView systemView;

    /**
     * Invoked by the ActivityManager to start a new Activity
     */
    @Override
    public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
    	super.start(containerWidget, eventBus);
    	/*if (isValidUser() == false) {
			goToLoginPlace();
			return;
		}*/
        systemView = ((ClientFactory) clientFactory).getSystemView();
        containerWidget.setWidget(systemView.asWidget());
        /*List<Shortcut> shortcuts = new ArrayList<Shortcut>();
        shortcuts.add(ShortcutFactory.getWelcomeShortcut());
        shortcuts.add(ShortcutFactory.getSystemShortcut());
        eventBus.fireEvent(new BreadCrumbChangeEvent(shortcuts));*/
        ClientSystemParametersList.getParameters(this);
    }

    private void setValues(Map<String, String> parameters)  {
    	systemView.setApplicationVersion(parameters.get(ParameterConstants.APPLICATION_VERSION_PARAMETER_NAME));
    	systemView.setJavaVersion(parameters.get(ParameterConstants.JAVA_VERSION_PARAMETER_NAME));
    	systemView.setTestMode(parameters.get(ParameterConstants.TEST_MODE));
	}

	/**
     * Ask user before stopping this activity
     */
    @Override
    public String mayStop() {
    	return null;
    }

	@Override
	public void postLoadProcess(Map<String, String> result) 
	{
		setValues(result);
	}
}