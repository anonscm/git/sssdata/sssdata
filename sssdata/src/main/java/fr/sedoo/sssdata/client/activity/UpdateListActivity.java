package fr.sedoo.sssdata.client.activity;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.sssdata.client.ClientFactory;
import fr.sedoo.sssdata.client.event.ActionEventConstants;
import fr.sedoo.sssdata.client.message.CommonMessages;
import fr.sedoo.sssdata.client.place.UpdateListPlace;
import fr.sedoo.sssdata.client.service.JournalService;
import fr.sedoo.sssdata.client.service.JournalServiceAsync;
import fr.sedoo.sssdata.client.ui.component.PagedUpdatesTable;
import fr.sedoo.sssdata.client.ui.component.PagedUpdatesTablePresenter;
import fr.sedoo.sssdata.client.ui.view.api.UpdateListView;
import fr.sedoo.sssdata.shared.UpdateDTO;

public class UpdateListActivity extends SSSAdministrationActivity implements PagedUpdatesTablePresenter{

	public final static JournalServiceAsync JOURNAL_SERVICE = GWT.create(JournalService.class);
	
	public final static int PAGE_SIZE = PagedUpdatesTable.PAGE_SIZE;
	public final static int FIRST = 1;
	
	private UpdateListView view;
		
	public UpdateListActivity(UpdateListPlace place, AuthenticatedClientFactory clientFactory) {
		super(clientFactory, place);
	}

	@Override
	public void start(AcceptsOneWidget panel, final EventBus eventBus) {
		super.start(panel, eventBus);
		view = ((ClientFactory) clientFactory).getUpdateListView();
		view.setUpdatesTablePresenter(this);
		panel.setWidget(view.asWidget());
				
		view.reset();
						
		
		final ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstants.BASIC_LOADING_EVENT, true);
		eventBus.fireEvent(e);

		JOURNAL_SERVICE.getUpdateHits(new DefaultAbstractCallBack<Integer>(e, eventBus) {
			@Override
			public void onSuccess(Integer hits) {
				super.onSuccess(hits);
				view.setHits(hits);
				if (hits > 0) {
					eventBus.fireEvent(e);

					JOURNAL_SERVICE.getUpdatesByPage(FIRST, PAGE_SIZE,
							new DefaultAbstractCallBack<List<UpdateDTO>>(e, eventBus) {
								@Override
								public void onSuccess(List<UpdateDTO> items) {
									super.onSuccess(items);
									view.setByRange(FIRST - 1, items);
								}
							});

				}
			}
		});
			
	}

	@Override
	public void loadPageEntries(final int i) {
		final ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstants.BASIC_LOADING_EVENT, true);
		EventBus eventBus = clientFactory.getEventBus();
		eventBus.fireEvent(e);

		JOURNAL_SERVICE.getUpdatesByPage(i, PAGE_SIZE,
				new DefaultAbstractCallBack<List<UpdateDTO>>(e, eventBus) {
					@Override
					public void onSuccess(List<UpdateDTO> items) {
						super.onSuccess(items);
						view.setByRange(i - 1, items);
					}
				});
		
	}

	
	@Override
	public void delete(final UpdateDTO u) {
		ActionStartEvent event = new ActionStartEvent(CommonMessages.INSTANCE.loading() , ActionEventConstants.BASIC_LOADING_EVENT, true);
		eventbus.fireEvent(event);
		JOURNAL_SERVICE.deleteFile(u, new DefaultAbstractCallBack<Void>(event, eventbus) {
			@Override
			public void onSuccess(Void result) {
				super.onSuccess(result);
				loadPageEntries(view.getStartIndex() + 1);
			}
		});
	}

	@Override
	public void view(UpdateDTO u) {
		view.displayUpdate(u);		
	}
}
