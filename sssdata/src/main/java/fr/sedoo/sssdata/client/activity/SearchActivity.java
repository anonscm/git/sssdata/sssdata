package fr.sedoo.sssdata.client.activity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.sedoo.commons.client.callback.LoadCallBack;
import fr.sedoo.commons.client.config.ClientPropertiesList;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.event.BreadCrumbChangeEvent;
import fr.sedoo.commons.client.event.PlaceNavigationEvent;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;
import fr.sedoo.commons.shared.domain.GeographicBoundingBoxDTO;
import fr.sedoo.sssdata.client.ClientFactory;
import fr.sedoo.sssdata.client.event.ActionEventConstants;
import fr.sedoo.sssdata.client.event.FeatureIsSelectedEvent;
import fr.sedoo.sssdata.client.event.FeatureIsSelectedHandler;
import fr.sedoo.sssdata.client.event.HidePopUpEvent;
import fr.sedoo.sssdata.client.event.NoFeatureIsSelectedEvent;
import fr.sedoo.sssdata.client.event.NoFeatureIsSelectedHandler;
import fr.sedoo.sssdata.client.event.NotificationEvent;
import fr.sedoo.sssdata.client.message.CommonMessages;
import fr.sedoo.sssdata.client.misc.ClientBoatList;
import fr.sedoo.sssdata.client.place.SearchPlace;
import fr.sedoo.sssdata.client.place.UserInfoPlace;
import fr.sedoo.sssdata.client.service.SearchTrajectoriesService;
import fr.sedoo.sssdata.client.service.SearchTrajectoriesServiceAsync;
import fr.sedoo.sssdata.client.ui.component.ShortcutFactory;
import fr.sedoo.sssdata.client.ui.view.api.SearchView;
import fr.sedoo.sssdata.shared.BoatDTO;
import fr.sedoo.sssdata.shared.ConfigPropertiesCode;
import fr.sedoo.sssdata.shared.SearchCriterion;
import fr.sedoo.sssdata.shared.Trajectory;

public class SearchActivity extends AbstractActivity implements SearchView.Presenter ,FeatureIsSelectedHandler, NoFeatureIsSelectedHandler, LoadCallBack<ArrayList<BoatDTO>>{

    private fr.sedoo.sssdata.client.ClientFactory clientFactory;
    
    public final static SearchTrajectoriesServiceAsync SEARCH_TRAJECTORIES_SERVICE = GWT.create(SearchTrajectoriesService.class);

    public SearchActivity(SearchPlace place, ClientFactory clientFactory) {
        this.clientFactory = clientFactory;
        if (!eventBusHandlerAdded){
	        this.clientFactory.getEventBus().addHandler(FeatureIsSelectedEvent.TYPE, this);
	        this.clientFactory.getEventBus().addHandler(NoFeatureIsSelectedEvent.TYPE, this);
	        eventBusHandlerAdded = true;
        }
        
    }
    
    private SearchView searchView;
    private EventBus eventbus;
    private static boolean eventBusHandlerAdded = false;

    
    private List<BoatDTO> boatList;
    private BoatDTO currentBoat;
    private int cpt;
    private int size;
    private boolean stop = false;
    SearchCriterion searchCriterion;
    
    /**
     * Invoked by the ActivityManager to start a new Activity
     */
    @Override
    public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
    	
    	this.eventbus = eventBus;
    	searchView = clientFactory.getSearchView();
    	searchView.setPresenter(this);
    	searchView.reset();
        containerWidget.setWidget(searchView.asWidget());
        List<Shortcut> shortcuts = new ArrayList<Shortcut>();
        shortcuts.add(ShortcutFactory.getWelcomeShortcut());
        shortcuts.add(ShortcutFactory.getDataAccessShortcut());
        eventbus.fireEvent(new BreadCrumbChangeEvent(shortcuts));
        
        ClientBoatList.getBoats(this);
    }


	/**
     * Ask user before stopping this activity
     */
    @Override
    public String mayStop() {
    	if (eventbus != null)
    	{
    		eventbus.fireEvent(new HidePopUpEvent());
    	}
    	return null;
    }

   	@Override
	public void search(SearchCriterion searchCriterion) {
   		this.boatList = searchCriterion.getBoatListSelection();
   		this.searchCriterion = searchCriterion;
   		this.cpt = 0;
   		this.stop = false;
   		this.size = boatList.size();
   		if (size > 0){
   			searchView.initFeatureLayerOnMap(eventbus);
   			searchView.initProgressBar(size + 1);
   			searchView.hideSelectTrajectoriesButtons();
   			searchNextBoat();
   		}
   	}
   	
   	private void searchNextBoat(){
   		if ((boatList.size() == 0)){
   			//Fini
   			searchView.updateProgressBar();
   		}else{
   			this.currentBoat = boatList.remove(0);
   			cpt++;
   			
   			//searchView.addMessage("Boat " + cpt + " / " + size + "...");
   			searchView.updateProgressBar();
   			
   			ActionStartEvent event = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstants.BASIC_LOADING_EVENT, true);
   			eventbus.fireEvent(event);
   			//SEARCH_TRAJECTORIES_SERVICE.searchTrajectories(searchCriterion, new DefaultAbstractCallBack<HashMap<Boat,List<Trajectory>>>(event,eventbus) {						
   			SEARCH_TRAJECTORIES_SERVICE.searchTrajectories(currentBoat,searchCriterion, new DefaultAbstractCallBack<List<Trajectory>>(event,eventbus) {

   				@Override
   				public void onSuccess(List<Trajectory> result) {
   					super.onSuccess(result);
   					searchView.addFeatureOnMap(currentBoat, result);
   					//TODO afficher qqch
   					if (result.size()>=1){
   						searchView.showSelectTrajectoriesButtons();
   					}
   					if (stop){
   						searchView.resetProgressBar();
   					}else{
   						searchNextBoat();
   					}
   				}

   				@Override
   				public void onFailure(Throwable caught) {

   				}
   			});
   		}
   	}
	
   	@Override
	public void stop() {
		this.stop=true;
		clientFactory.getEventBus().fireEvent(new NotificationEvent("The download will be stopped after the current boat."));
	}
   	
	@Override
	public void getDefaultCriterion(){
		ClientPropertiesList.getProperties(new LoadCallBack<Map<String, String>>() {
			
			@Override
			public void postLoadProcess(Map<String, String> result) {
				SearchCriterion sc = new SearchCriterion();
				sc.setTemperatureMax(new Double(result.get(ConfigPropertiesCode.TEMPERATURE_MAX)));
				sc.setTemperatureMin(new Double(result.get(ConfigPropertiesCode.TEMPERATURE_MIN)));
				sc.setSalinityMax(new Double(result.get(ConfigPropertiesCode.SALINITY_MAX)));
				sc.setSalinityMin(new Double(result.get(ConfigPropertiesCode.SALINITY_MIN)));
				DateTimeFormat format = DateTimeFormat.getFormat("dd/MM/yyyy");
				sc.setBeginDate(format.parse(StringUtil.trimToEmpty(result.get(ConfigPropertiesCode.DATE_MIN))));
				
				if (StringUtil.isEmpty(result.get(ConfigPropertiesCode.DATE_MAX))){
					sc.setEndDate(new Date());	
				}else{
					sc.setEndDate(format.parse(result.get(ConfigPropertiesCode.DATE_MAX)));	
				}
								
				GeographicBoundingBoxDTO bbox = new GeographicBoundingBoxDTO();
				bbox.setEastBoundLongitude("");
				bbox.setWestBoundLongitude("");
				bbox.setSouthBoundLatitude("");
				bbox.setNorthBoundLatitude("");
				sc.setBoundingBox(bbox);
				sc.setBoatListSelection(new ArrayList<BoatDTO>());
				searchView.setSearchCriterion(sc);
			}
		});


	}


	@Override
	public void onNoFeatureIsSelected(NoFeatureIsSelectedEvent event) {
		searchView.hideDowloadButton();
	}


	@Override
	public void onFeatureIsSelected(FeatureIsSelectedEvent event) {
		searchView.showDowloadButton();
		
	}


	@Override
	public void postLoadProcess(ArrayList<BoatDTO> result) {
		
		searchView.setBoatList(result);
		
	}

	@Override
	public void downloadFromSearchCriterions(SearchCriterion searchCriterion,
			List<String> allSelectedTrajectories) {
		// New event puis setParameters
		UserInfoPlace userInfoPlace = new UserInfoPlace();
		userInfoPlace.setAllSelectedTrajectories(allSelectedTrajectories);
		userInfoPlace.setSearchCriterion(searchCriterion);
		eventbus.fireEvent(new PlaceNavigationEvent(userInfoPlace));
	}

	@Override
	public void downloadPretreatedArchive(String archiveType) {
		UserInfoPlace userInfoPlace = new UserInfoPlace();
		userInfoPlace.setPretreatedArchiveType(archiveType);
		eventbus.fireEvent(new PlaceNavigationEvent(userInfoPlace));
	}
}