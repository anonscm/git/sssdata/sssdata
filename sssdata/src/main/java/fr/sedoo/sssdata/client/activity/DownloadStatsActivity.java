package fr.sedoo.sssdata.client.activity;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.sssdata.client.ClientFactory;
import fr.sedoo.sssdata.client.event.ActionEventConstants;
import fr.sedoo.sssdata.client.message.CommonMessages;
import fr.sedoo.sssdata.client.place.DownloadStatsPlace;
import fr.sedoo.sssdata.client.service.StatsService;
import fr.sedoo.sssdata.client.service.StatsServiceAsync;
import fr.sedoo.sssdata.client.ui.view.api.DownloadStatsView;
import fr.sedoo.sssdata.client.ui.view.api.DownloadStatsView.Presenter;
import fr.sedoo.sssdata.shared.StatsDTO;

public class DownloadStatsActivity extends SSSAdministrationActivity implements Presenter {

	public final static StatsServiceAsync STATS_SERVICE = GWT.create(StatsService.class);

	DownloadStatsView view;

	public DownloadStatsActivity(DownloadStatsPlace place, AuthenticatedClientFactory clientFactory) {
		super(clientFactory, place);

	}
	
	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		super.start(containerWidget, eventBus);

		if (isValidUser()) {
			view = ((ClientFactory) clientFactory).getDownloadStatsView();
			view.setPresenter(this);

			containerWidget.setWidget(view.asWidget());

			//TODO Récupérer les stats
			final ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstants.BASIC_LOADING_EVENT, true);
			eventbus.fireEvent(e);
			STATS_SERVICE.getDownloadStats(new DefaultAbstractCallBack<StatsDTO>(e, eventbus){

				@Override
				public void onSuccess(StatsDTO result) {
					super.onSuccess(result);
					if (result != null){
						view.setStats(result);
						view.display();
					}else{
						//TODO si pas de résultat ?
					}
				}
			});

		}
	}
}