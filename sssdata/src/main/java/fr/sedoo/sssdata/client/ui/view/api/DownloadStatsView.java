package fr.sedoo.sssdata.client.ui.view.api;

import com.google.gwt.user.client.ui.IsWidget;

import fr.sedoo.sssdata.shared.StatsDTO;

public interface DownloadStatsView extends IsWidget {

	void setPresenter(Presenter presenter);

	void setStats(StatsDTO stats);
	
	void display();
	
	public interface Presenter{

	}
	
}