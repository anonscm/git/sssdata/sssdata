package fr.sedoo.sssdata.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fr.sedoo.sssdata.shared.metadata.DoiInfos;
import fr.sedoo.sssdata.shared.metadata.HistoryItem;

@RemoteServiceRelativePath("doiHistory")
public interface DoiHistoryService extends RemoteService {
	DoiInfos getDoiInfos(String doiSuffix)throws ServiceException;
	//ArrayList<HistoryItem> findAll() throws ServiceException;
	void delete(HistoryItem item) throws ServiceException;
	HistoryItem edit(HistoryItem item, String doiSuffix) throws ServiceException;
	HistoryItem create(HistoryItem item, String doiSuffix) throws ServiceException;
}
