package fr.sedoo.sssdata.client.ui.component.impl.menu;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.dom.client.Style.Visibility;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.PlaceChangeEvent;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.AbstractImagePrototype;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.MenuItem;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.cms.place.CMSConsultPlace;
import fr.sedoo.commons.client.component.impl.menu.PlaceCommand;
import fr.sedoo.commons.client.contact.message.ContactMessages;
import fr.sedoo.commons.client.contact.place.ContactPlace;
import fr.sedoo.commons.client.event.UserLoginEvent;
import fr.sedoo.commons.client.event.UserLoginEventHandler;
import fr.sedoo.commons.client.event.UserLogoutEvent;
import fr.sedoo.commons.client.event.UserLogoutEventHandler;
import fr.sedoo.commons.client.faq.place.FaqConsultPlace;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;
import fr.sedoo.sssdata.client.GlobalBundle;
import fr.sedoo.sssdata.client.SssData;
import fr.sedoo.sssdata.client.message.ApplicationMessages;
import fr.sedoo.sssdata.client.place.GriddedProductMetadataPlace;
import fr.sedoo.sssdata.client.place.MetadataPlace;
import fr.sedoo.sssdata.client.place.SearchPlace;
import fr.sedoo.sssdata.client.place.WelcomePlace;
import fr.sedoo.sssdata.client.ui.component.api.HorizontalMenu;
import fr.sedoo.sssdata.shared.GriddedProduct;


public class HorizontalMenuImpl extends Composite implements HorizontalMenu, UserLoginEventHandler, UserLogoutEventHandler{

	@UiField
	MenuBar menu;

	@UiField 
	HorizontalPanel contentPanel;
	
	
	@UiField Anchor signOutLink;
	@UiField Anchor signInLink;

	@UiField SpanElement userName;
	
	
	MenuItem menuAdmin;
	
	private static StatusBarImplUiBinder uiBinder = GWT
			.create(StatusBarImplUiBinder.class);

	interface StatusBarImplUiBinder extends UiBinder<Widget, HorizontalMenuImpl> {
	}

	public HorizontalMenuImpl(EventBus eventBus) {
		super();
		GWT.<GlobalBundle>create(GlobalBundle.class).css().ensureInjected();
		initWidget(uiBinder.createAndBindUi(this));
		//menu.addItem(AbstractImagePrototype.create(GlobalBundle.INSTANCE.home()).getSafeHtml(),new PlaceCommand(eventBus, new WelcomePlace()));
		
		menu.addItem(createSafeHtml(GlobalBundle.INSTANCE.home(), CommonMessages.INSTANCE.home()),new PlaceCommand(eventBus, new WelcomePlace()));	
		//menu.addItem(createSafeHtml(GlobalBundle.INSTANCE.home(), CommonMessages.INSTANCE.home()),new PlaceCommand(eventBus, new CMSConsultPlace(ScreenNames.HOME)));
		menu.addItem(ApplicationMessages.INSTANCE.dataMenuTitle(), new PlaceCommand(eventBus, new SearchPlace()));
		menu.addItem(ApplicationMessages.INSTANCE.metadataTitle(), new PlaceCommand(eventBus, new MetadataPlace()));
		//menu.addItem(ApplicationMessages.INSTANCE.policyMenuTitle(), new PlaceCommand(eventBus, new DataPolicyPlace()));
		menu.addItem(ApplicationMessages.INSTANCE.policyMenuTitle(), new PlaceCommand(eventBus, new CMSConsultPlace(ScreenNames.DATA_POLICY)));
		
		menu.addItem(ApplicationMessages.INSTANCE.gridHist(), new PlaceCommand(eventBus, new GriddedProductMetadataPlace(GriddedProduct.SSS_HIST)));
		
		menu.addItem(new MenuItem(ApplicationMessages.INSTANCE.gridMenuTitle(), new GriddedProductsMenu(eventBus)));
		
		this.menuAdmin = new MenuItem(ApplicationMessages.INSTANCE.administrationMenuTitle(), new HorizontalAdministrationMenu(eventBus));
		menu.addItem(menuAdmin);
		menuAdmin.setVisible(false);
		
		menu.addItem(ApplicationMessages.INSTANCE.faqManagementMenuTitle(), new PlaceCommand(eventBus, new FaqConsultPlace()));
		//menu.addItem(createSafeHtml(GlobalBundle.INSTANCE.help(), FaqMessages.INSTANCE.faqConsultationViewTitle()), new PlaceCommand(eventBus, new FaqConsultPlace()));
		menu.addItem(createSafeHtml(GlobalBundle.INSTANCE.contact(), ContactMessages.INSTANCE.title()), new PlaceCommand(eventBus, new ContactPlace()));
		
		//menu.addItem(createSafeHtml(GlobalBundle.INSTANCE.help(), CommonMessages.INSTANCE.help()), new HelpMenu(eventBus));
	    menu.setAutoOpen(true);
	    
	    eventBus.addHandler(UserLoginEvent.TYPE, this);
	    eventBus.addHandler(UserLogoutEvent.TYPE, this);
			    
	}
	
	private SafeHtml createSafeHtml(ImageResource resource, String tootltip) 
	{
		Image aux = AbstractImagePrototype.create(resource).createImage();
		aux.setTitle(tootltip);
		return SafeHtmlUtils.fromTrustedString(aux.toString());
	}
	@UiHandler("signInLink")
	void onSignInClicked(ClickEvent event) {
		 SssData.getClientFactory().getEventBus().fireEvent(new PlaceChangeEvent(((AuthenticatedClientFactory) SssData.getClientFactory()).getLoginPlace()));
	}

	@UiHandler("signOutLink")
	void onSignOutClicked(ClickEvent event) {
		SssData.getClientFactory().getEventBus().fireEvent(new UserLogoutEvent());
		SssData.getClientFactory().getEventBus().fireEvent(new PlaceChangeEvent(new WelcomePlace()));
	}

	@Override
	public void onNotification(UserLoginEvent event) {
		if (event.getUser().isAdmin()){
			menuAdmin.setVisible(true);
		}
		
		signInLink.setVisible(false);
		signOutLink.setVisible(true);
		userName.setInnerHTML(event.getUser().getName());
		userName.getStyle().setVisibility(Visibility.VISIBLE);
		
	}

	@Override
	public void onNotification(UserLogoutEvent event) {
		menuAdmin.setVisible(false);
		
		signInLink.setVisible(true);
		signOutLink.setVisible(false);
		userName.setInnerHTML("");
		userName.getStyle().setVisibility(Visibility.HIDDEN);
	}
	
	@Override
	public double getPreferredHeight() {
		return 30;
	}

}
