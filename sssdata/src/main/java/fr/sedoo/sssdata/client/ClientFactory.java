package fr.sedoo.sssdata.client;

import fr.sedoo.commons.client.analytics.GAEventHandler;
import fr.sedoo.commons.client.config.ConfigClientFactory;
import fr.sedoo.commons.client.contact.mvp.ContactClientFactory;
import fr.sedoo.commons.client.faq.mvp.FaqClientFactory;
import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;
import fr.sedoo.commons.client.mvp.CMSClientFactory;
import fr.sedoo.commons.client.mvp.CommonClientFactory;
import fr.sedoo.commons.client.news.mvp.MessageClientFactory;
import fr.sedoo.commons.client.news.mvp.NewsClientFactory;
import fr.sedoo.sssdata.client.ui.component.api.NavigationBar;
import fr.sedoo.sssdata.client.ui.component.api.StatusBar;
import fr.sedoo.sssdata.client.ui.view.api.AdministratorManagementView;
import fr.sedoo.sssdata.client.ui.view.api.DoiHistoryManagementView;
import fr.sedoo.sssdata.client.ui.view.api.DownloadLogView;
import fr.sedoo.sssdata.client.ui.view.api.DownloadStatsView;
import fr.sedoo.sssdata.client.ui.view.api.FileSearchView;
import fr.sedoo.sssdata.client.ui.view.api.GriddedProductView;
import fr.sedoo.sssdata.client.ui.view.api.HeaderView;
import fr.sedoo.sssdata.client.ui.view.api.ImportView;
import fr.sedoo.sssdata.client.ui.view.api.LoginView;
import fr.sedoo.sssdata.client.ui.view.api.MetadataView;
import fr.sedoo.sssdata.client.ui.view.api.SearchView;
import fr.sedoo.sssdata.client.ui.view.api.StatsView;
import fr.sedoo.sssdata.client.ui.view.api.SystemView;
import fr.sedoo.sssdata.client.ui.view.api.UpdateListView;
import fr.sedoo.sssdata.client.ui.view.api.UserContactView;
import fr.sedoo.sssdata.client.ui.view.api.UserInfoView;
import fr.sedoo.sssdata.client.ui.view.api.UserStatsView;
import fr.sedoo.sssdata.client.ui.view.api.WelcomeView;

public interface ClientFactory extends CommonClientFactory, AuthenticatedClientFactory, MessageClientFactory, 
	NewsClientFactory, FaqClientFactory, CMSClientFactory, ContactClientFactory, ConfigClientFactory, GAEventHandler{

	WelcomeView getWelcomeView();
	SystemView getSystemView();
	AdministratorManagementView getAdministratorManagementView();
	SearchView getSearchView();
	StatsView getStatsView();
	UserStatsView getUserStatsView();
	DownloadStatsView getDownloadStatsView();
	HeaderView getHeaderView();
	StatusBar getStatusBar();
	NavigationBar getNavigationBar();
	ImportView getImportView();
	UserInfoView getUserInfoView();
	LoginView getLoginView();
	UserContactView getUserContactView();
	FileSearchView getFileSearchView();
	DownloadLogView getDownloadLogView();
	UpdateListView getUpdateListView();
	MetadataView getMetadataView();
	DoiHistoryManagementView getDoiHistoryManagementView();
	GriddedProductView getGriddedProductView();

}
