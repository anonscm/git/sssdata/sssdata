package fr.sedoo.sssdata.client.misc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;

import fr.sedoo.commons.client.callback.LoadCallBack;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.sssdata.client.SssData;
import fr.sedoo.sssdata.client.event.ActionEventConstants;
import fr.sedoo.sssdata.client.message.CommonMessages;
import fr.sedoo.sssdata.client.service.ParameterService;
import fr.sedoo.sssdata.client.service.ParameterServiceAsync;

public class ClientSystemParametersList {

	private static Map<String,String> parameters = new HashMap<String,String>();
	private static boolean loaded = false;
	private static List<LoadCallBack<Map<String,String>>> callBacks = new ArrayList<LoadCallBack< Map<String,String>>>();
	private final static ParameterServiceAsync PARAM_SERVICE = GWT.create(ParameterService.class);
	
	
	public static void getParameters(LoadCallBack<Map<String,String>> callBack) {
		if (loaded){
			callBack.postLoadProcess(parameters);
		}else{
			callBacks.add(callBack);
		}
	}
	
	public static void loadParameters()
	{
		EventBus eventBus = SssData.getClientFactory().getEventBus();
		ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstants.BASIC_LOADING_EVENT, true);
		eventBus.fireEvent(e);
		PARAM_SERVICE.getSystemParameters(new DefaultAbstractCallBack<Map<String,String>>(e, eventBus){
	      	
	      	@Override
				public void onSuccess(Map<String,String> params) {
					super.onSuccess(params);
					ClientSystemParametersList.parameters = params;
					Iterator<LoadCallBack<Map<String,String>>> iterator = callBacks.iterator();
					while (iterator.hasNext()) {
						LoadCallBack<Map<String,String>> loadCallBack = (LoadCallBack<Map<String,String>>) iterator.next();
						loadCallBack.postLoadProcess(params);
					}
					loaded = true;
					callBacks.clear();
				}
	      	
	      });	
	}
	  
	
	
}
