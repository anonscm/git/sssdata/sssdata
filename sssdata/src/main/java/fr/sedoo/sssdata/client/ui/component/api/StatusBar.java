package fr.sedoo.sssdata.client.ui.component.api;

import fr.sedoo.commons.client.event.ActionEndEventHandler;
import fr.sedoo.commons.client.event.ActionStartEventHandler;
import fr.sedoo.sssdata.client.event.ParameterLoadedEventHandler;
import fr.sedoo.sssdata.client.ui.widget.PreferredHeightWidget;

public interface StatusBar extends ParameterLoadedEventHandler, ActionEndEventHandler, ActionStartEventHandler, PreferredHeightWidget 
{

}