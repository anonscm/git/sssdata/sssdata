package fr.sedoo.sssdata.client.ui.view.api;

import java.util.ArrayList;

import com.google.gwt.editor.client.Editor;
import com.google.gwt.user.client.ui.IsWidget;

import fr.sedoo.sssdata.shared.BoatDTO;
import fr.sedoo.sssdata.shared.UserContactInformations;

public interface UserContactView extends IsWidget, Editor<UserContactInformations> {

	void reset();
	
	void setPresenter(Presenter presenter);
	
	void addMessage(String msg);
	void addErrorMessage(String msg);
	void resetMessages();
	
	public interface Presenter{
		void sendMessage(UserContactInformations infos);
		void addNews(String title, String body);
		void getDefaultCriterion();
	}

	void setBoatList(ArrayList<BoatDTO> boats);

	void setSearchCriterion(UserContactInformations infos);

	
	
}
