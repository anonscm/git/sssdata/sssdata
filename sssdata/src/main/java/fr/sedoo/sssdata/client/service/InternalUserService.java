package fr.sedoo.sssdata.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fr.sedoo.commons.client.user.AuthenticatedUser;
import fr.sedoo.commons.client.util.ServiceException;

@RemoteServiceRelativePath("login")
public interface InternalUserService extends RemoteService {
	AuthenticatedUser internalLogin(String login, String password) throws ServiceException;

}
