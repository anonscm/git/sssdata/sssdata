package fr.sedoo.sssdata.client.ui.component.impl.menu;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.MenuBar;

import fr.sedoo.commons.client.component.impl.menu.PlaceCommand;
import fr.sedoo.commons.client.contact.message.ContactMessages;
import fr.sedoo.commons.client.contact.place.ContactPlace;
import fr.sedoo.commons.client.faq.bundle.FaqMessages;
import fr.sedoo.commons.client.faq.place.FaqConsultPlace;

public class HelpMenu extends MenuBar{
	private EventBus eventBus;
	
	public HelpMenu(EventBus eventBus){
		super(true);
		this.eventBus = eventBus;
		initContent();
	}

	private void initContent() {
		//addItem(ApplicationMessages.INSTANCE.instructions(), new PlaceCommand(eventBus, new CMSConsultPlace(ScreenNames.INSTRUCTIONS)));
		addItem(FaqMessages.INSTANCE.faqConsultationViewTitle(), new PlaceCommand(eventBus, new FaqConsultPlace()));
		addItem(ContactMessages.INSTANCE.title(), new PlaceCommand(eventBus, new ContactPlace()));
		//addSeparator();
		//addItem(CommonMessages.INSTANCE.about(), new PlaceCommand(eventBus, new AboutPlace()));
	}
		
}
