package fr.sedoo.sssdata.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class AdministrationPlace extends Place
{
	public static AdministrationPlace instance;
	
	public AdministrationPlace()
	{
	}

	public static class Tokenizer implements PlaceTokenizer<AdministrationPlace>
	{
		
		public AdministrationPlace getPlace(String token) {
			if (instance == null)
			{
				instance = new AdministrationPlace();
			}
			return instance;
		}

		public String getToken(AdministrationPlace place) {
			return "";
		}

		
	}
}