package fr.sedoo.sssdata.client.ui.view.api;

import com.google.gwt.user.client.ui.IsWidget;

import fr.sedoo.sssdata.shared.UserStatsDTO;

public interface UserStatsView extends IsWidget {

	void setPresenter(Presenter presenter);

	void setStats(UserStatsDTO stats);
	
	void display();
	
	public interface Presenter{

	}
	
}
