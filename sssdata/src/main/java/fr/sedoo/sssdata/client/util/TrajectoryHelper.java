package fr.sedoo.sssdata.client.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.gwtopenmaps.openlayers.client.Map;
import org.gwtopenmaps.openlayers.client.Projection;
import org.gwtopenmaps.openlayers.client.Style;
import org.gwtopenmaps.openlayers.client.control.SelectFeature;
import org.gwtopenmaps.openlayers.client.control.SelectFeature.ClickFeatureListener;
import org.gwtopenmaps.openlayers.client.control.SelectFeatureOptions;
import org.gwtopenmaps.openlayers.client.event.VectorFeatureSelectedListener;
import org.gwtopenmaps.openlayers.client.event.VectorFeatureUnselectedListener;
import org.gwtopenmaps.openlayers.client.feature.VectorFeature;
import org.gwtopenmaps.openlayers.client.geometry.LineString;
import org.gwtopenmaps.openlayers.client.geometry.Point;
import org.gwtopenmaps.openlayers.client.layer.Layer;
import org.gwtopenmaps.openlayers.client.layer.Vector;
import org.gwtopenmaps.openlayers.client.util.Attributes;
import org.gwtopenmaps.openlayers.client.util.JSObject;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.i18n.client.NumberFormat;

import fr.sedoo.commons.client.widget.map.impl.AreaSelectorWidget;
import fr.sedoo.sssdata.client.event.FeatureIsSelectedEvent;
import fr.sedoo.sssdata.client.event.HidePopUpEvent;
import fr.sedoo.sssdata.client.event.NoFeatureIsSelectedEvent;
import fr.sedoo.sssdata.client.event.ShowPopUpEvent;
import fr.sedoo.sssdata.shared.BoatDTO;
import fr.sedoo.sssdata.shared.LatLong;
import fr.sedoo.sssdata.shared.Trajectory;

public class TrajectoryHelper {

	private Style defaultStyle;
	private Style hoverStyle;
	private Style selectedStyle;
	private EventBus eventBus;
	
	private Vector trajectoryLayer;
	/*private Vector allTrajectoriesLayer;
	private List<VectorFeature> selectedFeatures;
	private List<VectorFeature> drawnFeatures;*/

	public TrajectoryHelper(EventBus eventBus) {

		defaultStyle = new Style();
		defaultStyle.setStrokeColor("#FF0000");
		defaultStyle.setStrokeWidth(2);
		defaultStyle.setPointRadius(1);
		defaultStyle.setCursor("default");
				
		hoverStyle = new Style();
		hoverStyle.setStrokeColor("#FFFF00");
		hoverStyle.setStrokeWidth(2);
		hoverStyle.setPointRadius(1);
		hoverStyle.setCursor("pointer");

		selectedStyle = new Style();
		selectedStyle.setStrokeColor("#00FF00");
		selectedStyle.setStrokeWidth(2);
		selectedStyle.setPointRadius(1);
		selectedStyle.setCursor("pointer");
/*
		selectedFeatures = new ArrayList<VectorFeature>();
		drawnFeatures = new ArrayList<VectorFeature>();
*/
		this.eventBus = eventBus;
	}

	/**
	 * Ajoute les trajectoires d'un bateau.
	 * @param boat
	 * @param trajectories
	 * @param boundingBox
	 */
	public void addTrajectories(BoatDTO boat, List<Trajectory> trajectories,
			AreaSelectorWidget boundingBox) {
		
		if (trajectoryLayer == null){
			return;
		}
		
		if (boat != null & !trajectories.isEmpty()) {

			/*Vector newTrajectoryLayer = new Vector("trajectoryLayer-"+boat.getName());
			boundingBox.getMap().addLayer(newTrajectoryLayer);*/
			 
			Projection p = new Projection(boundingBox.getMap().getProjection());
			Projection etalon = new Projection("EPSG:4326");

			/*SelectFeature hoverTrajectorySelectFeature = null;

			TrajectoryLayerListener trajectoryLayerListener = new TrajectoryLayerListener(
					newTrajectoryLayer, boundingBox);
			SelectFeatureOptions trajectoryHoverFeatureOptions = new SelectFeatureOptions();
			trajectoryHoverFeatureOptions.setHover();
			trajectoryHoverFeatureOptions.clickFeature(trajectoryLayerListener);
			hoverTrajectorySelectFeature = new SelectFeature(newTrajectoryLayer,
					trajectoryHoverFeatureOptions);
			newTrajectoryLayer
			.addVectorFeatureSelectedListener(trajectoryLayerListener);
			newTrajectoryLayer
			.addVectorFeatureUnselectedListener(trajectoryLayerListener);

			boundingBox.getMap().addControl(hoverTrajectorySelectFeature);
			hoverTrajectorySelectFeature.activate();*/

			int trajId = 1;
			
			for (Trajectory t : trajectories) {
				for (List<LatLong> line : t.getLines()) {
					ArrayList<Point> linePoints = new ArrayList<Point>();
					for (LatLong coord : line) {
						Point point = new Point(NumberFormat.getDecimalFormat()
								.parse(coord.getLon() + ""), NumberFormat
								.getDecimalFormat().parse(coord.getLat() + ""));
						point.transform(etalon, p);
						linePoints.add(point);				
					}

					
					VectorFeature lineFeature;
					if (linePoints.size() == 1){
						lineFeature = new VectorFeature(linePoints.get(0), defaultStyle);
					}else{
						// LineString constructor take a Point[] in parameter so
						// convert ArrayList<Point> to Point[]
						Point pointTab[] = new Point[linePoints.size()];
						final LineString lineString = new LineString(
								linePoints.toArray(pointTab));

						//final
						lineFeature = new VectorFeature(
								lineString, defaultStyle);
					}
								
					
					Attributes attributes = lineFeature.getAttributes();
					attributes.setAttribute("boat", boat.getName());
					attributes.setAttribute("sssFile", t.getSssFileName());
					attributes.setAttribute("resultSize", t.getResultSize());

					displayedFiles.add(t.getSssFileName());
					
					//lineFeature.setFeatureId(boat.getName() + "::"+trajId);
					lineFeature.setFeatureId(t.getSssFileName());

					trajectoryLayer.addFeature(lineFeature);
					//drawnFeatures.add(lineFeature);

					trajectoryLayer.drawFeature(lineFeature, defaultStyle);
					trajId++;

				}
			}
					
			trajectoryLayer.redraw();
			
		}
	}
	
	/**
	 * Détruit la couche des trajectoires et en crée une nouvelle.
	 * @param boundingBox
	 */
	public void resetTrajectories(AreaSelectorWidget boundingBox){
		destroyOldTrajectoryLayer(boundingBox.getMap());
		
		displayedFiles = new HashSet<String>();
		selectedFiles = new HashSet<String>();
		
		this.trajectoryLayer = new Vector("trajectoryLayer");
		boundingBox.getMap().addLayer(trajectoryLayer);
		 
		/*Projection p = new Projection(boundingBox.getMap().getProjection());
		Projection etalon = new Projection("EPSG:4326");*/

		SelectFeature hoverTrajectorySelectFeature = null;

		TrajectoryLayerListener trajectoryLayerListener = new TrajectoryLayerListener(
				trajectoryLayer, boundingBox);
		SelectFeatureOptions trajectoryHoverFeatureOptions = new SelectFeatureOptions();
		trajectoryHoverFeatureOptions.setHover();
		trajectoryHoverFeatureOptions.clickFeature(trajectoryLayerListener);
		hoverTrajectorySelectFeature = new SelectFeature(trajectoryLayer,
				trajectoryHoverFeatureOptions);
		trajectoryLayer.addVectorFeatureSelectedListener(trajectoryLayerListener);
		trajectoryLayer.addVectorFeatureUnselectedListener(trajectoryLayerListener);

		boundingBox.getMap().addControl(hoverTrajectorySelectFeature);
		hoverTrajectorySelectFeature.activate();
		
		trajectoryLayer.redraw();
		
		boundingBox.pushMapToBack();
	}
			
			
	
/*	public void addTrajectories(
			HashMap<Boat, List<Trajectory>> trajectories,
			AreaSelectorWidget boundingBox) {
		
		Map map = boundingBox.getMap();
		//Destroy old trajectories
		for (Layer l: map.getLayers()){
			if (!l.isBaseLayer()){
				map.removeLayer(l);
			}
		}
	
		for (Entry<Boat, List<Trajectory>> boat : trajectories.entrySet()) {
			addTrajectories(boat.getKey(), boat.getValue(), boundingBox);
		}
		
		boundingBox.pushMapToBack();
		
	}*/
	
	
	/*public void addTrajectoriesOld(
			HashMap<Boat, List<Trajectory>> trajectories,
			AreaSelectorWidget boundingBox) {

		destroyOldTrajectoryLayer(boundingBox.getMap());

		// create new trajectory layer
		
		
		Vector newTrajectoryLayer = new Vector("trajectoryLayer");
		boundingBox.getMap().addLayer(newTrajectoryLayer);
		ArrayList<Point> linePoints = new ArrayList<Point>();

		Projection p = new Projection(boundingBox.getMap().getProjection());
		Projection etalon = new Projection("EPSG:4326");

		SelectFeature hoverTrajectorySelectFeature = null;

		TrajectoryLayerListener trajectoryLayerListener = new TrajectoryLayerListener(
				newTrajectoryLayer, boundingBox);
		SelectFeatureOptions trajectoryHoverFeatureOptions = new SelectFeatureOptions();
		trajectoryHoverFeatureOptions.setHover();
		trajectoryHoverFeatureOptions.clickFeature(trajectoryLayerListener);
		hoverTrajectorySelectFeature = new SelectFeature(newTrajectoryLayer,
				trajectoryHoverFeatureOptions);
		newTrajectoryLayer
				.addVectorFeatureSelectedListener(trajectoryLayerListener);
		newTrajectoryLayer
				.addVectorFeatureUnselectedListener(trajectoryLayerListener);

		boundingBox.getMap().addControl(hoverTrajectorySelectFeature);
		hoverTrajectorySelectFeature.activate();

		for (Entry<Boat, List<Trajectory>> boat : trajectories.entrySet()) {

			if (boat.getKey() != null) {

				int trajId = 1;
								
				for (Trajectory t : boat.getValue()) {
					linePoints = new ArrayList<Point>();
										
					Point point = null;
					for (LatLong coord : t.getLine()) {
						point = new Point(NumberFormat.getDecimalFormat()
								.parse(coord.getLon() + ""), NumberFormat
								.getDecimalFormat().parse(coord.getLat() + ""));
						point.transform(etalon, p);
						linePoints.add(point);

						if (linePoints.size() == 3){
							
							Point pointTab[] = new Point[linePoints.size()];
							final LineString lineString = new LineString(
									linePoints.toArray(pointTab));

							final VectorFeature lineFeature = new VectorFeature(
									lineString, defaultStyle);

							Attributes attributes = lineFeature.getAttributes();

							attributes.setAttribute("boat", boat.getKey().getName());
							attributes.setAttribute("sssFile", t.getSssFileName());
							
							lineFeature.setFeatureId(boat.getKey().getName() + "::"+trajId);
							System.out.println("Feature: " + boat.getKey().getName() + "::"+trajId);
							newTrajectoryLayer.addFeature(lineFeature);
							drawnFeatures.add(lineFeature);

							newTrajectoryLayer.drawFeature(lineFeature, defaultStyle);
							trajId++;
							
							linePoints = new ArrayList<Point>();
						}
						
						
					}

					// LineString constructor take a Point[] in parameter so
					// convert ArrayList<Point> to Point[]
					Point pointTab[] = new Point[linePoints.size()];
					final LineString lineString = new LineString(
							linePoints.toArray(pointTab));

					final VectorFeature lineFeature = new VectorFeature(
							lineString, defaultStyle);

					Attributes attributes = lineFeature.getAttributes();

					attributes.setAttribute("boat", boat.getKey().getName());
					
					
					
					lineFeature.setFeatureId(boat.getKey().getName() + "::"+trajId);

					newTrajectoryLayer.addFeature(lineFeature);
					drawnFeatures.add(lineFeature);

					newTrajectoryLayer.drawFeature(lineFeature, defaultStyle);
					trajId++;
				}

			}
		}

		newTrajectoryLayer.redraw();

		boundingBox.pushMapToBack();

	}*/

	public void destroyOldTrajectoryLayer(Map map) {

		// Destroy old trajectory layer
		Layer oldTrajectoryLayer = map.getLayerByName("trajectoryLayer");
		
		//JSObject j_map = map.getJSObject();
		
		if (oldTrajectoryLayer != null) {
			//oldTrajectoryLayer.destroy(true);
			map.removeLayer(oldTrajectoryLayer);
			
//			JSObject layer = oldTrajectoryLayer.getJSObject();
//			destroyLayer(layer);
		}
		
		//selectedFeatures.clear();
	}
	
	// UNUSED
	public native void destroyLayer(JSObject layer)/*-{
	    layer.removeAllFeatures();
	}-*/;

	public class TrajectoryLayerListener implements VectorFeatureSelectedListener,
			VectorFeatureUnselectedListener, ClickFeatureListener {

		//private AreaSelectorWidget boundingBox;
		//private Vector trajectoryLayer;

		//private boolean toBeRedrawn;

		//private boolean selected = false;
		
		TrajectoryLayerListener(Vector trajectoryLayer,
				AreaSelectorWidget boundingBox) {
			//this.trajectoryLayer = trajectoryLayer;
			//this.boundingBox = boundingBox;
			//allTrajectoriesLayer = trajectoryLayer;
		}

		@Override
		public void onFeatureSelected(FeatureSelectedEvent eventObject) {
			String sssFile = eventObject.getVectorFeature().getAttributes().getAttributeAsString("sssFile");
			
			int size = eventObject.getVectorFeature().getAttributes().getAttributeAsInt("resultSize");
			
			String boatName = eventObject.getVectorFeature().getAttributes().getAttributeAsString("boat");
			if (!isSelected(sssFile)){
				updateStyle(sssFile, hoverStyle);
				/*for (VectorFeature f : trajectoryLayer.getFeatures()){
					f.setStyle(hoverStyle);
				}
				trajectoryLayer.redraw();*/
				
			}
			
			/*
			// if the feature has NOT BEEN selected, change color
			if (!featureSelected(eventObject.getVectorFeature())) {
				eventObject.getVectorFeature().setStyle(hoverStyle);
				trajectoryLayer.redraw();
			}*/
			eventBus.fireEvent(new ShowPopUpEvent(boatName + " - " + sssFile + " - " + size + " points"));
		}

		@Override
		public void onFeatureUnselected(FeatureUnselectedEvent eventObject) {
			String sssFile = eventObject.getVectorFeature().getAttributes().getAttributeAsString("sssFile");
			
			if (!isSelected(sssFile)){
				updateStyle(sssFile, defaultStyle);
				/*for (VectorFeature f : trajectoryLayer.getFeatures()){
					f.setStyle(defaultStyle);
				}
				trajectoryLayer.redraw();*/
			}
			/*
			// if the feature has NOT BEEN selected, change color back to default
			if (!featureSelected(eventObject.getVectorFeature())) {
				eventObject.getVectorFeature().setStyle(defaultStyle);
				trajectoryLayer.redraw();
			}*/
			eventBus.fireEvent(new HidePopUpEvent());
		}

		
		
		
		@Override
		public void onFeatureClicked(VectorFeature vectorFeature) {
			String sssFile = vectorFeature.getAttributes().getAttributeAsString("sssFile");
			if (isSelected(sssFile)){
				updateStyle(sssFile, defaultStyle);
				selectedFiles.remove(sssFile);
				/*for (VectorFeature f : trajectoryLayer.getFeatures()){
						f.setStyle(defaultStyle);
				}*/
			}else{
				updateStyle(sssFile, selectedStyle);
				selectedFiles.add(sssFile);
				/*for (VectorFeature f : trajectoryLayer.getFeatures()){
					f.setStyle(selectedStyle);
				}*/
			}
			
						
			//this.selected = !this.selected;
			
			/*
			// if the feature clicked was selected, so unselect it
			if (featureSelected(vectorFeature)) {
				removeFeature(vectorFeature);
				vectorFeature.setStyle(defaultStyle);
				trajectoryLayer.redraw();
				
			// if the feature clicked wasn't select then select it, 
			} else {
				selectedFeatures.add(vectorFeature);
				vectorFeature.setStyle(selectedStyle);
				trajectoryLayer.redraw();
			}
			
			if (selectedFeatures.size()>0){
				eventBus.fireEvent(new FeatureIsSelectedEvent());
			}
			
			if (selectedFeatures.size()==0){
				eventBus.fireEvent(new NoFeatureIsSelectedEvent());
			}
			*/
			
			if (selectedFiles.size() > 0){
				eventBus.fireEvent(new FeatureIsSelectedEvent());
			}else{
				eventBus.fireEvent(new NoFeatureIsSelectedEvent());
			}
			
		}
		
		

	}
	
	/*public boolean featureSelected(VectorFeature vectorFeature){
		for(VectorFeature f : selectedFeatures){
			if (f.getFeatureId().equalsIgnoreCase(vectorFeature.getFeatureId())){
				return true;
			}
		}
		return false;
	}
	
	public void removeFeature(VectorFeature vectorFeature){
		int counter=0;
		for(VectorFeature f : selectedFeatures){
			if (f.getFeatureId().equalsIgnoreCase(vectorFeature.getFeatureId())){
				selectedFeatures.remove(counter);
				return;
			}
			counter++;
		}
	}*/
	
	Set<String> displayedFiles = new HashSet<String>();
	Set<String> selectedFiles = new HashSet<String>();
	private boolean isSelected(String sssFile){
		return sssFile != null && selectedFiles.contains(sssFile);
	}
	private void updateStyle(String sssFile, Style newStyle){
		if (sssFile != null){
			for (VectorFeature f : trajectoryLayer.getFeatures()){
				if (sssFile.equals(f.getAttributes().getAttributeAsString("sssFile"))){
					f.setStyle(newStyle);
				}
			}
			trajectoryLayer.redraw();
		}
	}
	private void updateAllStyles(Style newStyle){
		for (VectorFeature f : trajectoryLayer.getFeatures()){
			f.setStyle(newStyle);
		}
		trajectoryLayer.redraw();
	}
	
	public void selectAllFeatures(){
		updateAllStyles(selectedStyle);
		selectedFiles.addAll(displayedFiles);
		eventBus.fireEvent(new FeatureIsSelectedEvent());		
	}
	
	public void unselectAllFeatures(){
		updateAllStyles(defaultStyle);
		selectedFiles.clear();
		eventBus.fireEvent(new NoFeatureIsSelectedEvent());
	}
	
	/**
	 * TODO
	 * @return
	 */
	public List<String> getAllSelectedTrajectories(){
		return new ArrayList<String>(selectedFiles);
	}
	
/*	public void selectAllFeatures(){
		unselectAllFeatures();
		for (VectorFeature f : drawnFeatures){
			f.setStyle(selectedStyle);
			selectedFeatures.add(f);
		}
		eventBus.fireEvent(new FeatureIsSelectedEvent());
		allTrajectoriesLayer.redraw();
	}
	
	public void unselectAllFeatures(){
		for (VectorFeature f : allTrajectoriesLayer.getFeatures()){
			f.setStyle(defaultStyle);
			removeFeature(f);
		}
		eventBus.fireEvent(new NoFeatureIsSelectedEvent());
		allTrajectoriesLayer.redraw();
	}
	
	public List<String> getAllSelectedTrajectories(){
		List<String> trajectories = new ArrayList<String>();
		for (VectorFeature f : selectedFeatures){
			trajectories.add(f.getFeatureId());
		}
		return trajectories;
	}
	*/
	
}
