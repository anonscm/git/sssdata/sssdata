package fr.sedoo.sssdata.client.ui.view.impl;

import java.util.ArrayList;
import java.util.Collection;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.component.AbstractView;
import fr.sedoo.sssdata.client.GlobalBundle;
import fr.sedoo.sssdata.client.ui.view.api.ImportView;

public class ImportViewImpl extends AbstractView implements ImportView {
	
	private static final String PAUSE_LABEL = "Pause";
	private static final String RESUME_LABEL = "Resume";

	private static ImportViewImplUiBinder uiBinder = GWT
			.create(ImportViewImplUiBinder.class);

	interface ImportViewImplUiBinder extends UiBinder<Widget, ImportViewImpl> {
	}
	
	@UiField
	Button importButton;
	
	@UiField
	Button stopButton;
	
	@UiField
	Button pauseButton;
	
	@UiField
	Button archiveButton;

	@UiField
	ScrollPanel scroll;

	@UiField
	VerticalPanel console;
	
	/*@UiField
	VerticalPanel racines;
	List<CheckBox> selectableDirs = new ArrayList<CheckBox>();*/
	@UiField
	ListBox dirsListSelection;
	
	
	private Presenter presenter;
	
	public ImportViewImpl() {
		super();
		GWT.<GlobalBundle>create(GlobalBundle.class).css().ensureInjected();
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
		stopButton.setVisible(false);
		stopButton.setEnabled(false);
		pauseButton.setVisible(false);
		pauseButton.setEnabled(false);
		//archiveButton.setEnabled(true);
		//archiveButton.setVisible(true);
		enableArchiveGeneration(true);
		dirsListSelection.setVisibleItemCount(20);
	}



	@UiHandler("importButton")
	void onImportButtonClicked(ClickEvent event)
	{
		console.clear();
		pauseButton.setText(PAUSE_LABEL);
		presenter.launchImport();
		stopButton.setVisible(true);
		stopButton.setEnabled(true);
		pauseButton.setVisible(true);
		pauseButton.setEnabled(true);
		importButton.setEnabled(false);
		enableArchiveGeneration(false);
	}
	
	@UiHandler("pauseButton")
	void onPauseButtonClicked(ClickEvent event)
	{
		if (pauseButton.getText().compareTo(PAUSE_LABEL)==0)
		{
			pauseButton.setText(RESUME_LABEL);
			stopButton.setEnabled(false);
			presenter.pause();
		}
		else
		{
			pauseButton.setText(PAUSE_LABEL);
			stopButton.setEnabled(true);
			presenter.resume();
		}
		importButton.setEnabled(false);
	}
	
	@UiHandler("stopButton")
	void onStopButtonClicked(ClickEvent event)
	{
		presenter.stop();
		stopButton.setEnabled(false);
		stopButton.setVisible(false);
		pauseButton.setVisible(false);
		pauseButton.setEnabled(false);
		importButton.setEnabled(true);
		enableArchiveGeneration(true);
	}

	@UiHandler("archiveButton")
	void onArchiveButtonClicked(ClickEvent event) {
		console.clear();
		presenter.launchArchiveGeneration();
		enableArchiveGeneration(false);
		enableImport(false);
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public void logNormal(String message) {
		console.add(new Label(message));
		scroll.scrollToBottom();
		
	}

	@Override
	public void logOK(String message) {
		Label aux = new Label(message);
		aux.getElement().getStyle().setColor("green");
		console.add(aux);
		scroll.scrollToBottom();
	}
	
	public void logInfo(String message) {
		Label aux = new Label(message);
		aux.getElement().getStyle().setColor("orange");
		console.add(aux);
		scroll.scrollToBottom();
	}

	@Override
	public void logKO(String message) {
		Label aux = new Label(message);
		aux.getElement().getStyle().setColor("red");
		console.add(aux);
		scroll.scrollToBottom();
	}

	@Override
	public void broadcastImportEnd() {
		importButton.setEnabled(true);
		pauseButton.setEnabled(false);
		stopButton.setEnabled(false);
		pauseButton.setVisible(false);
		stopButton.setVisible(false);
	}
	@Override
	public Collection<String> getSelectedDirs() {
		Collection<String> dirs =  new ArrayList<String>();
		
		for (int i=0;i<dirsListSelection.getItemCount();i++){
			if (dirsListSelection.isItemSelected(i)){
				dirs.add(dirsListSelection.getItemText(i));
			}
			
		}
		/*for (CheckBox cb: this.selectableDirs){
			if (cb.getValue()){
				dirs.add(cb.getText());
			}
		}*/
		return dirs;
	}

	@UiHandler("selectAllDirs")
	void onselectAllDirsClicked(ClickEvent event){
		selectAllDirs();
	}
	
	@UiHandler("unselectAllDirs")
	void onunselectAllDirsClicked(ClickEvent event){
		resetDirsList();
	}
	
	private void resetDirsList(){
		for (int i=0;i<dirsListSelection.getItemCount();i++){
			dirsListSelection.setItemSelected(i, false);
		}
	}
	
	private void selectAllDirs(){
		for (int i=0;i<dirsListSelection.getItemCount();i++){
			dirsListSelection.setItemSelected(i, true);
		}
	}
	
	@Override
	public void setRacines(Collection<String> racines) {
		dirsListSelection.clear();
		for (String racine: racines){
			dirsListSelection.addItem(racine);
		}
		/*this.racines.clear();
		this.selectableDirs.clear();
		for (String racine: racines){
			CheckBox cb = new CheckBox(racine);
			
			this.racines.add(cb);
			this.selectableDirs.add(cb); 
		}*/
	}

	@Override
	public void enableArchiveGeneration(boolean b) {
		archiveButton.setEnabled(b);
		archiveButton.setVisible(b);
	}
	@Override
	public void enableImport(boolean b) {
		importButton.setEnabled(b);
		importButton.setVisible(b);
	}

}
