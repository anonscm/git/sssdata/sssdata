package fr.sedoo.sssdata.client.ui.component.impl.menu;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.MenuBar;

import fr.sedoo.commons.client.component.impl.menu.PlaceCommand;
import fr.sedoo.sssdata.client.message.ApplicationMessages;
import fr.sedoo.sssdata.client.place.SearchPlace;

public class HorizontalSearchMenu extends MenuBar
{


	private EventBus eventBus;
	
	public HorizontalSearchMenu(EventBus eventBus) 
	{
		super(true);
		this.eventBus = eventBus;
		initContent();
		
	}

	private void initContent() {
		
		addItem(ApplicationMessages.INSTANCE.searchViewTitle(), new PlaceCommand(eventBus, new SearchPlace()));
		
	}
	
	
}
