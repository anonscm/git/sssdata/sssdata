package fr.sedoo.sssdata.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class UpdateListPlace extends Place
{
	public static UpdateListPlace instance;
	
	public UpdateListPlace()
	{
	}

	public static class Tokenizer implements PlaceTokenizer<UpdateListPlace>
	{
		
		public UpdateListPlace getPlace(String token) {
			if (instance == null){
				instance = new UpdateListPlace();
			}
			return instance;
		}

		public String getToken(UpdateListPlace place) {
			return "";
		}

		
	}
}