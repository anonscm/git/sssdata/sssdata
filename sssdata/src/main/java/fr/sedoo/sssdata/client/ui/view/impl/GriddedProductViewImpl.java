package fr.sedoo.sssdata.client.ui.view.impl;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.component.AbstractView;
import fr.sedoo.sssdata.client.ui.view.api.GriddedProductView;

public class GriddedProductViewImpl extends AbstractView implements GriddedProductView {

	private static GriddedProductViewImplUiBinder uiBinder = GWT
			.create(GriddedProductViewImplUiBinder.class);

	interface GriddedProductViewImplUiBinder extends UiBinder<Widget, GriddedProductViewImpl> {
	}
	
	Presenter presenter;
	
	@UiField
	DivElement messageContainer;
	
	@UiField
	Label pageTitle;
	
	@UiField
	Anchor downloadButton;
	
	@UiField
	Anchor metadataButton;
	
	
	public GriddedProductViewImpl() {
		super();
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
	}

	@Override
	public void setContent(String content) {
		messageContainer.setInnerHTML(content);
	}
	
	@Override
	public void setPageTitle(String title) {
		pageTitle.setText(title);
	}
	
	@UiHandler("metadataButton")
	void onMetadataButtonClicked(ClickEvent event){
		this.presenter.toMetadata();
	}
	
	@UiHandler("downloadButton")
	void onDownloadButtonClicked(ClickEvent event){
		this.presenter.toDownloadPage();
	}

	@Override
	public void setPresenter(Presenter p) {
		this.presenter = p;		
	}

}
