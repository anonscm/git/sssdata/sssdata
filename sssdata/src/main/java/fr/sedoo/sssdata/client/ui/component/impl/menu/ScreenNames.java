package fr.sedoo.sssdata.client.ui.component.impl.menu;

import fr.sedoo.sssdata.shared.GriddedProduct;

public class ScreenNames {
	public final static String HOME = "Welcome";
	public final static String DATA_POLICY = "Data Policy";
	
	public final static String GRID_ATL = GriddedProduct.GRID_ATLANTIC;
	public final static String GRID_PAC = GriddedProduct.GRID_PACIFIC;
	public final static String GRID_NASG = GriddedProduct.GRID_NASG;
	public final static String GRID_NASPG = GriddedProduct.GRID_NASPG;
	public final static String GRID_BIN_ATL = GriddedProduct.GRID_BIN_ATL;
	
	public final static String SSS_HIST = GriddedProduct.SSS_HIST;
	
}