package fr.sedoo.sssdata.client.ui.component.impl.menu;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.MenuItem;

import fr.sedoo.commons.client.component.impl.menu.PlaceCommand;
import fr.sedoo.sssdata.client.message.ApplicationMessages;
import fr.sedoo.sssdata.client.place.GriddedProductMetadataPlace;
import fr.sedoo.sssdata.shared.GriddedProduct;

public class GriddedProductsMenu extends MenuBar{
private EventBus eventBus;
	
	public GriddedProductsMenu(EventBus eventBus){
		super(true);
		this.eventBus = eventBus;
		initContent();
		
	}

	private void initContent() {
		setAutoOpen(true);
		
		addItem(ApplicationMessages.INSTANCE.gridAtlantic(), new PlaceCommand(eventBus, new GriddedProductMetadataPlace(GriddedProduct.GRID_ATLANTIC)));
		addItem(ApplicationMessages.INSTANCE.gridPacific(), new PlaceCommand(eventBus, new GriddedProductMetadataPlace(GriddedProduct.GRID_PACIFIC)));
		
		//addItem(ApplicationMessages.INSTANCE.gridNasg(), new PlaceCommand(eventBus, new GriddedProductMetadataPlace(GriddedProduct.GRID_NASG)));
		
		MenuBar nasgBar = new MenuBar(true);
		nasgBar.addItem(ApplicationMessages.INSTANCE.gridNasgSubMenu(), new PlaceCommand(eventBus, new GriddedProductMetadataPlace(GriddedProduct.GRID_NASG)));
		nasgBar.addItem(ApplicationMessages.INSTANCE.gridNaspgSubMenu(), new PlaceCommand(eventBus, new GriddedProductMetadataPlace(GriddedProduct.GRID_NASPG)));
		MenuItem nasgSubMenu = new MenuItem(ApplicationMessages.INSTANCE.gridNasgMenu(), nasgBar);
		addItem(nasgSubMenu);
		
		addItem(ApplicationMessages.INSTANCE.gridBinAtlanticTitle(), new PlaceCommand(eventBus, new GriddedProductMetadataPlace(GriddedProduct.GRID_BIN_ATL)));
		
		/*MenuBar atlBar = new MenuBar(true);
		atlBar.addItem(ApplicationMessages.INSTANCE.metadataTitle(), new PlaceCommand(eventBus, new GriddedProductMetadataPlace(GriddedProduct.GRID_ATLANTIC)));
		atlBar.addItem(ApplicationMessages.INSTANCE.dataMenuTitle(), new PlaceCommand(eventBus, new GriddedProductPlace(GriddedProduct.GRID_ATLANTIC)));
		MenuItem atlSubMenu = new MenuItem(ApplicationMessages.INSTANCE.gridAtlantic(), atlBar);
		addItem(atlSubMenu);
		
		MenuBar pacBar = new MenuBar(true);
		pacBar.addItem(ApplicationMessages.INSTANCE.metadataTitle(), new PlaceCommand(eventBus, new GriddedProductMetadataPlace(GriddedProduct.GRID_PACIFIC)));
		pacBar.addItem(ApplicationMessages.INSTANCE.dataMenuTitle(), new PlaceCommand(eventBus, new GriddedProductPlace(GriddedProduct.GRID_PACIFIC)));
		MenuItem pacSubMenu = new MenuItem(ApplicationMessages.INSTANCE.gridPacific(), pacBar);
		addItem(pacSubMenu);
		*/
		//addItem(ApplicationMessages.INSTANCE.gridAtlantic(), new PlaceCommand(eventBus, new GriddedProductPlace(GriddedProductPlace.GRID_ATLANTIC)));
		//addItem(ApplicationMessages.INSTANCE.gridPacific(), new PlaceCommand(eventBus, new GriddedProductPlace(GriddedProductPlace.GRID_PACIFIC)));
		//addItem(ApplicationMessages.INSTANCE.gridAtlantic(), new PlaceCommand(eventBus, new MetadataPlace(DoiConstants.GRID_ATL)));
		//addItem(ApplicationMessages.INSTANCE.gridPacific(), new PlaceCommand(eventBus, new MetadataPlace(DoiConstants.GRID_PAC)));
	}
}
