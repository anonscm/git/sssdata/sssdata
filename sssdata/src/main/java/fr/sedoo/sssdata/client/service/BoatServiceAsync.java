package fr.sedoo.sssdata.client.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.sedoo.sssdata.shared.BoatDTO;

public interface BoatServiceAsync {

	void findAll(AsyncCallback<ArrayList<BoatDTO>> callback);

	//void findByName(String name, AsyncCallback<BoatDTO> callback);

	//void add(BoatDTO b, AsyncCallback<Void> callback);

}
