package fr.sedoo.sssdata.client.ui.component.api;

import fr.sedoo.commons.client.event.BackEventHandler;
import fr.sedoo.commons.client.event.BreadCrumbChangeEventHandler;
import fr.sedoo.sssdata.client.ui.widget.PreferredHeightWidget;

public interface NavigationBar extends PreferredHeightWidget, BreadCrumbChangeEventHandler, BackEventHandler
{
}