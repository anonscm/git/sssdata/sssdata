package fr.sedoo.sssdata.client.mvp;

import com.google.gwt.activity.shared.Activity;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.place.shared.Place;

import fr.sedoo.commons.client.analytics.GAEvent;
import fr.sedoo.commons.client.cms.place.CMSConsultPlace;
import fr.sedoo.commons.client.cms.place.CMSEditPlace;
import fr.sedoo.commons.client.config.place.ConfigPlace;
import fr.sedoo.commons.client.contact.mvp.ContactClientFactory;
import fr.sedoo.commons.client.contact.place.ContactPlace;
import fr.sedoo.commons.client.faq.mvp.FaqClientFactory;
import fr.sedoo.commons.client.faq.place.FaqConsultPlace;
import fr.sedoo.commons.client.faq.place.FaqEditPlace;
import fr.sedoo.commons.client.faq.place.FaqManagePlace;
import fr.sedoo.commons.client.mvp.place.LoginPlace;
import fr.sedoo.commons.client.news.activity.NewDisplayActivity;
import fr.sedoo.commons.client.news.activity.NewsArchiveActivity;
import fr.sedoo.commons.client.news.mvp.NewsClientFactory;
import fr.sedoo.commons.client.news.place.MessageEditPlace;
import fr.sedoo.commons.client.news.place.MessageManagePlace;
import fr.sedoo.commons.client.news.place.NewDisplayPlace;
import fr.sedoo.commons.client.news.place.NewsArchivePlace;
import fr.sedoo.sssdata.client.ClientFactory;
import fr.sedoo.sssdata.client.activity.AdministratorManagementActivity;
import fr.sedoo.sssdata.client.activity.DoiHistoryManagementActivity;
import fr.sedoo.sssdata.client.activity.DownloadLogActivity;
import fr.sedoo.sssdata.client.activity.DownloadStatsActivity;
import fr.sedoo.sssdata.client.activity.FileSearchActivity;
import fr.sedoo.sssdata.client.activity.GriddedProductActivity;
import fr.sedoo.sssdata.client.activity.GriddedProductMetadataActivity;
import fr.sedoo.sssdata.client.activity.ImportActivity;
import fr.sedoo.sssdata.client.activity.LoginActivity;
import fr.sedoo.sssdata.client.activity.MetadataActivity;
import fr.sedoo.sssdata.client.activity.SSSCMSConsultActivity;
import fr.sedoo.sssdata.client.activity.SSSCMSEditActivity;
import fr.sedoo.sssdata.client.activity.SSSConfigActivity;
import fr.sedoo.sssdata.client.activity.SSSContactActivity;
import fr.sedoo.sssdata.client.activity.SSSFaqConsultActivity;
import fr.sedoo.sssdata.client.activity.SSSFaqEditActivity;
import fr.sedoo.sssdata.client.activity.SSSFaqManageActivity;
import fr.sedoo.sssdata.client.activity.SSSMessageEditActivity;
import fr.sedoo.sssdata.client.activity.SSSMessageManagementActivity;
import fr.sedoo.sssdata.client.activity.SearchActivity;
import fr.sedoo.sssdata.client.activity.StatsActivity;
import fr.sedoo.sssdata.client.activity.SystemActivity;
import fr.sedoo.sssdata.client.activity.UpdateListActivity;
import fr.sedoo.sssdata.client.activity.UserContactActivity;
import fr.sedoo.sssdata.client.activity.UserInfoActivity;
import fr.sedoo.sssdata.client.activity.UserStatsActivity;
import fr.sedoo.sssdata.client.activity.WelcomeActivity;
import fr.sedoo.sssdata.client.place.AdministratorManagementPlace;
import fr.sedoo.sssdata.client.place.DoiHistoryPlace;
import fr.sedoo.sssdata.client.place.DownloadLogPlace;
import fr.sedoo.sssdata.client.place.DownloadStatsPlace;
import fr.sedoo.sssdata.client.place.FileSearchPlace;
import fr.sedoo.sssdata.client.place.GriddedProductMetadataPlace;
import fr.sedoo.sssdata.client.place.GriddedProductPlace;
import fr.sedoo.sssdata.client.place.ImportPlace;
import fr.sedoo.sssdata.client.place.MetadataPlace;
import fr.sedoo.sssdata.client.place.SearchPlace;
import fr.sedoo.sssdata.client.place.StatsPlace;
import fr.sedoo.sssdata.client.place.SystemPlace;
import fr.sedoo.sssdata.client.place.UpdateListPlace;
import fr.sedoo.sssdata.client.place.UserContactPlace;
import fr.sedoo.sssdata.client.place.UserInfoPlace;
import fr.sedoo.sssdata.client.place.UserStatsPlace;
import fr.sedoo.sssdata.client.place.WelcomePlace;


public class AppActivityMapper implements ActivityMapper {

	private ClientFactory clientFactory;

	/**
	 * AppActivityMapper associates each Place with its corresponding
	 * {@link Activity}
	 * 
	 * @param clientFactory
	 *            Factory to be passed to activities
	 */
	public AppActivityMapper(ClientFactory clientFactory) {
		super();
		this.clientFactory = clientFactory;
	}

	/**
	 * Map each Place to its corresponding Activity. This would be a great use
	 * for GIN.
	 */
	@Override
	public Activity getActivity(Place place) {
		
		clientFactory.getEventBus().fireEvent(new GAEvent(place));
		
		if (place instanceof WelcomePlace)
		{
			return new WelcomeActivity((WelcomePlace) place, clientFactory);
		}
		if (place instanceof MetadataPlace)
		{
			return new MetadataActivity((MetadataPlace) place, clientFactory);
		}
		if (place instanceof GriddedProductPlace){
			return new GriddedProductActivity((GriddedProductPlace) place, clientFactory);
		}	
		if (place instanceof GriddedProductMetadataPlace){
				return new GriddedProductMetadataActivity((GriddedProductMetadataPlace) place, clientFactory);
		}	
		if (place instanceof SystemPlace)
		{
			return new SystemActivity((SystemPlace) place, clientFactory);
		}
		if (place instanceof DoiHistoryPlace)
		{
			return new DoiHistoryManagementActivity((DoiHistoryPlace) place, clientFactory);
		}
		if (place instanceof AdministratorManagementPlace)
		{
			return new AdministratorManagementActivity((AdministratorManagementPlace) place, clientFactory);
		}
		
		if (place instanceof MessageManagePlace)
		{
			return new SSSMessageManagementActivity((MessageManagePlace) place, clientFactory);
		}
		
		if (place instanceof MessageEditPlace)
		{
			return new SSSMessageEditActivity((MessageEditPlace) place, clientFactory);
		}
		
		if (place instanceof SearchPlace)
		{
			return new SearchActivity((SearchPlace) place, clientFactory);
		}
				
		if (place instanceof UserInfoPlace)
		{
			return new UserInfoActivity((UserInfoPlace) place, clientFactory);
		}
		if (place instanceof StatsPlace)
		{
			return new StatsActivity((StatsPlace) place, clientFactory);
		}
		
		if (place instanceof ImportPlace)
		{
			return new ImportActivity((ImportPlace) place, clientFactory);
		}
						
		if (place instanceof LoginPlace)
		{
			return new LoginActivity((LoginPlace) place, clientFactory);
		}
		
		if (place instanceof UserContactPlace)
		{
			return new UserContactActivity((UserContactPlace) place, clientFactory);
		}
		
		if (place instanceof FileSearchPlace)
		{
			return new FileSearchActivity((FileSearchPlace) place, clientFactory);
		}
		
		if (place instanceof DownloadLogPlace)
		{
			return new DownloadLogActivity((DownloadLogPlace) place, clientFactory);
		}
		
		if (place instanceof NewsArchivePlace)
		{
			return new NewsArchiveActivity((NewsArchivePlace) place, (NewsClientFactory)clientFactory);
		}
		
		if (place instanceof NewDisplayPlace)
		{
			return new NewDisplayActivity((NewDisplayPlace) place, (NewsClientFactory)clientFactory);
		}
		
		if (place instanceof FaqManagePlace)
		{
			return new SSSFaqManageActivity((FaqManagePlace) place, (FaqClientFactory)clientFactory);
		}
		if (place instanceof FaqEditPlace)
		{
			return new SSSFaqEditActivity((FaqEditPlace) place, (FaqClientFactory)clientFactory);
		}
		if (place instanceof FaqConsultPlace)
		{
			return new SSSFaqConsultActivity((FaqConsultPlace) place, clientFactory);
		}
		if (place instanceof CMSEditPlace){
			return new SSSCMSEditActivity((CMSEditPlace) place, clientFactory);
		}
		if (place instanceof CMSConsultPlace){
			return new SSSCMSConsultActivity((CMSConsultPlace) place, clientFactory);
		}
		
		if (place instanceof ContactPlace)
		{
			return new SSSContactActivity((ContactPlace) place, (ContactClientFactory)clientFactory);
		}
		if (place instanceof UpdateListPlace)
		{
			return new UpdateListActivity((UpdateListPlace) place, clientFactory);
		}
		if (place instanceof UserStatsPlace){
			return new UserStatsActivity((UserStatsPlace) place, clientFactory);
		}
		if (place instanceof DownloadStatsPlace){
			return new DownloadStatsActivity((DownloadStatsPlace) place, clientFactory);
		}
		if (place instanceof ConfigPlace){
			return new SSSConfigActivity((ConfigPlace) place, clientFactory);
		}
		return null;
		
	}

}	
