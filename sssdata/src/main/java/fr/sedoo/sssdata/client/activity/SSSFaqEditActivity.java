package fr.sedoo.sssdata.client.activity;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.sedoo.commons.client.event.BreadCrumbChangeEvent;
import fr.sedoo.commons.client.faq.activity.FaqEditActivity;
import fr.sedoo.commons.client.faq.place.FaqEditPlace;
import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;
import fr.sedoo.sssdata.client.ui.component.ShortcutFactory;

public class SSSFaqEditActivity extends FaqEditActivity {

	public SSSFaqEditActivity(FaqEditPlace place,
			AuthenticatedClientFactory clientFactory) {
		super(place, clientFactory);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		super.start(containerWidget, eventBus);
		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		shortcuts.add(ShortcutFactory.getWelcomeShortcut());
		shortcuts.add(ShortcutFactory.getAdminShortcut());
		shortcuts.add(ShortcutFactory.getFaqManagementShortcut());
		shortcuts.add(ShortcutFactory.getFaqEditShortcut());
		eventBus.fireEvent(new BreadCrumbChangeEvent(shortcuts));
	}
	
	@Override
	protected boolean isValidUser() {
		return isLoggedUser();
	}
	
}
