package fr.sedoo.sssdata.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fr.sedoo.commons.client.util.ServiceException;
import fr.sedoo.sssdata.shared.FileDTO;
import fr.sedoo.sssdata.shared.UserInformations;

@RemoteServiceRelativePath("userinfos")
public interface UserInfoService extends RemoteService {
	UserInformations getUserInfos(String email);
	
	Long insertDownload(UserInformations userInfos);
	void updateDownload(Long id, FileDTO file) throws ServiceException;

	String getArchiveName(String flag, String format);

	void updateDownloadArchive(Long id, String archiveName) throws ServiceException;
	
}
