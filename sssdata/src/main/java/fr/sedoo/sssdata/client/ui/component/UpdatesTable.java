package fr.sedoo.sssdata.client.ui.component;

import java.util.ArrayList;

import com.google.gwt.cell.client.Cell;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.Image;

import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.widget.table.ImagedActionCell;
import fr.sedoo.commons.client.widget.table.TableBundle;
import fr.sedoo.sssdata.shared.UpdateDTO;

public class UpdatesTable extends CellTable<UpdateDTO> {
			
	private UpdatesTablePresenter presenter;
	
	private Image deleteImage = new Image(TableBundle.INSTANCE.delete());
	private Image viewImage = new Image(TableBundle.INSTANCE.view());
	
	public UpdatesTable() {
		this(false);
	}
	
	public UpdatesTable(boolean compact) {
		super();
				
		TextColumn<UpdateDTO> date = new TextColumn<UpdateDTO>() {
			@Override
			public String getCellStyleNames(Context context, UpdateDTO object) {
				return "updates-list";
			}
			@Override
			public String getValue(UpdateDTO u) {
				return u.getDate();
			}
		};
		
		//date.setSortable(true);
			
		TextColumn<UpdateDTO> file = new TextColumn<UpdateDTO>() {
			@Override
			public String getCellStyleNames(Context context, UpdateDTO object) {
				return "updates-list";
			}
			@Override
			public String getValue(UpdateDTO u) {
				return u.getFileName() + " - " + u.getBoatName();
			}
			
		};
		
		UpdateCell cell = new UpdateCell(compact);
		Column<UpdateDTO, UpdateDTO> description = new Column<UpdateDTO, UpdateDTO>(cell) {
			@Override
			public UpdateDTO getValue(UpdateDTO object) {
				return object;
			}
		};
						
		if (compact){
			addColumn(date);
			addColumn(description);
		}else{
			
			Cell<UpdateDTO> deleteActionCell = new ImagedActionCell<UpdateDTO>(deleteAction, deleteImage, CommonMessages.INSTANCE.delete());
			Column<UpdateDTO, UpdateDTO> deleteColumn = new Column<UpdateDTO, UpdateDTO>(deleteActionCell) {
				@Override
				public UpdateDTO getValue(UpdateDTO object) {
					return object;
				}
			};
			Cell<UpdateDTO> viewActionCell = new ImagedActionCell<UpdateDTO>(viewAction, viewImage, CommonMessages.INSTANCE.view());
			Column<UpdateDTO, UpdateDTO> viewColumn = new Column<UpdateDTO, UpdateDTO>(viewActionCell) {
				@Override
				public UpdateDTO getValue(UpdateDTO object) {
					return object;
				}
			};
						
			addColumn(date, "Date");
			addColumn(description, "File");
			addColumn(viewColumn);
			setColumnWidth(viewColumn, 30.0, Unit.PX);
			addColumn(deleteColumn);
			setColumnWidth(deleteColumn, 30.0, Unit.PX);
		}
	}
	
	ImagedActionCell.Delegate<UpdateDTO> deleteAction = new ImagedActionCell.Delegate<UpdateDTO>() {
		@Override
		public void execute(UpdateDTO dl) {
			presenter.delete(dl);
		}
	};
	
	ImagedActionCell.Delegate<UpdateDTO> viewAction = new ImagedActionCell.Delegate<UpdateDTO>() {
		@Override
		public void execute(UpdateDTO dl) {
			presenter.view(dl);
		}
	};
		
	public void reset(){
		setRowData(new ArrayList<UpdateDTO>());
	}
	
	public void setPresenter(UpdatesTablePresenter presenter) {
		this.presenter = presenter;
	}
	
}
