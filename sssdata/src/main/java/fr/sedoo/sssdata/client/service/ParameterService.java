package fr.sedoo.sssdata.client.service;

import java.util.Map;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("parameters")

public interface ParameterService extends RemoteService {

        Map<String,String> getSystemParameters();
 
}
