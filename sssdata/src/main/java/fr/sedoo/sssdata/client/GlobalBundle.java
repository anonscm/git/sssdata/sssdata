package fr.sedoo.sssdata.client;


import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.CssResource.NotStrict;
import com.google.gwt.resources.client.DataResource;
import com.google.gwt.resources.client.ImageResource;
//import com.google.gwt.resources.client.TextResource;


public interface GlobalBundle extends ClientBundle {
	    @NotStrict
	    @Source("sssdata.css")
	    CssResource css();
	    
	    @NotStrict
	    @Source("bootstrap.min.css")
	    CssResource bootstrapCss();
	    
//	    @ClientBundle.Source(value="css/bootstrap.min.css")
//	    TextResource bootstrapCss();
	    
//	    @ClientBundle.Source(value="js/jquery-1.7.2.js")
//	    TextResource jquery();
	    
//	    @ClientBundle.Source(value="js/bootstrap.js")
//	    TextResource bootstrapJs();
	    
	    @Source("home2.png")
        ImageResource home();
	    
	    @Source("orcid.png")
        ImageResource orcid();
	    
	    @Source("contact.png")
        ImageResource contact();
	    
	    @Source("leftArrow.png")
        ImageResource left();
	    
	    @Source("rightArrow.png")
        ImageResource right();
	    
	    @Source("bandeau.jpg")
        ImageResource bandeau();

	    @Source("minimize.png")
        ImageResource minimize();
	    
	    @Source("maximize.png")
        ImageResource maximize();
	    
	    @Source("ajax-loader.gif")
        ImageResource loading();
	    
	    @Source("delete.png")
        ImageResource delete();
	    
	    @Source("edit.png")
        ImageResource edit();
	    
	    @Source("puce_fleche_bleu.png")
        ImageResource menuDot();
	    
	    @Source("add.png")
	    ImageResource add();
	    
	    @Source("legosLogo.png")
	    ImageResource legosLogo();
	    
	    @Source("fakeMap.png")
	    ImageResource fakeMap();
	    
	    @Source("fakeGraph.png")
	    ImageResource fakeGraph(); 
	    	    
	    @Source("refresh.png")
	    ImageResource refresh(); 
	    
	    @Source("help.png")
	    ImageResource help(); 
	    
	    @Source("lessWidget.png")
	    ImageResource lessWidget();
	    
	    @Source("moreWidget.png")
	    ImageResource moreWidget();
	    
	    @Source("dragWidget.png")
	    ImageResource dragWidget();
	    
	    @Source("scaleWidget.png")
	    DataResource scaleWidget();
	    
	    public static final GlobalBundle INSTANCE = GWT.create(GlobalBundle.class);
		
		
	  }