package fr.sedoo.sssdata.client.ui.component;

import fr.sedoo.commons.client.cms.bundle.CMSMessages;
import fr.sedoo.commons.client.cms.place.CMSConsultPlace;
import fr.sedoo.commons.client.cms.place.CMSEditPlace;
import fr.sedoo.commons.client.contact.message.ContactMessages;
import fr.sedoo.commons.client.contact.place.ContactPlace;
import fr.sedoo.commons.client.date.DateUtil;
import fr.sedoo.commons.client.faq.bundle.FaqMessages;
import fr.sedoo.commons.client.faq.place.FaqConsultPlace;
import fr.sedoo.commons.client.faq.place.FaqEditPlace;
import fr.sedoo.commons.client.faq.place.FaqManagePlace;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.mvp.place.LoginPlace;
import fr.sedoo.commons.client.news.bundle.NewsMessages;
import fr.sedoo.commons.client.news.place.MessageEditPlace;
import fr.sedoo.commons.client.news.place.MessageManagePlace;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;
import fr.sedoo.sssdata.client.message.ApplicationMessages;
import fr.sedoo.sssdata.client.place.AdministratorManagementPlace;
import fr.sedoo.sssdata.client.place.GriddedProductMetadataPlace;
import fr.sedoo.sssdata.client.place.GriddedProductPlace;
import fr.sedoo.sssdata.client.place.HelpPlace;
import fr.sedoo.sssdata.client.place.ImportPlace;
import fr.sedoo.sssdata.client.place.MetadataPlace;
import fr.sedoo.sssdata.client.place.SearchPlace;
import fr.sedoo.sssdata.client.place.StatsPlace;
import fr.sedoo.sssdata.client.place.SystemPlace;
import fr.sedoo.sssdata.client.place.UpdateListPlace;
import fr.sedoo.sssdata.client.place.UserContactPlace;
import fr.sedoo.sssdata.client.place.UserInfoPlace;
import fr.sedoo.sssdata.client.place.WelcomePlace;
import fr.sedoo.sssdata.client.ui.component.impl.menu.ScreenNames;

public class ShortcutFactory {

	static public Shortcut getWelcomeShortcut()
	{
		return new Shortcut(CommonMessages.INSTANCE.home(), new WelcomePlace());
	}
	
	static public Shortcut getMetadataShortcut()
	{
		return new Shortcut(ApplicationMessages.INSTANCE.metadataTitle(), new MetadataPlace());
	}
	
	static public Shortcut getGridShortcut(String grid)
	{
		return new Shortcut(ApplicationMessages.INSTANCE.gridMenuTitle(), new GriddedProductPlace(grid));
	}
	
	static public Shortcut getHelpShortcut()
	{
		return new Shortcut(CommonMessages.INSTANCE.help(), new HelpPlace());
	}
	
	static public Shortcut getContactShortcut()
	{
		return new Shortcut(ContactMessages.INSTANCE.title(), new ContactPlace());
	}
	
	static public Shortcut getDataPolicyShortcut()
	{
		return new Shortcut(ApplicationMessages.INSTANCE.policyMenuTitle(), new CMSConsultPlace(ScreenNames.DATA_POLICY));
	}
	
	static public Shortcut getFaqShortcut()
	{
		return new Shortcut(FaqMessages.INSTANCE.faqConsultationViewTitle(), new FaqConsultPlace());
	}
	
	static public Shortcut getAdminShortcut()
	{
		return new Shortcut(ApplicationMessages.INSTANCE.administrationMenuTitle(), new WelcomePlace());
	}

	public static Shortcut getSystemShortcut() {
		return new Shortcut(ApplicationMessages.INSTANCE.systemViewTitle(), new SystemPlace());
	}

	public static Shortcut getAdministratorManagementShortcut() {
		return new Shortcut(ApplicationMessages.INSTANCE.administratorManagementViewTitle(), new AdministratorManagementPlace());
	}

	public static Shortcut getMessageManagementShortcut() {
		return new Shortcut(NewsMessages.INSTANCE.header(), new MessageManagePlace());
	}
	
	public static Shortcut getFaqManagementShortcut() {
		return new Shortcut(FaqMessages.INSTANCE.faqManagementViewTitle(), new FaqManagePlace());
	}
	
	public static Shortcut getFaqEditShortcut() {
		return new Shortcut(FaqMessages.INSTANCE.faqEditionViewTitle(), new FaqEditPlace());
	}
	
	public static Shortcut getCMSEditShortcut() {
		return new Shortcut(CMSMessages.INSTANCE.screenModification(), new CMSEditPlace());
	}
	
	public static Shortcut getMessageEditionShortcut() {
		return new Shortcut(ApplicationMessages.INSTANCE.messageEditViewTitle(), new MessageEditPlace());
	}
	
	public static Shortcut getSearchShortcut() {
		return new Shortcut(ApplicationMessages.INSTANCE.searchViewTitle(), new SearchPlace());
	}
	
	public static Shortcut getDataAccessShortcut() {
		return new Shortcut(ApplicationMessages.INSTANCE.dataMenuTitle(), new SearchPlace());
	}
	
	
	public static Shortcut getUserInfoShortcut() {
		return new Shortcut(ApplicationMessages.INSTANCE.userInfoViewTitle(), new UserInfoPlace());
	}
			
	public static Shortcut getStatsShortcut() {
		return new Shortcut(ApplicationMessages.INSTANCE.statsViewTitle(), new StatsPlace(DateUtil.getCurrentYearAsString()));
	}

	public static Shortcut getLoginShortcut() {
		return new Shortcut(CommonMessages.INSTANCE.login(), new LoginPlace());
	}
	
	public static Shortcut getImportShortcut() {
		return new Shortcut(ApplicationMessages.INSTANCE.importViewTitle(), new ImportPlace());
	}

	public static Shortcut getUserContactShortcut() {
		return new Shortcut(ApplicationMessages.INSTANCE.userContactViewTitle(), new UserContactPlace());
	}
	
	public static Shortcut getDownloadLogShortcut() {
		return new Shortcut(ApplicationMessages.INSTANCE.downloadLogMenuTitle(), new UserContactPlace());
	}
	
	public static Shortcut getUpdatesShortcut() {
		return new Shortcut(ApplicationMessages.INSTANCE.updatesViewTitle(), new UpdateListPlace());
	}
	
}
