package fr.sedoo.sssdata.client.message;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.LocalizableResource.DefaultLocale;
import com.google.gwt.i18n.client.LocalizableResource.Generate;
import com.google.gwt.i18n.client.Messages;

@Generate(
format = { "com.google.gwt.i18n.rebind.format.PropertiesFormat" }, 
fileName = "Message", 
locales = {"en"})
@DefaultLocale("en")
public interface CommonMessages extends Messages {
	
	public static final CommonMessages INSTANCE = GWT.create(CommonMessages.class);
	
    @Key("error")
    public String error();
    
    @Key("anErrorHasOccurred")
    public String anErrorHasOccurred();
    
    @Key("confirm")
    public String confirm();

    @Key("yes")
    public String yes();
    
    @Key("no")
    public String no();
    
    @Key("ok")
    public String ok();
    
    @Key("cancel")
    public String cancel();
    
    @Key("version")
    public String version();
    
    @Key("minimize")
    public String minimize();
    
    @Key("maximize")
    public String maximize();
    
    @Key("breadCrumbIntroductionText")
    public String breadCrumbIntroductionText();
    
    @Key("parameterLoading")
    public String parameterLoading();
    
    @Key("loading")
    public String loading();
    
    @Key("edit")
    public String edit();
    
    @Key("delete")
    public String delete();
    
    @Key("emptyList")
    public String emptyList();
    
    @Key("deletionConfirmMessage")
    public String deletionConfirmMessage();
    
    @Key("deletedElement")
    public String deletedElement();
    
    @Key("saving")
    public String saving();
    
    @Key("savedElement")
    public String savedElement();
    
    @Key("name")
    public String name();
    
    @Key("login")
    public String login();
    
    @Key("password")
    public String password();
    
    @Key("content")
    public String content();
    
    @Key("value")
    public String value();
    
    @Key("param")
    public String param();
    
    @Key("download")
    public String download();

   
}
