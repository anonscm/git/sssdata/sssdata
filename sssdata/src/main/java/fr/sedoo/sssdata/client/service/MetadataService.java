package fr.sedoo.sssdata.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fr.sedoo.sssdata.shared.metadata.MetadataDTO;

@RemoteServiceRelativePath("metadata")
public interface MetadataService extends RemoteService {
	
        MetadataDTO getMetadata(String doiSuffix) throws ServiceException;

        MetadataDTO updateMetadata(String doiSuffix) throws ServiceException;
        
}
