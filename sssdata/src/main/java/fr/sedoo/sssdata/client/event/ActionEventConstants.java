package fr.sedoo.sssdata.client.event;

public interface ActionEventConstants {

	public final int BASIC_LOADING_EVENT = 1;
	public final int PARAMETER_LOADING_EVENT = 2;
	public final int BASIC_DELETING_EVENT = 3;
	public final int BASIC_SAVING_EVENT = 4;
	public final int NEWS_LOADING_EVENT = 5;
	public final int PARAMETER_RESET_EVENT = 6;
}
