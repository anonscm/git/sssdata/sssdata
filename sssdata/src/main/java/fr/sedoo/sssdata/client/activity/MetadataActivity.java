package fr.sedoo.sssdata.client.activity;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.sedoo.commons.client.callback.LoadCallBack;
import fr.sedoo.commons.client.cms.ClientScreenList;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.event.BreadCrumbChangeEvent;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.commons.client.util.LocaleUtil;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;
import fr.sedoo.commons.shared.domain.cms.TitledScreen;
import fr.sedoo.sssdata.client.ClientFactory;
import fr.sedoo.sssdata.client.event.ActionEventConstants;
import fr.sedoo.sssdata.client.message.ApplicationMessages;
import fr.sedoo.sssdata.client.message.CommonMessages;
import fr.sedoo.sssdata.client.place.DOIPlace;
import fr.sedoo.sssdata.client.place.SearchPlace;
import fr.sedoo.sssdata.client.service.MetadataService;
import fr.sedoo.sssdata.client.service.MetadataServiceAsync;
import fr.sedoo.sssdata.client.ui.component.ShortcutFactory;
import fr.sedoo.sssdata.client.ui.view.api.MetadataView;
import fr.sedoo.sssdata.shared.metadata.MetadataDTO;

public class MetadataActivity extends AbstractActivity implements LoadCallBack<TitledScreen>, MetadataView.Presenter{

	protected ClientFactory clientFactory;
	protected MetadataView metadataView;
	protected EventBus eventBus;
	
	private DOIPlace place;
	
	private String doi;
	
	public final static MetadataServiceAsync METADATA_SERVICE = GWT.create(MetadataService.class);
	
	public MetadataActivity(DOIPlace place, ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
		this.place = place;
	}
	
	protected void setShortcut(EventBus eventBus){
		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		shortcuts.add(ShortcutFactory.getWelcomeShortcut());
		shortcuts.add(ShortcutFactory.getMetadataShortcut());
		eventBus.fireEvent(new BreadCrumbChangeEvent(shortcuts));
	}
	
	protected void setOptionalContent(){
		metadataView.setOptionalContent("");
	}
	
	protected void setPageTitle(){
		setPageTitle(null);
	}
	protected void setPageTitle(String title){
		if (StringUtil.isNotEmpty(title)){
			metadataView.setPageTitle(title);
		}else{
			metadataView.setPageTitle(ApplicationMessages.INSTANCE.metadataTitle());
		}
	}
	
	@Override
	public void setDoi(String doi){
		this.doi = doi;
	}
		
	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		this.metadataView = clientFactory.getMetadataView();
		metadataView.setPresenter(this);
		metadataView.reset();
		this.eventBus = eventBus;
		containerWidget.setWidget(metadataView.asWidget());
				
		setShortcut(eventBus);
		setPageTitle();
		setOptionalContent();		
		
		final ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstants.BASIC_LOADING_EVENT, true);
		eventBus.fireEvent(e);
		
		METADATA_SERVICE.getMetadata(place.getDoiSuffix(), new DefaultAbstractCallBack<MetadataDTO>(e, eventBus) {
			@Override
			public void onSuccess(MetadataDTO metadata) {
				super.onSuccess(metadata);
				metadataView.reset();
				metadataView.displayMetadata(metadata);
				setPageTitle(metadata.getTitle());
				setDoi(metadata.getDoi());
			}
			@Override
			public void onFailure(Throwable caught) {
				super.onFailure(caught);
			}
			
		});
		
		
	}

	@Override
	public void refreshMetadata() {
		final ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstants.BASIC_LOADING_EVENT, true);
		eventBus.fireEvent(e);
		
		METADATA_SERVICE.updateMetadata(place.getDoiSuffix(), new DefaultAbstractCallBack<MetadataDTO>(e, eventBus) {
			@Override
			public void onSuccess(MetadataDTO metadata) {
				super.onSuccess(metadata);
				metadataView.reset();
				metadataView.displayMetadata(metadata);
				setDoi(metadata.getDoi());
			}
			@Override
			public void onFailure(Throwable caught) {
				super.onFailure(caught);
			}
			
		});
		
		
	}
	@Override
	public void exportMetadata(){
		String url = GWT.getHostPageBaseURL() + "services/iso19139?doi=" + doi;
		Window.open(url, "_blank", null);
	}

	@Override
	public void toDataPage() {
		clientFactory.getPlaceController().goTo(new SearchPlace());				
	}
	
	@Override
	public void postLoadProcess(TitledScreen resultScreen) {
		String result = resultScreen.getContent();
		metadataView.setOptionalContent(StringUtil.trimToEmpty(result));
	}
}
