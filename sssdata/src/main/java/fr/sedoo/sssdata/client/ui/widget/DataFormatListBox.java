package fr.sedoo.sssdata.client.ui.widget;

import org.gwtbootstrap3.client.ui.ListBox;

import com.google.gwt.editor.client.LeafValueEditor;

public class DataFormatListBox extends ListBox implements LeafValueEditor<String>{
		
	@Override
	public void setValue(String value) {
		if (value!=null){
			this.addItem(value);
		}
	}

	@Override
	public String getValue() {
		String format = null;
		for (int i=0;i<this.getItemCount();i++){
			if (this.isItemSelected(i)){
				format = this.getValue(i);
			}
		}
		return format;
	}
}
