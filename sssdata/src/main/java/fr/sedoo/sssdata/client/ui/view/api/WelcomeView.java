package fr.sedoo.sssdata.client.ui.view.api;

import java.util.ArrayList;

import com.google.gwt.user.client.ui.IsWidget;

import fr.sedoo.commons.client.news.widget.NewsListPresenter;
import fr.sedoo.commons.shared.domain.New;
import fr.sedoo.sssdata.shared.DBStatusDTO;

public interface WelcomeView extends IsWidget {

	void setWelcomeMessage(String welcomeMessage);
	void setNews(ArrayList<? extends New> carrouselNews, ArrayList<? extends New> news);
	boolean areNewsLoaded();
	void reset();
	
	//void setPresenter(NewsListPresenter presenter);
	void setNewsPresenter(NewsListPresenter presenter);
	
	//void setUpdates(List<UpdateDTO> updates);
	//void setUpdatesPresenter(UpdatesTablePresenter presenter);
	
	void setDbStatus(DBStatusDTO status);
	
}
