package fr.sedoo.sssdata.client.event;

import java.util.HashMap;

import com.google.gwt.event.shared.GwtEvent;

public class MessageModificationEvent extends GwtEvent<MessageModificationEventHandler>{

	HashMap<String, String> parameters;
	
    public static final Type<MessageModificationEventHandler> TYPE = new Type<MessageModificationEventHandler>();

    public MessageModificationEvent(HashMap<String, String> parameters)
    {
    	this.parameters = parameters;
    }
    
	@Override
    protected void dispatch(MessageModificationEventHandler handler) {
            handler.onNotification(this);
    }

    @Override
    public com.google.gwt.event.shared.GwtEvent.Type<MessageModificationEventHandler> getAssociatedType() {
            return TYPE;
    }
    
    public HashMap<String, String> getParameters() {
		return parameters;
	}

}