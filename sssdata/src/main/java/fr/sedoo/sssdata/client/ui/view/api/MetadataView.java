package fr.sedoo.sssdata.client.ui.view.api;

import com.google.gwt.user.client.ui.IsWidget;

import fr.sedoo.sssdata.shared.metadata.MetadataDTO;

public interface MetadataView extends IsWidget {

	void setPresenter(Presenter presenter);

	void reset();
	void displayMetadata(MetadataDTO metadata);
	void setPageTitle(String title);

	void setOptionalContent(String content);
	
	public interface Presenter {
		void refreshMetadata();
		void exportMetadata();
		void toDataPage();
		void setDoi(String doi);
	}
	
}
