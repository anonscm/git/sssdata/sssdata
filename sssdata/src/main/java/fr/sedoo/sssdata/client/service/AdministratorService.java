package fr.sedoo.sssdata.client.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fr.sedoo.sssdata.shared.Administrator;

@RemoteServiceRelativePath("administrators")

public interface AdministratorService extends RemoteService {
        ArrayList<Administrator> findAll() throws ServiceException;
        void delete(Administrator administrator) throws ServiceException;
        Administrator edit(Administrator administrator) throws ServiceException;
        Administrator create(Administrator administrator) throws ServiceException;
}
