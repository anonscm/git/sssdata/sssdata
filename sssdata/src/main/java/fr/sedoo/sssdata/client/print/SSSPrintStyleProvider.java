package fr.sedoo.sssdata.client.print;

import fr.sedoo.commons.client.print.PrintStyleProvider;

public class SSSPrintStyleProvider implements PrintStyleProvider
{

	@Override
	public String getPrintStyle() {
		String aux = "<style type='text/css'> .title1 { font-family:Arial; font-size: 32px; color: #4a99c6;} table { font-family:Arial; } </style>";
		return aux;
	}

}
