package fr.sedoo.sssdata.client.ui.dialog;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.crud.widget.CrudConfirmCallBack;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.dialog.EditDialogContent;
import fr.sedoo.sssdata.shared.Administrator;

public class AdministratorEditDialogContent extends EditDialogContent implements DialogBoxContent {

	private static AdministratorEditDialogContentUiBinder uiBinder = GWT
			.create(AdministratorEditDialogContentUiBinder.class);

	interface AdministratorEditDialogContentUiBinder extends
	UiBinder<Widget, AdministratorEditDialogContent> {
	}

	Long id;
	
	@UiField
	TextBox name;

	@UiField
	TextBox login;

	@UiField
	TextBox password;

	public AdministratorEditDialogContent(CrudConfirmCallBack confirmCallback, Administrator administrator) {
		super(confirmCallback);
		initWidget(uiBinder.createAndBindUi(this));
		edit(administrator);
	}

	
	private void edit(Administrator administrator)
	{
		id = administrator.getId();
		name.setText(StringUtil.trimToEmpty(administrator.getName()));
		login.setText(StringUtil.trimToEmpty(administrator.getLogin()));
		password.setText(StringUtil.trimToEmpty(administrator.getPassword()));
	}
	

	@Override
	public Administrator flush() 
	{
		Administrator aux = new Administrator();
		aux.setId(id);
		aux.setName(name.getText());
		aux.setLogin(login.getText());
		aux.setPassword(password.getText());
		return aux;
	}


	@Override
	public String getPreferredHeight() {
		return "200px";
	}


	
	

}
