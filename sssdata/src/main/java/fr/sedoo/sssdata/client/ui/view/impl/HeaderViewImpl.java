package fr.sedoo.sssdata.client.ui.view.impl;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.component.AbstractView;
import fr.sedoo.commons.client.config.event.ConfigChangeEvent;
import fr.sedoo.commons.client.event.MaximizeEvent;
import fr.sedoo.commons.client.event.MinimizeEvent;
import fr.sedoo.sssdata.client.GlobalBundle;
import fr.sedoo.sssdata.client.ui.view.api.HeaderView;
import fr.sedoo.sssdata.shared.ConfigPropertiesCode;

public class HeaderViewImpl extends AbstractView implements HeaderView {

	private static WelcomeViewImplUiBinder uiBinder = GWT
			.create(WelcomeViewImplUiBinder.class);

	interface WelcomeViewImplUiBinder extends UiBinder<Widget, HeaderViewImpl> {
	}

	@UiField 
	DockLayoutPanel contentPanel;
	
	@UiField
	HTMLPanel north;
	
	@UiField
	Label websiteTitle;
	
	public HeaderViewImpl() {
		super();
		GWT.<GlobalBundle>create(GlobalBundle.class).css().ensureInjected();
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
	}
	
	@Override
	public void onNotification(MinimizeEvent event) {
		contentPanel.setWidgetHidden(north, false);
		contentPanel.animate(500);		
	}

	@Override
	public void onNotification(MaximizeEvent event) {
		contentPanel.setWidgetHidden(north, true);
		contentPanel.animate(500);	
	}

	@Override
	public double getPreferredHeight() {
		return 182;
	}

	@Override
	public void onConfigChange(ConfigChangeEvent event) {
		if (event.getProperty().getCode().equals(ConfigPropertiesCode.WEBSITE_TITLE)){
			setWebsiteTitle(event.getProperty().getValue());			
		}
	}

	@Override
	public void setWebsiteTitle(String title) {
		websiteTitle.setText(title);			
	}

	@Override
	public void postLoadProcess(String result) {
		setWebsiteTitle(result);
	}

}
