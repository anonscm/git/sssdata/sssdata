package fr.sedoo.sssdata.client.ui.view.api;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.editor.client.Editor;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.IsWidget;

import fr.sedoo.sssdata.shared.BoatDTO;
import fr.sedoo.sssdata.shared.SearchCriterion;
import fr.sedoo.sssdata.shared.Trajectory;

public interface SearchView extends IsWidget, Editor<SearchCriterion>{

	void reset();
	void setSearchMessage(String searchMessage);
	void setBoatList(ArrayList<BoatDTO> boats);
	void setPresenter(Presenter presenter);

	void addErrorMessage(String errorMessage);
	void resetErrorMessages();
	
	public interface Presenter 
	{
		void search(SearchCriterion searchCriterion);
		void getDefaultCriterion();
		void downloadFromSearchCriterions(SearchCriterion flush,
				List<String> allSelectedTrajectories);
		void stop();

		void downloadPretreatedArchive(String archiveType);
	}

	//void addFeatureOnMap(HashMap<Boat, List<Trajectory>> trajectoriesToPrint, EventBus eventBus);
	
	void initFeatureLayerOnMap(EventBus eventBus);
	void addFeatureOnMap(BoatDTO b, List<Trajectory> trajectoriesToPrint);
	
	public void showSelectTrajectoriesButtons();
	public void hideSelectTrajectoriesButtons();
	public void showDowloadButton();
	public void hideDowloadButton();
	void setSearchCriterion(SearchCriterion searchCriterion);
		
	/*void addMessage(String message);
	void resetMessages();*/
	
	void initProgressBar(int maxProgress);
	void updateProgressBar();
	void resetProgressBar();
}
