package fr.sedoo.sssdata.client.ui.component.impl;

import java.util.LinkedList;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.component.AbstractStyledResizeComposite;
import fr.sedoo.commons.client.event.ActionEndEvent;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.util.ElementUtil;
import fr.sedoo.commons.client.util.ListUtil;
import fr.sedoo.sssdata.client.GlobalBundle;
import fr.sedoo.sssdata.client.event.ParameterLoadedEvent;
import fr.sedoo.sssdata.client.ui.component.api.StatusBar;
import fr.sedoo.sssdata.shared.ParameterConstants;

public class StatusBarImpl extends AbstractStyledResizeComposite implements StatusBar {
	
	@UiField
	Element message;
	
	@UiField 
	Label version;
	
	@UiField 
	Image loading;
	
	@UiField 
	DockLayoutPanel contentPanel;
	
	@UiField
	HTMLPanel south;
	
	LinkedList<String> messages = new LinkedList<String>();
	
	private static StatusBarImplUiBinder uiBinder = GWT
			.create(StatusBarImplUiBinder.class);

	interface StatusBarImplUiBinder extends UiBinder<Widget, StatusBarImpl> {
	}

	public StatusBarImpl() {
		GWT.<GlobalBundle>create(GlobalBundle.class).css().ensureInjected();
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
		version.setText("");
		updateDisplay();
	}

	private void updateDisplay(){
		if (messages.isEmpty()){
			ElementUtil.hide(loading);
			message.setInnerText(" ");
		}else{
			ElementUtil.show(loading);
			message.setInnerText(messages.get(ListUtil.FIRST_INDEX));
		}
	}

	
	@Override
	public void onNotification(ActionEndEvent event) {
		 messages.remove(event.getMessage());
         updateDisplay();
	}

	@Override
	public void onNotification(ActionStartEvent event) {
		 messages.add(event.getMessage());
         updateDisplay();
	}

	@Override
	public double getPreferredHeight() {
		return 25;
	}

	@Override
	public void onNotification(ParameterLoadedEvent event) 
	{
		Map<String, String> parameters = event.getParameters();
		String versionText = parameters.get(ParameterConstants.APPLICATION_VERSION_PARAMETER_NAME);
		if (versionText != null){
			version.setText(versionText);
		}
	}

}
