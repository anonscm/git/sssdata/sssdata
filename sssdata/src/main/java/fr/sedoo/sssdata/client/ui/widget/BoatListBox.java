package fr.sedoo.sssdata.client.ui.widget;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.user.client.ui.ListBox;

import fr.sedoo.sssdata.shared.BoatDTO;

public class BoatListBox extends ListBox implements LeafValueEditor<List<BoatDTO>>{

	@Override
	public void setValue(List<BoatDTO> value) {
		for (BoatDTO b : value){
			//this.addItem(b.getName(), b.getId()+"");
			this.addItem(b.getName());
		}
		
	}

	@Override
	public List<BoatDTO> getValue() {
		List<BoatDTO> boats = new ArrayList<BoatDTO>();
		for (int i=0;i<this.getItemCount();i++){
			if (this.isItemSelected(i)){
				BoatDTO b = new BoatDTO();
				//b.setId(Long.parseLong(this.getValue(i)));
				b.setName(this.getItemText(i));
				boats.add(b);
			}
			
		}
		return boats;
	}
	
	public void setAllItemSelected(){
		for (int i=0;i<this.getItemCount();i++){
			this.setItemSelected(i, true);
		}
	}
	
	public void reset(){
		for (int i=0;i<this.getItemCount();i++){
			this.setItemSelected(i, false);
		}
	}
}
