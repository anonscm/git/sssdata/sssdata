package fr.sedoo.sssdata.client.ui.view.impl;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.component.AbstractView;
import fr.sedoo.sssdata.client.ui.component.DownloadTablePanel;
import fr.sedoo.sssdata.client.ui.dialog.DownloadLogDialogBox;
import fr.sedoo.sssdata.client.ui.view.api.DownloadLogView;
import fr.sedoo.sssdata.shared.DownloadDTO;

public class DownloadLogViewImpl extends AbstractView implements DownloadLogView {

	interface DownloadLogViewImplUiBinder extends UiBinder<Widget, DownloadLogViewImpl> {}
	private static DownloadLogViewImplUiBinder uiBinder = GWT.create(DownloadLogViewImplUiBinder.class);
		
	Presenter presenter;
	
	@UiField
	DownloadTablePanel table;
	
	DownloadLogDialogBox box;
	
	public DownloadLogViewImpl() {
		super();
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
		box = new DownloadLogDialogBox();
	}	
	
	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
		table.setPresenter(presenter);
	}

	@Override
	public void setDownloads(List<DownloadDTO> downloads) {
		table.setData(downloads);
	}

	@Override
	public void displayDownload(DownloadDTO download) {
		box.setData(download);
		box.center();
	}

}
