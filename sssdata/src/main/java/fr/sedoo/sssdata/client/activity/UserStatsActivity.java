package fr.sedoo.sssdata.client.activity;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.sssdata.client.ClientFactory;
import fr.sedoo.sssdata.client.event.ActionEventConstants;
import fr.sedoo.sssdata.client.message.CommonMessages;
import fr.sedoo.sssdata.client.place.UserStatsPlace;
import fr.sedoo.sssdata.client.service.StatsService;
import fr.sedoo.sssdata.client.service.StatsServiceAsync;
import fr.sedoo.sssdata.client.ui.view.api.UserStatsView;
import fr.sedoo.sssdata.client.ui.view.api.UserStatsView.Presenter;
import fr.sedoo.sssdata.shared.UserStatsDTO;

public class UserStatsActivity extends SSSAdministrationActivity implements Presenter {

	public final static StatsServiceAsync STATS_SERVICE = GWT.create(StatsService.class);

	UserStatsView view;

	public UserStatsActivity(UserStatsPlace place, AuthenticatedClientFactory clientFactory) {
		super(clientFactory, place);

	}

	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		super.start(containerWidget, eventBus);

		if (isValidUser()) {
			view = ((ClientFactory) clientFactory).getUserStatsView();
			view.setPresenter(this);

			containerWidget.setWidget(view.asWidget());

			//TODO Récupérer les stats
			final ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstants.BASIC_LOADING_EVENT, true);
			eventbus.fireEvent(e);
			STATS_SERVICE.getUserStats(new DefaultAbstractCallBack<UserStatsDTO>(e, eventbus){

				@Override
				public void onSuccess(UserStatsDTO result) {
					super.onSuccess(result);
					if (result != null){
						view.setStats(result);
						view.display();
					}else{
						//TODO si pas de résultat ?
					}
				}
			});

		}
	}
}
