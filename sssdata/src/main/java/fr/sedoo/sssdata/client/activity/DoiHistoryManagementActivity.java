package fr.sedoo.sssdata.client.activity;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.event.BreadCrumbChangeEvent;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.sssdata.client.ClientFactory;
import fr.sedoo.sssdata.client.event.ActionEventConstants;
import fr.sedoo.sssdata.client.event.NotificationEvent;
import fr.sedoo.sssdata.client.message.CommonMessages;
import fr.sedoo.sssdata.client.place.DoiHistoryPlace;
import fr.sedoo.sssdata.client.service.DoiHistoryService;
import fr.sedoo.sssdata.client.service.DoiHistoryServiceAsync;
import fr.sedoo.sssdata.client.ui.component.ShortcutFactory;
import fr.sedoo.sssdata.client.ui.view.api.DoiHistoryManagementView;
import fr.sedoo.sssdata.shared.metadata.DoiInfos;
import fr.sedoo.sssdata.shared.metadata.HistoryItem;

public class DoiHistoryManagementActivity extends SSSCrudActivity  {

	public final static DoiHistoryServiceAsync DOI_HISTORY_SERVICE = GWT.create(DoiHistoryService.class);
	
	DoiHistoryManagementView view;
	DoiHistoryPlace doiPlace;
	
	public DoiHistoryManagementActivity(DoiHistoryPlace place, ClientFactory clientFactory) {
    	super(clientFactory, place);
    	this.doiPlace = place;
    }
	
	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		if (isValidUser() == false) {
			goToLoginPlace();
			return;
		}
		
		view = ((ClientFactory) clientFactory).getDoiHistoryManagementView();
        containerWidget.setWidget(view.asWidget());
        view.setCrudPresenter(this);
		
        List<Shortcut> shortcuts = new ArrayList<Shortcut>();
        shortcuts.add(ShortcutFactory.getWelcomeShortcut());
        shortcuts.add(ShortcutFactory.getAdminShortcut());
        eventBus.fireEvent(new BreadCrumbChangeEvent(shortcuts));
		 
        ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstants.BASIC_LOADING_EVENT, true);
    	eventBus.fireEvent(e);
        
    	DOI_HISTORY_SERVICE.getDoiInfos(doiPlace.getDoiSuffix(), new DefaultAbstractCallBack<DoiInfos>(e, eventBus) {
			@Override
			public void onSuccess(DoiInfos infos) {
				super.onSuccess(infos);
				view.setHistoryItems(infos.getHistory());
				view.setDOI(infos.getDoi());
				view.setCurrentVersion(infos.getVersion());
			}
			
		});
        
	}

	@Override
	public void delete(final HasIdentifier hasIdentifier) {
		EventBus eventBus = clientFactory.getEventBus();
		ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstants.BASIC_DELETING_EVENT, true);
    	eventBus.fireEvent(e);
    	DOI_HISTORY_SERVICE.delete((HistoryItem) hasIdentifier, new DefaultAbstractCallBack<Void>(e, eventBus) {

			@Override
			public void onSuccess(Void result) {
				super.onSuccess(result);
				view.broadcastDeletion(hasIdentifier);
				clientFactory.getEventBus().fireEvent(new NotificationEvent(CommonMessages.INSTANCE.deletedElement()));
			}
		});
	}

	@Override
	public void create(HasIdentifier hasIdentifier) {
		EventBus eventBus = clientFactory.getEventBus();
		ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.saving(), ActionEventConstants.BASIC_SAVING_EVENT, true);
    	eventBus.fireEvent(e);
    	DOI_HISTORY_SERVICE.create((HistoryItem) hasIdentifier, doiPlace.getDoiSuffix(), new DefaultAbstractCallBack<HistoryItem>(e, eventBus) {
			@Override
			public void onSuccess(HistoryItem savedItem) {
				super.onSuccess(savedItem);
				view.broadcastCreation(savedItem);
				clientFactory.getEventBus().fireEvent(new NotificationEvent(CommonMessages.INSTANCE.savedElement()));
			}
		});
	}

	@Override
	public void edit(HasIdentifier hasIdentifier) {
		EventBus eventBus = clientFactory.getEventBus();
		ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.saving(), ActionEventConstants.BASIC_SAVING_EVENT, true);
    	eventBus.fireEvent(e);
    	DOI_HISTORY_SERVICE.edit((HistoryItem) hasIdentifier, doiPlace.getDoiSuffix(), new DefaultAbstractCallBack<HistoryItem>(e, eventBus) {

			@Override
			public void onSuccess(HistoryItem savedItem) {
				super.onSuccess(savedItem);
				view.broadcastEdition(savedItem);
				clientFactory.getEventBus().fireEvent(new NotificationEvent(CommonMessages.INSTANCE.savedElement()));
			}
		});			
	}
	
}
