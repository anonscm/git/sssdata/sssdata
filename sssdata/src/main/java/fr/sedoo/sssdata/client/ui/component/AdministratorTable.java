package fr.sedoo.sssdata.client.ui.component;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;

import fr.sedoo.commons.client.crud.widget.CreateConfirmCallBack;
import fr.sedoo.commons.client.crud.widget.CrudTable;
import fr.sedoo.commons.client.crud.widget.EditConfirmCallBack;
import fr.sedoo.commons.client.widget.dialog.EditDialogContent;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.sssdata.client.message.ApplicationMessages;
import fr.sedoo.sssdata.client.message.CommonMessages;
import fr.sedoo.sssdata.client.ui.dialog.AdministratorEditDialogContent;
import fr.sedoo.sssdata.shared.Administrator;

public class AdministratorTable extends CrudTable{

	public AdministratorTable() {
		super();
		setAddButtonEnabled(true);
	}

	

	

	@Override
	public String getAddItemText() {
		return ApplicationMessages.INSTANCE.administratorManagementViewAddAdministrator();
	}

	protected void initColumns() 
	{

		TextColumn<HasIdentifier> nameColumn = new TextColumn<HasIdentifier>() {
			@Override
			public String getValue(HasIdentifier aux) 
			{
				Administrator administrator = (Administrator) aux;
				return administrator.getName();
			}
		};

		TextColumn<HasIdentifier> loginColumn = new TextColumn<HasIdentifier>() {
			@Override
			public String getValue(HasIdentifier aux) 
			{
				Administrator administrator = (Administrator) aux;
				return administrator.getLogin();
			}
		};
		
		TextColumn<HasIdentifier> passwordColumn = new TextColumn<HasIdentifier>() {
			@Override
			public String getValue(HasIdentifier aux) 
			{
				Administrator administrator = (Administrator) aux;
				return administrator.getPassword();
			}
		};

		table.addColumn(nameColumn, CommonMessages.INSTANCE.name());
		table.setColumnWidth(nameColumn, 100.0, Unit.PX);
		table.addColumn(loginColumn, CommonMessages.INSTANCE.login());
		table.setColumnWidth(loginColumn, 100.0, Unit.PX);
		table.addColumn(passwordColumn, CommonMessages.INSTANCE.password());
		table.setColumnWidth(passwordColumn, 100.0, Unit.PX);
		table.addColumn(editColumn);
		table.addColumn(deleteColumn);
		table.setColumnWidth(editColumn, 30.0, Unit.PX);
		table.setColumnWidth(deleteColumn, 30.0, Unit.PX);
	}

	@Override
	public String getEditDialogTitle() {
		return ApplicationMessages.INSTANCE.administratorManagementViewEditAdministrator();
	}

	@Override
	public String getCreateDialogTitle() {
		return ApplicationMessages.INSTANCE.administratorManagementViewAddAdministrator();
	}
	
	@Override
	public EditDialogContent getEditDialogContent(EditConfirmCallBack callBack, HasIdentifier hasIdentifier) {
		return new AdministratorEditDialogContent(callBack, (Administrator) hasIdentifier);
	}

	@Override
	public EditDialogContent getCreateDialogContent(CreateConfirmCallBack callBack, HasIdentifier hasIdentifier) {
		return new AdministratorEditDialogContent(callBack, (Administrator) hasIdentifier);
	}

	@Override
	public HasIdentifier createNewItem() {
		
		return new Administrator();
	}
}
