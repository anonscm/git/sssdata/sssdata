package fr.sedoo.sssdata.client.ui.view.api;

import java.util.ArrayList;

import fr.sedoo.commons.client.crud.component.CrudView;
import fr.sedoo.sssdata.shared.metadata.HistoryItem;

public interface DoiHistoryManagementView extends CrudView {

	void setHistoryItems(ArrayList<HistoryItem> items);
	
	void setCurrentVersion(String version);
	void setDOI(String doi);

}
