package fr.sedoo.sssdata.client.ui.view.api;

import fr.sedoo.commons.client.callback.LoadCallBack;
import fr.sedoo.commons.client.config.event.ConfigChangeEventHandler;
import fr.sedoo.commons.client.event.MaximizeEventHandler;
import fr.sedoo.commons.client.event.MinimizeEventHandler;
import fr.sedoo.sssdata.client.ui.widget.PreferredHeightWidget;

public interface HeaderView extends LoadCallBack<String>, MinimizeEventHandler, MaximizeEventHandler, PreferredHeightWidget, ConfigChangeEventHandler {
		void setWebsiteTitle(String title);
}
