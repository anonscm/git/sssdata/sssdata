package fr.sedoo.sssdata.client.service;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.sedoo.sssdata.shared.DBStatusDTO;
import fr.sedoo.sssdata.shared.DownloadDTO;
import fr.sedoo.sssdata.shared.FileSearchInformations;
import fr.sedoo.sssdata.shared.UpdateDTO;

public interface JournalServiceAsync {

	void getAllDownloads(AsyncCallback<List<DownloadDTO>> callback);

	void getLastUpdates(int nb, AsyncCallback<List<UpdateDTO>> callback);

	void getAllUpdates(AsyncCallback<List<UpdateDTO>> callback);

	void getUpdateHits(AsyncCallback<Integer> callback);

	void getUpdatesByPage(int first, int pageSize,
			AsyncCallback<List<UpdateDTO>> callback);

	void getDownload(int id, AsyncCallback<DownloadDTO> callback);
	
	void searchFiles(FileSearchInformations infos,
			AsyncCallback<List<UpdateDTO>> callback);

	void deleteFile(UpdateDTO u, AsyncCallback<Void> callback);

	void getDBStatus(AsyncCallback<DBStatusDTO> callback);

	void delete(int id, AsyncCallback<Void> callback);
}
