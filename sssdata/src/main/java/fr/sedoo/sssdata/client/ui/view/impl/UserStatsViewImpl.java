package fr.sedoo.sssdata.client.ui.view.impl;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.visualization.client.AbstractDataTable.ColumnType;
import com.google.gwt.visualization.client.DataTable;
import com.google.gwt.visualization.client.VisualizationUtils;
import com.google.gwt.visualization.client.visualizations.corechart.LineChart;
import com.google.gwt.visualization.client.visualizations.corechart.Options;
import com.google.gwt.visualization.client.visualizations.corechart.PieChart;

import fr.sedoo.commons.client.component.AbstractView;
import fr.sedoo.commons.client.widget.panel.HorizontalCollapsingPanel;
import fr.sedoo.sssdata.client.ui.view.api.UserStatsView;
import fr.sedoo.sssdata.shared.CountryStat;
import fr.sedoo.sssdata.shared.UserStatsDTO;

public class UserStatsViewImpl extends AbstractView implements UserStatsView {

	private static UserStatsViewImplUiBinder uiBinder = GWT.create(UserStatsViewImplUiBinder.class);

	interface UserStatsViewImplUiBinder extends UiBinder<Widget, UserStatsViewImpl> {
	}
	
	Presenter presenter;
	
	@UiField
	HorizontalCollapsingPanel countryStatsPanel;
	/*@UiField
	HorizontalCollapsingPanel userStatsPanel;*/
	
	UserStatsDTO stats;
	
	PieChart countryChart;
	LineChart registrationsChart;
	
	Options countryChartOptions;
	Options registrationsChartOptions;
	
	public UserStatsViewImpl() {
		super();
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();		
		
		
		
		/*VisualizationUtils.loadVisualizationApi(new Runnable() {
			@Override
			public void run() {
				
			}
		}, LineChart.PACKAGE);*/
		
	}
	
		
	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}
		
	@Override
	public void display() {
		VisualizationUtils.loadVisualizationApi(new Runnable() {
			public void run() {
				displayUsersByCountry();
			}}, PieChart.PACKAGE);
	}
	
	private void displayUsersByCountry() {
		if (stats.getNbUsersByCountry() != null){
			DataTable data = DataTable.create();
									
			data.addColumn(ColumnType.STRING, "Country");
			data.addColumn(ColumnType.NUMBER, "Users");
			data.addRows(stats.getNbUsersByCountry().size());
			int row = 0;
			
			for (CountryStat cs: stats.getNbUsersByCountry()){
				data.setValue(row, 0, cs.getCountry());
				data.setValue(row, 1, cs.getUsers());
				row++;
			}
			/*for(String country: stats.getNbUsersByCountry().keySet()){
				data.setValue(row, 0, country);
				data.setValue(row, 1, stats.getNbUsersByCountry().get(country));
				row++;
			}*/
			
			countryChartOptions = Options.create();
			countryChartOptions.setWidth(800);
			countryChartOptions.setHeight(600);
			//countryChartOptions.set3D(true);
			countryChartOptions.setTitle("");
			
			countryChart = new PieChart(data, countryChartOptions);
			countryStatsPanel.clear();
			countryStatsPanel.add(countryChart);
		}
		
		/*if (stats.getRegistrationDates() != null){
			DataTable data = DataTable.create();
			data.addColumn(ColumnType.DATE, "Date");
			data.addColumn(ColumnType.NUMBER, "Users");
			data.addRows(stats.getRegistrationDates().size());
			int row = 0;
			for (Date date: stats.getRegistrationDates()){
				data.setValue(row, 0, date);
				data.setValue(row, 1, row);
				row++;
			}
			
			//DateFormat.Options dateFormatOptions = DateFormat.Options.create();
			//dateFormatOptions.setPattern("MMM y");
			//DateFormat dateFormatter = DateFormat.create(dateFormatOptions);
			//dateFormatter.format(data, 0);
			
			registrationsChartOptions = Options.create();
			registrationsChartOptions.setWidth(600);
			registrationsChartOptions.setHeight(400);
			registrationsChartOptions.setLegend(LegendPosition.NONE);
			AxisOptions vAxisOptions = AxisOptions.create();
			vAxisOptions.setTitle("Registered users");
			vAxisOptions.setMinValue(0);
			vAxisOptions.setMaxValue(stats.getRegistrationDates().size());
			AxisOptions hAxisOptions = AxisOptions.create();
			hAxisOptions.set("format", "MMM y");
			
			registrationsChartOptions.setHAxisOptions(hAxisOptions);
			registrationsChartOptions.setVAxisOptions(vAxisOptions);
			registrationsChart = new LineChart(data, registrationsChartOptions);
			userStatsPanel.clear();
			userStatsPanel.add(registrationsChart);
		}*/
	}


	@Override
	public void setStats(UserStatsDTO stats) {
		this.stats = stats;		
	}

}
