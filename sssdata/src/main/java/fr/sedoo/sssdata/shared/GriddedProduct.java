package fr.sedoo.sssdata.shared;

public class GriddedProduct {
	public static final String GRID_ATLANTIC = "ATLANTIC";
	public static final String GRID_PACIFIC = "PACIFIC";
	public static final String GRID_NASG = "NASG";
	public static final String GRID_NASPG = "NASPG";
	
	public static final String GRID_BIN_ATL = "BIN_ATL";
	
	public static final String SSS_HIST = "HIST";
	
	public static final int GRID_SIZE = -20;
}
