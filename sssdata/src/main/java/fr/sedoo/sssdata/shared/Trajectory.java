package fr.sedoo.sssdata.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Trajectory implements Serializable{

	private List<List<LatLong>> lines;
	
	private String sssFileName;
	
	private int resultSize;

	public Trajectory() {
		super();
		lines = new ArrayList<List<LatLong>>();
	}

	public List<List<LatLong>> getLines() {
		return lines;
	}

	public void addLine(List<LatLong> line) {
			this.lines.add(line);
	}
	
	public String getSssFileName() {
		return sssFileName;
	}

	public void setSssFileName(String sssFileName) {
		this.sssFileName = sssFileName;
	}
	
	public int getSize(){
		int size = 0;
		for (List<LatLong> line: lines){
			size += line.size(); 
		}
		return size;
	}

	/**
	 * 
	 * @return nombre de points qui seront retournés par l'extracteur
	 */
	public int getResultSize() {
		return resultSize;
	}
	public void setResultSize(int resultSize) {
		this.resultSize = resultSize;
	}
	
}

