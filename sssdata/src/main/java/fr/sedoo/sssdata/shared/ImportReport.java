package fr.sedoo.sssdata.shared;

import java.io.Serializable;


public class ImportReport implements Serializable{

	private int importedFileNumber;
	private String errorMessage="";
	
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public int getImportedFileNumber() {
		return importedFileNumber;
	}
	public void setImportedFileNumber(int importedFileNumber) {
		this.importedFileNumber = importedFileNumber;
	}
	
	
}
