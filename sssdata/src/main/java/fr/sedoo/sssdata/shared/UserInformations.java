package fr.sedoo.sssdata.shared;

import java.io.Serializable;

public class UserInformations implements Serializable {
	
	private String userName;
	private String userOrganism;
	private String userObjective;
	private String country;
	private String userEmail;
	private String dataFormat;
	private String dataCompression;
	

	public UserInformations() {
		super();
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserOrganism() {
		return userOrganism;
	}
	public void setUserOrganism(String userOrganism) {
		this.userOrganism = userOrganism;
	}

	public String getUserObjective() {
		return userObjective;
	}

	public void setUserObjective(String userObjective) {
		this.userObjective = userObjective;
	}

	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getDataFormat() {
		return dataFormat;
	}

	public void setDataFormat(String dataFormat) {
		this.dataFormat = dataFormat;
	}
	public String getDataCompression() {
		return dataCompression;
	}
	public void setDataCompression(String dataCompression) {
		this.dataCompression = dataCompression;
	}
}
