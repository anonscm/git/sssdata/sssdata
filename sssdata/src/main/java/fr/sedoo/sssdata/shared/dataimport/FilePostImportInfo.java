package fr.sedoo.sssdata.shared.dataimport;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

public class FilePostImportInfo implements IsSerializable
{
	private boolean success;
	private int lines;
	private String duration;
	
	//private boolean adjustedSspsMissing;
	
	private List<String> messages;
	
	public FilePostImportInfo() {
		super();
		this.messages = new ArrayList<String>();
	}
		
	public List<String> getMessages() {
		return messages;
	}
	public void setMessages(List<String> messages) {
		this.messages = messages;
	}
	
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public int getLines() {
		return lines;
	}
	public void setLines(int lines) {
		this.lines = lines;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}

	/*public boolean isAdjustedSspsMissing() {
		return adjustedSspsMissing;
	}
	public void setAdjustedSspsMissing(boolean adjustedSspsMissing) {
		this.adjustedSspsMissing = adjustedSspsMissing;
	}*/
	
}
