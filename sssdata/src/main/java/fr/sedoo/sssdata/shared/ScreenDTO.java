package fr.sedoo.sssdata.shared;

import java.io.Serializable;

public class ScreenDTO implements Serializable {
 
	private String name;
	private String content;
	
	public ScreenDTO() {
		super();
	}
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
}
