package fr.sedoo.sssdata.shared.metadata;

import java.io.Serializable;

public class RelatedIdentifierDTO implements Serializable {


	private String type;
	private String identifier;
	
	public RelatedIdentifierDTO() {
		super();
	}
			
	public RelatedIdentifierDTO(String identifier, String type) {
		super();
		this.identifier = identifier;
		this.type = type;
	}

	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
}
