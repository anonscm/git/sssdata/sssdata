package fr.sedoo.sssdata.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FileSearchInformations implements Serializable{

	private List<BoatDTO> boatListSelection;
	
	private Date beginDate;
	private Date endDate;
	
	public FileSearchInformations() {
		super();
		this.boatListSelection = new ArrayList<BoatDTO>();
	}

	public List<BoatDTO> getBoatListSelection() {
		return boatListSelection;
	}

	public void setBoatListSelection(List<BoatDTO> boatListSelection) {
		this.boatListSelection = boatListSelection;
	}

	public Date getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
		
}
