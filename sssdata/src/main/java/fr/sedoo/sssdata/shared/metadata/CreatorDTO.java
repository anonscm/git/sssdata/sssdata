package fr.sedoo.sssdata.shared.metadata;

import java.io.Serializable;

public class CreatorDTO implements Serializable{

	private String name;
	private String orcid;
	
	public CreatorDTO() {
		super();
	}

	public CreatorDTO(String name) {
		this(name, null);
	}
	
	public CreatorDTO(String name, String orcid) {
		super();
		this.name = name;
		this.orcid = orcid;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOrcid() {
		return orcid;
	}
	public void setOrcid(String orcid) {
		this.orcid = orcid;
	}
	
}
