package fr.sedoo.sssdata.shared;

public interface ParameterConstants {

	public final static String APPLICATION_VERSION_PARAMETER_NAME = "applicationVersion";
	public final static String JAVA_VERSION_PARAMETER_NAME = "javaVersion";
	public final static String TEST_MODE = "testMode";
	
	public final static String WELCOME_SCREEN = "welcomeScreen";
	public final static String POLICY_SCREEN = "dataPolicyScreen";
}
