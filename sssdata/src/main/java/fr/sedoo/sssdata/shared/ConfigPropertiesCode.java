package fr.sedoo.sssdata.shared;

public interface ConfigPropertiesCode {

	public static final String SALINITY_MIN = "search.salinity.min"; 
	public static final String SALINITY_MAX = "search.salinity.max";
	public static final String TEMPERATURE_MIN = "search.temperature.min";
	public static final String TEMPERATURE_MAX = "search.temperature.max";
	public static final String DATE_MIN = "search.date.min";
	public static final String DATE_MAX = "search.date.max";

	public static final String EMAIL_PREFIX = "email.prefix";
	public static final String EMAIL_GREETING = "email.greetings";
	public static final String EMAIL_SALUTATION = "email.salutation";
	public static final String EMAIL_SIGNATURE = "email.signature";
		
	public static final String DOWNLOAD_INTRO = "download.intro";
	public static final String DOWNLOAD_INTRO2 = "download.intro2";
	public static final String DOWNLOAD_POLICY = "download.policy";
	/*public static final String DOWNLOAD_ARCHIVE_GOOD_ASCII = "download.archive.good.ascii";
	public static final String DOWNLOAD_ARCHIVE_GOOD_NETCDF = "download.archive.good.netcdf";
	public static final String DOWNLOAD_ARCHIVE_ALL_ASCII = "download.archive.all.ascii";
	public static final String DOWNLOAD_ARCHIVE_ALL_NETCDF = "download.archive.all.netcdf";
	*/
	public static final String CONTACT_EMAIL = "contact.email";
	public static final String WEBSITE_TITLE = "website.title";
		
	public static final String OUTPUT_DATS_USE_CONSTRAINTS = "output.dats.useConstraints";
	public static final String OUTPUT_DATS_ACKNOWLEDGEMENT = "output.dats.acknowledgement";
	public static final String OUTPUT_DATS_TITLE = "output.dats.title";
	public static final String OUTPUT_DATS_ABSTRACT = "output.dats.abstract";
	public static final String OUTPUT_PROJECT_NAME = "output.project.name";
	public static final String OUTPUT_PROJECT_CONTACT = "output.project.email";
	public static final String OUTPUT_PROJECT_URL = "output.project.url";
	
}
