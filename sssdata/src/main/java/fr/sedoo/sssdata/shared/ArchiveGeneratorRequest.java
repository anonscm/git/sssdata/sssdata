package fr.sedoo.sssdata.shared;

import java.io.Serializable;

public class ArchiveGeneratorRequest implements Serializable {
	
	public static final String ARCHIVE_FLAG_ALL = "all";
	public static final String ARCHIVE_FLAG_GOOD = "good";
	
	/*public static final String REQUEST_ID_ASCII_GOOD = FileDTO.FORMAT_ASCII + "_" + ARCHIVE_FLAG_GOOD;
	public static final String REQUEST_ID_ASCII_ALL = FileDTO.FORMAT_ASCII + "_" + ARCHIVE_FLAG_ALL;
	public static final String REQUEST_ID_NETCDF_GOOD = FileDTO.FORMAT_NETCDF + "_" + ARCHIVE_FLAG_GOOD;
	public static final String REQUEST_ID_NETCDF_ALL = FileDTO.FORMAT_NETCDF + "_" + ARCHIVE_FLAG_ALL;*/
		
	private String requestId;
	private String fileFormat;
	private String flag;
	
	private SearchCriterion searchCriterion;
		
	public ArchiveGeneratorRequest() {
		super();
	}
	
	public ArchiveGeneratorRequest(String flag, String fileFormat, SearchCriterion searchCriterion) {
		super();
		this.requestId = fileFormat + "_" + flag;
		this.fileFormat = fileFormat;
		this.flag = flag;
		this.searchCriterion = searchCriterion;
	}

	public String getRequestId() {
		return requestId;
	}

	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	
	public String getFileFormat() {
		return fileFormat;
	}

	public void setFileFormat(String fileFormat) {
		this.fileFormat = fileFormat;
	}

	public SearchCriterion getSearchCriterion() {
		return searchCriterion;
	}

	public void setSearchCriterion(SearchCriterion searchCriterion) {
		this.searchCriterion = searchCriterion;
	}

}
