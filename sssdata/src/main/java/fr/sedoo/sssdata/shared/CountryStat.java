package fr.sedoo.sssdata.shared;

import java.io.Serializable;

public class CountryStat implements Comparable<CountryStat>, Serializable {

	private String country;
	private int users;
	
	public CountryStat() {
		super();
	}
		
	public CountryStat(String country, int users) {
		super();
		this.country = country;
		this.users = users;
	}

	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public int getUsers() {
		return users;
	}
	public void setUsers(int users) {
		this.users = users;
	}

	@Override
	public int compareTo(CountryStat o) {
		return -1 * Integer.compare(users, o.getUsers());
	}
		
}
