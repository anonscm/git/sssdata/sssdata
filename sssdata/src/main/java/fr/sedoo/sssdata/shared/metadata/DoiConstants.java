package fr.sedoo.sssdata.shared.metadata;

public class DoiConstants {

	public static final String SSS_DATA = "SSS-LEGOS";
	
	public static final String SSS_HIST = "HIST-SSS-LEGOS";
	
	public static final String GRID_ATL = "SSS-LEGOS-GRID-ATL";
	public static final String GRID_PAC = "SSS-LEGOS-GRID-PAC";
	
	public static final String SSS_BIN_NASG = "SSS-BIN-NASG";
	
	public static final String SSS_BIN_ATL = "SSS-BINS-ATL";
		
	public static final String TSD_BINS_NASPG = "TSD-BINS-NASPG";
	
		
}
