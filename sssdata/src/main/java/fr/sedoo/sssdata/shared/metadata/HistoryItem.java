package fr.sedoo.sssdata.shared.metadata;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.commons.util.StringUtil;

public class HistoryItem implements Serializable, Comparable<HistoryItem>, HasIdentifier{

	private int id;
	private String date;
	private String version;
	private String comment;
				
	public HistoryItem() {
		super();
	}
	@Override
	public int compareTo(HistoryItem o) {
		return date.compareTo(o.getDate());
	}
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	
	@Override
	public String getIdentifier() {
		return ""+getId();
	}
	@Override
	public List<ValidationAlert> validate() {
		List<ValidationAlert> alerts = new ArrayList<ValidationAlert>();
		if (StringUtil.isEmpty(comment)){
			alerts.add(new ValidationAlert("comment", "required"));
		}
		
		return alerts;
	}
		
}

