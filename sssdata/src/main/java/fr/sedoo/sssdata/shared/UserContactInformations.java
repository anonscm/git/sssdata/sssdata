package fr.sedoo.sssdata.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserContactInformations implements Serializable {

	private String title;
	private String message;
	private String greeting;
	private String salutation;
	private String signature;
	private String subjectPrefix;
	
	private String fromEmail;
	
	private List<BoatDTO> boatListSelection;
	
	private Date beginDate;
	private Date endDate;
	
	private boolean addNews;
	
	public UserContactInformations() {
		super();
		this.boatListSelection = new ArrayList<BoatDTO>();
	}
	
	public String getSubject() {
		return subjectPrefix + " " + title;
	}
	public String getBody() {
		return greeting + "\n\n" + message + "\n\n" + salutation + "\n" + signature;
	}
	
	public List<BoatDTO> getBoatListSelection() {
		return boatListSelection;
	}
	public void setBoatListSelection(List<BoatDTO> boatListSelection) {
		this.boatListSelection = boatListSelection;
	}
	public Date getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public boolean isAddNews() {
		return addNews;
	}
	public void setAddNews(boolean addNews) {
		this.addNews = addNews;
	}

	public String getSalutation() {
		return salutation;
	}
	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getGreeting() {
		return greeting;
	}

	public void setGreeting(String greeting) {
		this.greeting = greeting;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}
	
	public String getSubjectPrefix() {
		return subjectPrefix;
	}
	
	public void setSubjectPrefix(String subjectPrefix) {
		this.subjectPrefix = subjectPrefix;
	}

	public String getFromEmail() {
		return fromEmail;
	}

	public void setFromEmail(String fromEmail) {
		this.fromEmail = fromEmail;
	}
		
}
