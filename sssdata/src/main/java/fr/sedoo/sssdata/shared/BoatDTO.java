package fr.sedoo.sssdata.shared;

import java.io.Serializable;

public class BoatDTO implements Serializable {

	private String name;
	
	public BoatDTO() {
		super();
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
}
