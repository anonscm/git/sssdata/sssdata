package fr.sedoo.sssdata.shared.dataimport;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.IsSerializable;

public class BoatPreImportInfo implements IsSerializable
{
	private boolean alreadyPresent;
	private ArrayList<String> fileNames;
	
	public boolean isAlreadyPresent() {
		return alreadyPresent;
	}
	public void setAlreadyPresent(boolean alreadyPresent) {
		this.alreadyPresent = alreadyPresent;
	}
	public ArrayList<String> getFileNames() {
		return fileNames;
	}
	public void setFileNames(ArrayList<String> fileNames) {
		this.fileNames = fileNames;
	}
	
}
