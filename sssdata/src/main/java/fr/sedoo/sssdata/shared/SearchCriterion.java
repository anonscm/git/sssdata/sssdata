package fr.sedoo.sssdata.shared;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

import fr.sedoo.commons.client.util.DoubleUtil;
import fr.sedoo.commons.shared.domain.GeographicBoundingBoxDTO;

public class SearchCriterion implements IsSerializable {
	

	private Date beginDate;
	private Date endDate;

	private boolean uncontrolledData;
	private boolean goodData;
	private boolean probablyGoodData;
	private boolean probablyBadData;
	private boolean badData;
	private boolean harbourData;
	
	private List<BoatDTO> boatListSelection;
	private Double temperatureMin;
	private Double temperatureMax;
	private Double salinityMin;
	private Double salinityMax;
	private GeographicBoundingBoxDTO boundingBox;
	
	private int zoomLevel = 1;
	private Boundings mapExtent;
	private List<Boundings>boundings = null; 
	
	public void setZoomLevel(int zoomLevel) {
		this.zoomLevel = zoomLevel;
	}
	public int getZoomLevel() {
		return zoomLevel;
	}
	public void setMapExtent(Boundings mapExtent) {
		this.mapExtent = mapExtent;
		this.boundings = null;
	}
	public Boundings getMapExtent() {
		return mapExtent;
	}

	private List<Boundings> splitZone(Boundings zone){
		double latMin = zone.getSouthBoundLatitude();
		double latMax = zone.getNorthBoundLatitude();
		double lonMin = zone.getWestBoundLongitude();
		double lonMax = zone.getEastBoundLongitude();

		if (lonMin < -180){
			if (lonMax <= -180){
				return Collections.singletonList(new Boundings(latMax, lonMax + 360.0, latMin, lonMin + 360.0));
			}else if (lonMax < 180){
				List<Boundings> boundings = new ArrayList<Boundings>();
				boundings.add(new Boundings(latMax, 180.0, latMin, lonMin + 360));
				boundings.add(new Boundings(latMax, lonMax, latMin, -180.0));
				return boundings;
			}else {
				return Collections.singletonList(new Boundings(latMax, 180.0, latMin, -180.0));
			}
		}else if (lonMin >= 180){
			return Collections.singletonList(new Boundings(latMax, lonMax - 360.0, latMin, lonMin - 360.0));
		}else{
			//-180 <= lonMin < 180
			if (lonMax > 180 ){
				List<Boundings> boundings = new ArrayList<Boundings>();
				boundings.add(new Boundings(latMax, 180.0, latMin, lonMin));
				boundings.add(new Boundings(latMax, lonMax - 360, latMin, -180.0));
				return boundings;
			}else{
				return Collections.singletonList(zone);
			}
		}
	}

	public List<Boundings> getBoundings(){
		if (boundings == null) {
			boolean withBbox = DoubleUtil.checkDouble(getBoundingBox().getNorthBoundLatitude())  
					&& DoubleUtil.checkDouble(getBoundingBox().getSouthBoundLatitude())
					&& DoubleUtil.checkDouble(getBoundingBox().getEastBoundLongitude()) 
					&& DoubleUtil.checkDouble(getBoundingBox().getWestBoundLongitude());

			if (withBbox){
				double lonMin = Double.parseDouble(getBoundingBox().getWestBoundLongitude());
				double lonMax = Double.parseDouble(getBoundingBox().getEastBoundLongitude());
				double latMin = Double.parseDouble(getBoundingBox().getSouthBoundLatitude());
				double latMax = Double.parseDouble(getBoundingBox().getNorthBoundLatitude());		

				//Zoom level déterminé à partir de l'aire de la zone dessinée
				double boxArea = Math.abs(lonMax - lonMin) * Math.abs(latMax - latMin);
				
				if ( boxArea >= 45000 ){
					setZoomLevel(1);
				}else if ( boxArea >= 5400 ){
					setZoomLevel(3);
				}else if ( boxArea >= 400 ){
					setZoomLevel(5);
				}else if ( boxArea >= 25 ){
					setZoomLevel(7);
				}else{
					setZoomLevel(9);
				}
				
				this.boundings =  splitZone(new Boundings(latMax, lonMax, latMin, lonMin));				
			}else {
				//pas de zone dessinée => on utilise la zone affichée
				if (getZoomLevel() == 1){
					this.boundings = Collections.singletonList(new Boundings(90.0, 180.0, -90.0, -180.0));
				}else{
					this.boundings =  splitZone(getMapExtent());
				}
			}
		}
		return boundings;
	}
	
	
	public SearchCriterion() {
		super();
		boundingBox = new GeographicBoundingBoxDTO();
		boatListSelection = new ArrayList<BoatDTO>();
		this.boundings = null;
	}
	
	
	public List<BoatDTO> getBoatListSelection() {
		return boatListSelection;
	}
	public void setBoatListSelection(List<BoatDTO> boatList) {
		this.boatListSelection = boatList;
	}

	public Double getTemperatureMin() {
		return temperatureMin;
	}
	public void setTemperatureMin(Double temperatureMin) {
		this.temperatureMin = temperatureMin;
	}
	public Double getTemperatureMax() {
		return temperatureMax;
	}
	public void setTemperatureMax(Double temperatureMax) {
		this.temperatureMax = temperatureMax;
	}
	public Double getSalinityMin() {
		return salinityMin;
	}
	public void setSalinityMin(Double salinityMin) {
		this.salinityMin = salinityMin;
	}
	public Double getSalinityMax() {
		return salinityMax;
	}
	public void setSalinityMax(Double salinityMax) {
		this.salinityMax = salinityMax;
	}
	
	public Date getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public GeographicBoundingBoxDTO getBoundingBox() {
		return boundingBox;
	}

	public void setBoundingBox(GeographicBoundingBoxDTO boundingBox) {
		this.boundingBox = boundingBox;
		this.boundings = null;
	}


	public boolean isUncontrolledData() {
		return uncontrolledData;
	}


	public void setUncontrolledData(boolean uncontrolledData) {
		this.uncontrolledData = uncontrolledData;
	}


	public boolean isGoodData() {
		return goodData;
	}


	public void setGoodData(boolean goodData) {
		this.goodData = goodData;
	}


	public boolean isProbablyGoodData() {
		return probablyGoodData;
	}


	public void setProbablyGoodData(boolean probablyGoodData) {
		this.probablyGoodData = probablyGoodData;
	}


	public boolean isProbablyBadData() {
		return probablyBadData;
	}


	public void setProbablyBadData(boolean probablyBadData) {
		this.probablyBadData = probablyBadData;
	}


	public boolean isBadData() {
		return badData;
	}


	public void setBadData(boolean badData) {
		this.badData = badData;
	}


	public boolean isHarbourData() {
		return harbourData;
	}


	public void setHarbourData(boolean harbourData) {
		this.harbourData = harbourData;
	}


	public boolean isEmpty() 
	{
		if (isDataQualityEmpty() == false)
		{
			return false;
		}
		return true;
	}
	
	/**
	 * @return false is no quality criterion is set. true otherwise.
	 */
	public boolean isDataQualityEmpty()
	{
		return !(isBadData() || isHarbourData() || isGoodData() || isProbablyBadData() || isProbablyGoodData() || isUncontrolledData());
	}


	public boolean isTemperatureEmpty() 
	{
		return ((getTemperatureMax() == null) && (getTemperatureMin()== null));
	}
	
	public boolean isSalinityEmpty()
	{
		return ((getSalinityMax() == null) && (getSalinityMin() == null));
	}


	public boolean isDateEmpty() {
		return ((getBeginDate() == null) && (getEndDate() == null));
	}
	
	
}
