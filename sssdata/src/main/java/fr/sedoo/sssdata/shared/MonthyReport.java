package fr.sedoo.sssdata.shared;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class MonthyReport implements Serializable{
	
	private String month; 
	private Integer downloadsNumber; 
	private Set<String> users;
	
	public MonthyReport(){
		this.users = new HashSet<String>();
	}
	
	public MonthyReport(String month, Integer downloadsNumber, Set<String> users) {
		super();
		this.month = month;
		this.downloadsNumber = downloadsNumber;
		this.users = users;
	}
	
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public Integer getDownloadsNumber() {
		return downloadsNumber;
	}
	public void setDownloadsNumber(Integer downloadsNumber) {
		this.downloadsNumber = downloadsNumber;
	}
	
	public Set<String> getUsers() {
		return users;
	}
	public void setUsers(Set<String> users) {
		this.users = users;
	}

}
