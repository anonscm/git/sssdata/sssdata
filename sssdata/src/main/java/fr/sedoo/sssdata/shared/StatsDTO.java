package fr.sedoo.sssdata.shared;

import java.io.Serializable;
import java.util.Map;

public class StatsDTO implements Serializable {

	public static int MIN_YEAR = 2013;
	
	private Map<Integer,Integer> downloadsByYear;
	private Map<Integer,Integer> usersByYear;
	private Map<Integer,Double> volumeByYear;
	
	private Map<Integer,Map<Integer,Integer>> downloadsByMonth;
	private Map<Integer,Map<Integer,Integer>> usersByMonth;
	private Map<Integer,Map<Integer,Double>> volumeByMonth;
	
	private int maxMonthDownloads;
			
	public Map<Integer, Map<Integer, Integer>> getDownloadsByMonth() {
		return downloadsByMonth;
	}
	public void setDownloadsByMonth(Map<Integer, Map<Integer, Integer>> downloadsByMonth) {
		this.downloadsByMonth = downloadsByMonth;
	}
	public Map<Integer, Integer> getDownloadsByYear() {
		return downloadsByYear;
	}
	public void setDownloadsByYear(Map<Integer, Integer> downloadsByYear) {
		this.downloadsByYear = downloadsByYear;
	}
	public Map<Integer, Map<Integer, Integer>> getUsersByMonth() {
		return usersByMonth;
	}
	public void setUsersByMonth(Map<Integer, Map<Integer, Integer>> usersByMonth) {
		this.usersByMonth = usersByMonth;
	}
	public Map<Integer, Integer> getUsersByYear() {
		return usersByYear;
	}
	public void setUsersByYear(Map<Integer, Integer> usersByYear) {
		this.usersByYear = usersByYear;
	}
	public void setMaxMonthDownloads(int maxMonthDownloads) {
		this.maxMonthDownloads = maxMonthDownloads;
	}
	public int getMaxMonthDownloads() {
		return maxMonthDownloads;
	}
	public Map<Integer, Double> getVolumeByYear() {
		return volumeByYear;
	}
	public void setVolumeByYear(Map<Integer, Double> volumeByYear) {
		this.volumeByYear = volumeByYear;
	}
	public Map<Integer, Map<Integer, Double>> getVolumeByMonth() {
		return volumeByMonth;
	}
	public void setVolumeByMonth(Map<Integer, Map<Integer, Double>> volumeByMonth) {
		this.volumeByMonth = volumeByMonth;
	}
}
