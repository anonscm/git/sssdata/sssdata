package fr.sedoo.sssdata.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserStatsDTO implements Serializable{

	//Map<String, Integer> nbUsersByCountry;
	List<CountryStat> nbUsersByCountry;
	
	List<Date> registrationDates;
	
	public UserStatsDTO() {
		super();
		this.nbUsersByCountry = new ArrayList<CountryStat>();
	}
	
	public List<CountryStat> getNbUsersByCountry() {
		return nbUsersByCountry;
	}
	public void setNbUsersByCountry(List<CountryStat> nbUsersByCountry) {
		this.nbUsersByCountry = nbUsersByCountry;
	}
	
	/*public Map<String, Integer> getNbUsersByCountry() {
		return nbUsersByCountry;
	}
	public void setNbUsersByCountry(Map<String, Integer> nbUsersByCountry) {
		this.nbUsersByCountry = nbUsersByCountry;
	}*/
	public List<Date> getRegistrationDates() {
		return registrationDates;
	}
	public void setRegistrationDates(List<Date> registrationDates) {
		this.registrationDates = registrationDates;
	}
}
