package fr.sedoo.sssdata.shared.tool;

import java.util.ArrayList;
import java.util.List;

import fr.sedoo.sssdata.server.model.SssFile;
import fr.sedoo.sssdata.shared.BoatDTO;


public class BoatHelper {

	public static ArrayList<BoatDTO> getSimpleBoatList(ArrayList<BoatDTO> boats) {
		ArrayList<BoatDTO> list = new ArrayList<BoatDTO>();
		for (BoatDTO b : boats){
			BoatDTO boat = new BoatDTO();
			//boat.setId(b.getId());
			boat.setName(b.getName());
			list.add(boat);
		}
		return list;
	}

	public static List<SssFile> createListSssFile(List<SssFile> files) {
		ArrayList<SssFile> list = new ArrayList<SssFile>();
		for (SssFile f : files){
			SssFile file = new SssFile();
			file.setId(f.getId());
			file.setName(f.getName());
			//file.setDateCreation(f.getDateCreation());
			file.setDateModification(f.getDateModification());
			file.setBoat(f.getBoat());
			list.add(file);
		}
		return list;
	}
}
