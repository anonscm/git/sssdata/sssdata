package fr.sedoo.sssdata.shared;

import java.io.Serializable;

public class Boundings implements Serializable {

	private LatLong lowerLeft;
	private LatLong upperRight;
	
	public Boundings() {
		super();
	}
		
	public Boundings(LatLong lowerLeft, LatLong upperRight) {
		super();
		this.lowerLeft = lowerLeft;
		this.upperRight = upperRight;
	}
	
	public Boundings(double northBoundLatitude, double eastBoundLongitude, double southBoundLatitude, double westBoundLongitude){
		this.lowerLeft = new LatLong(southBoundLatitude, westBoundLongitude);
		this.upperRight = new LatLong(northBoundLatitude, eastBoundLongitude);
	}
	
	public double getNorthBoundLatitude() {
		return upperRight.getLat();
	}
	public double getSouthBoundLatitude() {
		return lowerLeft.getLat();
	}
	public double getEastBoundLongitude() {
		return upperRight.getLon();
	}
	public double getWestBoundLongitude() {
		return lowerLeft.getLon();
	}
	
	public void setNorthBoundLatitude(double northBoundLatitude) {
		upperRight.setLat(northBoundLatitude);
	}
	public void setSouthBoundLatitude(double southBoundLatitude) {
		lowerLeft.setLat(southBoundLatitude);
	}
	public void setEastBoundLongitude(double eastBoundLongitude) {
		upperRight.setLon(eastBoundLongitude);
	}
	public void setWestBoundLongitude(double westBoundLongitude) {
		lowerLeft.setLon(westBoundLongitude);
	}
	
	public LatLong getLowerLeft() {
		return lowerLeft;
	}
	public void setLowerLeft(LatLong lowerLeft) {
		this.lowerLeft = lowerLeft;
	}
	public LatLong getUpperRight() {
		return upperRight;
	}
	public void setUpperRight(LatLong upperRight) {
		this.upperRight = upperRight;
	}
	
	
	
}
