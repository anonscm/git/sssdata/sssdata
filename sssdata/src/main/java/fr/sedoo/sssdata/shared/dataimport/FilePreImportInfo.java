package fr.sedoo.sssdata.shared.dataimport;

import com.google.gwt.user.client.rpc.IsSerializable;

public class FilePreImportInfo implements IsSerializable
{
	private boolean alreadyPresent;
	private boolean needUpdate;
	
	public boolean isAlreadyPresent() {
		return alreadyPresent;
	}
	public void setAlreadyPresent(boolean alreadyPresent) {
		this.alreadyPresent = alreadyPresent;
	}
	public boolean isNeedUpdate() {
		return needUpdate;
	}
	public void setNeedUpdate(boolean needUpdate) {
		this.needUpdate = needUpdate;
	}
	
}
