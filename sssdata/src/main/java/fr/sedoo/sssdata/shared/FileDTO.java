package fr.sedoo.sssdata.shared;

import java.io.Serializable;

public class FileDTO implements Serializable {

	public static final String FORMAT_ASCII = "ascii";
	public static final String FORMAT_NETCDF = "netcdf";
	
	public static final String COMPRESSION_ZIP = "zip";
	public static final String COMPRESSION_GZ = "gzip";
	
	public static final String FILENAME_PREFIX = "sssdata_";
	public final static String EXTRACT_PATH = "/tmp";
	public static final String ARCHIVE_FOLDER = "/archive";
	
	
	
	private String sssFileName;
	private String generatedFileName;
		
	private Long volume;
	
	public String getSssFileName() {
		return sssFileName;
	}
	public void setSssFileName(String sssFileName) {
		this.sssFileName = sssFileName;
	}
	public String getGeneratedFileName() {
		return generatedFileName;
	}
	public void setGeneratedFileName(String generatedFileName) {
		this.generatedFileName = generatedFileName;
	}
	public Long getVolume() {
		return volume;
	}
	public void setVolume(Long volume) {
		this.volume = volume;
	}
}
