package fr.sedoo.sssdata.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class StatsReport implements Serializable {
	
	private String year;
	private List<MonthyReport> monthlyDetails; 	
	
	private int downloadsNumber;
	private int usersNumber;
	private int maxDownloads;
	
	public StatsReport() {
		super();
		setMonthlyDetails(new ArrayList<MonthyReport>());		
	}	

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	
	public List<MonthyReport> getMonthlyDetails() {
		return monthlyDetails;
	}

	public void setMonthlyDetails(List<MonthyReport> monthlyDetails) {
		this.monthlyDetails = monthlyDetails;
		process();
	}
			
	public void process(){
		this.downloadsNumber = 0;
		this.maxDownloads = 0;
		for(MonthyReport mReport: monthlyDetails){
			downloadsNumber += mReport.getDownloadsNumber();
			this.maxDownloads = Math.max(maxDownloads, mReport.getDownloadsNumber());
		}
		Set<String> users = new HashSet<String>(); 
		for(MonthyReport mReport: monthlyDetails){
			users.addAll(mReport.getUsers());
		}
		this.usersNumber = users.size();
		
		this.maxDownloads = (this.maxDownloads + 10) / 10;
		this.maxDownloads = this.maxDownloads * 10;
	}

	public String getDownloadNumber() {
		return ""+downloadsNumber;
	}
	
	public String getUserNumber() {
		return ""+usersNumber;
	}
	
	public int getMaxDownloads() {
		return maxDownloads;
	}
	
}
