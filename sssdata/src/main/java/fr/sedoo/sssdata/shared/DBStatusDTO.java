package fr.sedoo.sssdata.shared;

import java.io.Serializable;

public class DBStatusDTO implements Serializable{

	private String lastDbUpdateDate;
	private String lastModifiedFileDate;
	private String lastUpdatedFileDate;
	private String lastCreatedFileDate;
	
	private String startDate;
	private String endDate;
	
	private int insertedFilesNb;
	private int shipNb;
	
	public DBStatusDTO() {
		super();
	}
	
	public String getLastDbUpdateDate() {
		return lastDbUpdateDate;
	}
	public void setLastDbUpdateDate(String lastDbUpdateDate) {
		this.lastDbUpdateDate = lastDbUpdateDate;
	}
	public String getLastModifiedFileDate() {
		return lastModifiedFileDate;
	}
	public void setLastModifiedFileDate(String lastModifiedFileDate) {
		this.lastModifiedFileDate = lastModifiedFileDate;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public int getInsertedFilesNb() {
		return insertedFilesNb;
	}
	public void setInsertedFilesNb(int insertedFilesNb) {
		this.insertedFilesNb = insertedFilesNb;
	}
	public int getShipNb() {
		return shipNb;
	}
	public void setShipNb(int shipNb) {
		this.shipNb = shipNb;
	}

	public String getLastUpdatedFileDate() {
		return lastUpdatedFileDate;
	}

	public void setLastUpdatedFileDate(String lastUpdatedFileDate) {
		this.lastUpdatedFileDate = lastUpdatedFileDate;
	}

	public String getLastCreatedFileDate() {
		return lastCreatedFileDate;
	}

	public void setLastCreatedFileDate(String lastCreatedFileDate) {
		this.lastCreatedFileDate = lastCreatedFileDate;
	}
		
}
