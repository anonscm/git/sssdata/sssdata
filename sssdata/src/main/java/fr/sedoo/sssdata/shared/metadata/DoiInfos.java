package fr.sedoo.sssdata.shared.metadata;

import java.io.Serializable;
import java.util.ArrayList;

public class DoiInfos implements Serializable{
	
	private String doi;
	private String version;
	
	private ArrayList<HistoryItem> history;
	
	public DoiInfos() {
		super();
		this.history = new ArrayList<HistoryItem>();
	}
	
	public String getDoi() {
		return doi;
	}
	public void setDoi(String doi) {
		this.doi = doi;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public ArrayList<HistoryItem> getHistory() {
		return history;
	}
	public void setHistory(ArrayList<HistoryItem> history) {
		this.history = history;
	}
}
