package fr.sedoo.sssdata.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class UpdateDTO  implements Serializable {

	private String date;
	private String fileName;
	
	private String boatName;
	
	private String startDate;
	private String endDate;
	
	private List<String> journal;
	
	public UpdateDTO() {
		super();
		this.journal = new ArrayList<String>();
	}
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public String getBoatName() {
		return boatName;
	}
	public void setBoatName(String boatName) {
		this.boatName = boatName;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public List<String> getJournal() {
		return journal;
	}
	public void setJournal(List<String> journal) {
		this.journal = journal;
	}
		
}
