package fr.sedoo.sssdata.shared.metadata;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MetadataDTO implements Serializable{

	
	private String doi;
	
	private String title;
	private String summary;
	private String publisher;
	private String publicationYear;
	
	private String dateBegin;
	private String dateEnd;
		
	private String version;
	private List<String> dataFormats;
	private String rights;
	
	private List<CreatorDTO> creators;
	
	private List<String> keywords;
	private List<String> contributors;
	
	private List<HistoryItem> history;
	
	private List<RelatedIdentifierDTO> relatedIdentifiers;
	
	private String citation;
	
	public MetadataDTO() {
		super();
		this.creators = new ArrayList<CreatorDTO>();
		this.keywords = new ArrayList<String>();
		this.contributors = new ArrayList<String>();
		this.dataFormats = new ArrayList<String>();
		this.relatedIdentifiers = new ArrayList<RelatedIdentifierDTO>();
		this.history = new ArrayList<HistoryItem>();
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	public String getPublicationYear() {
		return publicationYear;
	}
	public void setPublicationYear(String publicationYear) {
		this.publicationYear = publicationYear;
	}
	public List<CreatorDTO> getCreators() {
		return creators;
	}
	public void setCreators(List<CreatorDTO> creators) {
		this.creators = creators;
	}
	public List<String> getKeywords() {
		return keywords;
	}
	public void setKeywords(List<String> keywords) {
		this.keywords = keywords;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public List<String> getDataFormats() {
		return dataFormats;
	}
	public void setDataFormats(List<String> dataFormats) {
		this.dataFormats = dataFormats;
	}

	public String getRights() {
		return rights;
	}

	public void setRights(String rights) {
		this.rights = rights;
	}
	
	public List<String> getContributors() {
		return contributors;
	}
	public void setContributors(List<String> contributors) {
		this.contributors = contributors;
	}
	public String getDoi() {
		return doi;
	}
	public void setDoi(String doi) {
		this.doi = doi;
	}
	public List<HistoryItem> getHistory() {
		return history;
	}
	public void setHistory(List<HistoryItem> history) {
		this.history = history;
	}
		
	public String getCitation() {
		return citation;
	}
	public void setCitation(String citation) {
		this.citation = citation;
	}

	public String getDateBegin() {
		return dateBegin;
	}
	public void setDateBegin(String dateBegin) {
		this.dateBegin = dateBegin;
	}
	public String getDateEnd() {
		return dateEnd;
	}
	public void setDateEnd(String dateEnd) {
		this.dateEnd = dateEnd;
	}
	
	public List<RelatedIdentifierDTO> getRelatedIdentifiers() {
		return relatedIdentifiers;
	}
	public void setRelatedIdentifiers(List<RelatedIdentifierDTO> relatedIdentifiers) {
		this.relatedIdentifiers = relatedIdentifiers;
	}
}
