package fr.sedoo.sssdata.server.extract;

import java.io.File;
import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import org.apache.log4j.Logger;

import fr.sedoo.sssdata.server.dao.gosud.GosudConstants;
import fr.sedoo.sssdata.server.model.Measure;
import fr.sedoo.sssdata.shared.FileDTO;

public abstract class FichierSortie {

	private static Logger logger = Logger.getLogger(FichierSortie.class);
	
	public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	public static final String LATITUDE_FORMAT = "#0.0000";
	public static final String LONGITUDE_FORMAT = "##0.0000";
	public static final String DATA_FORMAT = "#0.000";
	
	public static final String SSS_VAR_NAME = "Practical Sea Surface Salinity";
	public static final String SSS_VAR_UNIT = "PSS-78";
	public static final String SST_VAR_NAME = "Sea Surface Temperature";
	public static final String SST_VAR_UNIT = "degree Celsius";
	public static final String SST_VAR_UNIT_SHORT = "degC";
	
	
	//
	
	
	public static final int MISSING_FLAG = GosudConstants.UNCONTROLLED_DATA;
	public static final float MISSING_VALUE = 999.999f;
	
	private FichierSortieHeader header;
	
	protected SimpleDateFormat sdf;
	protected DecimalFormat dfLat;
	protected DecimalFormat dfLon;
	protected DecimalFormat dfDat;
	
	private File file;
	private int size;
	
	public FichierSortie(String filename, FichierSortieHeader header) {
										
		this.header = header;
		this.file = new File(FileDTO.EXTRACT_PATH +  "/" + filename + getExtension());
		logger.debug("<init>() - " + file.getAbsolutePath() );
		
		this.sdf = new SimpleDateFormat(DATE_FORMAT);
		this.sdf.setLenient(false);
		DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance(Locale.ENGLISH);
		dfLat = new DecimalFormat(LATITUDE_FORMAT, dfs);
		dfLat.setRoundingMode(RoundingMode.HALF_UP);
		dfLon = new DecimalFormat(LONGITUDE_FORMAT, dfs);
		dfLon.setRoundingMode(RoundingMode.HALF_UP);
		dfDat = new DecimalFormat(DATA_FORMAT, dfs);
		dfDat.setRoundingMode(RoundingMode.HALF_UP);
	}
			
	/**
	 * 
	 * @return nombre de mesures écrites
	 * @throws Exception
	 */
	public int write(List<Measure> mesures) throws IOException {
		this.size = mesures.size();
		open();
		printHeader();
		writeData(mesures);
		close();
		return getSize();
	}
	
	
	
	protected abstract void open() throws IOException;
	protected abstract void close() throws IOException;
	
	protected abstract void printHeader() throws IOException;
	
	protected void writeData(List<Measure> mesures) {
		for (Measure mesure: mesures){
			writeMesure(mesure);
		}
	}
	
	protected abstract void writeMesure(Measure mesure);
	protected abstract String getExtension();
		
	public int getSize() {
		return size;
	}
	
	protected void setSize(int size) {
		this.size = size;
	}
	
	public File getFile() {
		return file;
	}
	public FichierSortieHeader getHeaderInfos() {
		return header;
	}
}
