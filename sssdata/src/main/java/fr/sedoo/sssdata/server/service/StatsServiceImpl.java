package fr.sedoo.sssdata.server.service;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.io.FileUtils;
import org.apache.commons.math3.util.MathUtils;
import org.apache.commons.math3.util.Precision;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.sedoo.sssdata.client.service.ServiceException;
import fr.sedoo.sssdata.client.service.StatsService;
import fr.sedoo.sssdata.server.SSSDataApplication;
import fr.sedoo.sssdata.server.dao.api.DownloadDAO;
import fr.sedoo.sssdata.server.dao.api.UserDAO;
import fr.sedoo.sssdata.server.model.Download;
import fr.sedoo.sssdata.shared.CountryStat;
import fr.sedoo.sssdata.shared.MonthyReport;
import fr.sedoo.sssdata.shared.StatsDTO;
import fr.sedoo.sssdata.shared.StatsReport;
import fr.sedoo.sssdata.shared.UserStatsDTO;

public class StatsServiceImpl extends RemoteServiceServlet implements StatsService{

	private DownloadDAO dlDao;
	
	private UserDAO userDao;
	
	private void initDao(){
		if (dlDao == null){
			dlDao = (DownloadDAO) SSSDataApplication.getSpringBeanFactory().getBeanByName(DownloadDAO.BEAN_NAME);
		}
		if (userDao == null){
			userDao = (UserDAO) SSSDataApplication.getSpringBeanFactory().getBeanByName(UserDAO.BEAN_NAME);
		}
	}

	@Override
	public StatsReport getGlobalStats(int year) throws ServiceException {
		initDao();
		
		int maxMonth = 12;
		Calendar today = Calendar.getInstance();
		if (today.get(Calendar.YEAR) == year){
			maxMonth = today.get(Calendar.MONTH) + 1;
		}
		
		//TODO s'arrêter au mois en cours
		
		DateFormatSymbols dfs = new DateFormatSymbols(Locale.ENGLISH);
		List <MonthyReport> list = new ArrayList <MonthyReport>();
		for (int month = 1;month <= maxMonth;month++){
			List<Download> dls = dlDao.getDownloadsByMonth(year, month, true);
			int dlNum = 0;
			Set<String> users = new HashSet<String>();
			if (dls != null){
				dlNum = dls.size();
				for (Download dl : dls){
					if (dl.getUser() != null){
						users.add(dl.getUser().getEmail());
					}
				}
			}
			
			list.add(new MonthyReport(dfs.getMonths()[month-1], dlNum, users));
		}
		
		
		StatsReport report = new StatsReport();
		
		report.setYear(""+year);
		/*
		int aux = year;
		MonthyReport janvier= new MonthyReport("January",aux,aux*2); 
		aux = aux-10;
		MonthyReport fevrier= new MonthyReport("February",aux,aux*2);
		aux = aux-10;
		MonthyReport mars= new MonthyReport("March",aux,aux*2);
		aux = aux-10;
		MonthyReport avril= new MonthyReport("April",aux,aux*2);
		aux = aux-10;
		MonthyReport mai= new MonthyReport("May",aux,aux*2);
		aux = aux-10;
		MonthyReport juin= new MonthyReport("June",aux,aux*2);
		aux = aux-10;
		MonthyReport juillet= new MonthyReport("July",aux,aux*2);
		aux = aux-10;
		MonthyReport aout= new MonthyReport("August",aux,aux*2);
		aux = aux-10;
		MonthyReport septembre= new MonthyReport("September",aux,aux*2);
		aux = aux-10;
		MonthyReport octobre= new MonthyReport("October",aux,aux*2);
		aux = aux-10;
		MonthyReport novembre= new MonthyReport("November",aux,aux*2);
		aux = aux-10;
		MonthyReport decembre= new MonthyReport("December",aux,aux*2);
		
		List <MonthyReport> list = new ArrayList <MonthyReport>();
 		list.add(janvier);
 		list.add(fevrier);
 		list.add(mars);
 		list.add(avril);
 		list.add(mai);
 		list.add(juin);
 		list.add(juillet);
 		list.add(aout);
 		list.add(septembre);
 		list.add(octobre);
 		list.add(novembre);		
 		list.add(decembre);
		*/
		report.setMonthlyDetails(list); 
		
		return report;

	}

	@Override
	public UserStatsDTO getUserStats() throws ServiceException {
		initDao();
						
		UserStatsDTO stats = new UserStatsDTO();
		Map<String, Integer> aux = userDao.getNbUsersByCountry();
		for(String country: aux.keySet()){
			stats.getNbUsersByCountry().add(new CountryStat(country, aux.get(country)));
		}
		Collections.sort(stats.getNbUsersByCountry());
		
		List<Date> dates = userDao.getRegistrationDates();
		Calendar cal = Calendar.getInstance();
		cal.set(2014, 1, 1, 0, 0, 0);
		dates.add(0, new Date(cal.getTimeInMillis()));
		stats.setRegistrationDates(dates);
		return stats;
	}

	@Override
	public StatsDTO getDownloadStats() throws ServiceException {
		initDao();
		StatsDTO stats = new StatsDTO(); 
		
		Map<Integer,Integer> downloadsByYear = new TreeMap<Integer, Integer>();
		Map<Integer,Map<Integer,Integer>> downloadsByMonth = new TreeMap<Integer, Map<Integer,Integer>>();
		
		Map<Integer,Double> volumeByYear = new TreeMap<Integer, Double>();
		Map<Integer,Map<Integer,Double>> volumeByMonth = new TreeMap<Integer, Map<Integer,Double>>();
		
		//downloadsByYear.put("2013", 12);
		//downloadsByYear.put("2014", 7);
		
		Map<Integer,Integer> usersByYear = new TreeMap<Integer, Integer>();
		Map<Integer,Map<Integer,Integer>> usersByMonth = new TreeMap<Integer, Map<Integer,Integer>>();
		//usersByYear.put("2013", 5);
		//usersByYear.put("2014", 1);
				
		Calendar today = Calendar.getInstance();
		
		int maxDl = 0;
		
		for (int year = StatsDTO.MIN_YEAR;year <= today.get(Calendar.YEAR);year++){
			int maxMonth = 12;
			if (today.get(Calendar.YEAR) == year){
				maxMonth = today.get(Calendar.MONTH) + 1;
			}
			int yearDownloads = 0;
			long yearVolume = 0;
			Set<String> yearUsers = new HashSet<String>();
			
			downloadsByMonth.put(year, new TreeMap<Integer,Integer>());
			usersByMonth.put(year, new TreeMap<Integer,Integer>());
			volumeByMonth.put(year, new TreeMap<Integer,Double>());
			
			for (int month = 1;month <= maxMonth;month++){
				List<Download> dls = dlDao.getDownloadsByMonthLazyLoaded(year,
						month);
				yearDownloads += dls.size();
				maxDl = Math.max(maxDl, dls.size());
				long monthVolume = 0;
				Set<String> monthUsers = new HashSet<String>();
				for (Download dl : dls){
					if (dl.getUser() != null){
						yearUsers.add(dl.getUser().getEmail()); // ici on peut
																// grouper le
																// traitement
																// des deux
																// getEmail()
						monthUsers.add(dl.getUser().getEmail());
					}
					if (dl.getVolume() != null){
						yearVolume += dl.getVolume();
						monthVolume += dl.getVolume();
					}
				}
				downloadsByMonth.get(year).put(month, dls.size());
				usersByMonth.get(year).put(month, monthUsers.size());
				volumeByMonth.get(year).put(month, volumeToGb(monthVolume));
			}
			downloadsByYear.put(year, yearDownloads);
			usersByYear.put(year, yearUsers.size());
			volumeByYear.put(year, volumeToGb(yearVolume));
		}
		
		stats.setDownloadsByYear(downloadsByYear);
		stats.setUsersByYear(usersByYear);
		stats.setDownloadsByMonth(downloadsByMonth);
		stats.setUsersByMonth(usersByMonth);
		stats.setMaxMonthDownloads(maxDl);
		stats.setVolumeByYear(volumeByYear);
		stats.setVolumeByMonth(volumeByMonth);
		return stats;
	}
	
	private double volumeToMb(long volume){
		return Precision.round((double) volume / FileUtils.ONE_MB, 1);
	}
	private double volumeToGb(long volume){
		return Precision.round((double) volume / FileUtils.ONE_GB, 3);
	}
	
}








