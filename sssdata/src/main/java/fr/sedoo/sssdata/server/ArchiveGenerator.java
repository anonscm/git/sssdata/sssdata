package fr.sedoo.sssdata.server;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.server.DefaultServerApplication;
import fr.sedoo.commons.server.config.dao.api.ConfigCategDAO;
import fr.sedoo.commons.server.config.dao.api.ConfigPropertyDAO;
import fr.sedoo.commons.server.config.model.ConfigProperty;
import fr.sedoo.commons.shared.domain.GeographicBoundingBoxDTO;
import fr.sedoo.sssdata.server.dao.api.ArchiveDAO;
import fr.sedoo.sssdata.server.dao.api.BoatDAO;
import fr.sedoo.sssdata.server.dao.api.SssFileDAO;
import fr.sedoo.sssdata.server.dao.api.TrajectoryDAO;
import fr.sedoo.sssdata.server.extract.ArchiveSortie;
import fr.sedoo.sssdata.server.misc.DownloadConfig;
import fr.sedoo.sssdata.server.model.Archive;
import fr.sedoo.sssdata.server.model.Boat;
import fr.sedoo.sssdata.server.model.SssFile;
import fr.sedoo.sssdata.shared.ArchiveGeneratorRequest;
import fr.sedoo.sssdata.shared.BoatDTO;
import fr.sedoo.sssdata.shared.ConfigPropertiesCode;
import fr.sedoo.sssdata.shared.FileDTO;
import fr.sedoo.sssdata.shared.SearchCriterion;

public class ArchiveGenerator {

	public static final String BEAN_NAME = "archiveGenerator";

	private static Logger logger = Logger
			.getLogger(ArchiveGenerator.class);

	private TrajectoryDAO trajDao;
	private SssFileDAO fileDao;
	private ConfigPropertyDAO propDao;
	private BoatDAO boatDao;
	private ConfigPropertyDAO configPropDao;
	private ConfigCategDAO configCatDao;
	
	private ArchiveDAO archiveDao;

	/*private static final String CONFIG_CATEGORY_DOWNLOAD_NAME = "Download page";
	private static final String CONFIG_PROP_GOOD_ASCII_LABEL = "Archive good/probably good data ascii";
	private static final String CONFIG_PROP_GOOD_NETCDF_LABEL = "Archive good/probably good data netcdf";
	private static final String CONFIG_PROP_ALL_ASCII_LABEL = "Archive all data ascii";
	private static final String CONFIG_PROP_ALL_NETCDF_LABEL = "Archive all good data netcdf";*/

	//private String root;
	
	private DownloadConfig downloadConfig;
	
	public ArchiveGenerator() {
		super();
	}
	
	private SearchCriterion getSearchCriterion(ArrayList<BoatDTO> list, HashMap<String, String> propertiesMap, boolean allData) throws ParseException{
		SearchCriterion sc = new SearchCriterion();

		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		
		sc.setGoodData(true);
		sc.setProbablyGoodData(true);
		sc.setHarbourData(allData);
		sc.setProbablyBadData(allData);
		sc.setUncontrolledData(allData);
		sc.setBadData(allData);
		sc.setBoatListSelection(list);
		sc.setBeginDate(format.parse(StringUtil
				.trimToEmpty(propertiesMap
						.get(ConfigPropertiesCode.DATE_MIN))));
		
		if (StringUtil.isEmpty(propertiesMap.get(ConfigPropertiesCode.DATE_MAX))){
			sc.setEndDate(new Date());	
		}else{
			sc.setEndDate(format.parse(propertiesMap.get(ConfigPropertiesCode.DATE_MAX)));	
		}
						
		sc.setTemperatureMin(Double.parseDouble(StringUtil
				.trimToEmpty(propertiesMap
						.get(ConfigPropertiesCode.TEMPERATURE_MIN))));
		sc.setTemperatureMax(Double.parseDouble(StringUtil
				.trimToEmpty(propertiesMap
						.get(ConfigPropertiesCode.TEMPERATURE_MAX))));
		sc.setSalinityMin(Double.parseDouble(StringUtil
				.trimToEmpty(propertiesMap
						.get(ConfigPropertiesCode.SALINITY_MIN))));
		sc.setSalinityMax(Double.parseDouble(StringUtil
				.trimToEmpty(propertiesMap
						.get(ConfigPropertiesCode.SALINITY_MAX))));
		GeographicBoundingBoxDTO bbox = new GeographicBoundingBoxDTO();
		bbox.setEastBoundLongitude("");
		bbox.setWestBoundLongitude("");
		bbox.setSouthBoundLatitude("");
		bbox.setNorthBoundLatitude("");
		sc.setBoundingBox(bbox);
		
		return sc;
	}
	
	public List<ArchiveGeneratorRequest> init() throws ParseException {
		initDao();
		List<ArchiveGeneratorRequest> requests = new ArrayList<ArchiveGeneratorRequest>();

		ArrayList<BoatDTO> list = new ArrayList<BoatDTO>();

		List<Boat> boats = boatDao.findAll();
		for (Boat b : boats) {
			BoatDTO boat = new BoatDTO();
			boat.setName(b.getName());
			list.add(boat);
		}

		List<ConfigProperty> properties = propDao.findAll();
		HashMap<String, String> propertiesMap = new HashMap<String, String>();
		for (ConfigProperty p : properties) {
			propertiesMap.put(p.getCode(), p.getValueOrDefault());
		}

		// criterion for good/probably good data :
		SearchCriterion sc1 = getSearchCriterion(list, propertiesMap, false);
		// criterion for all data :
		SearchCriterion sc2 = getSearchCriterion(list, propertiesMap, true);

		requests.add(new ArchiveGeneratorRequest(ArchiveGeneratorRequest.ARCHIVE_FLAG_GOOD, FileDTO.FORMAT_ASCII, sc1));
		requests.add(new ArchiveGeneratorRequest(ArchiveGeneratorRequest.ARCHIVE_FLAG_GOOD, FileDTO.FORMAT_NETCDF, sc1));
		requests.add(new ArchiveGeneratorRequest(ArchiveGeneratorRequest.ARCHIVE_FLAG_ALL, FileDTO.FORMAT_ASCII, sc2));
		requests.add(new ArchiveGeneratorRequest(ArchiveGeneratorRequest.ARCHIVE_FLAG_ALL, FileDTO.FORMAT_NETCDF, sc2));

		return requests;
	}

	public String createArchive(ArchiveGeneratorRequest request, List<FileDTO> temporaryFiles) throws Exception {
		try{
			String filename = createArchive(request.getRequestId(), temporaryFiles, request.getFileFormat());
			// save filename in configProperty

		/*	if (request.getRequestId().equals("ascii_good")){
				saveArchiveFilenameInConfigProperty(filename,
						ConfigPropertiesCode.DOWNLOAD_ARCHIVE_GOOD_ASCII,
						CONFIG_PROP_GOOD_ASCII_LABEL);
			}else if (request.getRequestId().equals("netcdf_good")){
				saveArchiveFilenameInConfigProperty(filename,
						ConfigPropertiesCode.DOWNLOAD_ARCHIVE_GOOD_NETCDF,
						CONFIG_PROP_GOOD_NETCDF_LABEL);
			}else if (request.getRequestId().equals("ascii_all")){
				saveArchiveFilenameInConfigProperty(filename,
						ConfigPropertiesCode.DOWNLOAD_ARCHIVE_ALL_ASCII,
						CONFIG_PROP_ALL_ASCII_LABEL);
			}else if (request.getRequestId().equals("netcdf_all")){
				saveArchiveFilenameInConfigProperty(filename,
						ConfigPropertiesCode.DOWNLOAD_ARCHIVE_ALL_NETCDF,
						CONFIG_PROP_ALL_NETCDF_LABEL);
			}*/
			
			archiveDao.save(new Archive(filename, temporaryFiles.size(), request.getFlag(), request.getFileFormat()));
			
			String message = "Archive " + filename + "successfully created. It contains " + temporaryFiles.size() + " files.";
			
			return message;
		} catch (Exception e) {
			logger.error("Error in run(): " + e.getClass() + " , " + e);
			throw e;
		} finally {
			if (temporaryFiles != null) {
				cleanGeneratedFiles(temporaryFiles);
			}
		}
	}
	
	
	public List<FileDTO> getArchiveFilesByBoat(ArchiveGeneratorRequest request, BoatDTO boat) throws Exception {
		List<FileDTO> result = new ArrayList<>();
		try{
			
			SearchCriterion sc = request.getSearchCriterion();

			Date startDate = new Date();
			logger.info("Process boat '" + boat.getName() + "' :");
			List<SssFile> files = fileDao.getFilesByBoat(boat.getName(),
					sc.getBeginDate(), sc.getEndDate(), false);

			for (SssFile file : files) {
				String generatedFile = trajDao.downloadTrajectory(
						request.getRequestId(), file.getName(), sc, request.getFileFormat());

				if (generatedFile != null) {
					FileDTO tmp = new FileDTO();
					tmp.setSssFileName(file.getName());
					tmp.setGeneratedFileName(generatedFile);
					result.add(tmp);
				}
			}

			Date endDate = new Date();
			long time = (endDate.getTime() - startDate.getTime()) / 1000;
			logger.info("Time for boat '" + boat.getName()
			+ "' : "
			+ time + " second(s)");
			return result;
		} catch (Exception e) {
			if (result != null) {
				cleanGeneratedFiles(result);
			}
			logger.error("Error in getFiles(): " + e.getClass() + " , " + e);
			throw e;
		}

	}

	
	
	/*
	
	public void run() {

		List<FileDTO> temporaryFiles = null;

		try {
				initDao();

				// building criterions :

				ArrayList<BoatDTO> list = new ArrayList<BoatDTO>();

				List<Boat> boats = boatDao.findAll();
				for (Boat b : boats) {
					BoatDTO boat = new BoatDTO();
					boat.setName(b.getName());
					list.add(boat);
				}

				SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

				List<ConfigProperty> properties = propDao.findAll();
				HashMap<String, String> propertiesMap = new HashMap<String, String>();
				for (ConfigProperty p : properties) {
					propertiesMap.put(p.getCode(), p.getValueOrDefault());
				}

				// criterion for good/probably good data :
				SearchCriterion sc1 = new SearchCriterion();

				sc1.setGoodData(true);
				sc1.setProbablyGoodData(true);
				sc1.setHarbourData(false);
				sc1.setProbablyBadData(false);
				sc1.setUncontrolledData(false);
				sc1.setBadData(false);
				sc1.setBoatListSelection(list);
				sc1.setBeginDate(format.parse(StringUtil
						.trimToEmpty(propertiesMap
								.get(ConfigPropertiesCode.DATE_MIN))));
				
				if (StringUtil.isEmpty(propertiesMap.get(ConfigPropertiesCode.DATE_MAX))){
					sc1.setEndDate(new Date());	
				}else{
					sc1.setEndDate(format.parse(propertiesMap.get(ConfigPropertiesCode.DATE_MAX)));	
				}
								
				sc1.setTemperatureMin(Double.parseDouble(StringUtil
						.trimToEmpty(propertiesMap
								.get(ConfigPropertiesCode.TEMPERATURE_MIN))));
				sc1.setTemperatureMax(Double.parseDouble(StringUtil
						.trimToEmpty(propertiesMap
								.get(ConfigPropertiesCode.TEMPERATURE_MAX))));
				sc1.setSalinityMin(Double.parseDouble(StringUtil
						.trimToEmpty(propertiesMap
								.get(ConfigPropertiesCode.SALINITY_MIN))));
				sc1.setSalinityMax(Double.parseDouble(StringUtil
						.trimToEmpty(propertiesMap
								.get(ConfigPropertiesCode.SALINITY_MAX))));
				GeographicBoundingBoxDTO bbox = new GeographicBoundingBoxDTO();
				bbox.setEastBoundLongitude("");
				bbox.setWestBoundLongitude("");
				bbox.setSouthBoundLatitude("");
				bbox.setNorthBoundLatitude("");
				sc1.setBoundingBox(bbox);

				// criterion for all data :
				SearchCriterion sc2 = new SearchCriterion();

				sc2.setGoodData(true);
				sc2.setProbablyGoodData(true);
				sc2.setHarbourData(true);
				sc2.setProbablyBadData(true);
				sc2.setUncontrolledData(true);
				sc2.setBadData(true);
				sc2.setBoatListSelection(list);
				sc2.setBeginDate(format.parse(StringUtil
						.trimToEmpty(propertiesMap
								.get(ConfigPropertiesCode.DATE_MIN))));
				if (StringUtil.isEmpty(propertiesMap.get(ConfigPropertiesCode.DATE_MAX))){
					sc2.setEndDate(new Date());	
				}else{
					sc2.setEndDate(format.parse(propertiesMap.get(ConfigPropertiesCode.DATE_MAX)));	
				}
				sc2.setTemperatureMin(Double.parseDouble(StringUtil
						.trimToEmpty(propertiesMap
								.get(ConfigPropertiesCode.TEMPERATURE_MIN))));
				sc2.setTemperatureMax(Double.parseDouble(StringUtil
						.trimToEmpty(propertiesMap
								.get(ConfigPropertiesCode.TEMPERATURE_MAX))));
				sc2.setSalinityMin(Double.parseDouble(StringUtil
						.trimToEmpty(propertiesMap
								.get(ConfigPropertiesCode.SALINITY_MIN))));
				sc2.setSalinityMax(Double.parseDouble(StringUtil
						.trimToEmpty(propertiesMap
								.get(ConfigPropertiesCode.SALINITY_MAX))));
				sc2.setBoundingBox(bbox);

				// create archives (2 formats) and store file name in
				// ConfigProperty table :

				SimpleDateFormat requestformat = new SimpleDateFormat(
						"yyyyMMdd");
				
				//Long requestId = Long.parseLong(requestformat
				//		.format(new Date()));

				// archives for good/probably good data :

				// create ascii archive
				String requestId = "ascii_good";
				temporaryFiles = getFiles(requestId, sc1, FileDTO.FORMAT_ASCII);
				String asciiFilename = createArchive(requestId, temporaryFiles,
						FileDTO.FORMAT_ASCII);
				// save filename in configProperty
				saveArchiveFilenameInConfigProperty(asciiFilename,
						ConfigPropertiesCode.DOWNLOAD_ARCHIVE_GOOD_ASCII,
						CONFIG_PROP_GOOD_ASCII_LABEL);

				// create netcdf archive
				temporaryFiles.clear();
				requestId = "netcdf_good";
				temporaryFiles = getFiles(requestId, sc1, FileDTO.FORMAT_NETCDF);
				String netcdfFilename = createArchive(requestId,
						temporaryFiles, FileDTO.FORMAT_NETCDF);
				// save filename in configProperty
				saveArchiveFilenameInConfigProperty(netcdfFilename,
						ConfigPropertiesCode.DOWNLOAD_ARCHIVE_GOOD_NETCDF,
						CONFIG_PROP_GOOD_NETCDF_LABEL);

			// archives for all data :

				// create ascii archive
				temporaryFiles.clear();
				requestId = "ascii_all";
				temporaryFiles = getFiles(requestId, sc2, FileDTO.FORMAT_ASCII);
				String allAsciiFilename = createArchive(requestId,
						temporaryFiles, FileDTO.FORMAT_ASCII);
				// save filename in configProperty
				saveArchiveFilenameInConfigProperty(allAsciiFilename,
						ConfigPropertiesCode.DOWNLOAD_ARCHIVE_ALL_ASCII,
						CONFIG_PROP_ALL_ASCII_LABEL);

				// create netcdf archive
				temporaryFiles.clear();
				requestId = "netcdf_all";
				temporaryFiles = getFiles(requestId, sc2, FileDTO.FORMAT_NETCDF);
				String allNetcdfFilename = createArchive(requestId,
						temporaryFiles, FileDTO.FORMAT_NETCDF);
				// save filename in configProperty
				saveArchiveFilenameInConfigProperty(allNetcdfFilename,
						ConfigPropertiesCode.DOWNLOAD_ARCHIVE_ALL_NETCDF,
						CONFIG_PROP_ALL_NETCDF_LABEL);

		} catch (Exception e) {
			logger.error("Error in run(): " + e.getClass() + " , " + e);
			e.printStackTrace();
		} finally {
			if (temporaryFiles != null) {
				cleanGeneratedFiles(temporaryFiles);
			}
		}

	}
*/
	/*private void saveArchiveFilenameInConfigProperty(String filename,
			String propertyCode, String propertyLabel) throws Exception {
		logger.debug("Save archive filename '" + filename
				+ "' in table ConfigProperty :");
		ConfigProperty conf = configPropDao.find(propertyCode);
		if (conf == null) {
			ConfigCateg cat = configCatDao.find(CONFIG_CATEGORY_DOWNLOAD_NAME);
			if (cat != null) {
				conf = new ConfigProperty();
				conf.setCode(propertyCode);
				conf.setCateg(cat);
				conf.setLabel(propertyLabel);
			} else {
				throw new Exception("Error : ConfigCateg with name '"
						+ CONFIG_CATEGORY_DOWNLOAD_NAME
						+ "' not found in data base");
			}
		} else {
			File file = new File(root + FileDTO.ARCHIVE_FOLDER, conf.getValue());
			if (file.exists()) {
				//logger.debug("Delete old archive with path '"
				//		+ file.getAbsolutePath() + "' from directory");
				//file.delete();
			}
		}
		conf.setDefaultValue(filename);
		conf.setValue(filename);
		configPropDao.save(conf);
	}*/

	/*
	
	private List<FileDTO> getFiles(String requestId, SearchCriterion sc, String outputFormat) throws Exception {
		initDao();

		List<FileDTO> result = new ArrayList<>();

		try {
			for (BoatDTO boat : sc.getBoatListSelection()) {
				Date startDate = new Date();
				logger.info("Process boat '" + boat.getName() + "' :");
				List<SssFile> files = fileDao.getFilesByBoat(boat.getName(),
						sc.getBeginDate(), sc.getEndDate(), false);

				for (SssFile file : files) {
					String generatedFile = trajDao.downloadTrajectory(
							requestId, file.getName(), sc, outputFormat);

					if (generatedFile != null) {
						FileDTO tmp = new FileDTO();
						tmp.setSssFileName(file.getName());
						tmp.setGeneratedFileName(generatedFile);
						result.add(tmp);
					}
				}

				Date endDate = new Date();
				long time = (endDate.getTime() - startDate.getTime()) / 1000;
				logger.info("Time for boat '" + boat.getName()
 + "' : "
						+ time + " second(s)");
			}
		} catch (Exception e) {
			if (result != null) {
				cleanGeneratedFiles(result);
			}
			logger.error("Error in getFiles(): " + e.getClass() + " , " + e);
			throw e;
		}
		return result;
	}
*/
	private String createArchive(String requestId, List<FileDTO> fichiers,
			String compression) throws IOException {
		logger.info("Create archive from " + compression + " files :");
		ArchiveSortie archive = new ArchiveSortie(compression, requestId);
		archive.setFichiers(fichiers);
		archive.open();
		archive.write();
		archive.close();
		cleanGeneratedFiles(fichiers);
						
		logger.info("Archive '" + archive.getFile().getName() + "' for "
				+ compression + " files has been created");
		File destDir = new File(downloadConfig.getRoot(), FileDTO.ARCHIVE_FOLDER);
		FileUtils.moveFileToDirectory(archive.getFile(), destDir, true);
		return archive.getFile().getName();
	}

	private void cleanGeneratedFiles(List<FileDTO> fichiers) {
		for (FileDTO fileDto : fichiers) {
			File file = new File(
					fileDto.getGeneratedFileName());
			if (file.exists()) {
				file.delete();
			}
		}

	}

	private void initDao() {
		if (trajDao == null) {
			trajDao = (TrajectoryDAO) SSSDataApplication.getSpringBeanFactory()
					.getBeanByName(TrajectoryDAO.BEAN_NAME);
		}
		if (fileDao == null) {
			fileDao = (SssFileDAO) SSSDataApplication.getSpringBeanFactory()
					.getBeanByName(SssFileDAO.BEAN_NAME);
		}
		if (propDao == null) {
			propDao = (ConfigPropertyDAO) DefaultServerApplication
					.getSpringBeanFactory().getBeanByName(
							ConfigPropertyDAO.BEAN_NAME);
		}
		if (boatDao == null) {
			boatDao = (BoatDAO) SSSDataApplication.getSpringBeanFactory()
					.getBeanByName(BoatDAO.BEAN_NAME);
		}
		if (configPropDao == null) {
			configPropDao = (ConfigPropertyDAO) SSSDataApplication
					.getSpringBeanFactory().getBeanByName(
							ConfigPropertyDAO.BEAN_NAME);
		}
		if (configCatDao == null) {
			configCatDao = (ConfigCategDAO) SSSDataApplication
					.getSpringBeanFactory().getBeanByName(
							ConfigCategDAO.BEAN_NAME);
		}
		if (archiveDao == null) {
			archiveDao = (ArchiveDAO) SSSDataApplication
					.getSpringBeanFactory().getBeanByName(
							ArchiveDAO.BEAN_NAME);
		}
	}

	public DownloadConfig getDownloadConfig() {
		return downloadConfig;
	}
	public void setDownloadConfig(DownloadConfig downloadConfig) {
		this.downloadConfig = downloadConfig;
	}
	
}
