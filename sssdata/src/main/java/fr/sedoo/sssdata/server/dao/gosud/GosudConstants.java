package fr.sedoo.sssdata.server.dao.gosud;

public interface GosudConstants {
	
	//Parameters
	public final String SHIP_CALL_SIGN="SHIP_CALL_SIGN";
	public final String TYPE_TSG="TYPE_TSG";
	public final String TYPE_TINT="TYPE_TINT";
	public final String PLATFORM_NAME="PLATFORM_NAME";
	
	public final String DATE_START="DATE_START";
	public final String DATE_END="DATE_END";
	public final String SOUTH_LATX="SOUTH_LATX";
	public final String NORTH_LATX="NORTH_LATX";
	public final String WEST_LONX="WEST_LONX";
	public final String EAST_LONX="EAST_LONX";
	
	public final String DATE_CREATION = "DATE_CREATION";
	public final String DATE_UPDATE = "DATE_UPDATE";
	
	public final int UNCONTROLLED_DATA = 0;
	public final int GOOD_DATA = 1;
	public final int PROBABLY_GOOD_DATA = 2;
	public final int PROBABLY_BAD_DATA = 3;
	public final int BAD_DATA = 4;
	public final int HARBOUR_DATA = 6;
	
	/**
	 * ORIGIN OF TIME
	 */
	public static final String REFERENCE_DATE_TIME = "REFERENCE_DATE_TIME";
	
	/**
	 * DATE OF MAIN INSTRUMENT MEASUREMENT
	 */
	public static final String DATE = "DATE";
	
	/**
	 * DATE OF EACH EXTERNAL DATA MEASUREMENT
	 */
	public static final String DATE_EXT = "DATE_EXT";
	
	/**
	 * LATITUDE OF EACH MEASUREMENT
	 */
	public static final String LATX = "LATX";
	
	/**
	 * LATITUDE OF EACH EXTERNAL DATA MEASUREMENT
	 */
	public static final String LATX_EXT = "LATX_EXT";
	
	/**
	 * LONGITUDE OF EACH MEASUREMENT
	 */
	public static final String LONX = "LONX";
	
	/**
	 * LONGITUDE OF EACH EXTERNAL DATA MEASUREMENT
	 */
	public static final String LONX_EXT = "LONX_EXT";
	
	/**
	 * QUALITY FLAG OF POSITION
	 */
	public static final String POSITION_QC = "POSITION_QC";
	
	/**
	 * SHIP SPEED COMPUTED FROM NAVIGATION
	 */
	public static final String SPDC = "SPDC";
	
	/**
	 * SEA PRESSURE IN TSG
	 */
	public static final String PRES = "PRES";
	
	/**
	 * MEASURING FLOW IN THE TSG INLET
	 */
	public static final String FLOW = "FLOW";
	
	/**
	 * ELECTRICAL CONDUCTIVITY
	 */
	public static final String CNDC = "CNDC";
	
	/**
	 * CONDUCTIVITY STANDARD DEVIATION
	 */
	public static final String CNDC_STD = "CNDC_STD";
	
	/**
	 * CONDUCTIVITY CALIBRATED
	 */
	public static final String CNDC_CAL = "CNDC_CAL";
	
	/**
	 * SENSOR CONDUCTIVITY FREQUENCY
	 */
	public static final String CNDC_FREQ = "CNDC_FREQ";
	
	/**
	 * CONDUCTIVITY CALIBRATION COEFFICIENTS
	 */
	public static final String CNDC_CALCOEF = "CNDC_CALCOEF";
	
	/**
	 * CONDUCTIVITY CALIBRATION COEFFICIENTS CONVENTION
	 */
	public static final String CNDC_CALCOEF_CONV = "CNDC_CALCOEF_CONV";
	
	/**
	 * CONDUCTIVITY LINEAR DRIFT CORRECTION COEFFICIENTS
	 */
	public static final String CNDC_LINCOEF = "CNDC_LINCOEF";
	
	/**
	 * CONDUCTIVITY LINEAR DRIFT CORRECTION COEFFICIENTS CONVENTION
	 */
	public static final String CNDC_LINCOEF_CONV = "CNDC_LINCOEF_CONV";
	
	/**
	 * WATER JACKET TEMPERATURE"
	 */
	public static final String SSJT = "SSJT";
	
	/**
	 * WATER JACKET TEMPERATURE QUALITY FLAG
	 */
	public static final String SSJT_QC = "SSJT_QC";
	
	/**
	 * WATER JACKET TEMPERATURE STANDARD DEVIATION
	 */
	public static final String SSJT_STD = "SSJT_STD";
	
	/**
	 * WATER JACKET TEMPERATURE CALIBRATED
	 */
	public static final String SSJT_CAL = "SSJT_CAL";
	
	/**
	 * WATER JACKET SENSOR TEMPERATURE FREQUENCY
	 */
	public static final String SSJT_FREQ = "SSJT_FREQ";
	
	/**
	 * TEMPERATURE CALIBRATION COEFFICIENTS
	 */
	public static final String SSJT_CALCOEF = "SSJT_CALCOEF";
	
	/**
	 * TEMPERATURE CALIBRATION COEFFICIENTS CONVENTION
	 */
	public static final String SSJT_CALCOEF_CONV = "SSJT_CALCOEF_CONV";
	
	/**
	 * TEMPERATURE LINEAR DRIFT CORRECTION COEFFICIENTS
	 */
	public static final String SSJT_LINCOEF = "SSJT_LINCOEF";
	
	/**
	 * TEMPERATURE LINEAR DRIFT CORRECTION COEFFICIENTS CONVENTION
	 */
	public static final String SSJT_LINCOEF_CONV = "SSJT_LINCOEF_CONV";
	
	/**
	 * WATER JACKET TEMPERATURE ADJUSTED
	 */
	public static final String SSJT_ADJUSTED = "SSJT_ADJUSTED";
	
	/**
	 * ERROR ON ADJUSTED WATER JACKET TEMPERATURE
	 */
	public static final String SSJT_ADJUSTED_ERROR = "SSJT_ADJUSTED_ERROR";
	
	/**
	 * WATER JACKET TEMPERATURE QUALITY FLAG
	 */
	public static final String SSJT_ADJUSTED_QC = "SSJT_ADJUSTED_QC";
	
	/**
	 * ADJUSTED WATER JACKET TEMPERATURE PROCESSING HISTORY
	 */
	public static final String SSJT_ADJUSTED_HIST = "SSJT_ADJUSTED_HIST";
	
	/**
	 * !!! SEA SURFACE SALINITY !!!
	 */
	public static final String SSPS = "SSPS";
	
	/**
	 * SEA SURFACE SALINITY QUALITY FLAG
	 */
	public static final String SSPS_QC = "SSPS_QC";
	
	/**
	 * SEA SURFACE SALINITY STANDARD DEVIATION
	 */
	public static final String SSPS_STD = "SSPS_STD";
	
	/**
	 * SEA SURFACE SALINITY CALIBRATED
	 */
	public static final String SSPS_CAL = "SSPS_CAL";
	
	/**
	 * SEA SURFACE SALINITY ADJUSTED
	 */
	public static final String SSPS_ADJUSTED = "SSPS_ADJUSTED";
	
	/**
	 * ERROR ON SEA SURFACE SALINITY ADJUSTED
	 */
	public static final String SSPS_ADJUSTED_ERROR = "SSPS_ADJUSTED_ERROR";
	
	/**
	 * ERROR ON SEA SURFACE SALINITY ADJUSTED QUALITY FLAG
	 */
	public static final String SSPS_ADJUSTED_QC = "SSPS_ADJUSTED_QC";
	
	/**
	 * ADJUSTED SEA SURFACE SALINITY PROCESSING HISTORY
	 */
	public static final String SSPS_ADJUSTED_HIST = "SSPS_ADJUSTED_HIST";
	
	/**
	 * NOMINAL DEPTH OF WATER INTAKE FOR SALINITY MEASUREMENT
	 */
	public static final String SSPS_DEPH = "SSPS_DEPH";
	
	/**
	 * MINIMUM DEPTH OF WATER INTAKE FOR SALINITY MEASUREMENT
	 */
	public static final String SSPS_DEPH_MIN = "SSPS_DEPH_MIN";
	
	/**
	 * MAXIMUM DEPTH OF WATER INTAKE FOR SALINITY MEASUREMENT
	 */
	public static final String SSPS_DEPH_MAX = "SSPS_DEPH_MAX";
	
	/**
	 * !!!  SEA SURFACE TEMPERATURE !!!
	 */
	public static final String SSTP = "SSTP";
	
	/**
	 * SEA SURFACE TEMPERATURE QUALITY FLAG
	 */
	public static final String SSTP_QC = "SSTP_QC";
	
	/**
	 * SEA SURFACE TEMPERATURE CALIBRATED
	 */
	public static final String SSTP_CAL = "SSTP_CAL";
	
	/**
	 * SEA SURFACE TEMPERATURE FREQUENCY
	 */
	public static final String SSTP_FREQ = "SSTP_FREQ";
	
	/**
	 * SEA SURFACE TEMPERATURE ADJUSTED
	 */
	public static final String SSTP_ADJUSTED = "SSTP_ADJUSTED";
	
	/**
	 * ERROR ON SEA SURFACE TEMPERATURE ADJUSTED
	 */
	public static final String SSTP_ADJUSTED_ERROR = "SSTP_ADJUSTED_ERROR";
	
	/**
	 * SEA SURFACE TEMPERATURE ADJUSTED QUALITY FLAG
	 */
	public static final String SSTP_ADJUSTED_QC = "SSTP_ADJUSTED_QC";
	
	/**
	 * ADJUSTED SEA SURFACE TEMPERATURE PROCESSING HISTORY
	 */
	public static final String SSTP_ADJUSTED_HIST = "SSTP_ADJUSTED_HIST";
	
	/**
	 * NOMINAL DEPTH OF WATER INTAKE FOR TEMPERATURE MEASUREMENT
	 */
	public static final String SSTP_DEPH = "SSTP_DEPH";
	
	/**
	 * MINIMUM DEPTH OF WATER INTAKE FOR TEMPERATURE MEASUREMENT
	 */
	public static final String SSTP_DEPH_MIN = "SSTP_DEPH_MIN";
	
	/**
	 * MAXIMUM DEPTH OF WATER INTAKE FOR TEMPERATURE MEASUREMENT
	 */
	public static final String SSTP_DEPH_MAX = "SSTP_DEPH_MAX";
	
	/**
	 * TEMPERATURE CALIBRATION COEFFICIENTS
	 */
	public static final String SSTP_CALCOEF = "SSTP_CALCOEF";
	
	/**
	 * TEMPERATURE CALIBRATION COEFFICIENTS CONVENTION
	 */
	public static final String SSTP_CALCOEF_CONV = "SSTP_CALCOEF_CONV";
	
	/**
	 * TEMPERATURE LINEAR DRIFT CORRECTION COEFFICIENTS
	 */
	public static final String SSTP_LINCOEF = "SSTP_LINCOEF";
	
	/**
	 * TEMPERATURE LINEAR DRIFT CORRECTION COEFFICIENTS CONVENTION
	 */
	public static final String SSTP_LINCOEF_CONV = "SSTP_LINCOEF_CONV";
	
	/**
	 * SEA SURFACE TEMPERATURE FROM EXTERNAL DATA
	 */
	public static final String SSTP_EXT = "SSTP_EXT";
	
	/**
	 * SEA SURFACE TEMPERATURE FROM EXTERNAL DATA QUALITY FLAG
	 */
	public static final String SSTP_EXT_QC = "SSTP_EXT_QC";
	
	/**
	 * TYPE OF EXTERNAL SEA SURFACE TEMPERATURE  DATA ORIGIN
	 */
	public static final String SSTP_EXT_TYPE = "SSTP_EXT_TYPE";
	
	/**
	 * SEA SURFACE SALINITY FROM EXTERNAL DATA
	 */
	public static final String SSPS_EXT = "SSPS_EXT";
	
	/**
	 * SEA SURFACE SALINITY FROM EXTERNAL DATA QUALITY FLAG
	 */
	public static final String SSPS_EXT_QC = "SSPS_EXT_QC";
	
	/**
	 * TYPE OF EXTERNAL SEA SURFACE SALINITY DATA ORIGIN
	 */
	public static final String SSPS_EXT_TYPE = "SSPS_EXT_TYPE";
	
	/**
	 * DATE OF WATER SAMPLE SURFACE SALINITY ANALYSIS
	 */
	public static final String SSPS_EXT_ANALDATE = "SSPS_EXT_ANALDATE";
	
	/**
	 * SEA SURFACE SALINITY BOTTLE NUMBER
	 */
	public static final String SSPS_EXT_BOTTLE = "SSPS_EXT_BOTTLE";
	
	
	//Observed Property
	public final String SST_OBSERVABLE_PROPERTY = "http://mmisw.org/ont/cf/parameter/sea_surface_temperature";
	public final String SSS_OBSERVABLE_PROPERTY = "http://mmisw.org/ont/cf/parameter/sea_surface_salinity";
	public final String DATE_FORMAT = "yyyyMMddHHmmss";
	
	//CRS : 2D GPS
	public final String CRS_NAME = "http://www.opengis.net/def/crs/EPSG/0/4326";
}
