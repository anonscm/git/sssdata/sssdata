package fr.sedoo.sssdata.server.dao.api;

import java.util.List;

import fr.sedoo.sssdata.server.model.Boat;

public interface BoatDAO {
	public final static String BEAN_NAME="boatDAO";
	List<Boat> findAll();	
	Boat save (Boat b);
	Boat findByName(String name);
	Boat findByCode(String code);
	Boat findAllFiles(String name);
	
	Long getSize();
}
