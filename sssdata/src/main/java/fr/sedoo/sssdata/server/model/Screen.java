package fr.sedoo.sssdata.server.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SCREEN")
public class Screen {
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@Column(name="NAME")
	private String name;
	
	@Column(name="VALUE", columnDefinition="TEXT")
	private String value;
	
		
	public Screen(){
		
	}
	
	public Screen(String name, String value)
	{
		this.name = name;
		this.value = value;
	}

		
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
}
