package fr.sedoo.sssdata.server.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.sedoo.sssdata.server.dao.api.MetadataDAO;
import fr.sedoo.sssdata.server.model.Metadata;

@Repository
public class MetadataDAOJPAImpl implements MetadataDAO {

	protected EntityManager em;
	public EntityManager getEntityManager() {
		return em;
	}

	@PersistenceContext
	public void setEntityManager(EntityManager entityManager) {
		this.em = entityManager;
	}
	

	@Override
	@Transactional
	public List<Metadata> findAll() {
		Query query = em.createQuery("SELECT m FROM Metadata m");
		try{			
			return (List<Metadata>) query.getResultList();
		}
		catch (NoResultException e){
			return new ArrayList<Metadata>();
		}
	}

	@Override
	@Transactional
	public Metadata save(Metadata m) {
		if (m.getId() == null){
			getEntityManager().persist(m);
			return m;
		}else{
			getEntityManager().merge(m);
			return m;
		}
	}

	@Override
	@Transactional
	public Metadata findByDoi(String doi) {
		Query query = em.createQuery("Select m from Metadata m where m.doi=:arg1");
		query.setParameter("arg1", doi);
		Metadata m = null;
		try{
			m = (Metadata) query.getSingleResult();
			//On force le chargement de l'historique
			m.getHistory().size();
		}
		catch (NoResultException e){
			return null;
		}
		return m;
	}

}
