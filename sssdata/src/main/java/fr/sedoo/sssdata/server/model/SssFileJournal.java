package fr.sedoo.sssdata.server.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name=SssFileJournal.TABLE_NAME)
public class SssFileJournal implements Comparable<SssFileJournal> {

	public static final String TABLE_NAME ="SSSFILE_JOURNAL";
	
	public static final String TYPE_DELETION = "Deletion";
	public static final String TYPE_INSERTION = "Insertion";
	
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(name="DATE",nullable=false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;
	
	@Column(name="TYPE",nullable=false)
	private String type;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="FILE_ID")
	private SssFile file;
	
	public SssFileJournal() {
		super();
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public SssFile getFile() {
		return file;
	}

	public void setFile(SssFile file) {
		this.file = file;
	}

	@Override
	public int compareTo(SssFileJournal o) {
		return getDate().compareTo(o.getDate());
	}
		
}
