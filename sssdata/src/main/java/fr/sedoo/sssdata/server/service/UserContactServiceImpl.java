package fr.sedoo.sssdata.server.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.sedoo.commons.client.util.ServiceException;
import fr.sedoo.commons.server.mail.MailConfig;
import fr.sedoo.commons.server.mail.MailSimple;
import fr.sedoo.sssdata.client.service.UserContactService;
import fr.sedoo.sssdata.server.SSSDataApplication;
import fr.sedoo.sssdata.server.dao.api.SssFileDAO;
import fr.sedoo.sssdata.server.dao.api.UserDAO;
import fr.sedoo.sssdata.server.model.Download;
import fr.sedoo.sssdata.server.model.SssFile;
import fr.sedoo.sssdata.server.model.User;
import fr.sedoo.sssdata.shared.BoatDTO;
import fr.sedoo.sssdata.shared.UserContactInformations;

public class UserContactServiceImpl extends RemoteServiceServlet implements UserContactService {

	private static final String MAIL_CONFIG_BEAN_NAME = "mailConfig";
		
	private MailConfig mailConfig;
	
	private SssFileDAO sssFileDao;
	private UserDAO userDao;
	
	@Override
	public Set<String> getUsers(UserContactInformations infos)	throws ServiceException {
		Set<String> result = new HashSet<String>();
		
		initDao();
		List<SssFile> files = new ArrayList<SssFile>();
				
		if (infos.getBoatListSelection().isEmpty()){
			if (infos.getBeginDate() == null && infos.getEndDate() == null){
				for (User u: userDao.getAll()){
					result.add(u.getEmail());
				}
				return result;
			}else{
				files.addAll(sssFileDao.getFilesByDates(infos.getBeginDate(),
						infos.getEndDate(), true));
			}
		}else{
			for (BoatDTO boat: infos.getBoatListSelection()){
				files.addAll(sssFileDao.getFilesByBoat(boat.getName(),
						infos.getBeginDate(), infos.getEndDate(), true));
			}
		}
		for (SssFile file: files){
			for (Download dl: file.getDownloads()){
				result.add(dl.getUser().getEmail());
			}
		}
	
		//result.add("gbrissebrat@yahoo.fr");
		return result;
	}

	private void initDao(){
		if (sssFileDao == null){
			sssFileDao = (SssFileDAO) SSSDataApplication.getSpringBeanFactory().getBeanByName(SssFileDAO.BEAN_NAME);
		}
		if (userDao == null){
			userDao = (UserDAO) SSSDataApplication.getSpringBeanFactory().getBeanByName(UserDAO.BEAN_NAME);
		}
	}
	
	@Override
	public void sendMessage(Set<String> emails, String subject, String body, String from) throws ServiceException {
		if (mailConfig == null)	{
			mailConfig = (MailConfig) SSSDataApplication.getSpringBeanFactory().getBeanByName(MAIL_CONFIG_BEAN_NAME);
		}
		
		MailSimple m = new MailSimple(mailConfig.getHost());
		
		//Envoi d'une copie à l'admin
		emails.add(mailConfig.getAdmin());
		
		for (String email: emails){
			try{
				//m.send(mailConfig.getFrom(), email, subject, body);
				m.send(from, email, subject, body);
			}catch(Exception e){
				throw new ServiceException("Error while sending email to " + email + ". Cause: " + e);
			}
		}
		
	}

}
