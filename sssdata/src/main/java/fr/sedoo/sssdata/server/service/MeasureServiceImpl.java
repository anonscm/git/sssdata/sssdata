package fr.sedoo.sssdata.server.service;

import java.sql.SQLException;

import fr.sedoo.sssdata.server.SSSDataApplication;
import fr.sedoo.sssdata.server.dao.api.MeasureDAO;
import fr.sedoo.sssdata.server.model.Measure;
import fr.sedoo.sssdata.server.model.SssFile;

public class MeasureServiceImpl {

	private MeasureDAO dao;
	
	private void initDao(){
		if (dao == null){
			dao = (MeasureDAO) SSSDataApplication.getSpringBeanFactory().getBeanByName(MeasureDAO.BEAN_NAME);
		}
	}
	
	/*
	public void add(Measure m){
		initDao();
		dao.add(m);
	}
	*/
		
	public void initBatch() throws SQLException{
		initDao();
		dao.initBatch();
	}
	public void addToBatch(Measure measure) throws SQLException{
		initDao();
		dao.addToBatch(measure);
	}
	public void closeBatch() throws SQLException{
		initDao();
		dao.closeBatch();
	}
	
	/*
	public void bulkAdd(List<Measure> measures){
		initDao();
		dao.bulkAdd(measures);
	}*/

	/*public Trajectory getMeasurePrecisionByFile(SssFile f, int precision, SearchCriterion sc) {
		initDao();
		return dao.getMeasurePrecisionByFile(f,precision, sc);
	}*/

	public void deleteByFile(SssFile sssFile) throws Exception{
		initDao();
		dao.deleteByFile(sssFile);
	}
}
