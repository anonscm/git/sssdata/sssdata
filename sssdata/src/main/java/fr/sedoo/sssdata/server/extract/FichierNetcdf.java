package fr.sedoo.sssdata.server.extract;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

import org.apache.log4j.Logger;

import ucar.ma2.ArrayFloat;
import ucar.ma2.ArrayInt;
import ucar.ma2.DataType;
import ucar.nc2.Attribute;
import ucar.nc2.Group;
import ucar.nc2.NetcdfFileWriter;
import ucar.nc2.NetcdfFileWriter.Version;
import ucar.nc2.Variable;
import fr.sedoo.sssdata.server.dao.gosud.GosudUtils;
import fr.sedoo.sssdata.server.model.Measure;

public class FichierNetcdf extends FichierSortie {

	private static Logger logger = Logger.getLogger(FichierNetcdf.class);
	public final static String EXTENSION = ".nc";
	
	//TODO Ajouter aux paramètres configurables ?
	public final static String REFERENCE_DATE = "1950-01-01 00:00:00";
	
	public static final String DIMENSION_N1 = "N1";
	public static final int DIMENSION_N1_SIZE = 1;
	public static final int MISSING_FLAG = 99;
	
	private NetcdfFileWriter ncWriter;
	private Group rootGroup;
		
	private NetcdfVariableWithData.Time timeVar;
	private NetcdfVariableWithData.Param lonVar;
	private NetcdfVariableWithData.Param latVar;
	private NetcdfVariableWithData.Param sstVar;
	private NetcdfVariableWithData.Flag sstQcVar;
	private NetcdfVariableWithData.Param sstErrVar;
	private NetcdfVariableWithData.Param sssVar;
	private NetcdfVariableWithData.Flag sssQcVar;
	private NetcdfVariableWithData.Param sssErrVar;
		
		
	public FichierNetcdf(String filename, FichierSortieHeader header) {
		super(filename, header);
	}

	@Override
	protected void open() throws IOException{
		logger.debug("open()");

		this.ncWriter = NetcdfFileWriter.createNew(Version.netcdf3, getFile().getAbsolutePath());
		this.rootGroup = ncWriter.getNetcdfFile().getRootGroup();
		
	}

	@Override
	protected void close() throws IOException{
		logger.debug("close()");
		//Ecrire les valeurs		
		timeVar.write(ncWriter);
		lonVar.write(ncWriter);
		latVar.write(ncWriter);
		sstVar.write(ncWriter);
		sstQcVar.write(ncWriter);
		sstErrVar.write(ncWriter);
		sssVar.write(ncWriter);
		sssQcVar.write(ncWriter);
		sssErrVar.write(ncWriter);
		
		ncWriter.close();
	}

	@Override
	protected void printHeader() throws IOException {
		logger.debug("printHeader()");
		
		//Dimensions
		//ncWriter.addUnlimitedDimension("TIME");
		ncWriter.addDimension(rootGroup, "TIME", getSize());
		ncWriter.addDimension(rootGroup, DIMENSION_N1, DIMENSION_N1_SIZE);
				
		//Variables
		Variable timeVar = ncWriter.addVariable(rootGroup, "TIME", DataType.DOUBLE , "TIME");
		ncWriter.addVariableAttribute(timeVar, new Attribute("standard_name","time"));
		ncWriter.addVariableAttribute(timeVar, new Attribute("long_name","time of measurement"));
		ncWriter.addVariableAttribute(timeVar, new Attribute("calendar","julian"));
		ncWriter.addVariableAttribute(timeVar, new Attribute("units", "days since " + REFERENCE_DATE + " +00:00"));
		
		Variable lonVar = ncWriter.addVariable(rootGroup, "LON", DataType.FLOAT , "TIME");
		ncWriter.addVariableAttribute(lonVar, new Attribute("standard_name","longitude"));
		ncWriter.addVariableAttribute(lonVar, new Attribute("long_name","longitude"));
		ncWriter.addVariableAttribute(lonVar, new Attribute("units","degrees_east"));
				
		Variable latVar = ncWriter.addVariable(rootGroup, "LAT", DataType.FLOAT , "TIME");
		ncWriter.addVariableAttribute(latVar, new Attribute("standard_name","latitude"));
		ncWriter.addVariableAttribute(latVar, new Attribute("long_name","latitude"));
		ncWriter.addVariableAttribute(latVar, new Attribute("units","degrees_north"));
						
		Variable sstVar = ncWriter.addVariable(rootGroup, "SST", DataType.FLOAT , "TIME");
		ncWriter.addVariableAttribute(sstVar, new Attribute("standard_name","sea_surface_temperature"));
		ncWriter.addVariableAttribute(sstVar, new Attribute("long_name", SST_VAR_NAME));
		ncWriter.addVariableAttribute(sstVar, new Attribute("_FillValue", MISSING_VALUE));
		ncWriter.addVariableAttribute(sstVar, new Attribute("missing_value", MISSING_VALUE));
		ncWriter.addVariableAttribute(sstVar, new Attribute("units","degree_Celsius"));
		ncWriter.addVariableAttribute(sstVar, new Attribute("coordinates","time lon lat"));
		ncWriter.addVariableAttribute(sstVar, new Attribute("ancillary_variables","SST_QC SST_ERROR"));
				
		Variable sstQcVar = ncWriter.addVariable(rootGroup, "SST_QC", DataType.INT , "TIME");
		ncWriter.addVariableAttribute(sstQcVar, new Attribute("standard_name","sea_surface_temperature_status_flag"));
		ncWriter.addVariableAttribute(sstQcVar, new Attribute("_FillValue", MISSING_FLAG));
		ncWriter.addVariableAttribute(sstQcVar, new Attribute("missing_value", MISSING_FLAG));
		addFlags(sstQcVar);
		
		Variable sstErrVar = ncWriter.addVariable(rootGroup, "SST_ERROR", DataType.FLOAT , "TIME");
		ncWriter.addVariableAttribute(sstErrVar, new Attribute("standard_name","sea_surface_temperature_standard_error"));
		ncWriter.addVariableAttribute(sstErrVar, new Attribute("_FillValue", MISSING_VALUE));
		ncWriter.addVariableAttribute(sstErrVar, new Attribute("units","degree_Celsius"));
		ncWriter.addVariableAttribute(sstErrVar, new Attribute("missing_value", MISSING_VALUE));
				
		Variable sssVar = ncWriter.addVariable(rootGroup, "SSS", DataType.FLOAT , "TIME");
		ncWriter.addVariableAttribute(sssVar, new Attribute("standard_name","sea_surface_salinity"));
		ncWriter.addVariableAttribute(sssVar, new Attribute("long_name", SSS_VAR_NAME));
		ncWriter.addVariableAttribute(sssVar, new Attribute("_FillValue", MISSING_VALUE));
		ncWriter.addVariableAttribute(sssVar, new Attribute("missing_value", MISSING_VALUE));
		ncWriter.addVariableAttribute(sssVar, new Attribute("units","PSS-78"));
		ncWriter.addVariableAttribute(sssVar, new Attribute("coordinates","time lon lat"));
		ncWriter.addVariableAttribute(sssVar, new Attribute("ancillary_variables","SSS_QC SSS_ERROR"));

		Variable sssQcVar = ncWriter.addVariable(rootGroup, "SSS_QC", DataType.INT , "TIME");
		ncWriter.addVariableAttribute(sssQcVar, new Attribute("standard_name","sea_surface_salinity_status_flag"));
		ncWriter.addVariableAttribute(sssQcVar, new Attribute("_FillValue", MISSING_FLAG));
		ncWriter.addVariableAttribute(sssQcVar, new Attribute("missing_value", MISSING_FLAG));
		addFlags(sssQcVar);
		
		Variable sssErrVar = ncWriter.addVariable(rootGroup, "SSS_ERROR", DataType.FLOAT , "TIME");
		ncWriter.addVariableAttribute(sssErrVar, new Attribute("standard_name","sea_surface_salinity_standard_error"));
		ncWriter.addVariableAttribute(sssErrVar, new Attribute("_FillValue", MISSING_VALUE));
		ncWriter.addVariableAttribute(sssErrVar, new Attribute("units","PSS-78"));
		ncWriter.addVariableAttribute(sssErrVar, new Attribute("missing_value", MISSING_VALUE));
		
		//Attributs globaux
		addGlobalAttribute("Conventions", "CF-1.6");
		addGlobalAttribute("Metadata_Conventions", "Unidata Dataset Discovery v1.0");
		addGlobalAttribute("featureType", "trajectory");
		addGlobalAttribute("title", getHeaderInfos().getTitle());
		addGlobalAttribute("summary", getHeaderInfos().getSummary());
		addGlobalAttribute("doi", getHeaderInfos().getDoi());
		if (getHeaderInfos().getCitation() != null){
			addGlobalAttribute("citation", getHeaderInfos().getCitation());
		}
		
		addGlobalAttribute("platform_name", getHeaderInfos().getBoatName());
		addGlobalAttribute("project", getHeaderInfos().getProjectName());
		addGlobalAttribute("license", getHeaderInfos().getUseConstraints());
		addGlobalAttribute("acknowledgment", getHeaderInfos().getAcknowledgment());
		addGlobalAttribute("keywords","SEA SURFACE TEMPERATURE, SALINITY");
		
		addGlobalAttribute("source","surface observation");
		addGlobalAttribute("history", new Date() + ": data extracted from SSS database");
		
		addGlobalAttribute("creator_name", getHeaderInfos().getProjectName());
		addGlobalAttribute("creator_url", getHeaderInfos().getProjectUrl());
		addGlobalAttribute("creator_email", getHeaderInfos().getContact());
		
		addGlobalAttribute("standard_name_vocabulary", "CF-1.0");
		addGlobalAttribute("keywords_vocabulary","GCMD Science Keywords");
		addGlobalAttribute("cdm_data_type","Trajectory");
						
		this.lonVar = new NetcdfVariableWithData.Param(lonVar, getSize());
		this.latVar = new NetcdfVariableWithData.Param(latVar, getSize());
		this.sssVar = new NetcdfVariableWithData.Param(sssVar, getSize());
		this.sstVar = new NetcdfVariableWithData.Param(sstVar, getSize());
		this.sssQcVar = new NetcdfVariableWithData.Flag(sssQcVar, getSize());
		this.sstQcVar = new NetcdfVariableWithData.Flag(sstQcVar, getSize());
		this.sssErrVar = new NetcdfVariableWithData.Param(sssErrVar, getSize());
		this.sstErrVar = new NetcdfVariableWithData.Param(sstErrVar, getSize());
		
		try{
			Date dateReference = sdf.parse(REFERENCE_DATE);
			this.timeVar = new NetcdfVariableWithData.Time(timeVar, getSize(), dateReference.getTime());
		}catch(Exception e){
			throw new IOException(e);
		}
		
		Variable sspsDephVar = ncWriter.addVariable(rootGroup, "SSPS_DEPH", DataType.FLOAT , DIMENSION_N1);
		ncWriter.addVariableAttribute(sspsDephVar, new Attribute("long_name", "NOMINAL DEPTH OF WATER INTAKE FOR SALINITY MEASUREMENT"));
		ncWriter.addVariableAttribute(sspsDephVar, new Attribute("_FillValue", MISSING_VALUE));
		ncWriter.addVariableAttribute(sspsDephVar, new Attribute("missing_value", MISSING_VALUE));
		ncWriter.addVariableAttribute(sspsDephVar, new Attribute("units","meter"));
		ncWriter.addVariableAttribute(sspsDephVar, new Attribute("resolution","0.1f"));
		ncWriter.addVariableAttribute(sspsDephVar, new Attribute("valid_min","0.f"));
		ncWriter.addVariableAttribute(sspsDephVar, new Attribute("valid_max","100.f"));
		
		Variable sspsDephMinVar = ncWriter.addVariable(rootGroup, "SSPS_DEPH_MIN", DataType.FLOAT , DIMENSION_N1);
		ncWriter.addVariableAttribute(sspsDephMinVar, new Attribute("long_name", "MINIMUM DEPTH OF WATER INTAKE FOR SALINITY MEASUREMENT"));
		ncWriter.addVariableAttribute(sspsDephMinVar, new Attribute("_FillValue", MISSING_VALUE));
		ncWriter.addVariableAttribute(sspsDephMinVar, new Attribute("missing_value", MISSING_VALUE));
		ncWriter.addVariableAttribute(sspsDephMinVar, new Attribute("units","meter"));
		ncWriter.addVariableAttribute(sspsDephMinVar, new Attribute("resolution","0.1f"));
		ncWriter.addVariableAttribute(sspsDephMinVar, new Attribute("valid_min","0.f"));
		ncWriter.addVariableAttribute(sspsDephMinVar, new Attribute("valid_max","100.f"));
		
		Variable sspsDephMaxVar = ncWriter.addVariable(rootGroup, "SSPS_DEPH_MAX", DataType.FLOAT , DIMENSION_N1);
		ncWriter.addVariableAttribute(sspsDephMaxVar, new Attribute("long_name", "MAXIMUM DEPTH OF WATER INTAKE FOR SALINITY MEASUREMENT"));
		ncWriter.addVariableAttribute(sspsDephMaxVar, new Attribute("_FillValue", MISSING_VALUE));
		ncWriter.addVariableAttribute(sspsDephMaxVar, new Attribute("missing_value", MISSING_VALUE));
		ncWriter.addVariableAttribute(sspsDephMaxVar, new Attribute("units","meter"));
		ncWriter.addVariableAttribute(sspsDephMaxVar, new Attribute("resolution","0.1f"));
		ncWriter.addVariableAttribute(sspsDephMaxVar, new Attribute("valid_min","0.f"));
		ncWriter.addVariableAttribute(sspsDephMaxVar, new Attribute("valid_max","100.f"));

		NetcdfVariableWithData.Param sspsDeph = new  NetcdfVariableWithData.Param(sspsDephVar, DIMENSION_N1_SIZE);
		NetcdfVariableWithData.Param sspsDephMin = new  NetcdfVariableWithData.Param(sspsDephMinVar, DIMENSION_N1_SIZE);
		NetcdfVariableWithData.Param sspsDephMax = new  NetcdfVariableWithData.Param(sspsDephMaxVar, DIMENSION_N1_SIZE);
				
		ncWriter.create();
		
		
		sspsDeph.addValue(getHeaderInfos().getSspsDeph());
		sspsDephMin.addValue(getHeaderInfos().getSspsDephMin());
		sspsDephMax.addValue(getHeaderInfos().getSspsDephMax());
		
		sspsDeph.write(ncWriter);
		sspsDephMin.write(ncWriter);
		sspsDephMax.write(ncWriter);
	}
	
	
	
	private void addFlags(Variable var){
		
		ArrayInt.D1 flagValues = new ArrayInt.D1(6);
		String flagMeanings = "";
		
		int i =0;
		for (Map.Entry<Integer, String> entry : GosudUtils.getFlags().entrySet()) {
			flagValues.set(i++, entry.getKey());
			flagMeanings += " " + entry.getValue().toLowerCase().replaceAll(" ", "_");
		}
		
		ncWriter.addVariableAttribute(var, new Attribute("flag_values", flagValues));
		ncWriter.addVariableAttribute(var, new Attribute("flag_meanings", flagMeanings.substring(1)));
		
	}
	
	private void addGlobalAttribute(String name, String value){
		ncWriter.addGroupAttribute(rootGroup, new Attribute(name, value));
	}

	@Override
	protected void writeMesure(Measure mesure) {
				
		timeVar.addValue(mesure.getMeasureDate());
		
		lonVar.addValue(mesure.getLongitude());
		latVar.addValue(mesure.getLatitude());
				
		sstVar.addValue(mesure.getTemperature());
		sstQcVar.addValue(mesure.getTemperatureQuality());
		sstErrVar.addValue(mesure.getTemperatureError());
		
		sssVar.addValue(mesure.getSalinity());
		sssQcVar.addValue(mesure.getSalinityQuality());
		sssErrVar.addValue(mesure.getSalinityError());
	}

	@Override
	protected String getExtension() {
		return EXTENSION;
	}

}
