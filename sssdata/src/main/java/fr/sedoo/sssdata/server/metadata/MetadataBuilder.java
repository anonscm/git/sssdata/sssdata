package fr.sedoo.sssdata.server.metadata;

import java.util.Collections;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.client.ui.AbstractImagePrototype;
import com.google.gwt.user.client.ui.Image;

import fr.sedoo.metadata.doi.DOIConstants;
import fr.sedoo.metadata.doi.domain.Contributor;
import fr.sedoo.metadata.doi.domain.Creator;
import fr.sedoo.metadata.doi.domain.RelatedIdentifier;
import fr.sedoo.metadata.doi.domain.Subject;
import fr.sedoo.metadata.doi.utils.MetadataUtils;
import fr.sedoo.sssdata.server.model.Metadata;
import fr.sedoo.sssdata.server.model.MetadataHistory;
import fr.sedoo.sssdata.shared.metadata.CreatorDTO;
import fr.sedoo.sssdata.shared.metadata.HistoryItem;
import fr.sedoo.sssdata.shared.metadata.MetadataDTO;
import fr.sedoo.sssdata.shared.metadata.RelatedIdentifierDTO;

public class MetadataBuilder {

	private static Logger LOG = Logger.getLogger(MetadataBuilder.class);
			
	private SafeHtml createSafeHtml(ImageResource resource, String tootltip) 
	{
		Image aux = AbstractImagePrototype.create(resource).createImage();
		aux.setTitle(tootltip);
		return SafeHtmlUtils.fromTrustedString(aux.toString());
	}
	
	public static MetadataDTO buildDTO(Metadata m) {
		String xml = m.getXml();
		fr.sedoo.metadata.doi.domain.Metadata doiMetadata = MetadataUtils.readXml(xml);
		
		if (doiMetadata == null){
			return null;
		}else{
			MetadataDTO metadata = new MetadataDTO();
			metadata.setDoi(m.getDoi());
			metadata.setTitle(doiMetadata.getTitle());
			metadata.setPublisher(doiMetadata.getPublisher());
			metadata.setPublicationYear(doiMetadata.getPublicationYear());
			metadata.setVersion(doiMetadata.getVersion());
			metadata.setDateBegin(doiMetadata.getDateBegin());
			metadata.setDateEnd(doiMetadata.getDateEnd());
			for (Subject s: doiMetadata.getSubjects()){
				metadata.getKeywords().add(s.getValue());
			}
			
			metadata.getDataFormats().addAll(doiMetadata.getDataFormats());
			
			for (Creator c: doiMetadata.getCreators()){
				CreatorDTO dto = new CreatorDTO(c.toString());
				if (c.getNameIdentifier() != null && c.getNameIdentifier().isOrcid()){
					dto.setOrcid(c.getNameIdentifier().getValue());
				}
				metadata.getCreators().add(dto);
			}
			
			for (Contributor c: doiMetadata.getContributors()){
				metadata.getContributors().add(c.getName());
			}
			
			metadata.setSummary(doiMetadata.getSummary());
			metadata.setRights(doiMetadata.getUseConstraints());				
			if (!doiMetadata.getRights().isEmpty()){
				metadata.setRights(doiMetadata.getRights().get(0).getValue());
			}
			
			for (RelatedIdentifier relatedIdentifier: doiMetadata.getRelatedIdentifiers()){
				if (relatedIdentifier.getValue() != null){
					RelatedIdentifierDTO dto = new RelatedIdentifierDTO();
					
					if (StringUtils.isNotEmpty(relatedIdentifier.getRelationType())){
						dto.setType(relatedIdentifier.getType() + " (" + relatedIdentifier.getRelationType() + ")");
					}else{
						dto.setType(relatedIdentifier.getType());
					}
					if (RelatedIdentifier.TYPE_DOI.equalsIgnoreCase(relatedIdentifier.getType()) && ! relatedIdentifier.getValue().startsWith(DOIConstants.DOI_RESOLVER) ){
						dto.setIdentifier(DOIConstants.DOI_RESOLVER + relatedIdentifier.getValue());
					}else{
						dto.setIdentifier(relatedIdentifier.getValue());
					}
					metadata.getRelatedIdentifiers().add(dto);
				}
			}
			
			if (m.getHistory() != null){
				for (MetadataHistory h: m.getHistory()){
					HistoryItem item = HistoryItemBuilder.buildItem(h);
					metadata.getHistory().add(item);
				}
				Collections.sort(metadata.getHistory());
			}
			
			return metadata;
		}
		
	}
	
	/*public static MetadataDTO buildDTO(Metadata m) {
		try {
			String xml = m.getXml();
			InputStream stream = new ByteArrayInputStream(xml.getBytes("UTF-8"));
			SAXBuilder sb= new SAXBuilder();
			Document doc= sb.build(stream);
			MetadataDTO metadata = new MetadataDTO();
			metadata.setDoi(m.getDoi());

			Namespace ns = Namespace.getNamespace(DOIConstants.DATACITE_NAMESPACE);

			Element racine = doc.getRootElement();
			Element eltTitles = racine.getChild("titles", ns); 
			if (eltTitles != null){
				String title = eltTitles.getChildTextNormalize("title", ns);
				metadata.setTitle(title);
			}
			Element eltDescriptions = racine.getChild("descriptions", ns); 
			if (eltDescriptions != null){
				String summary = eltDescriptions.getChildTextNormalize("description", ns);
				metadata.setSummary(summary);
			}

			Element eltRightsList = racine.getChild("rightsList", ns); 
			if (eltRightsList != null){
				String rights = eltRightsList.getChildTextNormalize("rights", ns);
				metadata.setRights(rights);
			}

			Element eltSubjects = racine.getChild("subjects", ns); 
			if (eltSubjects != null){
				List<Element> elts = (List<Element>)eltSubjects.getChildren("subject", ns);
				for (Element elt: elts){
					metadata.getKeywords().add(elt.getTextNormalize());
				}
			}

			Element eltFormats = racine.getChild("formats", ns); 
			if (eltFormats != null){
				List<Element> elts = (List<Element>)eltFormats.getChildren("format", ns);
				for (Element elt: elts){
					metadata.getDataFormats().add(elt.getTextNormalize());
				}
			}

			Element eltCreators = racine.getChild("creators", ns); 
			if (eltCreators != null){
				List<Element> elts = (List<Element>)eltCreators.getChildren("creator", ns);
				for (Element elt: elts){
					Creator c = new Creator(elt.getChildTextNormalize("creatorName", ns));
					List<Element> aff = (List<Element>)elt.getChildren("affiliation", ns);
					for (Element a: aff){
						c.getAffiliations().add(a.getTextNormalize());
					}
					metadata.getCreators().add(c);
				}
			}
			
			Element eltDates = racine.getChild("dates", ns); 
			if (eltDates != null){
				List<Element> elts = (List<Element>)eltDates.getChildren("date", ns);
				for (Element elt: elts){
					String dateType = elt.getAttributeValue("dateType");
					if ("Collected".equals(dateType)){
						String[] dates = elt.getTextNormalize().split("/");
						if (dates.length == 2) {
							metadata.setDateBegin(dates[0]);
							metadata.setDateEnd(dates[1]);
						}
					}
				}
			}

			Element eltContributors = racine.getChild("contributors", ns); 
			if (eltContributors != null){
				List<Element> elts = (List<Element>)eltContributors.getChildren("contributor", ns);
				for (Element elt: elts){
					metadata.getContributors().add(elt.getChildTextNormalize("contributorName", ns));
				}
			}

			metadata.setPublisher(racine.getChildTextNormalize("publisher", ns));
			metadata.setPublicationYear(racine.getChildTextNormalize("publicationYear", ns));
			metadata.setVersion(racine.getChildTextNormalize("version", ns));

			if (m.getHistory() != null){
				for (MetadataHistory h: m.getHistory()){
					HistoryItem item = HistoryItemBuilder.buildItem(h);
					metadata.getHistory().add(item);
				}
				Collections.sort(metadata.getHistory());
			}
			
			
			return metadata;
		}catch(Exception e){
			LOG.error("Error in getMetadata(). Cause: " + e);
			return null;
		}

	}*/
	
}
