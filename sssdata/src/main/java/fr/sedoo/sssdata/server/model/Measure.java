package fr.sedoo.sssdata.server.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Index;

import fr.sedoo.sssdata.shared.OtherFields;

@Entity
@Table(name="MEASURE")
public class Measure {
	
	@Id
	@Column(name="ID", columnDefinition = "bigserial")
	private Long id;
	
	@Column(name="MEASURE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date measureDate;
	
	@Column(name="LATITUDE")
	private Double latitude;
	
	@Column(name="LONGITUDE")
	private Double longitude;
	
	@Column(name="POSITION_QUALITY")
	private int positionQuality;
	
	@Column(name="SALINITY")
	private Double salinity;
	
	@Column(name="SALINITY_QUALITY")
	private Integer salinityQuality;
	
	@Column(name="SALINITY_ERROR")
	private Double salinityError;
	
	/*@Column(name="SALINITY_ADJUSTED")
	private Double salinityAdjusted;
	
	@Column(name="SALINITY_ADJUSTED_QUALITY")
	private Integer salinityAdjustedQuality;
	
	@Column(name="SALINITY_ADJUSTED_ERROR")
	private Double salinityAdjustedError;
	*/
	
	@Column(name="TEMPERATURE")
	private Double temperature;
		
	@Column(name="TEMPERATURE_QUALITY")
	private Integer temperatureQuality;
	
	@Column(name="TEMPERATURE_ERROR")
	private Double temperatureError;
	
	/*
	@Column(name="TEMPERATURE_ADJUSTED")
	private Double temperatureAdjusted;
		
	@Column(name="TEMPERATURE_ADJUSTED_QUALITY")
	private Integer temperatureAdjustedQuality;
			
	@Column(name="TEMPERATURE_ADJUSTED_ERROR")
	private Double temperatureAdjustedError;
	
	
	@Column(name="JACKET_TEMPERATURE")
	private Double jacketTemperature;
		
	@Column(name="JACKET_TEMPERATURE_QUALITY")
	private Integer jacketTemperatureQuality;
	
	@Column(name="JACKET_TEMPERATURE_ADJUSTED")
	private Double jacketTemperatureAdjusted;
		
	@Column(name="JACKET_TEMPERATURE_ADJUSTED_QUALITY")
	private Integer jacketTemperatureAdjustedQuality;
			
	@Column(name="JACKET_TEMPERATURE_ADJUSTED_ERROR")
	private Double jacketTemperatureAdjustedError;
	*/
	
	@Column(name="MEASURE_INDEX")
	private int measureIndex;
	
	@ManyToOne(fetch=FetchType.LAZY) // cascade=CascadeType.MERGE
	@JoinColumn(name="SSSFILE_FK")
	@Index(name="file_index")
	private SssFile sssFile;
	
	@Transient
	private OtherFields otherFields;

	public Measure() {
	}

	/*public Measure(long id, Date measureDate, double salinity,
			double temperature, int salinityQuality, double latitude,
			double longitude, SssFile file, OtherFields otherFields) {
		super();
		this.id = id;
		this.measureDate = measureDate;
		this.salinity = salinity;
		this.temperature = temperature;
		this.salinityQuality = salinityQuality;
		this.latitude = latitude;
		this.longitude = longitude;
		this.sssFile = file;
		this.otherFields = otherFields;
	}*/

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getMeasureDate() {
		return measureDate;
	}

	public void setMeasureDate(Date measureDate) {
		this.measureDate = measureDate;
	}

	public Double getSalinity() {
		return salinity;
	}

	public void setSalinity(Double salinity) {
		this.salinity = salinity;
	}

	public Double getTemperature() {
		return temperature;
	}

	public void setTemperature(Double temperature) {
		this.temperature = temperature;
	}

	public Integer getSalinityQuality() {
		return salinityQuality;
	}

	public void setSalinityQuality(Integer salinityQuality) {
		this.salinityQuality = salinityQuality;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public SssFile getSssFile() {
		return sssFile;
	}

	public void setSssFile(SssFile file) {
		this.sssFile = file;
	}

	public OtherFields getOtherFields() {
		return otherFields;
	}

	public void setOtherFields(OtherFields otherFields) {
		this.otherFields = otherFields;
	}
	
	public int getPositionQuality() {
		return positionQuality;
	}
	
	public void setPositionQuality(int positionQuality) {
		this.positionQuality = positionQuality;
	}
	
	public Integer getTemperatureQuality() {
		return temperatureQuality;
	}
	
	public void setTemperatureQuality(Integer temperatureQuality) {
		this.temperatureQuality = temperatureQuality;
	}
	public void setMeasureIndex(int measureIndex) {
		this.measureIndex = measureIndex;
	}
	public int getMeasureIndex() {
		return measureIndex;
	}

	public Double getSalinityError() {
		return salinityError;
	}

	public void setSalinityError(Double salinityError) {
		this.salinityError = salinityError;
	}

	public Double getTemperatureError() {
		return temperatureError;
	}

	public void setTemperatureError(Double temperatureError) {
		this.temperatureError = temperatureError;
	}

	
	
	/*
	public Double getSalinityAdjusted() {
		return salinityAdjusted;
	}

	public void setSalinityAdjusted(Double salinityAdjusted) {
		this.salinityAdjusted = salinityAdjusted;
	}

	public Integer getSalinityAdjustedQuality() {
		return salinityAdjustedQuality;
	}

	public void setSalinityAdjustedQuality(Integer salinityAdjustedQuality) {
		this.salinityAdjustedQuality = salinityAdjustedQuality;
	}

	public Double getSalinityAdjustedError() {
		return salinityAdjustedError;
	}

	public void setSalinityAdjustedError(Double salinityAdjustedError) {
		this.salinityAdjustedError = salinityAdjustedError;
	}

	public Double getTemperatureAdjusted() {
		return temperatureAdjusted;
	}

	public void setTemperatureAdjusted(Double temperatureAdjusted) {
		this.temperatureAdjusted = temperatureAdjusted;
	}

	public Integer getTemperatureAdjustedQuality() {
		return temperatureAdjustedQuality;
	}

	public void setTemperatureAdjustedQuality(Integer temperatureAdjustedQuality) {
		this.temperatureAdjustedQuality = temperatureAdjustedQuality;
	}

	public Double getTemperatureAdjustedError() {
		return temperatureAdjustedError;
	}

	public void setTemperatureAdjustedError(Double temperatureAdjustedError) {
		this.temperatureAdjustedError = temperatureAdjustedError;
	}

	public Double getJacketTemperature() {
		return jacketTemperature;
	}

	public void setJacketTemperature(Double jacketTemperature) {
		this.jacketTemperature = jacketTemperature;
	}

	public Integer getJacketTemperatureQuality() {
		return jacketTemperatureQuality;
	}

	public void setJacketTemperatureQuality(Integer jacketTemperatureQuality) {
		this.jacketTemperatureQuality = jacketTemperatureQuality;
	}

	public Double getJacketTemperatureAdjusted() {
		return jacketTemperatureAdjusted;
	}

	public void setJacketTemperatureAdjusted(Double jacketTemperatureAdjusted) {
		this.jacketTemperatureAdjusted = jacketTemperatureAdjusted;
	}

	public Integer getJacketTemperatureAdjustedQuality() {
		return jacketTemperatureAdjustedQuality;
	}

	public void setJacketTemperatureAdjustedQuality(
			Integer jacketTemperatureAdjustedQuality) {
		this.jacketTemperatureAdjustedQuality = jacketTemperatureAdjustedQuality;
	}

	public Double getJacketTemperatureAdjustedError() {
		return jacketTemperatureAdjustedError;
	}

	public void setJacketTemperatureAdjustedError(
			Double jacketTemperatureAdjustedError) {
		this.jacketTemperatureAdjustedError = jacketTemperatureAdjustedError;
	}
	*/
	
}
