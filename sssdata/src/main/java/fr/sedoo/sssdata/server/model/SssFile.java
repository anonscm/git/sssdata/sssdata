package fr.sedoo.sssdata.server.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name=SssFile.TABLE_NAME)
public class SssFile {
	
	public final static String TABLE_NAME ="SSSFILE";
	public static final String BOAT_ID_COLUMN_NAME = "BOAT_FK";
	public static final String ID_COLUMN_NAME = "ID";
	public static final String NAME_COLUMN_NAME = "NAME";
	public static final String DATE_START_COLUMN_NAME = "DATE_START";
	public static final String DATE_END_COLUMN_NAME = "DATE_END";
	
	@Id
	@Column(name=SssFile.ID_COLUMN_NAME)
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@Column(name=SssFile.NAME_COLUMN_NAME, unique = true)
	private String name;
	
	@Column(name="DATE_CREATION")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateCreation;
	
	@Column(name="DATE_UPDATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateUpdate;
	
	@Column(name="DATE_START")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateStart;
	
	@Column(name="DATE_END")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateEnd;
	
	@Column(name="SOUTH_LATITUDE")
	private Double southLatitude;
	
	@Column(name="EAST_LONGITUDE")
	private Double eastLongitude;
	
	@Column(name="NORTH_LATITUDE")
	private Double northLatitude;
	
	@Column(name="WEST_LONGITUDE")
	private Double westLongitude;
	
	
	@Column(name="SSPS_DEPH")
	private Double sspsDeph;
	
	@Column(name="SSPS_DEPH_MIN")
	private Double sspsDephMin;
	
	@Column(name="SSPS_DEPH_MAX")
	private Double sspsDephMax;
	
	@Column(name="MEASURES_NB")
	private Integer measuresNb;
	
	@Column(name="DATE_MODIFICATION")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateModification;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name=SssFile.BOAT_ID_COLUMN_NAME)
	private Boat boat;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="sssFile", fetch=FetchType.LAZY)
	private List<Measure> measures;

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "files")
	private List<Download> downloads;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "file")
	private Set<SssFileJournal> fileJournal;
	
	public SssFile() {
		super();
		downloads = new ArrayList<Download>();
		measures = new ArrayList<Measure>();
		fileJournal = new TreeSet<SssFileJournal>();
	}

    public Boat getBoat(){
		return boat;
	}
    
    public List<Download> getDownloads() {
		return downloads;
	}

	public void setDownloads(List<Download> download) {
		downloads = download;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public Date getDateUpdate() {
		return dateUpdate;
	}
	public void setDateUpdate(Date dateUpdate) {
		this.dateUpdate = dateUpdate;
	}
	
	public Date getDateModification() {
		return dateModification;
	}

	public void setDateModification(Date dateModification) {
		this.dateModification = dateModification;
	}

	public void setBoat(Boat boat) {
		this.boat = boat;
	}

	public List<Measure> getMeasures() {
		return measures;
	}

	public void setMeasures(List<Measure> measures) {
		this.measures = measures;
	}
	
	public void addMeasure(Measure m){
		this.measures.add(m);
		m.setSssFile(this);
	}

	public Date getDateStart() {
		return dateStart;
	}

	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}

	public Date getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
	}

	public Double getSouthLatitude() {
		return southLatitude;
	}

	public void setSouthLatitude(Double southLatitude) {
		this.southLatitude = southLatitude;
	}

	public Double getEastLongitude() {
		return eastLongitude;
	}

	public void setEastLongitude(Double eastLongitude) {
		this.eastLongitude = eastLongitude;
	}

	public Double getNorthLatitude() {
		return northLatitude;
	}

	public void setNorthLatitude(Double northLatitude) {
		this.northLatitude = northLatitude;
	}

	public Double getWestLongitude() {
		return westLongitude;
	}

	public void setWestLongitude(Double westLongitude) {
		this.westLongitude = westLongitude;
	}

	public void setMeasuresNb(Integer measuresNb) {
		this.measuresNb = measuresNb;
	}
	public Integer getMeasuresNb() {
		return measuresNb;
	}
	public Set<SssFileJournal> getFileJournal() {
		return fileJournal;
	}
	public void setFileJournal(Set<SssFileJournal> fileJournal) {
		this.fileJournal = fileJournal;
	}
	public Double getSspsDeph() {
		return sspsDeph;
	}

	public void setSspsDeph(Double sspsDeph) {
		this.sspsDeph = sspsDeph;
	}

	public Double getSspsDephMin() {
		return sspsDephMin;
	}

	public void setSspsDephMin(Double sspsDephMin) {
		this.sspsDephMin = sspsDephMin;
	}

	public Double getSspsDephMax() {
		return sspsDephMax;
	}

	public void setSspsDephMax(Double sspsDephMax) {
		this.sspsDephMax = sspsDephMax;
	}
}
