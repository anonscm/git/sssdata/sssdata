package fr.sedoo.sssdata.server.metadata;

import fr.sedoo.metadata.doi.DOIConfig;
import fr.sedoo.metadata.doi.DataciteDAO;

public interface SSSDataciteDAO extends DataciteDAO {

	String BEAN_NAME = "dataciteDAO";
		
	DOIConfig getConf();
}
