package fr.sedoo.sssdata.server.service;

import java.util.List;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.sedoo.sssdata.client.service.SearchTrajectoriesService;
import fr.sedoo.sssdata.client.service.ServiceException;
import fr.sedoo.sssdata.server.SSSDataApplication;
import fr.sedoo.sssdata.server.dao.api.TrajectoryDAO;
import fr.sedoo.sssdata.shared.BoatDTO;
import fr.sedoo.sssdata.shared.SearchCriterion;
import fr.sedoo.sssdata.shared.Trajectory;

public class SearchTrajectoriesServiceImpl extends RemoteServiceServlet implements SearchTrajectoriesService{

	private TrajectoryDAO dao;
	
	
	@Override
	public List<Trajectory> searchTrajectories(BoatDTO b, SearchCriterion sc)
			throws ServiceException {
		initDao();
		
		return dao.searchTrajectories(b,sc);
	}
	
	/*public HashMap<Boat, List<Trajectory>> searchTrajectories(
			SearchCriterion sc) throws ServiceException {
		initDao();

		return dao.searchTrajectories(sc);
	}*/
	
	private void initDao()	{
		if (dao == null)	{
			dao = (TrajectoryDAO) SSSDataApplication.getSpringBeanFactory().getBeanByName(TrajectoryDAO.BEAN_NAME);
		}
	}

}
