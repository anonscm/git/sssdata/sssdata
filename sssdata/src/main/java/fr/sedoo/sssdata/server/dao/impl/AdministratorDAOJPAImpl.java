package fr.sedoo.sssdata.server.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.sedoo.sssdata.server.dao.api.AdministratorDAO;
import fr.sedoo.sssdata.shared.Administrator;

@Repository
public class AdministratorDAOJPAImpl implements AdministratorDAO{

	protected EntityManager em;
	public EntityManager getEntityManager() {
		return em;
	}

	@PersistenceContext
	public void setEntityManager(EntityManager entityManager) {
		this.em = entityManager;
	}

	@Override
	@Transactional
	public ArrayList<Administrator> findAll() {
		List<Administrator> aux = em.createQuery("select administrator from Administrator administrator").getResultList();
        ArrayList<Administrator> result = new ArrayList<Administrator>();
        result.addAll(aux);
		return result;
	}
	
	@Transactional
	public void delete(Administrator administrator) {
		
		Administrator aux = getEntityManager().find(Administrator.class, administrator.getId());
		if (aux != null)
		{
			getEntityManager().remove(aux);
		}
	}
	
	@Transactional
	public Administrator save(Administrator administrator) 
	{
		if (administrator.getId() == null)
		{
			getEntityManager().persist(administrator);
			return administrator;
		}
		else
		{
			getEntityManager().merge(administrator);
			return administrator;
		}
	}

	@Override
	public Administrator login(String login, String password) {
		try{
			Query query = em.createQuery("SELECT administrator FROM Administrator administrator WHERE administrator.login = :arg1 AND administrator.password = :arg2");
			query.setParameter("arg1",login);
			query.setParameter("arg2",password);
			return (Administrator) query.getSingleResult();
		}catch(NoResultException e){
			return null;
		}
	}


}