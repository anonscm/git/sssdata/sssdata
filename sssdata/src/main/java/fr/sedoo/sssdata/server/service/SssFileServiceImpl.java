package fr.sedoo.sssdata.server.service;

import fr.sedoo.sssdata.server.SSSDataApplication;
import fr.sedoo.sssdata.server.dao.api.SssFileDAO;
import fr.sedoo.sssdata.server.model.SssFile;

public class SssFileServiceImpl {

	private SssFileDAO dao;
	
	private void initDao()
	{
		if (dao == null)
		{
			dao = (SssFileDAO) SSSDataApplication.getSpringBeanFactory().getBeanByName(SssFileDAO.BEAN_NAME);
		}
	}
	
	public void deleteFileFromName(String fileName){
		initDao();
		dao.deleteSssFileFromName(fileName);
	}
	
	public SssFile getFileByNameAndBoat(String fileName, String boatName) {
		initDao();
		return dao.getFileByNameAndBoat(fileName, boatName, true);
	}

	public SssFile save(SssFile sssFile) 
	{
		initDao();
		return dao.save(sssFile, true);
		
	}
	
	public void markAsDeleted(SssFile sssFile) 
	{
		initDao();
		dao.markAsDeleted(sssFile);
		
	}
}
