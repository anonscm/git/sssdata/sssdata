package fr.sedoo.sssdata.server.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.sedoo.sssdata.server.ArchiveGenerator;
import fr.sedoo.sssdata.server.dao.api.ArchiveDAO;
import fr.sedoo.sssdata.server.model.Archive;

@Repository
public class ArchiveDAOJPAImpl implements ArchiveDAO {

	private static Logger logger = Logger.getLogger(ArchiveDAOJPAImpl.class);
	
	protected EntityManager em;
	public EntityManager getEntityManager() {
		return em;
	}

	@PersistenceContext
	public void setEntityManager(EntityManager entityManager) {
		this.em = entityManager;
	}
	
	@Override
	@Transactional
	public Archive save(Archive a) {
		if (a.getId() == null){
			getEntityManager().persist(a);
			return a;
		}else{
			getEntityManager().merge(a);
			return a;
		}
	}

	@Override
	@Transactional
	public Archive findByName(String name) {
		Query query = em.createQuery("Select a from Archive a where a.name=:arg1");
		query.setParameter("arg1", name);
		
		try{
			Archive result = (Archive) query.getSingleResult();
			return result;
		}
		catch (NoResultException e){
			return null;
		}
	}

	@Override
	public Archive findLastByFlagAndFormat(String flag, String format) {
		Query query = em.createQuery("SELECT a FROM Archive a WHERE a.flag=:arg1 AND a.format=:arg2 ORDER BY a.dateCreation DESC");
		query.setParameter("arg1", flag);
		query.setParameter("arg2", format);
		query.setMaxResults(1);
		
		try{
			Archive result = (Archive) query.getSingleResult();
			return result;
		}
		catch (Exception e){
			logger.error("Error in findLastByFlagAndFormat(). " + e);
			return null;
		}
	}

}
