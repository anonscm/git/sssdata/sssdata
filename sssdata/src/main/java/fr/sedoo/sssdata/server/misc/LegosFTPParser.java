package fr.sedoo.sssdata.server.misc;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.commons.lang.time.StopWatch;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ucar.ma2.ArrayChar;
import ucar.ma2.ArrayChar.StringIterator;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Variable;
import fr.sedoo.sssdata.server.SSSDataApplication;
import fr.sedoo.sssdata.server.dao.api.BoatDAO;
import fr.sedoo.sssdata.server.dao.api.SssFileDAO;
import fr.sedoo.sssdata.server.dao.gosud.GosudConstants;
import fr.sedoo.sssdata.server.dao.gosud.GosudUtils;
import fr.sedoo.sssdata.server.model.Boat;
import fr.sedoo.sssdata.server.model.Measure;
import fr.sedoo.sssdata.server.model.SssFile;
import fr.sedoo.sssdata.server.model.SssFileJournal;
import fr.sedoo.sssdata.server.service.MeasureServiceImpl;
import fr.sedoo.sssdata.shared.dataimport.FilePostImportInfo;
import fr.sedoo.sssdata.shared.dataimport.FilePreImportInfo;

public class LegosFTPParser {

	static Logger LOGGER = LoggerFactory.getLogger(LegosFTPParser.class);

	private static final String FTP_CONFIG_BEAN_NAME = "ftpConfig";

	private static MeasureServiceImpl measureService = new MeasureServiceImpl();

	private static BoatDAO boatDao;
	private static SssFileDAO sssFileDAO;
	
	static{
		boatDao = (BoatDAO) SSSDataApplication.getSpringBeanFactory().getBeanByName(BoatDAO.BEAN_NAME);
		sssFileDAO = (SssFileDAO) SSSDataApplication.getSpringBeanFactory().getBeanByName(SssFileDAO.BEAN_NAME);
		ftpConfig = (FTPConfig) SSSDataApplication.getSpringBeanFactory().getBeanByName(FTP_CONFIG_BEAN_NAME);
	}
	
	
	private static FTPConfig ftpConfig;
	
	public static void launch() {

	}

	private static void listFiles(FTPClient client, String chemin, Collection<String> files) throws Exception{
		toRootDir(client);
		LOGGER.debug("Lecture du répertoire " + chemin);
		int retour = client.cwd(chemin);
		LOGGER.debug("Retour: " + retour);
		FTPFile[] listFiles = client.listFiles(ftpConfig.getFilter());
		for (FTPFile f: listFiles){
			if (f.isDirectory()){
				listFiles(client, chemin + "/" + f.getName(), files);
			}else{
				LOGGER.debug("Ajout du fichier " + chemin + "/" + f.getName());
				files.add(chemin + "/" + f.getName());
			}
		}
		
		FTPFile[] listDirectories = client.listDirectories();
		for (FTPFile f: listDirectories){
			listFiles(client, chemin + "/" + f.getName(), files);
		}
		
	}
	
	private static void listDirs(FTPClient client, String chemin, Collection<String> dirs) throws Exception{
		toRootDir(client);
		LOGGER.debug("Lecture du répertoire " + chemin);
		int retour = client.cwd(chemin);
		LOGGER.debug("Retour: " + retour);
		FTPFile[] listDirectories = client.listDirectories();
		for (FTPFile f: listDirectories){
			dirs.add(chemin + "/" + f.getName());
		}
	}
	
	public static Collection<String> getDirs() throws Exception{
		String[] dirs = ftpConfig.getDirs().split(",");
		/*List<String> result = new ArrayList<String>();
		for (String dir: dirs){
			result.add(ftpConfig.getRootDir() + "/" + dir);
		}
		
		//return result;*/
		return getDirsList(Arrays.asList(dirs));
	}
	
	public static Set<String> getFilesList(Collection<String> chemins) throws Exception{
		FTPClient client = initFTPClient();
		Set<String> fichiers = new HashSet<String>();
		for (String racine: chemins){
			listFiles(client, racine.trim(), fichiers);
		}
		return fichiers;
	}
	
	public static Collection<String> getDirsList(Collection<String> chemins) throws Exception{
		FTPClient client = initFTPClient();
		List<String> dirs = new ArrayList<String>();
		for (String chemin: chemins){
			dirs.add(chemin);
			listDirs(client, chemin.trim(), dirs);
		}
		Collections.sort(dirs);
		return dirs;
	}
	
	public static Collection<String> getFilesList() throws Exception{
		return getFilesList(getDirs());
		/*FTPClient client = initFTPClient();
		
		Collection<String> fichiers = new ArrayList<String>();
		String[] racines = ftpConfig.getDirs().split(",");
		for (String racine: racines){
			listFiles(client, racine.trim(), fichiers);
		}
		return fichiers;*/
	}
	
	public static FilePreImportInfo getFilePreImportInfo(String file) throws Exception {
		LOGGER.debug("début FilePreImportInfo");
		FTPClient client = initFTPClient();
		toRootDir(client);
		File f = new File (file);
		LOGGER.debug("Tentative positionnement "+ f.getParent());		
		client.cwd(f.getParent());
		LOGGER.debug("Demande du fichier " + f.getName());
		FTPFile[] listFiles = null;
		try
		{
			listFiles = client.listFiles(f.getName());
		}
		catch (IOException e)
		{
			LOGGER.warn("IOException "+e.getMessage());
		}
		catch (Throwable e)
		{
			LOGGER.warn("Throwable "+e.getMessage());
		}
		FilePreImportInfo filePreImportInfo = new FilePreImportInfo();
		LOGGER.debug("Recupération du SssFile " + f.getName());
		//SssFile file = sssFileService.getFileByNameAndBoat(fileName, boatName);
		SssFile sssFile = sssFileDAO.getSimpleFileByName(f.getName(), true);
		if (sssFile == null){
			filePreImportInfo.setAlreadyPresent(false);
		}else{
			filePreImportInfo.setAlreadyPresent(true);
			if (sssFile.getDateModification() == null || listFiles[0].getTimestamp().getTime().after(sssFile.getDateModification()))
			{
				filePreImportInfo.setNeedUpdate(true);
			}
			else
			{
				filePreImportInfo.setNeedUpdate(false);
			}
		}
		disconnect(client);
		LOGGER.debug("fin FilePreImportInfo");
		return filePreImportInfo;
	}
	
	//private static boolean adjustedSspsMissing;
	private static boolean adjustedSspsPresent;
	
	public static FilePostImportInfo importFile(String file, boolean deleteFirst) throws Exception {
		FilePostImportInfo result = new FilePostImportInfo();
		
		File f = new File (file);
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		int lines = 0;
		//adjustedSspsMissing = false;
		adjustedSspsPresent = false;
		SssFile sssFile = null;
		if (deleteFirst == false){
			//New file
			sssFile = new SssFile();
			sssFile.setName(f.getName());
			//Boat boat = boatDao.findByName(boatName);

			//We put a false modificationDate to force the deletion of the mesurements next time in case of failure
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
			String fakeDate = "01/01/1000";
			sssFile.setDateModification(simpleDateFormat.parse(fakeDate));
			//sssFile.setBoat(boat);
			sssFile = sssFileDAO.save(sssFile, true);
		}else{
			sssFile = sssFileDAO.getSimpleFileByName(f.getName(), true);
			//Deletion of existing measurements
			if (sssFile.getDateModification() != null){
				measureService.deleteByFile(sssFile);
				sssFileDAO.markAsDeleted(sssFile);
			}
		}

		FTPClient client = initFTPClient();
		toRootDir(client);
		client.cwd(f.getParent());
		FTPFile[] listFiles = client.listFiles(f.getName());
		File tmpFile = File.createTempFile("ssstmp", ".nc");
		FileOutputStream output = new FileOutputStream(tmpFile);

		client.retrieveFile(f.getName(), output);
		output.close();
		disconnect(client);
						
		NetcdfFile ncfile = NetcdfFile.open(tmpFile.toURI().toString());
	
		//We add informations such as max long/lat ...
		sssFile = addTrajectoryInformation(ncfile, sssFile, result);
		lines = importNcFile(ncfile,sssFile);
		ncfile.close();
		tmpFile.delete();
		//We put the real timestamp after the import success
		sssFile.setDateModification(listFiles[0].getTimestamp().getTime());
		
		SssFileJournal j = new SssFileJournal();
		j.setDate(new Date());
		j.setType(SssFileJournal.TYPE_INSERTION);
		j.setFile(sssFile);
		sssFile.getFileJournal().add(j);
		
		sssFile = sssFileDAO.save(sssFile, true);
		
		
		stopWatch.stop();
		
		if (!adjustedSspsPresent){
			result.getMessages().add("WARNING: Pas de correction appliquée sur la SSS");
		}
		
		result.setSuccess(true);
		result.setDuration(stopWatch.toString());
		result.setLines(lines);
		return result;

	}
	
	private static void toRootDir(FTPClient client) throws Exception{
		LOGGER.debug("CWD " + ftpConfig.getRootDir());
		int retour = client.cwd(ftpConfig.getRootDir());
		LOGGER.debug("Retour: " + retour);
	}
	
	/**
	 * Ouvre une connexion FTP et se positionne dans le répertoire rootDir.
	 * @return
	 * @throws Exception
	 */
	private static FTPClient initFTPClient() throws Exception {
		/*if (ftpConfig == null){
			ftpConfig = (FTPConfig) DefaultServerApplication.getSpringBeanFactory().getBeanByName(FTP_CONFIG_BEAN_NAME);
		}*/

		FTPClient client = new FTPClient();
		client.setBufferSize(1024 * 1024);
		client.enterLocalPassiveMode();
		client.setAutodetectUTF8(true);
		client.enterLocalPassiveMode();
		
		
		LOGGER.debug("Tentative connexion à "+ftpConfig.getAdress());
		client.connect(ftpConfig.getAdress());
		client.enterLocalPassiveMode();
		LOGGER.debug("Tentative login avec l'utilisateur "+ftpConfig.getLogin()+"/"+ftpConfig.getPassword());

		if ( client.login(ftpConfig.getLogin(), ftpConfig.getPassword()) ){
			LOGGER.debug("Connexion réussie");	
			//LOGGER.debug("Tentative positionnement repertoire "+ftpConfig.getRoot());
			//int retour = client.cwd(ftpConfig.getRoot());
			//LOGGER.debug("Retour: " + retour);
			client.setFileType(FTP.BINARY_FILE_TYPE);
			client.setBufferSize(1024 * 1024);
						
			return client;
		}else{
			throw new Exception("Echec du login");
		}
		
	}


	private static void disconnect(FTPClient client) throws Exception 
	{
		LOGGER.debug("Tentative deconnexion");
		client.disconnect();
		LOGGER.debug("Deconnexion réusssie");
	}

	private static Double getDepth(NetcdfFile ncfile, String varName, FilePostImportInfo result) throws IOException{
		Variable var = ncfile.findVariable(varName);
		Double depth = null;
		if (var != null){
			NetcdfVariableReader varReader = new NetcdfVariableReader(var);
			depth = varReader.getNextDouble();
			if (depth == null ||depth == 0 || depth == 99999){
				depth = null;
			}
		}
		if (depth == null){
			result.getMessages().add("WARNING: " + varName + " is missing");
		}
		return depth;
	}
	
	private static SssFile addTrajectoryInformation(NetcdfFile ncfile, SssFile sssFile, FilePostImportInfo result) throws Exception {
		HashMap<String, String> params = GosudUtils.getParams(ncfile);
		sssFile.setDateStart(GosudUtils.getDateFromString(params.get(GosudConstants.DATE_START)));
		sssFile.setDateEnd(GosudUtils.getDateFromString(params.get(GosudConstants.DATE_END)));
		sssFile.setEastLongitude(new Double(params.get(GosudConstants.EAST_LONX)));
		sssFile.setWestLongitude(new Double(params.get(GosudConstants.WEST_LONX)));
		sssFile.setSouthLatitude(new Double(params.get(GosudConstants.SOUTH_LATX)));
		sssFile.setNorthLatitude(new Double(params.get(GosudConstants.NORTH_LATX)));
				
		//Profondeur
		sssFile.setSspsDeph(getDepth(ncfile, GosudConstants.SSPS_DEPH, result));
		sssFile.setSspsDephMin(getDepth(ncfile, GosudConstants.SSPS_DEPH_MIN, result));
		sssFile.setSspsDephMax(getDepth(ncfile, GosudConstants.SSPS_DEPH_MAX, result));
				
		//Ship
		
		if (sssFile.getBoat() == null){
			String shipName = params.get(GosudConstants.PLATFORM_NAME);
			String callSign = params.get(GosudConstants.SHIP_CALL_SIGN);	
			if (shipName != null){
				//Boat b = boatDao.findByName(shipName);
				if (StringUtils.trimToNull(callSign) == null){
					throw new Exception("Call sign is missing");
				}
				
				callSign = callSign.replaceAll("[^A-Za-z0-9]", "");
				shipName = shipName.replaceAll("[^A-Za-z0-9]", " ").trim().toLowerCase();
				shipName = WordUtils.capitalize(shipName);
				Boat b = boatDao.findByCode(callSign);
				if (b==null){
					//this boat doesn't exist we persist it
					b = new Boat();
					b.setName(shipName);
					b.setCode(callSign);
					boatDao.save(b);
				}
				sssFile.setBoat(b);
			}else{
				throw new Exception("Platform name is missing");
			}
		}
		
		
		try{
			if (params.get(GosudConstants.DATE_CREATION).length() == 8){
				sssFile.setDateCreation(GosudUtils.getDateFromString(params.get(GosudConstants.DATE_CREATION) + "000000"));
				LOGGER.warn("Incorrect date: " + params.get(GosudConstants.DATE_CREATION) + ". Changed to: " + params.get(GosudConstants.DATE_CREATION) + "000000");
			}else{
				sssFile.setDateCreation(GosudUtils.getDateFromString(params.get(GosudConstants.DATE_CREATION)));
			}
		}catch(ParseException e){
			LOGGER.error("Unable to parse date: " + params.get(GosudConstants.DATE_CREATION));
		}
		try{
			if (params.get(GosudConstants.DATE_UPDATE).length() == 8){
				LOGGER.warn("Incorrect date: " + params.get(GosudConstants.DATE_UPDATE) + ". Changed to: " + params.get(GosudConstants.DATE_UPDATE) + "000000");
				sssFile.setDateCreation(GosudUtils.getDateFromString(params.get(GosudConstants.DATE_UPDATE) + "000000"));
			}else{
				sssFile.setDateUpdate(GosudUtils.getDateFromString(params.get(GosudConstants.DATE_UPDATE)));
			}
		}catch(ParseException e){
			LOGGER.error("Unable to parse date: " + params.get(GosudConstants.DATE_UPDATE));
		}
		return sssFile;
	}



	private static int importNcFile(NetcdfFile ncfile, SssFile file) throws Exception {
		//int lines=0;
		/*Double minTemperature = null;
		Double maxTemperature = null;
		Double minSalinity = null;
		Double maxSalinity = null;
		SortedSet<String> qualityFlags = new TreeSet<String>();*/
		
		
		
		//Variables
		Variable dateVariable = ncfile.findVariable(GosudConstants.DATE);
		Variable longitudeVariable = ncfile.findVariable(GosudConstants.LONX);
		Variable latitudeVariable = ncfile.findVariable(GosudConstants.LATX);
				
		Variable salinityVariable = ncfile.findVariable(GosudConstants.SSPS);
		Variable sspsQualityVariable = ncfile.findVariable(GosudConstants.SSPS_QC);
		Variable adjSspsVariable = ncfile.findVariable(GosudConstants.SSPS_ADJUSTED);
		Variable adjSspsQualityVariable = ncfile.findVariable(GosudConstants.SSPS_ADJUSTED_QC);
		Variable adjSspsErrorVariable = ncfile.findVariable(GosudConstants.SSPS_ADJUSTED_ERROR);
		
		Variable temperatureVariable = ncfile.findVariable(GosudConstants.SSTP);
		Variable sstpQualityVariable = ncfile.findVariable(GosudConstants.SSTP_QC);
		Variable adjSstpVariable = ncfile.findVariable(GosudConstants.SSTP_ADJUSTED);
		Variable adjSstpQualityVariable = ncfile.findVariable(GosudConstants.SSTP_ADJUSTED_QC);
		Variable adjSstpErrorVariable = ncfile.findVariable(GosudConstants.SSTP_ADJUSTED_ERROR);
		
		Variable ssjtVariable = ncfile.findVariable(GosudConstants.SSJT);
		Variable ssjtQualityVariable = ncfile.findVariable(GosudConstants.SSJT_QC);
		Variable adjSsjtVariable = ncfile.findVariable(GosudConstants.SSJT_ADJUSTED);
		Variable adjSsjtQualityVariable = ncfile.findVariable(GosudConstants.SSJT_ADJUSTED_QC);
		Variable adjSsjtErrorVariable = ncfile.findVariable(GosudConstants.SSJT_ADJUSTED_ERROR);
		
		NetcdfVariableReader latx = new NetcdfVariableReader(latitudeVariable);
		NetcdfVariableReader lonx = new NetcdfVariableReader(longitudeVariable);
		
		NetcdfVariableReader ssps = new NetcdfVariableReader(salinityVariable);
		NetcdfVariableReader sspsQc = new NetcdfVariableReader(sspsQualityVariable);
		NetcdfVariableReader sspsAdjusted = new NetcdfVariableReader(adjSspsVariable);
		NetcdfVariableReader sspsAdjustedQc = new NetcdfVariableReader(adjSspsQualityVariable);
		NetcdfVariableReader sspsAdjustedError = new NetcdfVariableReader(adjSspsErrorVariable);
		
		NetcdfVariableReader sstp = new NetcdfVariableReader(temperatureVariable);
		NetcdfVariableReader sstpQc = new NetcdfVariableReader(sstpQualityVariable);
		NetcdfVariableReader sstpAdjusted = new NetcdfVariableReader(adjSstpVariable);
		NetcdfVariableReader sstpAdjustedQc = new NetcdfVariableReader(adjSstpQualityVariable);
		NetcdfVariableReader sstpAdjustedError = new NetcdfVariableReader(adjSstpErrorVariable);
		
		NetcdfVariableReader ssjt = new NetcdfVariableReader(ssjtVariable);
		NetcdfVariableReader ssjtQc = new NetcdfVariableReader(ssjtQualityVariable);
		NetcdfVariableReader ssjtAdjusted = new NetcdfVariableReader(adjSsjtVariable);
		NetcdfVariableReader ssjtAdjustedQc = new NetcdfVariableReader(adjSsjtQualityVariable);
		NetcdfVariableReader ssjtAdjustedError = new NetcdfVariableReader(adjSsjtErrorVariable);
		
		
		
		//Données
		ArrayChar.D2 dates = (ArrayChar.D2) dateVariable.read();
		/*Array longitudes = longitudeVariable.read();
		Array latitudes = latitudeVariable.read();
		
		Array salinities = salinityVariable.read();
		Array sspsQualities = sspsQualityVariable.read();
		Array adjSalinities = adjSspsVariable.read();
		Array adjSpsQualities = adjSspsQualityVariable.read();
		Array adjSspsErrors = adjSspsErrorVariable.read();
		
		Array temperatures = temperatureVariable.read();
		Array sstpQualities = sstpQualityVariable.read();
		Array adjTemperatures = adjSstpVariable.read();
		Array adjSstpQualities = adjSstpQualityVariable.read();
		Array adjSstpErrors = adjSstpErrorVariable.read();
		
		Array jacketTemperatures = ssjtVariable.read();
		Array ssjtQualities = ssjtQualityVariable.read();
		Array adjJacketTemperatures = adjSsjtVariable.read();
		Array adjSsjtQualities = adjSsjtQualityVariable.read();
		Array adjSsjtErrors = adjSsjtErrorVariable.read();
						*/
		//Iterators sur les données
		StringIterator datesIterator = dates.getStringIterator();
		/*IndexIterator latitudesIterator = latitudes.getIndexIterator();
		IndexIterator longitudesIterator = longitudes.getIndexIterator();
		
		IndexIterator salinitiesIterator = salinities.getIndexIterator();
		IndexIterator sspsQualitiesIterator = sspsQualities.getIndexIterator();
		IndexIterator adjSalinitiesIterator = adjSalinities.getIndexIterator();
		IndexIterator adjSspsQualitiesIterator = adjSpsQualities.getIndexIterator();
		IndexIterator adlSspsErrorsIterator = adjSspsErrors.getIndexIterator();
		
		IndexIterator temperaturesIterator = temperatures.getIndexIterator();
		IndexIterator sstpQualitiesIterator = sstpQualities.getIndexIterator();
		IndexIterator adjTemperaturesIterator = adjTemperatures.getIndexIterator();
		IndexIterator adjSstpQualitiesIterator = adjSstpQualities.getIndexIterator();
		IndexIterator adjSstpErrorsIterator = adjSstpErrors.getIndexIterator();
		
		IndexIterator jacketTemperaturesIterator = jacketTemperatures.getIndexIterator();
		IndexIterator ssjtQualitiesIterator = ssjtQualities.getIndexIterator();
		IndexIterator adjJacketTemperaturesIterator = adjJacketTemperatures.getIndexIterator();
		IndexIterator adjSsjtQualitiesIterator = adjSsjtQualities.getIndexIterator();
		IndexIterator adjSsjtErrorsIterator = adjSsjtErrors.getIndexIterator();
		*/
		//ArrayList<Measure> measuresToPersist = new ArrayList<Measure>();
		Measure m = null;

		measureService.initBatch();
		
		int measureCpt = 0;
		// Measures for this sssFile
		while (datesIterator.hasNext()) {

			String dateStr = datesIterator.next();
			/*	float lat = latitudesIterator.getFloatNext();
			float lon = longitudesIterator.getFloatNext();
				
			double sal = salinitiesIterator.getDoubleNext();
			int salQuality = sspsQualitiesIterator.getIntNext();
			double adjSal = adjSalinitiesIterator.getDoubleNext();
			int adjSalQuality = adjSspsQualitiesIterator.getIntNext();
			double adjSalError = adlSspsErrorsIterator.getDoubleNext();
						
			double temp = temperaturesIterator.getDoubleNext();
			int tempQuality = sstpQualitiesIterator.getIntNext();
			double adjTemp = adjTemperaturesIterator.getDoubleNext();
			int adjTempQuality = adjSstpQualitiesIterator.getIntNext();
			double adjTempError = adjSstpErrorsIterator.getDoubleNext();
			
			double jackTemp = jacketTemperaturesIterator.getDoubleNext();
			int jackTempQuality = ssjtQualitiesIterator.getIntNext();
			double adjJackTemp = adjJacketTemperaturesIterator.getDoubleNext();
			int adjJackTempQuality = adjSsjtQualitiesIterator.getIntNext();
			double adjJackTempError = adjSsjtErrorsIterator.getDoubleNext();
					*/
			Double lat = latx.getNextDouble();
			Double lon = lonx.getNextDouble();
			
			Double sal = ssps.getNextDouble();
			int salQuality = sspsQc.getNextInt();
			Double adjSal = sspsAdjusted.getNextDouble();
			int adjSalQuality = sspsAdjustedQc.getNextInt();
			Double adjSalError = sspsAdjustedError.getNextDouble();
						
			Double temp = sstp.getNextDouble();
			int tempQuality = sstpQc.getNextInt();
			Double adjTemp = sstpAdjusted.getNextDouble();
			int adjTempQuality = sstpAdjustedQc.getNextInt();
			Double adjTempError = sstpAdjustedError.getNextDouble();
			
			Double jackTemp = ssjt.getNextDouble();
			int jackTempQuality = ssjtQc.getNextInt();
			Double adjJackTemp = ssjtAdjusted.getNextDouble();
			int adjJackTempQuality = ssjtAdjustedQc.getNextInt();
			Double adjJackTempError = ssjtAdjustedError.getNextDouble();
			
			Mesure mesure = new Mesure(sal, salQuality, adjSal, adjSalQuality, adjSalError, 
					temp, tempQuality, adjTemp, adjTempQuality, adjTempError, 
					jackTemp, jackTempQuality, adjJackTemp, adjJackTempQuality, adjJackTempError);
			
			Valeur sss = mesure.getSSS();
			Valeur sst = mesure.getSST();
						
			if (sss.isNull() && sst.isNull()){
				continue;
			}
			
			/*if (mesure.getSalinityAdjusted() == null){
				adjustedSspsMissing = true;
			}*/
			
			if (mesure.getSalinityAdjusted() != null){
				adjustedSspsPresent = true;
			}
			
			
			//System.out.println(measureCpt);
			//Le flag sur la position n'est pas récupéré dans les fichiers
			//Il ne servira qu'à exclure certains points douteux lors du tracé des trajectoires
			int positionQuality = GosudConstants.UNCONTROLLED_DATA;
			/*if (lat == -0.0
					|| lon == -0.0){
				positionQuality = GosudConstants.BAD_DATA;				
			}*/
			
			
			
			/*SimpleDateFormat sdf = new SimpleDateFormat(GosudConstants.DATE_FORMAT);
			sdf.setLenient(true);
			Date d = new Date(sdf.parse(dateStr).getTime());*/
			Date d = GosudUtils.getDateFromString(dateStr);
			
			m = new Measure();
			m.setLatitude(lat);
			m.setLongitude(lon);
			m.setPositionQuality(positionQuality);
			
			m.setSalinity(sss.getValeur());
			m.setSalinityQuality(sss.getQuality());
			m.setSalinityError(sss.getError());
			
			m.setTemperature(sst.getValeur());
			m.setTemperatureQuality(sst.getQuality());
			m.setTemperatureError(sst.getError());
						
			m.setMeasureDate(d);
			m.setSssFile(file);
			
			m.setMeasureIndex(measureCpt++);
						
			measureService.addToBatch(m);
			
			/*
			measuresToPersist.add(m);
			if (measuresToPersist.size()>=1000){

				// PERSIST measuresToPersist 
				measureService.bulkAdd(measuresToPersist);
				lines = lines + measuresToPersist.size();

				// Clean collection
				measuresToPersist = new ArrayList<Measure>();
			}*/

		} // Measure loop for this file

		/*
		// If result has some file persist them
		if (measuresToPersist.size()!= 0){

			// PERSIST measuresToPersist 
			measureService.bulkAdd(measuresToPersist);
			lines = lines + measuresToPersist.size();

			// No need to clean collection it will be done next loop
		}
*/
		/*file.setMaxSalinity(maxSalinity);
		file.setMaxTemperature(maxTemperature);
		file.setMinSalinity(minSalinity);
		file.setMinTemperature(minTemperature);

		file.setQualityFlags(qualityFlagsToString(qualityFlags));*/
		
		measureService.closeBatch();
		
		file.setMeasuresNb(measureCpt);
		return measureCpt;
	}
/*
	private static String qualityFlagsToString(SortedSet<String> qualityFlags) 
	{
		StringBuilder result= new StringBuilder("");
		if (qualityFlags.isEmpty()==false)
		{
			result.append(SeparatorUtil.UNDERSCORE_SEPARATOR);
			Iterator<String> iterator = qualityFlags.iterator();
			while (iterator.hasNext()) {
				result.append(iterator.next());
				result.append(SeparatorUtil.UNDERSCORE_SEPARATOR);
			}
		}
		return result.toString();
	}*/

}
