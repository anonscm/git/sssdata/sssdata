package fr.sedoo.sssdata.server.metadata;

import java.sql.Date;

import fr.sedoo.sssdata.server.model.MetadataHistory;
import fr.sedoo.sssdata.shared.metadata.HistoryItem;

public final class HistoryItemBuilder {

	private HistoryItemBuilder() {}
	
	public static HistoryItem buildItem(MetadataHistory item){
		HistoryItem hi = new HistoryItem();
		if (item.getId() == null){
			hi.setId(0);
		}else{
			hi.setId(item.getId());
		}
		hi.setComment(item.getComment());
		hi.setDate(item.getDate().toString());
		hi.setVersion(item.getVersion());
		return hi;
	}
	
	public static MetadataHistory buildEntity(HistoryItem item){
		MetadataHistory mh = new MetadataHistory();
		if (item.getId() == 0){
			mh.setId(null);
		}else{
			mh.setId(item.getId());
		}
		mh.setComment(item.getComment());
		mh.setDate(Date.valueOf(item.getDate()));
		mh.setVersion(item.getVersion());
		return mh;
	}
	
}
