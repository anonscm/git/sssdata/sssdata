package fr.sedoo.sssdata.server.dao.api;

import java.util.ArrayList;

import fr.sedoo.sssdata.server.model.MetadataHistory;

public interface MetadataHistoryDAO {
	public final static String BEAN_NAME="metadataHistoryDAO";
	ArrayList<MetadataHistory> findAll();
	void delete(Integer id);
	MetadataHistory save(MetadataHistory item);

}
