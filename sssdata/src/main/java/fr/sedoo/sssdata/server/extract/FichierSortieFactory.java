package fr.sedoo.sssdata.server.extract;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import fr.sedoo.commons.server.DefaultServerApplication;
import fr.sedoo.commons.server.config.dao.api.ConfigPropertyDAO;
import fr.sedoo.commons.server.config.model.ConfigProperty;
import fr.sedoo.sssdata.server.metadata.SSSDataciteDAO;
import fr.sedoo.sssdata.server.model.SssFile;
import fr.sedoo.sssdata.shared.ConfigPropertiesCode;
import fr.sedoo.sssdata.shared.FileDTO;
import fr.sedoo.sssdata.shared.SearchCriterion;
import fr.sedoo.sssdata.shared.metadata.DoiConstants;

public final class FichierSortieFactory {

	private static Logger logger = Logger.getLogger(FichierSortieFactory.class);	
			
	private FichierSortieHeader header;
	
	private static FichierSortieFactory instance = new FichierSortieFactory();
	private FichierSortieFactory() {
		ConfigPropertyDAO propDao = (ConfigPropertyDAO)DefaultServerApplication.getSpringBeanFactory().getBeanByName(ConfigPropertyDAO.BEAN_NAME);
		SSSDataciteDAO dataciteDAO  = (SSSDataciteDAO)DefaultServerApplication.getSpringBeanFactory().getBeanByName(SSSDataciteDAO.BEAN_NAME);
		
		this.header = new FichierSortieHeader();
		ConfigProperty useConstraints = propDao.find(ConfigPropertiesCode.OUTPUT_DATS_USE_CONSTRAINTS);
		header.setUseConstraints(useConstraints.getValueOrDefault());
		
		ConfigProperty ack = propDao.find(ConfigPropertiesCode.OUTPUT_DATS_ACKNOWLEDGEMENT);
		header.setAcknowledgment(ack.getValueOrDefault());
		
		ConfigProperty title = propDao.find(ConfigPropertiesCode.OUTPUT_DATS_TITLE);
		header.setTitle(title.getValueOrDefault());
		
		ConfigProperty summary = propDao.find(ConfigPropertiesCode.OUTPUT_DATS_ABSTRACT);
		header.setSummary(summary.getValueOrDefault());
		
		ConfigProperty proj = propDao.find(ConfigPropertiesCode.OUTPUT_PROJECT_NAME);
		header.setProjectName(proj.getValueOrDefault());
		
		ConfigProperty contact = propDao.find(ConfigPropertiesCode.OUTPUT_PROJECT_CONTACT);
		header.setContact(contact.getValueOrDefault());
		
		ConfigProperty url = propDao.find(ConfigPropertiesCode.OUTPUT_PROJECT_URL);
		header.setProjectUrl(url.getValueOrDefault());
				
		//TODO
		String doi = dataciteDAO.getConf().getPrefix() + DoiConstants.SSS_DATA;
		header.setDoi(doi);
		header.setCitation(dataciteDAO.getCitation(doi));
		
	}
	
	public static FichierSortieFactory getInstance() {
		return instance;
	}
			
	public FichierSortie getFichierSortie(String requestId, SssFile trajectory, SearchCriterion sc, String outputFormat) throws IOException{
		
		//Nom du fichier
		//sssdata_<reqId>_<Code bateau>_<dMin>_<dMax>.<format>
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		sdf.setLenient(false);
		
		StringBuffer sb = new StringBuffer(FileDTO.FILENAME_PREFIX);
		sb.append(requestId);
		sb.append("_");
		sb.append(trajectory.getBoat().getCode());
		sb.append("_");
		
		Date dMinSc = sc.getBeginDate();
		if (dMinSc != null && dMinSc.after(trajectory.getDateStart())){
			sb.append(sdf.format(dMinSc));
		}else{
			sb.append(sdf.format(trajectory.getDateStart()));
		}
		sb.append("_");
		
		Date dMaxSc = sc.getEndDate();
		if (dMaxSc != null && dMaxSc.before(trajectory.getDateEnd())){
			sb.append(sdf.format(dMaxSc));
		}else{
			sb.append(sdf.format(trajectory.getDateEnd()));
		}

		//Infos à placer dans le header
		header.setBoatName(trajectory.getBoat().getName());
				
		header.setSspsDeph(trajectory.getSspsDeph());
		header.setSspsDephMin(trajectory.getSspsDephMin());
		header.setSspsDephMax(trajectory.getSspsDephMax());
		
		if (FileDTO.FORMAT_ASCII.equals(outputFormat)){
			return new FichierAscii(sb.toString(), header);
		}else if (FileDTO.FORMAT_NETCDF.equals(outputFormat)){
			return new FichierNetcdf(sb.toString(), header);
		}else{
			throw new IOException("format inconnu: " + outputFormat);
		}
		
	}
		
		
}
