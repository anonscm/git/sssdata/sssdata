package fr.sedoo.sssdata.server;

import fr.sedoo.commons.client.user.InternalUser;
import fr.sedoo.commons.spring.SpringBeanFactory;

public class SSSDataApplication {

	private static SpringBeanFactory springBeanFactory = new SpringBeanFactory();

	public static SpringBeanFactory getSpringBeanFactory() {
		return springBeanFactory;
	}

	public static void setSpringBeanFactory(SpringBeanFactory springBeanFactory) {
		SSSDataApplication.springBeanFactory = springBeanFactory;
	}
	
	public static InternalUser getSuperAdmin() {
		return (InternalUser) getSpringBeanFactory().getBeanByName("sedooAdmin");
	}
	
}
