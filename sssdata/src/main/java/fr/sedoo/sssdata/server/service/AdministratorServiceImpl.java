package fr.sedoo.sssdata.server.service;

import java.util.ArrayList;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.sedoo.sssdata.client.service.AdministratorService;
import fr.sedoo.sssdata.client.service.ServiceException;
import fr.sedoo.sssdata.server.SSSDataApplication;
import fr.sedoo.sssdata.server.dao.api.AdministratorDAO;
import fr.sedoo.sssdata.shared.Administrator;

public class AdministratorServiceImpl extends RemoteServiceServlet implements AdministratorService{

	private AdministratorDAO dao;

	@Override
	public ArrayList<Administrator> findAll() throws ServiceException {
		initDao();
		return dao.findAll();
	}

	@Override
	public void delete(Administrator administrator) throws ServiceException {
		initDao();
		dao.delete(administrator);
	}

	@Override
	public Administrator create(Administrator administrator) throws ServiceException {
		initDao();		
		return dao.save(administrator);
	}
	
	private void initDao()
	{
		if (dao == null)
		{
			dao = (AdministratorDAO) SSSDataApplication.getSpringBeanFactory().getBeanByName(AdministratorDAO.BEAN_NAME);
		}
	}
	
	@Override
	public Administrator edit(Administrator administrator) throws ServiceException {
		initDao();
		return dao.save(administrator);
	}

}
