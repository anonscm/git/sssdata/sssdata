package fr.sedoo.sssdata.server.service;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.sedoo.sssdata.client.service.DownloadTrajectoriesService;
import fr.sedoo.sssdata.server.SSSDataApplication;
import fr.sedoo.sssdata.server.dao.api.TrajectoryDAO;
import fr.sedoo.sssdata.server.extract.ArchiveSortie;
import fr.sedoo.sssdata.shared.FileDTO;
import fr.sedoo.sssdata.shared.SearchCriterion;

public class DownloadTrajectoriesServiceImpl extends RemoteServiceServlet implements DownloadTrajectoriesService {

	private static Logger logger = Logger.getLogger(DownloadTrajectoriesServiceImpl.class);
	
	private TrajectoryDAO dao;
		
	@Override
	public FileDTO downloadTrajectory(Long requestId, String trajectoryName, SearchCriterion sc, String outputFormat) {
		initDao();

		try{
			String generatedFile = dao.downloadTrajectory(""+requestId, trajectoryName, sc, outputFormat);

			if (generatedFile != null){
				FileDTO result = new FileDTO();
				result.setSssFileName(trajectoryName);
				result.setGeneratedFileName(generatedFile);
				result.setVolume(new File(generatedFile).length());
				return result;
			}
		}catch(Exception e){
			logger.error("Error in downloadTrajectory(): " + e);
		}
		return null;
	}

	private void initDao()	{
		if (dao == null)	{
			dao = (TrajectoryDAO) SSSDataApplication.getSpringBeanFactory().getBeanByName(TrajectoryDAO.BEAN_NAME);
		}
	}

	@Override
	public String createArchive(Long requestId, List<FileDTO> fichiers,
			String compression) throws IOException{

		ArchiveSortie archive = new ArchiveSortie(compression, ""+requestId);
		archive.setFichiers(fichiers);
		archive.open();
		archive.write();
		archive.close();
		return archive.getFile().getName();
	}
	
}
