package fr.sedoo.sssdata.server.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.sedoo.sssdata.server.dao.api.UserDAO;
import fr.sedoo.sssdata.server.model.User;

@Repository
public class UserDAOJPAImpl implements UserDAO {

	protected EntityManager em;
	public EntityManager getEntityManager() {
		return em;
	}

	@PersistenceContext
	public void setEntityManager(EntityManager entityManager) {
		this.em = entityManager;
	}
	
	@Override
	@Transactional
	public User getUser(String email) {
		Query query = em.createQuery("Select u from User u where lower(u.email) = :arg1 ORDER BY u.id DESC");
		query.setParameter("arg1", email.toLowerCase());
		try{
			return (User) query.getResultList().get(0);
		}
		catch (Exception e){
			return null;
		}
	}
	
	@Override
	@Transactional
	public User getUser(String email, String name, String country) {
		Query query = em.createQuery("Select u from User u where lower(u.email) = :arg1 and lower(u.name) = :arg2 and lower(u.country) = :arg3");
		query.setParameter("arg1", email.toLowerCase());
		query.setParameter("arg2", name.toLowerCase());
		query.setParameter("arg3", country.toLowerCase());
		try{
			return (User) query.getSingleResult();
		}
		catch (NoResultException e){
			return null;
		}
	}
		
	
	@Override
	@Transactional
	public List<User> getAll() {
		try{
			List<User> aux = em.createQuery("Select u from User u ").getResultList();
			List<User> users = new ArrayList<User>();
			users.addAll(aux);
			return users;
		}catch(NoResultException e){
			return new ArrayList<User>();
		}
	}
	
	@Override
	public Map<String,Integer> getNbUsersByCountry(){
		Query query = em.createQuery("SELECT u.country, COUNT(*) FROM User u "
				+ "WHERE EXISTS (SELECT d FROM Download d WHERE d.user.id = u.id) GROUP BY u.country ORDER BY u.country");
		 
		Map<String,Integer> result = new HashMap<String, Integer>();
		try{			
			List<Object[]> queryResult = query.getResultList();
			for (Object[] row : queryResult) {
			    String name = (String) row[0];
			    int count = ((Number) row[1]).intValue();
			    result.put(name, count);
			}
			return result;
		}
		catch (NoResultException e){
			return null;
		}
		
	}
	
	@Override
	public List<Date> getRegistrationDates() {
		Query query = em.createNativeQuery("SELECT u.id, min(date_dl) AS date_min FROM user_sss u, download d WHERE u.id = d.user_fk GROUP BY u.id ORDER BY date_min;");
		
		List<Date> result = new ArrayList<Date>();
		try{			
			List<Object[]> queryResult = query.getResultList();
			for (Object[] row : queryResult) {
			    Date min_date = ((Date) row[1]);
			    result.add(min_date);
			}
			return result;
		}
		catch (NoResultException e){
			return null;
		}
	}
	
	/*@Override
	public void insertDataFromUserInformation(UserInformations userinfo) {
		
			User user = new User();
			user.setId(userinfo.getUserId());
			user.setEmail(userinfo.getUserEmail());
			user.setName(userinfo.getUserName());
	
			Download dl = new Download();
			dl.setDataUsage(userinfo.getUserDataUsage());
			dl.setDateDL(new Date());
			dl.setGoal(userinfo.getUserObjective());
			dl.setLabo(userinfo.getUserOrganism());
			dl.setUser(save(user));
			dl.setFiles(new ArrayList<SssFile>());
			
			if ( getUser(userinfo.getUserEmail()) == null){
				getEntityManager().persist(user);
			}else{
				getEntityManager().merge(user);
			}
	}*/

	@Override
	@Transactional
	public User save(User user) {
		User u = getUser(user.getEmail(), user.getName(), user.getCountry());
		if (u == null){
			getEntityManager().persist(user);
			return getUser(user.getEmail());
		}else{
			return u;
		}
	}

}
