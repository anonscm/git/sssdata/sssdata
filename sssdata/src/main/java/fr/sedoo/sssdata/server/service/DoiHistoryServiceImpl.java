package fr.sedoo.sssdata.server.service;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Collections;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.sedoo.metadata.doi.DOIConstants;
import fr.sedoo.sssdata.client.service.DoiHistoryService;
import fr.sedoo.sssdata.client.service.ServiceException;
import fr.sedoo.sssdata.server.SSSDataApplication;
import fr.sedoo.sssdata.server.dao.api.MetadataDAO;
import fr.sedoo.sssdata.server.dao.api.MetadataHistoryDAO;
import fr.sedoo.sssdata.server.metadata.HistoryItemBuilder;
import fr.sedoo.sssdata.server.metadata.SSSDataciteDAO;
import fr.sedoo.sssdata.server.model.Metadata;
import fr.sedoo.sssdata.server.model.MetadataHistory;
import fr.sedoo.sssdata.shared.metadata.DoiConstants;
import fr.sedoo.sssdata.shared.metadata.DoiInfos;
import fr.sedoo.sssdata.shared.metadata.HistoryItem;

public class DoiHistoryServiceImpl extends RemoteServiceServlet implements DoiHistoryService {

	private static Logger LOG = Logger.getLogger(DoiHistoryServiceImpl.class);

	MetadataDAO metadataDAO;
	SSSDataciteDAO dataciteDAO;

	MetadataHistoryDAO dao;

	Metadata metadata;

	private void initDao(){
		if (metadataDAO == null){
			metadataDAO = (MetadataDAO) SSSDataApplication.getSpringBeanFactory().getBeanByName(MetadataDAO.BEAN_NAME);
		}
		if (dataciteDAO == null){
			dataciteDAO = (SSSDataciteDAO) SSSDataApplication.getSpringBeanFactory().getBeanByName(SSSDataciteDAO.BEAN_NAME);
		}
		if (dao == null){
			dao = (MetadataHistoryDAO) SSSDataApplication.getSpringBeanFactory().getBeanByName(MetadataHistoryDAO.BEAN_NAME);
		}
	}

	private void initMetadata(String doi){
		metadata = metadataDAO.findByDoi(doi);
		if (metadata == null){
			String xml = dataciteDAO.getXmlMetadata(doi);
			metadata = new Metadata();
			metadata.setDoi(doi);
			metadata.setXml(xml);
			metadataDAO.save(metadata);
		}
	}

	@Override
	public DoiInfos getDoiInfos(String doiSuffix) throws ServiceException {
		initDao();
		String doi =  dataciteDAO.getConf().getPrefix() + doiSuffix;
		initMetadata(doi);
		DoiInfos infos = readXml(metadata);
		if (metadata.getHistory() != null){
			for (MetadataHistory h: metadata.getHistory()){
				infos.getHistory().add(HistoryItemBuilder.buildItem(h));
			}
			Collections.sort(infos.getHistory());
		}
		return infos;
	}

	private DoiInfos readXml(Metadata m) {
		try {
			String xml = m.getXml();
			InputStream stream = new ByteArrayInputStream(xml.getBytes("UTF-8"));
			SAXBuilder sb= new SAXBuilder();
			Document doc= sb.build(stream);
			DoiInfos infos = new DoiInfos();
			infos.setDoi(m.getDoi());

			Namespace ns = Namespace.getNamespace(DOIConstants.DATACITE_NAMESPACE);

			Element racine = doc.getRootElement();
			infos.setVersion(racine.getChildTextNormalize("version", ns));

			return infos;
		}catch(Exception e){
			LOG.error("Error in readXml(). Cause: " + e);
			return null;
		}

	}

	/*	@Override
	public ArrayList<HistoryItem> findAll() throws ServiceException {
		initDao();
		ArrayList<HistoryItem> result = new ArrayList<HistoryItem>();
		String doi = dataciteDAO.getDoi();
		Metadata metadata = metadataDAO.findByDoi(doi);
		if (metadata == null){
			String xml = dataciteDAO.getMetadata(doi);
			metadata = new Metadata();
			metadata.setDoi(doi);
			metadata.setXml(xml);
			metadataDAO.save(metadata);
		}
		for (MetadataHistory h: metadata.getHistory()){
			result.add(HistoryItemBuilder.buildItem(h));
		}
		Collections.sort(result);
		return result;
	}*/

	@Override
	public void delete(HistoryItem item) throws ServiceException {
		initDao();
		dao.delete(item.getId());
	}

	@Override
	public HistoryItem edit(HistoryItem item, String doiSuffix) throws ServiceException {
		initDao();
		String doi =  dataciteDAO.getConf().getPrefix() + doiSuffix;
		initMetadata(doi);
		MetadataHistory entity = HistoryItemBuilder.buildEntity(item);
		entity.setMetadata(metadata);
		return HistoryItemBuilder.buildItem(dao.save(entity)); 
	}

	@Override
	public HistoryItem create(HistoryItem item, String doiSuffix) throws ServiceException {
		return edit(item, doiSuffix);
	}



}
