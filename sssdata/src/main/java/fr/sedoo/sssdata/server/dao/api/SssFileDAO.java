package fr.sedoo.sssdata.server.dao.api;

import java.util.Date;
import java.util.List;

import fr.sedoo.sssdata.server.model.SssFile;
import fr.sedoo.sssdata.server.model.UpdateInfos;
import fr.sedoo.sssdata.shared.SearchCriterion;

public interface SssFileDAO {
	public final static String BEAN_NAME="sssFileDAO";

	void deleteSssFileFromName(String fileName);

	SssFile getSimpleFileByName(String name, boolean loadLinkedObjects);

	SssFile getFileByNameAndBoat(String fileName, String boatName,
			boolean loadLinkedObjects);

	SssFile save(SssFile sssFile, boolean loadLinkedObjects);
	
	//public List<SssFile> getFilesByBoatBoundingsAndDates(String boatName, GeographicBoundingBoxDTO boundings, Date dateBegin, Date dateEnd);
	List<SssFile> getFilesByBoat(String boatName, SearchCriterion sc,
			boolean loadLinkedObjects);

	List<SssFile> getFilesByBoat(String boatName, Date beginDate, Date endDate, boolean loadJournal);

	List<SssFile> getFilesByDates(Date beginDate, Date endDate,
			boolean loadLinkedObjects);
	
	/**
	 * 
	 * @param limit nombre max de fichiers à renvoyer, 0 pour tout renvoyer
	 * @return
	 */
	List<SssFile> getLastUpdatedFiles(int limit, boolean loadLinkedObjects);
	Long getSize();

	List<SssFile> getUpdatedFilesByPage(int first, int size,
			boolean loadLinkedObjects);
	
	void markAsDeleted(SssFile sssFile);
	int getBoatNb();
	UpdateInfos getLastUpdateInfos();
	Date getLastDbUpdate();
}
