package fr.sedoo.sssdata.server.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.sedoo.sssdata.server.dao.api.DownloadDAO;
import fr.sedoo.sssdata.server.model.Download;
import fr.sedoo.sssdata.server.model.SssFile;
import fr.sedoo.sssdata.server.model.User;
import fr.sedoo.sssdata.shared.Administrator;

@Repository
public class DownloadDAOJPAImpl implements DownloadDAO {

	protected EntityManager em;
	public EntityManager getEntityManager() {
		return em;
	}

	// pour utilisation du jdbcTemplate et RowMapper
	private DataSource dataSource;

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	/*private UserDAO userDao;
	
	private void initDao(){
		if (userDao == null){
			userDao = (UserDAO) SSSDataApplication.getSpringBeanFactory().getBeanByName(UserDAO.BEAN_NAME);
		}
	}*/
	
	@PersistenceContext
	public void setEntityManager(EntityManager entityManager) {
		this.em = entityManager;
	}
	
	@Override
	@Transactional
	public Download getDownload(Long id, boolean loadLinkedObjects) {
		Query query = em.createQuery("SELECT d FROM Download d WHERE d.id = :id");
		query.setParameter("id", id);
		try {
			Download result = (Download) query.getSingleResult();
			if (loadLinkedObjects) {
				loadLinkedObjects(result);
			}
			return result;
		}
		catch (NoResultException e){
			return null;
		}
	}
	
	

	@Override
	@Transactional
	public List<Download> getDownloads(User u, boolean loadLinkedObjects) {
		Query query = em.createQuery("SELECT d FROM Download d WHERE d.user.id = :uid ORDER BY d.id DESC");
		query.setParameter("uid", u.getId());
		try{			
			List<Download> result = (List<Download>) query.getResultList();
			if (loadLinkedObjects) {
				for (Download d : result) {
					loadLinkedObjects(d);
				}
			}
			return result;
		}
		catch (NoResultException e){
			return null;
		}
	}

	@Override
	@Transactional
	public Download save(Download d, boolean loadLinkedObjects) {
		if (d.getId() == null){
			getEntityManager().persist(d);
			if (loadLinkedObjects) {
				loadLinkedObjects(d);
			}
			return d;
		}
		else{
			getEntityManager().merge(d);
			return d;
		}
	}
		

	@Override
	@Transactional
	public Download getLastDownload(User u, boolean loadLinkedObjects) {
		List<Download> downloads = getDownloads(u, loadLinkedObjects);
		if (downloads != null && downloads.size() > 0){
			return downloads.get(0);
		}else{
			return null;
		}
	}

	
	@Transactional
	@Override
	public List<Download> getDownloadsByMonth(int year, int month,
			boolean loadLinkedObjects) {
		Query query = em.createQuery("SELECT d FROM Download d WHERE d.dateDL >= :dMin AND d.dateDL < :dMax ORDER BY d.id DESC");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		Calendar cal = Calendar.getInstance();
		cal.set(year, month - 1, 1, 0, 0, 0);
		cal.set(Calendar.MILLISECOND, 0);
		query.setParameter("dMin", cal.getTime());
		cal.add(Calendar.MONTH, 1);
		query.setParameter("dMax", cal.getTime());
				
		try {
			List<Download> result = (List<Download>) query.getResultList();
			if (loadLinkedObjects) {
				for (Download d : result) {
					loadLinkedObjects(d);
				}
			}
			return result;
		}
		catch (NoResultException e){
			return null;
		}
	}

	/**
	 * Override getDownloadsByMonth(int year, int month) to escape slow loading
	 * by using unique SQL request.
	 * 
	 * @param year
	 * @param month
	 * @return List of Download
	 */
	@Override
	public List<Download> getDownloadsByMonthLazyLoaded(int year, int month) {

		Calendar cal = Calendar.getInstance();
		cal.set(year, month - 1, 1, 0, 0, 0);
		cal.set(Calendar.MILLISECOND, 0);
		final java.sql.Date dateMin = new java.sql.Date(cal.getTime().getTime());
		cal.add(Calendar.MONTH, 1);
		final java.sql.Date dateMax = new java.sql.Date(cal.getTime().getTime());

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		String sql = "SELECT d.id as ID_DL, d.date_dl as DATE_DL, d.goal as GOAL_DL, d.volume as VOLUME_DL, d.data_format as FORMAT_DL, d.data_compression as COMPRESSION_DL, d.labo as LABO_DL, u.email as USER_EMAIL, d.user_fk as USER_ID FROM download d LEFT JOIN user_sss u ON u.id = d.user_fk WHERE d.date_dl >= ? AND d.date_dl < ? ORDER BY d.id DESC";

		class DownloadRowMapper implements RowMapper<Download> {

			@Override
			public Download mapRow(ResultSet rs, int rowNum)
					throws SQLException {

				Download result = new Download();

				result.setId(rs.getLong("ID_DL"));
				result.setGoal(rs.getString("GOAL_DL"));
				result.setDataCompression(rs.getString("COMPRESSION_DL"));
				result.setDataFormat(rs.getString("FORMAT_DL"));
				result.setDateDL(rs.getDate("DATE_DL"));
				result.setLabo(rs.getString("LABO_DL"));
				result.setVolume(rs.getLong("VOLUME_DL"));

				User user = new User();
				user.setId(rs.getLong("USER_ID"));
				user.setEmail(rs.getString("USER_EMAIL"));
				result.setUser(user);

				return result;
			}
		}

		List<Download> result = jdbcTemplate.query(sql,
				new PreparedStatementSetter() {
					public void setValues(PreparedStatement preparedStatement)
							throws SQLException {
						preparedStatement.setDate(1, dateMin);
						preparedStatement.setDate(2, dateMax);
					}
				}, new DownloadRowMapper());

		return result;
	}



	@Override
	@Transactional
	public List<Download> getAllDownloads(boolean loadLinkedObjects) {
		Query query = em.createQuery("SELECT d FROM Download d ORDER BY d.dateDL DESC");
		try{			
			List<Download> result = (List<Download>) query.getResultList();
			if (loadLinkedObjects) {
				for (Download d : result) {
					loadLinkedObjects(d);
				}
			}
			return result;
		}
		catch (NoResultException e){
			return new ArrayList<Download>();
		}
	}
	
	@Transactional
	public void delete(Long id) {
		
		Download aux = getEntityManager().find(Download.class, id);
		if (aux != null){
			getEntityManager().remove(aux);
		}
	}

	private void loadLinkedObjects(Download obj) {
		if (obj != null) {
			List<SssFile> files = obj.getFiles();
			if (files != null) {
				files.size();
			}

			User user = obj.getUser();
			if (user != null) {
				user.getId();
			}
		}
	}

}
