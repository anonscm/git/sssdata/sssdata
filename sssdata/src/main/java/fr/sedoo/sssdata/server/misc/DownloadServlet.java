package fr.sedoo.sssdata.server.misc;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLDecoder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import fr.sedoo.sssdata.server.SSSDataApplication;
import fr.sedoo.sssdata.shared.FileDTO;

public class DownloadServlet extends HttpServlet {

	private final static Logger logger = Logger.getLogger(DownloadServlet.class);
	
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)	throws ServletException, IOException {

		OutputStream out = null;
		InputStream from = null;

		int bufferSize = 64 * 1024;
		
		try{
			String filename = URLDecoder.decode(request.getParameter("file"),"UTF8");
			File file;

			if (filename.contains(FileDTO.ARCHIVE_FOLDER)) {
				/*ArchiveGenerator archiveService = (ArchiveGenerator) SSSDataApplication
						.getSpringBeanFactory().getBeanByName(
								ArchiveGenerator.BEAN_NAME);*/
				
				DownloadConfig downloadConfig = (DownloadConfig)SSSDataApplication
						.getSpringBeanFactory().getBeanByName(
								DownloadConfig.BEAN_NAME);
				
				file = new File(downloadConfig.getRoot() + filename);

			} else {
				file = new File(FileDTO.EXTRACT_PATH + "/" + filename);
			}

			if (!file.isFile() || !file.exists()) {
				throw new IOException("File does not exist");
			}

			response.reset();
			response.setContentType("application/download");
			response.setHeader("Content-Disposition", "attachment;filename=\"" + file.getName() + "\"");
			response.setContentLength((int) file.length());

			logger.debug("Content Length: " + (int) file.length());

			from = new FileInputStream(file);
			out = response.getOutputStream();
			byte[] bufferFile = new byte[bufferSize];
			int len = 0;
			int read = 0;

			while ((len = from.read(bufferFile)) > 0){
				read += len;
				out.write(bufferFile, 0, len);
			}

			logger.debug("read: " + read);

			out.flush();
			response.flushBuffer();
		}catch(IOException e){
			logger.error("Error in downloadFile. Cause: " + e);
		}finally{
			try { 
				from.close();
			} catch (Exception e) {
				logger.error("Error in downloadFile (close). Cause: " + e);
			} 
			try { 
				out.close();
			} catch (Exception e) {
				logger.error("Error in downloadFile (close). Cause: " + e);
			}
		}
	}
	
	
}
