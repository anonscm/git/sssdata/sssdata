package fr.sedoo.sssdata.server.service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import fr.sedoo.commons.ogc.commons.wfs.Feature;
import fr.sedoo.commons.ogc.commons.wfs.LineFeature;
import fr.sedoo.commons.ogc.commons.wfs.MultiLineFeature;
import fr.sedoo.commons.ogc.commons.wfs.WFSUtils;
import fr.sedoo.commons.shared.domain.GeographicBoundingBoxDTO;
import fr.sedoo.sssdata.server.SSSDataApplication;
import fr.sedoo.sssdata.server.dao.api.TrajectoryDAO;
import fr.sedoo.sssdata.shared.BoatDTO;
import fr.sedoo.sssdata.shared.LatLong;
import fr.sedoo.sssdata.shared.SearchCriterion;
import fr.sedoo.sssdata.shared.Trajectory;


@Controller
@RequestMapping(value = "/wfs")
public class WFSController {

	private TrajectoryDAO dao;
	private void initDao()	{
		if (dao == null)	{
			dao = (TrajectoryDAO) SSSDataApplication.getSpringBeanFactory().getBeanByName(TrajectoryDAO.BEAN_NAME);
		}
	}
	
	
	@RequestMapping
	public ModelAndView getFeatures(HttpServletRequest request, HttpServletResponse response) throws IOException {

		try{
			
			initDao();
			
			BoatDTO b = new BoatDTO();
			b.setName("matisse");
			
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			sdf.setLenient(true);
			
			SearchCriterion sc = new SearchCriterion();
			sc.setBeginDate(sdf.parse("2008-01-01"));
			sc.setEndDate(sdf.parse("2009-01-01"));
			
			GeographicBoundingBoxDTO boundingBox = new GeographicBoundingBoxDTO();
			boundingBox.setEastBoundLongitude("170");
			boundingBox.setWestBoundLongitude("-150");
			boundingBox.setNorthBoundLatitude("60");
			boundingBox.setSouthBoundLatitude("-50");
			sc.setBoundingBox(boundingBox);
			
			//sc.setZoomLevel(10);
			
			List<Trajectory> trajectories = dao.searchTrajectories(b, sc);

			List<Feature> features = new ArrayList<Feature>();
			String nameSpace = "http://sss.sedoo.fr";
			String typeName = "trajectory";
			ArrayList<String> propertiesKeys = new ArrayList<String>();
			propertiesKeys.add("ship");
			
			for (Trajectory t: trajectories){
				MultiLineFeature lfs = new MultiLineFeature();
				lfs.setId(t.getSssFileName());
				for (List<LatLong> line: t.getLines()){
					LineFeature lf = new LineFeature();
					for (LatLong p: line){
						lf.addPoint(p.getLon(), p.getLat());		
					}
					lfs.addLine(lf);
				}
				Properties properties = new Properties();
				properties.put("ship", "matisse");
				lfs.setProperties(properties);

				features.add(lfs);
				
			}
			
			String wfs = WFSUtils.toGetFeature(features, nameSpace, typeName,propertiesKeys);

				response.setContentType("application/xml");
				response.getOutputStream().write(wfs.getBytes());

		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

}
