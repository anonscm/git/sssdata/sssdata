package fr.sedoo.sssdata.server.service;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.sedoo.commons.client.util.ServiceException;
import fr.sedoo.sssdata.client.service.UserInfoService;
import fr.sedoo.sssdata.server.SSSDataApplication;
import fr.sedoo.sssdata.server.dao.api.ArchiveDAO;
import fr.sedoo.sssdata.server.dao.api.DownloadDAO;
import fr.sedoo.sssdata.server.dao.api.SssFileDAO;
import fr.sedoo.sssdata.server.dao.api.UserDAO;
import fr.sedoo.sssdata.server.misc.DownloadConfig;
import fr.sedoo.sssdata.server.model.Archive;
import fr.sedoo.sssdata.server.model.Download;
import fr.sedoo.sssdata.server.model.SssFile;
import fr.sedoo.sssdata.server.model.User;
import fr.sedoo.sssdata.shared.FileDTO;
import fr.sedoo.sssdata.shared.UserInformations;

public class UserInfoServiceImpl extends RemoteServiceServlet implements UserInfoService{

	private UserDAO uDao;
	private DownloadDAO dlDao;
	private SssFileDAO fileDao;
	private ArchiveDAO archiveDao;
	private DownloadConfig downloadConfig;
	
	
	public UserInfoServiceImpl() {
		super();
		initDao();
	}

	@Override
	public UserInformations getUserInfos(String email) {
		initDao();
		User u = uDao.getUser(email);
		if (u != null){
			UserInformations userInfos = new UserInformations();
			userInfos.setUserEmail(u.getEmail());
			userInfos.setUserName(u.getName());
			userInfos.setCountry(u.getCountry());
			Download dl = dlDao.getLastDownload(u, true);
			if (dl != null){
				userInfos.setUserObjective(dl.getGoal());
				userInfos.setUserOrganism(dl.getLabo());
			}
			return userInfos;
		}
		return null;
	}
	
	/*@Override
	public void insertDataFromUserInformation(UserInformations userinfo) {
		dao.insertDataFromUserInformation(userinfo);
	}*/

	private void initDao(){
		if (uDao == null){
			uDao = (UserDAO) SSSDataApplication.getSpringBeanFactory().getBeanByName(UserDAO.BEAN_NAME);
		}
		if (dlDao == null){
			dlDao = (DownloadDAO) SSSDataApplication.getSpringBeanFactory().getBeanByName(DownloadDAO.BEAN_NAME);
		}
		if (fileDao == null){
			fileDao = (SssFileDAO) SSSDataApplication.getSpringBeanFactory().getBeanByName(SssFileDAO.BEAN_NAME);
		}
		if (archiveDao == null){
			archiveDao = (ArchiveDAO) SSSDataApplication.getSpringBeanFactory().getBeanByName(ArchiveDAO.BEAN_NAME);
		}
		if (downloadConfig == null){
			downloadConfig = (DownloadConfig)SSSDataApplication.getSpringBeanFactory().getBeanByName(DownloadConfig.BEAN_NAME);
		}
	}

	@Override
	public String getArchiveName(String flag, String format){
		initDao();
		Archive a = archiveDao.findLastByFlagAndFormat(flag, format);
		return a.getName();
	}

	@Override
	public void updateDownloadArchive(Long id, String archiveName) throws ServiceException{
		initDao();		
		Download dl = dlDao.getDownload(id, true);
		if (dl != null){
			Archive a = archiveDao.findByName(archiveName);
			if (a == null){
				throw new ServiceException("Archive not found: " + archiveName);
			}else{
				dl.setArchive(a);
				
				File f = new File(downloadConfig.getRoot() + FileDTO.ARCHIVE_FOLDER	+ "/" + archiveName);
				if (f.exists()){
					dl.setVolume(f.length());
				}else{
					throw new ServiceException("File not found: " + archiveName);
				}
				
				dlDao.save(dl, true);
			}
		}else{
			throw new ServiceException("Request with id " + id + " not found in database.");
		}
	}
	
	@Override
	public Long insertDownload(UserInformations userInfos) {
		initDao();		
		
		User user = new User();
		user.setEmail(userInfos.getUserEmail());
		user.setName(userInfos.getUserName());
		user.setCountry(userInfos.getCountry());
		
		user = uDao.save(user);
					
		Download dl = new Download();
		dl.setUser(user);
		dl.setDateDL(new Date());
		dl.setGoal(userInfos.getUserObjective());
		dl.setLabo(userInfos.getUserOrganism());
		dl.setFiles(new ArrayList<SssFile>());
		dl.setDataFormat(userInfos.getDataFormat());
		dl.setDataCompression(userInfos.getDataCompression());		
		dl.setVolume(0l);
		dlDao.save(dl, true);
		
		dl = dlDao.getLastDownload(user, false);
		return dl.getId();
	}
	
	@Override
	public void updateDownload(Long id, FileDTO file) throws ServiceException{
		initDao();		
		Download dl = dlDao.getDownload(id, true);
		if (dl != null){
			SssFile sssFile = fileDao.getSimpleFileByName(
					file.getSssFileName(), true);
			if (sssFile == null){
				throw new ServiceException("SssFile not found: " + file.getSssFileName());
			}else{
				dl.getFiles().add(sssFile);
				dl.setVolume(dl.getVolume() + file.getVolume());
				dlDao.save(dl, true);
			}
		}else{
			throw new ServiceException("Request with id " + id + " not found in database.");
		}
	}
	
	/*@Override
	public void insertDownload(UserInformations userInfos, List<FileDTO> files) {
		initDao();		
		
		User user = new User();
		user.setEmail(userInfos.getUserEmail());
		user.setName(userInfos.getUserName());
		
		user = uDao.save(user);
					
		Download dl = new Download();
		dl.setUser(user);
		dl.setDataUsage(userInfos.getUserDataUsage());
		dl.setDateDL(new Date());
		dl.setGoal(userInfos.getUserObjective());
		dl.setLabo(userInfos.getUserOrganism());
		dl.setFiles(new ArrayList<SssFile>());
		for (FileDTO file: files){
			SssFile f = fileDao.getSimpleFileByName(file.getSssFileName());
			if (f != null){
				dl.getFiles().add(f);
			}
		}
		
		dlDao.save(dl);		
		
	}*/
	
}
