package fr.sedoo.sssdata.server.dao.gosud;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import ucar.nc2.Attribute;
import ucar.nc2.NetcdfFile;

public class GosudUtils {

	private static List<String> managedParameters = new ArrayList<String>();
	
	static
	{
		managedParameters.add(GosudConstants.DATE_END);
		managedParameters.add(GosudConstants.DATE_START);
		managedParameters.add(GosudConstants.DATE_CREATION);
		managedParameters.add(GosudConstants.DATE_UPDATE);
		managedParameters.add(GosudConstants.EAST_LONX);
		managedParameters.add(GosudConstants.WEST_LONX);
		managedParameters.add(GosudConstants.NORTH_LATX);
		managedParameters.add(GosudConstants.SOUTH_LATX);
		managedParameters.add(GosudConstants.PLATFORM_NAME);
		managedParameters.add(GosudConstants.SHIP_CALL_SIGN);
		managedParameters.add(GosudConstants.TYPE_TINT);
		managedParameters.add(GosudConstants.TYPE_TSG);
	}
	
	private static SortedMap<Integer,String> flags = new TreeMap<Integer, String>(){{
		put(GosudConstants.UNCONTROLLED_DATA, "Uncontrolled data");
		put(GosudConstants.GOOD_DATA, "Good data");
		put(GosudConstants.PROBABLY_GOOD_DATA, "Probably good data");
		put(GosudConstants.PROBABLY_BAD_DATA, "Probably bad data");
		put(GosudConstants.BAD_DATA, "Bad data");
		put(GosudConstants.HARBOUR_DATA, "Harbour data");
	}};
	
	public static SortedMap<Integer,String> getFlags(){
		return flags;
	}
	
	public static String getShipNameFromFile(NetcdfFile inputFile)
	{
		HashMap<String, String> parameters = extractParamsFromGosud(inputFile);
		return parameters.get(GosudConstants.PLATFORM_NAME);
	}
	
	public static String getShipCodeFromFile(NetcdfFile inputFile)
	{
		HashMap<String, String> parameters = extractParamsFromGosud(inputFile);
		return parameters.get(GosudConstants.SHIP_CALL_SIGN);
	}
	
	public static HashMap<String, String> getParams(NetcdfFile inputFile)
	{
		return extractParamsFromGosud(inputFile);
	}
	

	private static HashMap<String, String> extractParamsFromGosud(
			NetcdfFile inputFile) {

		HashMap<String, String> params = new HashMap<String, String>();
		List<Attribute> globalAttributes = inputFile.getGlobalAttributes();
		Iterator<Attribute> iterator = globalAttributes.iterator();
		while (iterator.hasNext()) 
		{
			checkParameter(iterator.next(), params);
		}
		return params;
	}


	private static void checkParameter(Attribute attribute, HashMap<String, String> params) 
	{
		if (managedParameters.contains(attribute.getName().toUpperCase()))
		{
			params.put(attribute.getName().toUpperCase(), attribute.getStringValue());
		}
	}
	
	public static Date getDateFromString(String dateStr) throws ParseException{
		SimpleDateFormat sdf = new SimpleDateFormat(GosudConstants.DATE_FORMAT);
		sdf.setLenient(true);
		return new Date(sdf.parse(dateStr).getTime());
	}
	
/*
	public static Date getDateFromString(String dateString) 
	{
		Calendar calendar = new GregorianCalendar();
		calendar.set(Calendar.YEAR, Integer.parseInt(dateString.substring(0, 4)));
		calendar.set(Calendar.MONTH, Integer.parseInt(dateString.substring(4, 6)));
		calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dateString.substring(6,8)));
		calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(dateString.substring(8,10)));
		calendar.set(Calendar.MINUTE, Integer.parseInt(dateString.substring(10,12)));
		calendar.set(Calendar.SECOND, Integer.parseInt(dateString.substring(12,14)));

		return calendar.getTime();

	}*/

	
}

