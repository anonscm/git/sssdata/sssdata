package fr.sedoo.sssdata.server.dao.api;

import java.util.List;

import fr.sedoo.sssdata.server.model.Metadata;

public interface MetadataDAO {
	public final static String BEAN_NAME="metadataDAO";
	List<Metadata> findAll();	
	Metadata save (Metadata m);
	Metadata findByDoi(String doi);
	
}
