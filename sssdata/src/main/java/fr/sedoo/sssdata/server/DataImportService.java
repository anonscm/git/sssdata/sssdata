package fr.sedoo.sssdata.server;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import fr.sedoo.sssdata.server.misc.LegosFTPParser;

@Path("/dataimport")
public class DataImportService {
	
	 @GET
     @Path("/isAlive")
     public Response getVersion() 
     {
             String version = "Alive";
             return Response.status(200).entity(version).build();
     }
	 
	 @GET
     @Path("/launch")
     public Response lauch() 
     {
		 LegosFTPParser.launch();
             return Response.status(200).entity("sucess").build();
     }

}
