package fr.sedoo.sssdata.server.dao.api;

import java.sql.SQLException;
import java.util.List;

import javax.persistence.Query;

import fr.sedoo.sssdata.server.model.Measure;
import fr.sedoo.sssdata.server.model.SssFile;
import fr.sedoo.sssdata.shared.Trajectory;

public interface MeasureDAO {
	public final static String BEAN_NAME="measureDAO";
	void add(Measure m);
	void bulkAdd(List<Measure> measures);

	void deleteByFile(SssFile sssFile);
	
	int getMeasuresNb(SssFile f, Query countQuery);
	Trajectory getMeasuresByFile(SssFile f, Query query, int precision);
				
	List<Measure> getMeasuresByFile(SssFile f, Query query);
	

	void initBatch() throws SQLException;
	void addToBatch(Measure measure) throws SQLException;	
	void closeBatch() throws SQLException;
}
