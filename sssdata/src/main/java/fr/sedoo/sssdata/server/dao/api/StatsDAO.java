package fr.sedoo.sssdata.server.dao.api;

import java.util.ArrayList;

import fr.sedoo.sssdata.shared.MonthyReport;

public interface StatsDAO {
	public final static String BEAN_NAME="statsDataDAO";
	ArrayList<MonthyReport> findData();
	
}