package fr.sedoo.sssdata.server.misc;

public class Mesure {
		
	private Double salinity;
	private Integer salinityQuality;
	
	private Double salinityAdjusted;
	private Integer salinityAdjustedQuality;
	private Double salinityAdjustedError;
		
	private Double temperature;
	private Integer temperatureQuality;
		
	private Double temperatureAdjusted;
	private Integer temperatureAdjustedQuality;
	private Double temperatureAdjustedError;
	
	private Double jacketTemperature;
	private Integer jacketTemperatureQuality;
	
	private Double jacketTemperatureAdjusted;
	private Integer jacketTemperatureAdjustedQuality;
	private Double jacketTemperatureAdjustedError;

	public Mesure(Double sal, int salQuality, Double adjSal, int adjSalQuality, Double adjSalError,
			Double temp, int tempQuality, Double adjTemp, int adjTempQuality, Double adjTempError,
			Double jackTemp, int jackTempQuality, Double adjJackTemp, int adjJackTempQuality, Double adjJackTempError){
		
		super();
		
		setSalinity(sal);
		setSalinityQuality(salQuality);
				
		setSalinityAdjusted(adjSal);
		setSalinityAdjustedQuality(adjSalQuality);
		setSalinityAdjustedError(adjSalError);
				
		setTemperature(temp);
		setTemperatureQuality(tempQuality);
		
		setTemperatureAdjusted(adjTemp);
		setTemperatureAdjustedQuality(adjTempQuality);
		setTemperatureAdjustedError(adjTempError);
				
		setJacketTemperature(jackTemp);
		setJacketTemperatureQuality(jackTempQuality);
							
		setJacketTemperatureAdjusted(adjJackTemp);
		setJacketTemperatureAdjustedQuality(adjJackTempQuality);
		setJacketTemperatureAdjustedError(adjJackTempError);
				
	}

	public Valeur getSST(){
		return ValeurFactory.getSST(this);
	}
	
	public Valeur getSSS(){
		return ValeurFactory.getSSS(this);
	}
	
	public Double getSalinity() {
		return salinity;
	}

	public void setSalinity(Double salinity) {
		this.salinity = salinity;
	}

	public Integer getSalinityQuality() {
		return salinityQuality;
	}

	public void setSalinityQuality(Integer salinityQuality) {
		this.salinityQuality = salinityQuality;
	}

	public Double getSalinityAdjusted() {
		return salinityAdjusted;
	}

	public void setSalinityAdjusted(Double salinityAdjusted) {
		this.salinityAdjusted = salinityAdjusted;
	}

	public Integer getSalinityAdjustedQuality() {
		return salinityAdjustedQuality;
	}

	public void setSalinityAdjustedQuality(Integer salinityAdjustedQuality) {
		this.salinityAdjustedQuality = salinityAdjustedQuality;
	}

	public Double getSalinityAdjustedError() {
		return salinityAdjustedError;
	}

	public void setSalinityAdjustedError(Double salinityAdjustedError) {
		this.salinityAdjustedError = salinityAdjustedError;
	}

	public Double getTemperature() {
		return temperature;
	}

	public void setTemperature(Double temperature) {
		this.temperature = temperature;
	}

	public Integer getTemperatureQuality() {
		return temperatureQuality;
	}

	public void setTemperatureQuality(Integer temperatureQuality) {
		this.temperatureQuality = temperatureQuality;
	}

	public Double getTemperatureAdjusted() {
		return temperatureAdjusted;
	}

	public void setTemperatureAdjusted(Double temperatureAdjusted) {
		this.temperatureAdjusted = temperatureAdjusted;
	}

	public Integer getTemperatureAdjustedQuality() {
		return temperatureAdjustedQuality;
	}

	public void setTemperatureAdjustedQuality(Integer temperatureAdjustedQuality) {
		this.temperatureAdjustedQuality = temperatureAdjustedQuality;
	}

	public Double getTemperatureAdjustedError() {
		return temperatureAdjustedError;
	}

	public void setTemperatureAdjustedError(Double temperatureAdjustedError) {
		this.temperatureAdjustedError = temperatureAdjustedError;
	}

	public Double getJacketTemperature() {
		return jacketTemperature;
	}

	public void setJacketTemperature(Double jacketTemperature) {
		this.jacketTemperature = jacketTemperature;
	}

	public Integer getJacketTemperatureQuality() {
		return jacketTemperatureQuality;
	}

	public void setJacketTemperatureQuality(Integer jacketTemperatureQuality) {
		this.jacketTemperatureQuality = jacketTemperatureQuality;
	}

	public Double getJacketTemperatureAdjusted() {
		return jacketTemperatureAdjusted;
	}

	public void setJacketTemperatureAdjusted(Double jacketTemperatureAdjusted) {
		this.jacketTemperatureAdjusted = jacketTemperatureAdjusted;
	}

	public Integer getJacketTemperatureAdjustedQuality() {
		return jacketTemperatureAdjustedQuality;
	}

	public void setJacketTemperatureAdjustedQuality(
			Integer jacketTemperatureAdjustedQuality) {
		this.jacketTemperatureAdjustedQuality = jacketTemperatureAdjustedQuality;
	}

	public Double getJacketTemperatureAdjustedError() {
		return jacketTemperatureAdjustedError;
	}

	public void setJacketTemperatureAdjustedError(
			Double jacketTemperatureAdjustedError) {
		this.jacketTemperatureAdjustedError = jacketTemperatureAdjustedError;
	}
		
}
