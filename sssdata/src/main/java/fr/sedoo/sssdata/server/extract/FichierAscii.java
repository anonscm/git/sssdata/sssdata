package fr.sedoo.sssdata.server.extract;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Date;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import fr.sedoo.sssdata.server.dao.gosud.GosudUtils;
import fr.sedoo.sssdata.server.model.Measure;

public class FichierAscii extends FichierSortie {
	
	public static final int LAT_COLUMN_SIZE = 8;
	public static final int LON_COLUMN_SIZE = 9;
	public static final int DATA_COLUMN_SIZE = 7;
	
	private static Logger logger = Logger.getLogger(FichierAscii.class);
	
	//TODO Ajouter aux paramètres configurables ?
	public static final String SEPARATOR = ";";
	public final static String EXTENSION = ".txt";
	
	
	public static final String ENCODING = "UTF-8";
			
	private PrintStream out;
		
	public FichierAscii(String filename, FichierSortieHeader header) {
		super(filename, header);
	}

	@Override
	protected void open() throws IOException{
		logger.debug("open()");
		this.out = new PrintStream(new FileOutputStream(getFile()), true, ENCODING);		
	}

	@Override
	protected void close() {
		logger.debug("close()");
		out.close();
	}

	@Override
	protected void printHeader() {
		logger.debug("printHeader()");
		out.println("# Data extracted from SSS database");
		out.println("# Date: " + new Date());
		out.println("#");
		out.println("# Title: " + getHeaderInfos().getTitle());
		out.println("# Summary: " + getHeaderInfos().getSummary());
		out.println("#");
		out.println("# DOI: " + getHeaderInfos().getDoi());
		if (getHeaderInfos().getCitation() != null){
			out.println("# Citation: " + getHeaderInfos().getCitation());
		}
		out.println("#");
		out.println("# Project: " + getHeaderInfos().getProjectName() + 
				((getHeaderInfos().getProjectUrl() != null && !getHeaderInfos().getProjectUrl().isEmpty())?" ("+getHeaderInfos().getProjectUrl()+")":""));
		out.println("# Contact: " + getHeaderInfos().getContact());
		out.println("# Platform name: " + getHeaderInfos().getBoatName());
		out.println("# License: "  + getHeaderInfos().getUseConstraints());
		out.println("# Acknowledgement: " + getHeaderInfos().getAcknowledgment());
		out.println("#");
		out.println("# SST: " + SST_VAR_NAME + " (unit: " + SST_VAR_UNIT + ", missing value: " + MISSING_VALUE + ")");
		out.println("# SSS: " + SSS_VAR_NAME + " (unit: " + SSS_VAR_UNIT + ", missing value: " + MISSING_VALUE + ")");
		if (getHeaderInfos().getSspsDeph() != null){
			out.println("#");
			out.println("# Depth of water intake for salinity measurement (meter): ");
			out.println("# Nominal: " + getHeaderInfos().getSspsDeph());
			out.println("# Minimum: " + getHeaderInfos().getSspsDephMin());
			out.println("# Maximum: " + getHeaderInfos().getSspsDephMax());
		}
		out.println("#");
		out.println("# Flags (XXX_QC): ");
		for (Map.Entry<Integer, String> entry : GosudUtils.getFlags().entrySet()) {
			out.println("# " + entry.getKey() + ": " + entry.getValue());
		}
		out.println("#");
		writeColumnHeader();
	}

	private void writeColumnHeader(){
		out.println("# DATE;LON;LAT;SST (" + SST_VAR_UNIT_SHORT + ");SST_QC;SST_ERROR(" + SST_VAR_UNIT_SHORT + ");SSS (" + SSS_VAR_UNIT + ");SSS_QC;SSS_ERROR (" + SSS_VAR_UNIT + ")");
	}
	
	private String getValueOrMissing(Double value){
		if (value == null){
			return ""+MISSING_VALUE;
		}else{
			return StringUtils.leftPad(dfDat.format(value),DATA_COLUMN_SIZE);
		}
	}
			
	private int getFlagOrMissing(Integer flag){
		if (flag == null){
			return MISSING_FLAG;
		}else{
			return flag;
		}
	}
		
	@Override
	protected void writeMesure(Measure mesure){
		StringBuffer sb = new StringBuffer(sdf.format(mesure.getMeasureDate()));
		sb.append(SEPARATOR);
		sb.append(StringUtils.leftPad(dfLon.format(mesure.getLongitude()),LON_COLUMN_SIZE));
		sb.append(SEPARATOR);
		sb.append(StringUtils.leftPad(dfLat.format(mesure.getLatitude()),LAT_COLUMN_SIZE));
		sb.append(SEPARATOR);
		
		sb.append(getValueOrMissing(mesure.getTemperature()));
		sb.append(SEPARATOR);
		sb.append(getFlagOrMissing(mesure.getTemperatureQuality()));
		sb.append(SEPARATOR);
		sb.append(getValueOrMissing(mesure.getTemperatureError()));
		sb.append(SEPARATOR);
		
		sb.append(getValueOrMissing(mesure.getSalinity()));
		sb.append(SEPARATOR);
		sb.append(getFlagOrMissing(mesure.getSalinityQuality()));
		sb.append(SEPARATOR);
		sb.append(getValueOrMissing(mesure.getSalinityError()));

		out.println(sb.toString());
	}
	
	
	@Override
	protected String getExtension() {
		return EXTENSION;
	}

}
