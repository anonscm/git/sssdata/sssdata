package fr.sedoo.sssdata.server.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.sedoo.sssdata.server.dao.api.MetadataHistoryDAO;
import fr.sedoo.sssdata.server.model.MetadataHistory;

@Repository
public class MetadataHistoryDAOJPAImpl implements MetadataHistoryDAO{

	protected EntityManager em;
	public EntityManager getEntityManager() {
		return em;
	}

	@PersistenceContext
	public void setEntityManager(EntityManager entityManager) {
		this.em = entityManager;
	}

	@Override
	@Transactional
	public ArrayList<MetadataHistory> findAll() {
		List<MetadataHistory> aux = em.createQuery("select item from MetadataHistory item").getResultList();
        ArrayList<MetadataHistory> result = new ArrayList<MetadataHistory>();
        result.addAll(aux);
		return result;
	}
	
	@Transactional
	public void delete(Integer id) {
		MetadataHistory aux = getEntityManager().find(MetadataHistory.class, id);
		if (aux != null){
			getEntityManager().remove(aux);
		}
	}
	
	@Transactional
	public MetadataHistory save(MetadataHistory item) {
		if (item.getId() == null){
			getEntityManager().persist(item);
			return item;
		}else{
			getEntityManager().merge(item);
			return item;
		}
	}


}