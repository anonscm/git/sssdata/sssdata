package fr.sedoo.sssdata.server.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name=Boat.TABLE_NAME)
public class Boat implements Comparable<Boat>{

	public final static String TABLE_NAME ="BOAT";
	public static final String ID_COLUMN_NAME = "ID";
	public static final String NAME_COLUMN_NAME = "NAME";
	
	@Id
	@Column(name=Boat.ID_COLUMN_NAME)
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@Column(name=Boat.NAME_COLUMN_NAME)
	private String name;
	
	@Column(name="CODE")
	private String code;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="boat", fetch=FetchType.LAZY)
	private List<SssFile> files;
		
	
	public Boat() {
		super();
		files = new ArrayList<SssFile>();
	}
	
	
	public void setFiles(List<SssFile> files) {
		this.files = files;
	}
    public List<SssFile> getFiles(){
		return files;
	}
    
    

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

/*	@Override
	public String getIdentifier() {
		return ""+getId();
	}

	@Override
	public List<ValidationAlert> validate() {
		return new ArrayList<ValidationAlert>();
	}*/
	
	
	@Override
	public int compareTo(Boat o) {
		return this.getName().compareTo(o.getName());
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
            return false;
        }
		if (obj.getClass() != this.getClass()){
			return false;
		}
		Boat b = (Boat) obj;
		return this.getName().equals(b.getName());
	}
	
	public void addFile(SssFile file){
		this.files.add(file);
		file.setBoat(this);
	}
	
	public SssFile getFileByName(String name){
		for (SssFile f : this.files){
			if (f.getName().equals(name)){
				return f;
			}
		}
		return null;
	}


	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}

	

	
	
}
