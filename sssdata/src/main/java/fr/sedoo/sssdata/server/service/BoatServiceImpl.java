package fr.sedoo.sssdata.server.service;

import java.util.ArrayList;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.sedoo.sssdata.client.service.BoatService;
import fr.sedoo.sssdata.client.service.ServiceException;
import fr.sedoo.sssdata.server.SSSDataApplication;
import fr.sedoo.sssdata.server.dao.api.BoatDAO;
import fr.sedoo.sssdata.server.model.Boat;
import fr.sedoo.sssdata.shared.BoatDTO;

public class BoatServiceImpl extends RemoteServiceServlet implements BoatService{

	private BoatDAO dao;
	
	private void initDao()
	{
		if (dao == null)
		{
			dao = (BoatDAO) SSSDataApplication.getSpringBeanFactory().getBeanByName(BoatDAO.BEAN_NAME);
		}
	}
	
	@Override
	public ArrayList<BoatDTO> findAll() throws ServiceException {
		initDao();
		ArrayList<BoatDTO> list = new ArrayList<BoatDTO>();
		for (Boat b : dao.findAll()){
			BoatDTO boat = new BoatDTO();
			boat.setName(b.getName());
			list.add(boat);
		}
		return list;
	}
	
	/*
	public Boat findByName(String name) throws ServiceException {
		initDao();
		return dao.findByName(name);
	}

	public BoatDTO findAllFiles(String boatName) {
		initDao();
		return dao.findAllFiles(boatName);
	}

	
	
	public void add(Boat b) throws ServiceException {
		initDao();
		dao.save(b);
	}
	
	public void update(Boat b) {
		initDao();
		dao.save(b);
	}*/
}
