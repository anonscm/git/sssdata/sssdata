package fr.sedoo.sssdata.server.service;

import org.apache.log4j.Logger;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.sedoo.sssdata.client.service.MetadataService;
import fr.sedoo.sssdata.client.service.ServiceException;
import fr.sedoo.sssdata.server.SSSDataApplication;
import fr.sedoo.sssdata.server.dao.api.MetadataDAO;
import fr.sedoo.sssdata.server.metadata.MetadataBuilder;
import fr.sedoo.sssdata.server.metadata.SSSDataciteDAO;
import fr.sedoo.sssdata.server.model.Metadata;
import fr.sedoo.sssdata.shared.metadata.MetadataDTO;

public class MetadataServiceImpl extends RemoteServiceServlet implements MetadataService{

	private static Logger LOG = Logger.getLogger(MetadataServiceImpl.class);

	MetadataDAO metadataDAO;
	SSSDataciteDAO dataciteDAO;
	
	private void initDao(){
		if (metadataDAO == null){
			metadataDAO = (MetadataDAO) SSSDataApplication.getSpringBeanFactory().getBeanByName(MetadataDAO.BEAN_NAME);
		}
		if (dataciteDAO == null){
			dataciteDAO = (SSSDataciteDAO) SSSDataApplication.getSpringBeanFactory().getBeanByName(SSSDataciteDAO.BEAN_NAME);
		}
	}

	@Override
	public MetadataDTO getMetadata(String doiSuffix) throws ServiceException {
		initDao();

		//String doi = dataciteDAO.getDoi();
		String doi =  dataciteDAO.getConf().getPrefix() + doiSuffix;
		
		Metadata metadata = metadataDAO.findByDoi(doi);
		if (metadata == null){
			String xml = dataciteDAO.getXmlMetadata(doi);
			metadata = new Metadata();
			metadata.setDoi(doi);
			metadata.setXml(xml);
			metadataDAO.save(metadata);
		}
		MetadataDTO dto = MetadataBuilder.buildDTO(metadata);
		String citation = dataciteDAO.getCitation(doi, true);		
		dto.setCitation(citation);
		
		return dto;
	}

	@Override
	public MetadataDTO updateMetadata(String doiSuffix) throws ServiceException {
		initDao();
		//String doi = dataciteDAO.getDoi();
		String doi =  dataciteDAO.getConf().getPrefix() + doiSuffix;
		String xml = dataciteDAO.getXmlMetadata(doi);
		
		Metadata metadata = metadataDAO.findByDoi(doi);	
		if (metadata == null){
			metadata = new Metadata();
			metadata.setDoi(doi);
		}
		metadata.setXml(xml);
		metadataDAO.save(metadata);
		
		MetadataDTO dto = MetadataBuilder.buildDTO(metadata);
		String citation = dataciteDAO.getCitation(doi);		
		dto.setCitation(citation);
		
		return dto;
	}


	

}
