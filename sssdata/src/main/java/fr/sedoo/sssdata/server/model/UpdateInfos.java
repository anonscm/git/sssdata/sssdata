package fr.sedoo.sssdata.server.model;

import java.util.Date;

public class UpdateInfos {

	private Date dateMin;
	private Date dateMax;
	private Date dateModification;
	private Date dateCreation;
	private Date dateUpdate;
			
	public UpdateInfos(Date dateModification, Date dateCreation, Date dateUpdate, Date dateMin, Date dateMax) {
		super();
		this.dateModification = dateModification;
		this.dateMin = dateMin;
		this.dateMax = dateMax;
		this.dateCreation = dateCreation;
		this.dateUpdate = dateUpdate;
	}
	public Date getDateMin() {
		return dateMin;
	}
	public void setDateMin(Date dateMin) {
		this.dateMin = dateMin;
	}
	public Date getDateMax() {
		return dateMax;
	}
	public void setDateMax(Date dateMax) {
		this.dateMax = dateMax;
	}
	public Date getDateModification() {
		return dateModification;
	}
	public void setDateModification(Date dateModification) {
		this.dateModification = dateModification;
	}
	public Date getDateCreation() {
		return dateCreation;
	}
	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}
	public Date getDateUpdate() {
		return dateUpdate;
	}
	public void setDateUpdate(Date dateUpdate) {
		this.dateUpdate = dateUpdate;
	}
	
}
