package fr.sedoo.sssdata.server.misc;

public class DownloadConfig {
	
	public static final String BEAN_NAME = "downloadConfig";
	
	private String root;
	
	public DownloadConfig() {
		super();
	}
	
	public void setRoot(String root) {
		this.root = root;
	}

	public String getRoot() {
		return root;
	}
}
