package fr.sedoo.sssdata.server.metadata;

import org.springframework.beans.factory.annotation.Autowired;

import fr.sedoo.metadata.doi.DOIConfig;
import fr.sedoo.metadata.doi.DataciteDAOImplBase;


public class SSSDataciteDAOImpl extends DataciteDAOImplBase implements SSSDataciteDAO {

	@Autowired
	DOIConfig conf;

	public DOIConfig getConf(){
		return conf;
	}

}
