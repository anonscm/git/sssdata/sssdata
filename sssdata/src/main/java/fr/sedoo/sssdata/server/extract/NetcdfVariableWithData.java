package fr.sedoo.sssdata.server.extract;

import java.io.IOException;
import java.util.Date;

import ucar.ma2.Array;
import ucar.ma2.ArrayDouble;
import ucar.ma2.ArrayFloat;
import ucar.ma2.ArrayInt;
import ucar.nc2.NetcdfFileWriter;
import ucar.nc2.Variable;

public abstract class NetcdfVariableWithData {

	private Variable var;
	private int size;
	
	protected int index;
	
	public NetcdfVariableWithData(Variable var, int size) {
		this.var = var;
		this.size = size;
		this.index = 0;
	}
	
	public void write(NetcdfFileWriter writer) throws IOException{
		try{
			writer.write(getVar(), getData());
		}catch(Exception e){
			throw new IOException("Error while writing variable " + getVar().getFullName(), e);
		}

	}

	public int getSize() {
		return size;
	}
	
	public Variable getVar() {
		return var;
	}
			
	public abstract Array getData();
	
	public static class Time extends NetcdfVariableWithData{
		
		private long referenceTime;
		
		public Time(Variable var, int size, long referenceTime) {
			super(var, size);
			this.data = new ArrayDouble.D1(getSize());
			this.referenceTime = referenceTime;
		}

		private ArrayDouble.D1 data;
		
		public void addValue(Date value){
			if (value == null){
				data.set(index++, FichierNetcdf.MISSING_VALUE);	
			}else{
				long time = value.getTime();
				data.set(index++, (time - referenceTime) / (1000.0 * 3600.0 * 24.0));	
			}
			
		}
		
		@Override
		public Array getData() {
			return data;
		}
	}
	
	public static class Param extends NetcdfVariableWithData{
		
		public Param(Variable var, int size) {
			super(var, size);
			this.data = new ArrayFloat.D1(getSize());
		}

		private ArrayFloat.D1 data;
		
		public void addValue(Double value){
			if (value == null){
				data.set(index++, FichierNetcdf.MISSING_VALUE);	
			}else{
				data.set(index++, value.floatValue());	
			}
			
		}
		/*
		public void addValue(Float value){
			if (value == null){
				data.set(index++, FichierNetcdf.MISSING_VALUE);	
			}else{
				data.set(index++, value.floatValue());	
			}
			
		}*/
		
		@Override
		public Array getData() {
			return data;
		}
	}
	
	public static class Flag extends NetcdfVariableWithData{
		
		public Flag(Variable var, int size) {
			super(var, size);
			this.data = new ArrayInt.D1(getSize());
		}

		private ArrayInt.D1 data;
		
		public void addValue(Integer value){
			if (value == null){
				data.set(index++, FichierNetcdf.MISSING_FLAG);	
			}else{
				data.set(index++, value.intValue());	
			}
			
		}
		
		@Override
		public Array getData() {
			return data;
		}
	}
	
}
