package fr.sedoo.sssdata.server.dao.api;

import fr.sedoo.sssdata.server.model.Archive;

public interface ArchiveDAO {
	public final static String BEAN_NAME="archiveDAO";
	
	Archive save(Archive a);
	Archive findByName(String name);
	
	Archive findLastByFlagAndFormat(String flag, String format);
}
