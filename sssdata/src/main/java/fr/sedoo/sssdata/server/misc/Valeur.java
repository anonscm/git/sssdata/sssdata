package fr.sedoo.sssdata.server.misc;


/**
 * Représente une valeur avec flag qualité et erreur.
 * @author brissebr
 *
 */
public class Valeur {

	private Double valeur;
	private Integer quality;
	private Double error;
	
	public Valeur() {
		super();
		this.valeur = null;
		this.quality = null;
		this.error = null;
	}

	public boolean isNull(){
		return valeur == null;
	}
	
	public Double getValeur() {
		return valeur;
	}

	public void setValeur(Double valeur) {
		this.valeur = valeur;
	}

	public Integer getQuality() {
		return quality;
	}

	public void setQuality(Integer quality) {
		this.quality = quality;
	}

	public Double getError() {
		return error;
	}

	public void setError(Double error) {
		this.error = error;
	}
	
}
