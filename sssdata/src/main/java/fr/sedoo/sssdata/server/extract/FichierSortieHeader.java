package fr.sedoo.sssdata.server.extract;

import javax.persistence.Column;

public class FichierSortieHeader {

	private String title;
	private String summary;	
	private String projectName;
	private String projectUrl;
	
	private String doi;	
	private String citation;
	
	private String contact;
	
	private String boatName;
	private String useConstraints;
	private String acknowledgment;
			
	private Double sspsDeph;
	private Double sspsDephMin;	
	private Double sspsDephMax;
	
	public FichierSortieHeader() {
		super();
	}
	
	public String getBoatName() {
		return boatName;
	}
	public void setBoatName(String boatName) {
		this.boatName = boatName;
	}
	public void setUseConstraints(String useConstraints) {
		this.useConstraints = useConstraints;
	}
	public String getUseConstraints() {
		return useConstraints;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAcknowledgment() {
		return acknowledgment;
	}
	public void setAcknowledgment(String acknowledgment) {
		this.acknowledgment = acknowledgment;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getProjectUrl() {
		return projectUrl;
	}
	public void setProjectUrl(String projectUrl) {
		this.projectUrl = projectUrl;
	}
	public String getDoi() {
		return doi;
	}
	public void setDoi(String doi) {
		this.doi = doi;
	}
	public String getCitation() {
		return citation;
	}
	public void setCitation(String citation) {
		this.citation = citation;
	}

	public Double getSspsDeph() {
		return sspsDeph;
	}

	public void setSspsDeph(Double sspsDeph) {
		this.sspsDeph = sspsDeph;
	}

	public Double getSspsDephMin() {
		return sspsDephMin;
	}

	public void setSspsDephMin(Double sspsDephMin) {
		this.sspsDephMin = sspsDephMin;
	}

	public Double getSspsDephMax() {
		return sspsDephMax;
	}

	public void setSspsDephMax(Double sspsDephMax) {
		this.sspsDephMax = sspsDephMax;
	}
	
}
