package fr.sedoo.sssdata.server.service;

import java.text.ParseException;

import java.util.List;

import org.apache.log4j.Logger;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;


import fr.sedoo.sssdata.client.service.ArchiveGeneratorService;
import fr.sedoo.sssdata.server.ArchiveGenerator;
import fr.sedoo.sssdata.server.SSSDataApplication;
import fr.sedoo.sssdata.shared.ArchiveGeneratorRequest;
import fr.sedoo.sssdata.shared.BoatDTO;
import fr.sedoo.sssdata.shared.FileDTO;

public class ArchiveGeneratorServiceImpl extends RemoteServiceServlet implements ArchiveGeneratorService{

	private static Logger logger = Logger.getLogger(ArchiveGeneratorServiceImpl.class);


	private ArchiveGenerator archiveGenerator;

	private void initArchiveGenerator(){
		if (archiveGenerator == null){
			archiveGenerator = (ArchiveGenerator) SSSDataApplication
					.getSpringBeanFactory().getBeanByName(
							ArchiveGenerator.BEAN_NAME);
		}
	}

	@Override
	public List<ArchiveGeneratorRequest> initRequests() throws ParseException {
		initArchiveGenerator();
		return archiveGenerator.init();
	}

	@Override
	public String createArchive(ArchiveGeneratorRequest request, List<FileDTO> temporaryFiles) throws Exception {
		initArchiveGenerator();
		return archiveGenerator.createArchive(request, temporaryFiles);
	}
	@Override
	public List<FileDTO> getArchiveFilesByBoat(ArchiveGeneratorRequest request, BoatDTO boat) throws Exception {
		initArchiveGenerator();
		return archiveGenerator.getArchiveFilesByBoat(request, boat);
	}

}
