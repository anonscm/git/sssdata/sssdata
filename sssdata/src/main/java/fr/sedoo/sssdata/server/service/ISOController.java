package fr.sedoo.sssdata.server.service;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.sedoo.metadata.core.ContactBuilder;
import fr.sedoo.metadata.odatis.OdatisDataCenter;
import fr.sedoo.metadata.odatis.OdatisDataset;
import fr.sedoo.metadata.odatis.OdatisDatasetBuilder;
import fr.sedoo.metadata.odatis.OdatisDatasetType;
import fr.sedoo.metadata.odatis.OdatisTheme;
import fr.sedoo.metadata.odatis.OdatisUtils;
import fr.sedoo.metadata.doi.DOIConfig;
import fr.sedoo.metadata.doi.domain.Creator;
import fr.sedoo.sssdata.server.dao.api.MetadataDAO;
import fr.sedoo.sssdata.server.metadata.MetadataConfig;
import fr.sedoo.sssdata.server.model.Metadata;
import fr.sedoo.sssdata.shared.GriddedProduct;

@Controller
@RequestMapping(value = "/iso19139")
public class ISOController {

	private static Logger logger = Logger.getLogger(ISOController.class);
	
	@Autowired
	MetadataDAO metadataDAO;
	@Autowired
	MetadataConfig metadataConfig;
	@Autowired
	DOIConfig sssdoiConfig;
	
	private OdatisDataset buildMetadata(String doi) throws Exception{		
		Metadata sssMetadata = metadataDAO.findByDoi(doi);
		if (sssMetadata == null){
			logger.error("No metadata");
			return null;
		}
		
		fr.sedoo.metadata.doi.domain.Metadata doiMetadata = fr.sedoo.metadata.doi.utils.MetadataUtils.readXml(sssMetadata.getXml());
		
		//MetadataDTO dto = MetadataBuilder.buildDTO(sssMetadata);
		UUID uuid =  UUID.nameUUIDFromBytes(doi.getBytes());
				
		
		//TODO contacts
						
		OdatisDatasetBuilder builder = new OdatisDatasetBuilder(uuid.toString(), doiMetadata.getTitle(), doiMetadata.getSummary());
						
		builder.odatisDatasetType(OdatisDatasetType.NETWORKS).source("SNO SSS").dataset()
		.ship()
		.ocean()
		.english()
		.lineage("Data produced by the SNO SSS.")
		.useConstraints(doiMetadata.getUseConstraints())
		.useConstraints("The data are provided without any warranty for any non-profit educational and research purposes.")
		.dataPolicy("SSS data policy", "http://sss.sedoo.fr/#CMSConsultPlace:Data%20Policy", false)
		.annualUpdate().publicationDate(doiMetadata.getPublicationYear())
		.sedoo()
		.planned()
		.doi(doi)
		.coordinateReferenceSystem("EPSG:4326")
		.temporalExtent(doiMetadata.getDateBegin(), doiMetadata.getDateEnd())
		//.temporalExtent(java.sql.Date.valueOf(doiMetadata.getDateBegin()), java.sql.Date.valueOf(doiMetadata.getDateEnd()))
		//.boundings(-68, 77, -180, 180)
		.linkURL("http://sss.sedoo.fr/", "SSS data base")
		.project("SNO SSS")
		.parameter("Salinity", "SALINITY", "psu")
		//
		//.gcmdScienceKeyword("EARTH SCIENCE > OCEANS > SALINITY/DENSITY > SALINITY")
		//.gcmdScienceKeyword("EARTH SCIENCE > OCEANS > OCEAN TEMPERATURE > SEA SURFACE TEMPERATURE")
		.gcmdLocationKeyword("VERTICAL LOCATION > SEA SURFACE")
		.gcmdLocationKeyword("OCEAN")
		;
		
		//Contacts
		Map<String, ContactBuilder> contacts = new HashMap<>();
		contacts.put("Alory Gaël", new ContactBuilder().name("Gaël Alory").email("gael.alory@legos.obs-mip.fr"));
		contacts.put("Delcroix Thierry", new ContactBuilder().name("Thierry Delcroix").email("thierry.delcroix@legos.obs-mip.fr"));
		contacts.put("Téchiné Philippe", new ContactBuilder().name("Philippe Téchiné").email("Philippe.Techine@legos.obs-mip.fr"));
		contacts.put("Kestenare Elodie", new ContactBuilder().name("Elodie Kestenare ").email("elodie.kestenare@legos.obs-mip.fr"));
		contacts.put("Reverdin Gilles", new ContactBuilder().name("Gilles Reverdin").email("Gilles.Reverdin@locean-ipsl.upmc.fr"));
		contacts.put("Cravatte Sophie", new ContactBuilder().name("Sophie Cravatte ").email("sophie.cravatte@ird.fr"));
		  
		
		for (Creator c: doiMetadata.getCreators()){
			if (contacts.containsKey(c.getName())){
				builder.contact(contacts.get(c.getName()).organisation(c.getAffiliation()).author().build());
			}else{
				builder.contact(new ContactBuilder().name(c.getName()).organisation(c.getAffiliation()).author().build());
			}
		}
		
		//TODO boundings
		if ( "10.6096/SSS-LEGOS-GRID-ATL".equalsIgnoreCase(doi) ){
			builder.boundings(-30, 50, -95, 20).gcmdLocationKeyword("OCEAN > ATLANTIC OCEAN");
			builder.odatisTheme(OdatisTheme.VA)
			.thumbnail("http://sss.sedoo.fr/webContent/img/grid_atl.png")
			.downloadURL("http://sss.sedoo.fr/#GriddedProductMetadataPlace:" + GriddedProduct.GRID_ATLANTIC, "Data access")
			.wmsURL("http://twodoo.sedoo.fr/thredds/wms/sss/gsss_atl_1970-2016_v2.0.nc?service=WMS&version=1.3.0", "sss");
		}else if ( "10.6096/SSS-LEGOS-GRID-PAC".equalsIgnoreCase(doi) ){
			builder.boundings(-30, 30, 120, 290).gcmdLocationKeyword("OCEAN > PACIFIC OCEAN");
			builder.odatisTheme(OdatisTheme.VA)
			.thumbnail("http://sss.sedoo.fr/webContent/img/grid_pac.jpg")
			.downloadURL("http://sss.sedoo.fr/#GriddedProductMetadataPlace:" + GriddedProduct.GRID_PACIFIC, "Data access")
			.wmsURL("http://twodoo.sedoo.fr/thredds/wms/sss/gsss_pac_1950-2009_v1.0.nc?service=WMS&version=1.3.0", "SSS");
			
		}else if ( "10.6096/HIST-SSS-LEGOS".equalsIgnoreCase(doi) ){
			builder.boundings(-30, 30, -180, 180).gcmdLocationKeyword("OCEAN").gcmdLocationKeyword("GEOGRAPHIC REGION > TROPICS");
			builder.odatisTheme(OdatisTheme.OBS)
			.parameter("Temperature", "SEA SURFACE TEMPERATURE", "°C")
			.thumbnail("http://sss.sedoo.fr/webContent/img/map_sss_hist.png")
			.downloadURL("http://sss.sedoo.fr/#GriddedProductMetadataPlace:" + GriddedProduct.SSS_HIST, "Data access");			
		}else if ( "10.6096/SSS-BIN-NASG".equalsIgnoreCase(doi) ){
			builder.odatisTheme(OdatisTheme.VA)
				.parameter("Temperature", "SEA SURFACE TEMPERATURE", "°C")
				.parameter("Density", "DENSITY", "kg/m3")
				.downloadURL("http://sss.sedoo.fr/#GriddedProductMetadataPlace:" + GriddedProduct.GRID_NASG, "Data access")
				.thumbnail("http://sss.sedoo.fr/webContent/img/map_sss_bin_nasg.png")
				.boundings(-20, 70, -75, 10).gcmdLocationKeyword("OCEAN > ATLANTIC OCEAN > NORTH ATLANTIC OCEAN")
				;
		}else if ( "10.6096/SSS-BINS-ATL".equalsIgnoreCase(doi) ){
			builder.odatisTheme(OdatisTheme.VA)
			.downloadURL("http://sss.sedoo.fr/#GriddedProductMetadataPlace:" + GriddedProduct.GRID_BIN_ATL, "Data access")
			.thumbnail("http://sss.sedoo.fr/webContent/img/map_sss_bin_atl.png")
			.gcmdLocationKeyword("OCEAN > ATLANTIC OCEAN")
			;
	}else if ( "10.6096/TSD-BINS-NASPG".equalsIgnoreCase(doi) ){
			builder.odatisTheme(OdatisTheme.VA)
			.parameter("Temperature", "SEA SURFACE TEMPERATURE", "°C")
			.parameter("Density", "DENSITY", "kg/m3")
			.downloadURL("http://sss.sedoo.fr/#GriddedProductMetadataPlace:" + GriddedProduct.GRID_NASPG, "Data access")
			.thumbnail("http://sss.sedoo.fr/webContent/img/tsd_bin_naspg.png")
			.gcmdLocationKeyword("OCEAN > ATLANTIC OCEAN > NORTH ATLANTIC OCEAN")
			;
		}else {
			builder.odatisDataCenter(OdatisDataCenter.CORIOLIS)
				.boundings(-68, 77, -180, 180).instrument(null, null, "THERMOSALINOGRAPHS").parameter("Temperature", "SEA SURFACE TEMPERATURE", "°C")
				.thumbnail("http://sss.sedoo.fr/webContent/img/sss_map.jpg")
				.downloadURL("http://sss.sedoo.fr/#MetadataPlace:SSS-LEGOS", "Data access");
			
			/*builder.clearContacts();
			builder.contact(contacts.get("Alory Gaël").organisation("LEGOS").build());
			builder.contact(contacts.get("Delcroix Thierry").organisation("LEGOS").build());
			builder.contact(contacts.get("Téchiné Philippe").organisation("LEGOS").build());*/
			builder.odatisTheme(OdatisTheme.OBS);
		}
		
				
		for (String format: doiMetadata.getDataFormats()){
			builder.format(format);
		}
				
		 		
		//builder.logo("http://www.legos.obs-mip.fr/++theme++legos.www/images/logo.png");
		return builder.build();
	}
	
	@RequestMapping
	public ModelAndView getMetadata(HttpServletRequest request, HttpServletResponse response, @RequestParam String doi) throws IOException {
		try{
			//Dataset metadata = buildMetadata("10.6096/SSS-LEGOS");
			OdatisDataset metadata = buildMetadata(doi);
			if (metadata != null){
				
				String xml = OdatisUtils.toISO19139(metadata);
				
				File exportPath = new File (metadataConfig.getExportPath());
				if (!exportPath.isDirectory()){
					exportPath = new File ("/tmp");
				}
				String exportFileName = StringUtils.replace(doi, sssdoiConfig.getPrefix(), "") + ".xml";
				String exportFile = exportPath.getAbsolutePath() + File.separator + exportFileName;
				PrintWriter out = new PrintWriter(exportFile);
				out.print(xml);
				out.close();
				
				//String xml = MetadataTools.toISO19139(metadata);
				/*System.out.println(xml);
				PrintWriter out = new PrintWriter("/home/brissebr/test_iso/sss.xml");
				out.print(xml);
				out.close();
				 */
				response.setContentType("application/xml");
				response.getOutputStream().write(xml.getBytes());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
}
