package fr.sedoo.sssdata.server.service;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.sedoo.commons.web.dao.VersionDAO;
import fr.sedoo.commons.web.servlet.ServletContextProvider;
import fr.sedoo.sssdata.client.service.ParameterService;
import fr.sedoo.sssdata.server.SSSDataApplication;
import fr.sedoo.sssdata.shared.ParameterConstants;

public class ParameterServiceImpl extends RemoteServiceServlet implements ParameterService{

	private static final String INFORMATION_NON_DISPONIBLE = "Not available";
	
	@Override
	public Map<String, String> getSystemParameters() {
		
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put(ParameterConstants.JAVA_VERSION_PARAMETER_NAME, System.getProperty("java.version"));
		VersionDAO versionDAO = (VersionDAO) SSSDataApplication.getSpringBeanFactory().getBeanByName(VersionDAO.VERSION_DAO_BEAN_NAME);
		parameters.put(ParameterConstants.APPLICATION_VERSION_PARAMETER_NAME, versionDAO.getVersion());
				
		parameters.put(ParameterConstants.TEST_MODE, getParameter(ParameterConstants.TEST_MODE));
		
		return parameters;
	}
	
	
	private String getParameter(String paramName){
		ServletContext context = ServletContextProvider.getContext();
		if (context == null){
			return INFORMATION_NON_DISPONIBLE;
		}
		String initParameter = context.getInitParameter(paramName);
		if (StringUtils.isEmpty(initParameter)){
			return INFORMATION_NON_DISPONIBLE;
		}else{
			return initParameter.trim();
		}
	}
	

}
