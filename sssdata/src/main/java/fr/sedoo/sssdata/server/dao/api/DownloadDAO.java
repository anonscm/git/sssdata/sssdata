package fr.sedoo.sssdata.server.dao.api;

import java.util.List;

import fr.sedoo.sssdata.server.model.Download;
import fr.sedoo.sssdata.server.model.User;
import fr.sedoo.sssdata.shared.Administrator;

public interface DownloadDAO {

	public final static String BEAN_NAME="downloadDAO";
	
	List<Download> getAllDownloads(boolean loadLinkedObjects);
	
	Download getDownload(Long id, boolean loadLinkedObjects);
	
	List<Download> getDownloads(User u, boolean loadLinkedObjects);

	Download getLastDownload(User u, boolean loadLinkedObjects);
	
	Download save(Download d, boolean loadLinkedObjects);
	void delete(Long id);
	
	List<Download> getDownloadsByMonth(int year, int month,
			boolean loadLinkedObjects);

	List<Download> getDownloadsByMonthLazyLoaded(int year, int month);
	
	
}
