package fr.sedoo.sssdata.server.misc;

import java.io.IOException;

import ucar.ma2.Array;
import ucar.ma2.IndexIterator;
import ucar.nc2.Attribute;
import ucar.nc2.Variable;

public class NetcdfVariableReader {

	private static final float DEFAULT_MISSING_VALUE = 99999;
	
	private Variable var;
	private Array data;
	private IndexIterator iterator;
	
	private float missingValue;
	
	public NetcdfVariableReader(Variable var) throws IOException {
		this.var = var;
		this.data = var.read();
		this.iterator = data.getIndexIterator();
		Attribute fillValueAttr = var.findAttribute("_FillValue");
		if (fillValueAttr != null){
			this.missingValue = fillValueAttr.getNumericValue().floatValue();
		}else{
			this.missingValue = DEFAULT_MISSING_VALUE;
		}
	}
		
	public Variable getVar() {
		return var;
	}
	
	/*public Float getNextFloat(){
		float v =  iterator.getFloatNext();
		if (v != missingValue){
			return v;
		}else{
			return null;
		}
	}*/
	
	public Double getNextDouble(){
		double v = iterator.getDoubleNext();
		if (v != missingValue){
			return v;
		}else{
			return null;
		}
	}
	
	public int getNextInt(){
		return  iterator.getIntNext();
	}
			
}
