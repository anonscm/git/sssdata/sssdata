package fr.sedoo.sssdata.server.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.sedoo.sssdata.server.dao.api.SssFileDAO;
import fr.sedoo.sssdata.server.model.Boat;
import fr.sedoo.sssdata.server.model.Download;
import fr.sedoo.sssdata.server.model.SssFile;
import fr.sedoo.sssdata.server.model.SssFileJournal;
import fr.sedoo.sssdata.server.model.UpdateInfos;
import fr.sedoo.sssdata.shared.Boundings;
import fr.sedoo.sssdata.shared.SearchCriterion;

@Repository
public class SssFileDAOJPAImpl implements SssFileDAO{

	protected EntityManager em;
	public EntityManager getEntityManager() {
		return em;
	}

	@PersistenceContext
	public void setEntityManager(EntityManager entityManager) {
		this.em = entityManager;
	}
	
	@Override
	@Transactional
	public void deleteSssFileFromName(String fileName) {
		Query query = em.createQuery("delete from SssFile sssFile where sssFile.name =:arg1");
		query.setParameter("arg1", fileName);
		query.executeUpdate();
	}

	@Override
	@Transactional
	public SssFile getSimpleFileByName(String name, boolean loadLinkedObjects) {
		Query query = em.createQuery("Select file from SssFile file where file.name=:arg1");
		query.setParameter("arg1", name);
		
		try{
			SssFile result = (SssFile) query.getSingleResult();
			if (loadLinkedObjects) {
				loadLinkedObjects(result);
			}
			return result;
		}
		catch (NoResultException e){
			return null;
		}
	}

	@Override
	@Transactional
	public SssFile getFileByNameAndBoat(String fileName, String boatName,
			boolean loadLinkedObjects) {
		Query query = em.createQuery("Select file from SssFile file where file.name=:arg1 and file.boat.name=:arg2");
		query.setParameter("arg1", fileName);
		query.setParameter("arg2", boatName);
		
		try{
			SssFile result = (SssFile) query.getSingleResult();
			if (loadLinkedObjects) {
				loadLinkedObjects(result);
			}
			return result;
		}
		catch (NoResultException e){
			return null;
		}
	}
	
	@Override
	@Transactional
	public List<SssFile> getFilesByBoat(String boatName, Date beginDate, Date endDate, boolean loadJournal) {
			
		String query = "SELECT file FROM SssFile file " +
			"WHERE file.boat.name = :boatName";
								
		if (beginDate != null){
			query += " AND file.dateEnd >= :dMin";
		}
		
		if (endDate != null){
			query += " AND file.dateStart <= :dMax";
		}
		
		Query q = em.createQuery(query);
		
		q.setParameter("boatName", boatName);
		if (beginDate != null){
			q.setParameter("dMin", beginDate, TemporalType.DATE);
		}
		if (endDate != null){
			q.setParameter("dMax", endDate, TemporalType.DATE);
		}
								
		try{
			List<SssFile> result = (List<SssFile>) q.getResultList();
			if (loadJournal){
				for (SssFile d : result) {
					Set<SssFileJournal> fileJournals = d.getFileJournal();
					if (fileJournals != null) {
						fileJournals.size();
					}
				}
			}
			/*if (loadLinkedObjects) {
				for (SssFile d : result) {
					loadLinkedObjects(d);
				}
			}*/
			return result;
		}
		catch (NoResultException e){
			return new ArrayList<SssFile>();
		}
	}
			
	@Override
	@Transactional
	public List<SssFile> getFilesByDates(Date beginDate, Date endDate,
			boolean loadLinkedObjects) {
		String query = "SELECT file FROM SssFile file ";
		String where = "";						

		if (beginDate != null){
			where += " AND file.dateEnd >= :dMin";
		}
		if (endDate != null){
			where += " AND file.dateStart <= :dMax";
		}
		if (where.trim().length() > 0){
			query += " WHERE " + where.trim().substring(3);
		}

		Query q = em.createQuery(query);
		if (beginDate != null){
			q.setParameter("dMin", beginDate, TemporalType.DATE);
		}
		if (endDate != null){
			q.setParameter("dMax", endDate, TemporalType.DATE);
		}

		try{
			List<SssFile> result = (List<SssFile>) q.getResultList();
			if (loadLinkedObjects) {
				for (SssFile d : result) {
					loadLinkedObjects(d);
				}
			}
			return result;
		}
		catch (NoResultException e){
			return new ArrayList<SssFile>();
		}
	}
	
	@Override
	@Transactional
	public List<SssFile> getFilesByBoat(String boatName, SearchCriterion sc,
			boolean loadLinkedObjects) {

		String query = "SELECT file FROM SssFile file " +
			"WHERE file.measuresNb > 0 AND file.boat.name = :boatName AND file.dateEnd >= :dMin AND file.dateStart <= :dMax ";
						
		String whereBbox = "";
		for (Boundings b: sc.getBoundings()){
			whereBbox += "OR (file.northLatitude >= " + b.getSouthBoundLatitude() 
					+ " AND file.southLatitude <= " + b.getNorthBoundLatitude()
					+ " AND file.eastLongitude >= " + b.getWestBoundLongitude()
					+ " AND file.westLongitude <= " + b.getEastBoundLongitude()  + ")";
		}
		query += "AND (" + whereBbox.substring(3) + ") ";
				
		Query q = em.createQuery(query);
		
		q.setParameter("boatName", boatName);
		q.setParameter("dMin", sc.getBeginDate(), TemporalType.DATE);
		q.setParameter("dMax", sc.getEndDate(), TemporalType.DATE);
								
		try{
			List<SssFile> result = (List<SssFile>) q.getResultList();
			if (loadLinkedObjects) {
				for (SssFile d : result) {
					loadLinkedObjects(d);
				}
			}
			return result;
		}
		catch (NoResultException e){
			return null;
		}
	}
	
	@Override
	@Transactional
	public SssFile save(SssFile file, boolean loadLinkedObjects) {
		if (file.getId() == null)
		{
			getEntityManager().persist(file);
			if (loadLinkedObjects) {
				loadLinkedObjects(file);
			}
			return file;
		}
		else
		{
			getEntityManager().merge(file);
			return file;
		}
		
	}

	@Override
	public Long getSize() {
		Query q = em.createQuery("SELECT count(file.name) FROM SssFile file WHERE file.dateModification IS NOT null");
		
		try{
			return (Long) q.getSingleResult ();
		}
		catch (NoResultException e){
			return new Long(0);
		}
	}
	
	@Override
	public int getBoatNb() {
		Query q = em.createQuery("SELECT DISTINCT boat FROM SssFile file WHERE file.dateModification IS NOT null");
		
		try{			
			return q.getResultList().size();
		}
		catch (NoResultException e){
			return 0;
		}
	}
	
	@Override
	public Date getLastDbUpdate() {
		Query q = em.createQuery("SELECT max(journal.date) FROM SssFileJournal journal");
		
		try{			
			return (Date) q.getSingleResult ();
		}
		catch (NoResultException e){
			return null;
		}
	}
	
	
	/**
	 * @param first position of the first result, numbered from 0
	 * @param size maximum number of results to retrieve
	 */
	@Override
	@Transactional
	public List<SssFile> getUpdatedFilesByPage(int first, int size,
			boolean loadLinkedObjects) {
		Query q = em.createQuery("SELECT file FROM SssFile file WHERE file.dateModification IS NOT null ORDER BY file.dateModification DESC");

		q.setFirstResult(first);
		q.setMaxResults(size);
				
		try{
			List<SssFile> result = (List<SssFile>) q.getResultList();
			if (loadLinkedObjects) {
				for (SssFile d : result) {
					loadLinkedObjects(d);
				}
			}
			return result;
		}
		catch (NoResultException e){
			return new ArrayList<SssFile>();
		}
	}
	
	@Override
	@Transactional
	public List<SssFile> getLastUpdatedFiles(int limit,
			boolean loadLinkedObjects) {
		Query q = em.createQuery("SELECT file FROM SssFile file WHERE file.dateModification IS NOT null ORDER BY file.dateModification DESC");

		if (limit > 0){
			q.setMaxResults(limit);
		}
		
		try{
			List<SssFile> result = (List<SssFile>) q.getResultList();
			if (loadLinkedObjects) {
				for (SssFile d : result) {
					loadLinkedObjects(d);
				}
			}
			return result;
		}
		catch (NoResultException e){
			return new ArrayList<SssFile>();
		}
	}
	
	@Override
	public UpdateInfos getLastUpdateInfos() {
		try{
			Query q = em.createQuery(
					"SELECT NEW fr.sedoo.sssdata.server.model.UpdateInfos(max(file.dateModification),max(file.dateCreation),max(file.dateUpdate), min(file.dateStart), max(file.dateEnd))" +
					" FROM SssFile file");

			return (UpdateInfos) q.getSingleResult();
		}
		catch (Exception e){
			return null;
		}
	}
	
	@Override
	@Transactional
	public void markAsDeleted(SssFile sssFile){
		SssFileJournal j = new SssFileJournal();
		j.setDate(new Date());
		j.setType(SssFileJournal.TYPE_DELETION);
		j.setFile(sssFile);
		
		sssFile.setMeasuresNb(0);
		sssFile.setDateModification(null);
		sssFile.setDateStart(null);
		sssFile.setDateEnd(null);
		sssFile.setEastLongitude(null);
		sssFile.setWestLongitude(null);
		sssFile.setNorthLatitude(null);
		sssFile.setSouthLatitude(null);
		sssFile.getFileJournal().add(j);
		save(sssFile, true);
	}

	private void loadLinkedObjects(SssFile obj) {
		if (obj != null) {
			Boat boat = obj.getBoat();
			if (boat != null) {
				boat.getId();
			}

			List<Download> downloads = obj.getDownloads();
			if (downloads != null) {
				downloads.size();
			}

			Set<SssFileJournal> fileJournals = obj.getFileJournal();
			if (fileJournals != null) {
				fileJournals.size();
			}

			/*
			List<Measure> measures = obj.getMeasures();
			if (measures != null) {
				measures.size();
			}*/

		}
	}
}
