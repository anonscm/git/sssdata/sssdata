package fr.sedoo.sssdata.server.service;

import java.util.Collection;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.sedoo.sssdata.client.service.ImportDataService;
import fr.sedoo.sssdata.server.ArchiveGenerator;
import fr.sedoo.sssdata.server.SSSDataApplication;
import fr.sedoo.sssdata.server.misc.LegosFTPParser;
import fr.sedoo.sssdata.shared.dataimport.FilePostImportInfo;
import fr.sedoo.sssdata.shared.dataimport.FilePreImportInfo;

public class ImportDataServiceImpl extends RemoteServiceServlet implements ImportDataService{

	/*@Override
	public void importData() {
		LegosFTPParser.launch();
	}*/
	
	@Override
	public Collection<String> getDirs() throws Exception{
		return LegosFTPParser.getDirs();
	}
	
	@Override
	public Collection<String> getFiles() throws Exception{
		return LegosFTPParser.getFilesList();
	}
	
	@Override
	public Collection<String> getFiles(Collection<String> dirs) throws Exception{
		return LegosFTPParser.getFilesList(dirs);
	}
	
	
	
	/*
	@Override
	public String[] getRacines() throws Exception{
		return LegosFTPParser.getRacines();
	}
	
	@Override
	public ArrayList<String> getDistantBoatNames(String racine) throws Exception {
		return LegosFTPParser.getDistantBoatNames(racine);
	}

	@Override
	public BoatPreImportInfo getBoatPreImportInfo(String racine, String boatName)
			throws Exception {
		return LegosFTPParser.getBoatPreImportInfo(racine, boatName);
	}
*/
/*
	@Override
	public void launchArchiveGenerator() {
		ArchiveGenerator archiveService = (ArchiveGenerator) SSSDataApplication
				.getSpringBeanFactory().getBeanByName(
						ArchiveGenerator.BEAN_NAME);
		archiveService.run();
	}
*/
	@Override
	public FilePreImportInfo getFilePreImportInfo(String file) throws Exception {
		return LegosFTPParser.getFilePreImportInfo(file);
	}

	@Override
	public FilePostImportInfo importFile(String file, boolean deleteFirst) throws Exception 
	{
		return LegosFTPParser.importFile(file, deleteFirst);
	}
	
	

}
