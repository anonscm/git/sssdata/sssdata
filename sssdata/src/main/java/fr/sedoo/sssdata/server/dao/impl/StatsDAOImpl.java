package fr.sedoo.sssdata.server.dao.impl;

import java.util.ArrayList;

import fr.sedoo.sssdata.server.dao.api.StatsDAO;
import fr.sedoo.sssdata.shared.MonthyReport;

public class StatsDAOImpl implements StatsDAO {

	private ArrayList<MonthyReport> generalStatsList;
	
	
	@Override
	public ArrayList <MonthyReport> findData() {
		fillList();
		return generalStatsList;

	}
	

	private MonthyReport createListDwnld(long num) {
		MonthyReport d = new MonthyReport();
		d.setMonth("janvier");
		d.setDownloadsNumber((int) num);

		return d;
	}

	

	private void fillList() {
			
		if (generalStatsList==null){
				
			generalStatsList = new ArrayList<MonthyReport>();
			for (long i =1L;i<=20L;i++){
				generalStatsList.add(createListDwnld(i));
			}
		}
		
	}

	

}
