package fr.sedoo.sssdata.server.dao.api;

import java.io.IOException;
import java.util.List;

import fr.sedoo.sssdata.shared.BoatDTO;
import fr.sedoo.sssdata.shared.SearchCriterion;
import fr.sedoo.sssdata.shared.Trajectory;

public interface TrajectoryDAO {
	public final static String BEAN_NAME="trajectoryDAO";
	
	//java.util.HashMap<Boat, List<Trajectory>> searchTrajectories(SearchCriterion sc);

	List<Trajectory> searchTrajectories(BoatDTO b, SearchCriterion sc);
		
	String downloadTrajectory(String requestId, String trajectoryName,
			SearchCriterion sc, String outputFormat) throws IOException;
}
