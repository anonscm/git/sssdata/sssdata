package fr.sedoo.sssdata.server.dao.impl;


import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.sql.DataSource;

import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.sedoo.commons.server.DefaultServerApplication;
import fr.sedoo.sssdata.server.dao.api.MeasureDAO;
import fr.sedoo.sssdata.server.model.Measure;
import fr.sedoo.sssdata.server.model.SssFile;
import fr.sedoo.sssdata.shared.LatLong;
import fr.sedoo.sssdata.shared.Trajectory;

@Repository
public class MeasureDAOJPAImpl implements MeasureDAO{

	protected EntityManager em;
	private JdbcTemplate template;
	public EntityManager getEntityManager() {
		return em;
	}

	@PersistenceContext
	public void setEntityManager(EntityManager entityManager) {
		this.em = entityManager;
	}
	
	@Override
	@Transactional
	public void add(Measure m) {
		if (m.getId() == null)
		{
			getEntityManager().persist(m);
		}
		else
		{
			getEntityManager().merge(m);
		}
		
	}

//	@Override
//	@Transactional
//	public void bulkAdd(List<Measure> measures) {
//		for (Measure m : measures){
//			if (m.getSssFile().getId() == null)
//			{
//				getEntityManager().persist(m);
//			}
//			else
//			{
//				getEntityManager().merge(m);
//			}
//		}
//	}


	@Override
	public List<Measure> getMeasuresByFile(SssFile f, Query query) {
		query.setParameter("sssFileId", f.getId());
		try{
			return query.getResultList();
		}catch(NoResultException e){
			return null;
		}
	}
	
	@Override
	public int getMeasuresNb(SssFile f, Query countQuery) {
		countQuery.setParameter("sssFileId", f.getId());
		Number nb = (Number) countQuery.getSingleResult();
		return nb.intValue();
	}
	

	private long getDateDistanceLimit(int precision){
		if (precision <= 10){
			return 6 * 3600 * 1000;//6 heures
		}else if (precision <= 50){
			return 12 * 3600 * 1000;//1/2 journée
		}else if (precision <= 100){
			return 24 * 3600 * 1000;//1 jour
		}else if (precision <= 300){
			return 36 * 3600 * 1000;//36 heures
		}else if (precision <= 500){
			return 2 * 24 * 3600 * 1000;//2 jours
		}else{
			return 5 * 24 * 3600 * 1000;//5 jours
		}
	}
	
	@Override
	public Trajectory getMeasuresByFile(SssFile f, Query query, int precision) {
			
		query.setParameter("sssFileId", f.getId());
		if (precision > 0){
			query.setParameter("precision", precision);
			query.setParameter("trajSize", f.getMeasuresNb() - 1);
		}
		
		 long dateDistanceLimit = getDateDistanceLimit(precision);
		
		List<Measure> aux = query.getResultList();
						
		/******************************************* PROCESSING RESULT *************************************************/
		ArrayList<LatLong> line = new ArrayList<LatLong>();
		Trajectory traj = new Trajectory();
	
		if (aux.size() > 0){
			double prevLon = aux.get(0).getLongitude();
			double currLon = prevLon;
			double prevLat = aux.get(0).getLatitude();
			double currLat = prevLat;
			long prevTime = aux.get(0).getMeasureDate().getTime();
			long currTime = prevTime;
			//TODO comparer les dates
			
			//TODO comparer les ids ( id2 - id1 > precision => nlle ligne)
			
			//long dateDistanceLimit = 432000000;//5 jours
			
			for (int i = 0 ; i < aux.size(); i ++){
				prevLon = currLon;
				currLon = aux.get(i).getLongitude();
				prevLat = currLat;
				currLat = aux.get(i).getLatitude();
				prevTime = currTime;
				currTime = aux.get(i).getMeasureDate().getTime();
				
				System.out.println(aux.get(i).getMeasureDate() + ": " + currLon + ", " + currLat);
				
				long d = currTime - prevTime;
				
				if( d > dateDistanceLimit ){
					System.out.println("HOLE");
					traj.addLine(line);
					line = new ArrayList<LatLong>();
				}else if ( ((prevLon * currLon) < 0 && Math.abs(currLon) > 100) ){
					System.out.println("DATELINE");
					double lat = Math.min(currLat, prevLat) + Math.abs(prevLat - currLat) / 2;
					if (prevLon < 0){
						line.add(new LatLong(lat,-180.0));
					}else{
						line.add(new LatLong(lat,180.0));
					}
					
					traj.addLine(line);
					line = new ArrayList<LatLong>();
					if (currLon > 0){
						line.add(new LatLong(lat,180.0));
					}else{
						line.add(new LatLong(lat,-180.0));
					}
				} 
				line.add(new LatLong(currLat,currLon));
			}

			if (line.size() > 0){
				traj.addLine(line);	
			}

			traj.setSssFileName(f.getName());
		}
		return traj;
	}

	private PreparedStatement batchMesures;
	private int cptbatch;
	private int BATCH_CURSOR = 1000;
	
	@Override
	public void initBatch() throws SQLException{
		DataSource dataSource = (DataSource) DefaultServerApplication.getSpringBeanFactory().getBeanByName("dataSource");

		String sql = "INSERT INTO MEASURE " +
				"(MEASURE_DATE, LATITUDE, LONGITUDE, POSITION_QUALITY, " +
				"SALINITY, SALINITY_QUALITY, SALINITY_ERROR, " +
				"TEMPERATURE, TEMPERATURE_QUALITY, TEMPERATURE_ERROR, " +
				"SSSFILE_FK, MEASURE_INDEX) " +
				"VALUES (?,?,?,?," +
				"?,?,?," +
				"?,?,?," +
				"?,?)";

		batchMesures = dataSource.getConnection().prepareStatement(sql);
		cptbatch = 0;
	}
	@Override
	public void addToBatch(Measure measure) throws SQLException{
		batchMesures.setTimestamp(1, new Timestamp(measure.getMeasureDate().getTime()));
		
		if (measure.getLatitude() == null){
			batchMesures.setNull(2, Types.DOUBLE);
		}else{
			batchMesures.setDouble(2, measure.getLatitude());
		}
		if (measure.getLongitude() == null){
			batchMesures.setNull(3, Types.DOUBLE);
		}else{
			batchMesures.setDouble(3, measure.getLongitude());
		}
				
		batchMesures.setInt(4, measure.getPositionQuality());
		
		if (measure.getSalinity() == null){
			batchMesures.setNull(5, Types.DOUBLE);
			batchMesures.setNull(6, Types.INTEGER);
			batchMesures.setNull(7, Types.DOUBLE);
		}else{
			batchMesures.setDouble(5, measure.getSalinity());
			batchMesures.setInt(6, measure.getSalinityQuality());
			if (measure.getSalinityError() == null){
				batchMesures.setNull(7, Types.DOUBLE);
			}else{
				batchMesures.setDouble(7, measure.getSalinityError());
			}
		}
		
		if (measure.getTemperature() == null){
			batchMesures.setNull(8, Types.DOUBLE);
			batchMesures.setNull(9, Types.INTEGER);
			batchMesures.setNull(10, Types.DOUBLE);
		}else{
			batchMesures.setDouble(8, measure.getTemperature());
			batchMesures.setInt(9, measure.getTemperatureQuality());
			if (measure.getTemperatureError() == null){
				batchMesures.setNull(10, Types.DOUBLE);
			}else{
				batchMesures.setDouble(10, measure.getTemperatureError());
			}
		}
		
		batchMesures.setDouble(11, measure.getSssFile().getId());
		batchMesures.setInt(12, measure.getMeasureIndex());
		
		batchMesures.addBatch();
		cptbatch++;
		if (cptbatch % BATCH_CURSOR == 0)
			executeBatch(false);
	}

	private void executeBatch(boolean analyze) throws SQLException{
		batchMesures.executeBatch();
		if (analyze){
			DataSource dataSource = (DataSource) DefaultServerApplication.getSpringBeanFactory().getBeanByName("dataSource");	    	
	    	Statement stmt = dataSource.getConnection().createStatement();
			stmt.execute("ANALYZE");
			stmt.close();
	    }
	}
	@Override
	public void closeBatch() throws SQLException{
		executeBatch(true);
		batchMesures.close();
	}
	
	@Override
	public void bulkAdd(final List<Measure> measures)
	{
		if (template == null)
		{
			initTemplate();
		}
		  String sql = "INSERT INTO MEASURE " +
				  "(MEASURE_DATE, LATITUDE, LONGITUDE, POSITION_QUALITY, " +
				  "SALINITY, SALINITY_QUALITY, SALINITY_ADJUSTED, SALINITY_ADJUSTED_QUALITY, SALINITY_ADJUSTED_ERROR, " +
				  "TEMPERATURE, TEMPERATURE_QUALITY, TEMPERATURE_ADJUSTED, TEMPERATURE_ADJUSTED_QUALITY, TEMPERATURE_ADJUSTED_ERROR, " +
				  "JACKET_TEMPERATURE, JACKET_TEMPERATURE_QUALITY, JACKET_TEMPERATURE_ADJUSTED, JACKET_TEMPERATURE_ADJUSTED_QUALITY, JACKET_TEMPERATURE_ADJUSTED_ERROR, " +
				  "SSSFILE_FK, MEASURE_INDEX) " +
				  "VALUES (?,?,?,?," +
				  "?,?,?,?,?" +
				  "?,?,?,?,?" +
				  "?,?,?,?,?" +
				  "?,?)";
		 template.batchUpdate(sql, new BatchPreparedStatementSetter() {
		 
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Measure measure = measures.get(i);
				ps.setTimestamp(1, new Timestamp(measure.getMeasureDate().getTime()));
				if (measure.getSalinity() == null){
					ps.setNull(2, Types.DOUBLE);
					ps.setNull(3, Types.INTEGER);
				}else{
					ps.setDouble(2, measure.getSalinity());
					ps.setInt(3, measure.getSalinityQuality());
				}
				
				if (measure.getTemperature() == null){
					ps.setNull(4, Types.DOUBLE);
					ps.setNull(5, Types.INTEGER);
				}else{
					ps.setDouble(4, measure.getTemperature());
					ps.setInt(5, measure.getTemperatureQuality());
				}
				
				ps.setDouble(6, measure.getLatitude());
				ps.setDouble(7, measure.getLongitude());
				ps.setInt(8, measure.getPositionQuality());
				ps.setDouble(9, measure.getSssFile().getId());
				ps.setInt(10, measure.getMeasureIndex());
				
			}
		 
			@Override
			public int getBatchSize() {
				return measures.size();
			}
		  });
		}

	private void initTemplate() 
	{
		DataSource dataSource = (DataSource) DefaultServerApplication.getSpringBeanFactory().getBeanByName("dataSource");
		template = new JdbcTemplate(dataSource);
	}

	@Override
	@Transactional
	public void deleteByFile(SssFile sssFile) {
		Query query = em.createQuery("delete from Measure measure where measure.sssFile.id =:arg1");
		query.setParameter("arg1", sssFile.getId());
		query.executeUpdate();
	}

	
	
}
