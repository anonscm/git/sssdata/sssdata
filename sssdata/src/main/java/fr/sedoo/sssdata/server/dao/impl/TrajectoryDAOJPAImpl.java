package fr.sedoo.sssdata.server.dao.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import fr.sedoo.sssdata.server.SSSDataApplication;
import fr.sedoo.sssdata.server.dao.api.MeasureDAO;
import fr.sedoo.sssdata.server.dao.api.SssFileDAO;
import fr.sedoo.sssdata.server.dao.api.TrajectoryDAO;
import fr.sedoo.sssdata.server.dao.gosud.GosudConstants;
import fr.sedoo.sssdata.server.extract.FichierSortie;
import fr.sedoo.sssdata.server.extract.FichierSortieFactory;
import fr.sedoo.sssdata.server.model.Measure;
import fr.sedoo.sssdata.server.model.SssFile;
import fr.sedoo.sssdata.shared.BoatDTO;
import fr.sedoo.sssdata.shared.Boundings;
import fr.sedoo.sssdata.shared.SearchCriterion;
import fr.sedoo.sssdata.shared.Trajectory;

public class TrajectoryDAOJPAImpl implements TrajectoryDAO{

	static Logger LOGGER = LoggerFactory.getLogger(TrajectoryDAOJPAImpl.class);

	private EntityManager em;
	public EntityManager getEntityManager() {
		return em;
	}
	@PersistenceContext
	public void setEntityManager(EntityManager entityManager) {
		this.em = entityManager;
	}


	//private int precision = 200;
	//private MeasureServiceImpl measureService = new MeasureServiceImpl();
	//private BoatServiceImpl boatService = new BoatServiceImpl();

	private SssFileDAO sssFileDao;
	private MeasureDAO measureDao;
	
	
	
	private void initDao(){
		if (sssFileDao == null){
			sssFileDao = (SssFileDAO) SSSDataApplication.getSpringBeanFactory().getBeanByName(SssFileDAO.BEAN_NAME);
		}
		if (measureDao == null){
			measureDao = (MeasureDAO) SSSDataApplication.getSpringBeanFactory().getBeanByName(MeasureDAO.BEAN_NAME);
		}
		
	}

	//	@Override
	//	@Transactional
	//	public HashMap<Boat, ArrayList<Trajectory>> searchTrajectories(
	//			SearchCriterion sc) {
	//		
	//		HashMap<Boat, ArrayList<Trajectory>> trajectories = new HashMap<Boat, ArrayList<Trajectory>>();
	//		
	//		for (Boat b : sc.getBoatListSelection()) {
	//			try {
	//			
	//				Boat boatAndFiles = null ;
	//			
	//				if (b != null) {
	//					boatAndFiles = boatService.findByName(b.getName());
	//					ArrayList<Trajectory> allTrajectories = new ArrayList<Trajectory>();
	//					Trajectory traj = null;
	//					for (SssFile f : boatAndFiles.getFiles()){
	//						traj = measureService.getMeasurePrecisionByFile(f,precision, sc);
	//						allTrajectories.add(traj);
	//					}
	//					
	//					trajectories.put(b, allTrajectories);
	//				}
	//				
	//			} catch (ServiceException e) {
	//				e.printStackTrace();
	//			}
	//		}
	//		
	//		return trajectories;
	//	}

	@Override
	public String downloadTrajectory(String requestId, String trajectoryName, SearchCriterion sc, String outputFormat) throws IOException {
		//Query q = buildQuery(sc,1);
		Query q = buildQuery(sc);

		initDao();

		SssFile file = sssFileDao.getSimpleFileByName(trajectoryName, false);
		if (file != null){
			List<Measure> mesures = measureDao.getMeasuresByFile(file, q);
			if (mesures != null && mesures.size() > 0){
				FichierSortie out = FichierSortieFactory.getInstance().getFichierSortie(requestId,file,sc,outputFormat);
				
				
				out.write(mesures);
				return out.getFile().getAbsolutePath();
			}
		}else{
			//TODO erreur
		}

		return null;
	}
	
	@Override
	@Transactional
	public List<Trajectory> searchTrajectories(BoatDTO b, SearchCriterion sc){
		List<Trajectory> trajectories = new ArrayList<Trajectory>();
				
		int precision = getPrecision(sc);
				
		Query qc = buildCountQuery(sc);
		//Query q = buildSimpleQuery(sc, precision);
		Query q = buildQuery(sc, precision);
		
		initDao();
		//System.out.println("Boat " + b.getName());
		//List<SssFile> files = sssFileDao.getFilesByBoat(b.getName(), sc);
		List<SssFile> files = sssFileDao.getFilesByBoat(b.getName(),
				sc.getBeginDate(), sc.getEndDate(), false);
		if (files != null){
			for (SssFile f: files){
				int c = measureDao.getMeasuresNb(f, qc);
				System.out.println("File " + f.getName() + ":" + c);
				int minSize = 7;
				if (c > 0){
					int p = precision;
					if (c < minSize){
						p = 1;
					}
					Trajectory t = measureDao.getMeasuresByFile(f, q, p);
										
					while (t.getSize() < minSize && p > 1){
						p = p/2;
						System.out.println("Traj " + f.getName() + ":" + t.getSize() + " - KO");
						t = measureDao.getMeasuresByFile(f, q, p);
					}
					System.out.println("Traj " + f.getName() + ":" + t.getSize());
										
					if (t.getSize() > 0){
						t.setResultSize(c);
						trajectories.add(t);
					}

					
				}
			}
		}
		
		
		return trajectories;
	}
	
	
	
	//private Query buildQuery(SearchCriterion sc){
	private int getPrecision(SearchCriterion sc){
		int precision = 1;
		if (sc.getZoomLevel() == 1){
			precision = 500;
		}else if (sc.getZoomLevel() <= 3){
			precision = 100;
		}else if (sc.getZoomLevel() <= 5){
			precision = 50;
		}else if (sc.getZoomLevel() <= 7){
			precision = 10;
		}
		return precision;
	}
	//	return buildQuery(sc, precision);
	//}
	
	/**
	 * Pour déterminer le nombre de points qui seront renvoyés par l'extraction
	 * @param sc
	 * @return
	 */
	private Query buildCountQuery(SearchCriterion sc){
		String query = "SELECT count(m.id) FROM Measure m join m.sssFile f WHERE m.positionQuality != 4 AND f.id = :sssFileId " +
				"AND m.measureDate BETWEEN :startDate AND :endDate ";
		
		if (!sc.isSalinityEmpty()){
			query += "AND (m.salinity IS NULL OR m.salinity BETWEEN " + sc.getSalinityMin() + " AND " + sc.getSalinityMax() + ")";
		}

		if (!sc.isTemperatureEmpty()){
			query += "AND (m.temperature IS NULL OR m.temperature BETWEEN " + sc.getTemperatureMin() + " AND "+ sc.getTemperatureMax() + ") ";
		}

		if (!sc.isDataQualityEmpty()){
			String flags = "";
			if (sc.isBadData()){
				flags +=  " OR m.salinityQuality = " + GosudConstants.BAD_DATA;
			}
			if (sc.isGoodData()){
				flags +=  " OR m.salinityQuality = " + GosudConstants.GOOD_DATA;
			}
			if (sc.isHarbourData()){
				flags +=  " OR m.salinityQuality = " + GosudConstants.HARBOUR_DATA;
			}
			if (sc.isUncontrolledData()){
				flags +=  " OR m.salinityQuality = " + GosudConstants.UNCONTROLLED_DATA;
			}
			if (sc.isProbablyBadData()){
				flags +=  " OR m.salinityQuality = " + GosudConstants.PROBABLY_BAD_DATA;
			}
			if (sc.isProbablyGoodData()){
				flags +=  " OR m.salinityQuality = " + GosudConstants.PROBABLY_GOOD_DATA;
			}

			query += "AND (" + flags.substring(3) + ")";
		}
		
		String whereBbox = "";
		for (Boundings b: sc.getBoundings()){
			whereBbox += "OR (m.latitude BETWEEN " + b.getSouthBoundLatitude() + " AND " + b.getNorthBoundLatitude() + 
					" AND m.longitude BETWEEN " + b.getWestBoundLongitude() + " AND " + b.getEastBoundLongitude() + ")";
		}
		query += "AND (" + whereBbox.substring(3) + ") ";
						 
		Query q = em.createQuery(query);
		q.setParameter("startDate", sc.getBeginDate(), TemporalType.DATE);
		q.setParameter("endDate", sc.getEndDate(), TemporalType.DATE);
		
		return q;
	}
	
	/**
	 * requete pour le tracé
	 * @param sc
	 * @param precision
	 * @return
	 */
	private Query buildSimpleQuery(SearchCriterion sc, int precision){
		String query = "SELECT m FROM Measure m join m.sssFile f WHERE m.positionQuality != 4 AND f.id = :sssFileId " +
				"AND m.measureDate BETWEEN :startDate AND :endDate ";
		
		String whereBbox = "";
		for (Boundings b: sc.getBoundings()){
			whereBbox += "OR (m.latitude BETWEEN " + b.getSouthBoundLatitude() + " AND " + b.getNorthBoundLatitude() + 
					" AND m.longitude BETWEEN " + b.getWestBoundLongitude() + " AND " + b.getEastBoundLongitude() + ")";
		}
		query += "AND (" + whereBbox.substring(3) + ") ";
				
		if (precision > 1){
			query += "AND (m.measureIndex = 0 OR m.measureIndex = :trajSize OR MOD(m.measureIndex, :precision ) = 0 )";
		}
				
		query += "ORDER BY m.measureDate ";
				 
		Query q = em.createQuery(query);
		q.setParameter("startDate", sc.getBeginDate(), TemporalType.DATE);
		q.setParameter("endDate", sc.getEndDate(), TemporalType.DATE);
		
		return q;
	}
	
	/**
	 * requete pour l'extraction
	 * @param sc
	 * @return
	 */
	private Query buildQuery(SearchCriterion sc){
		return buildQuery(sc, 0);
	}
	
	/**
	 * requete pour le tracé de trajectoire
	 * @param sc
	 * @param precision
	 * @return
	 */
	private Query buildQuery(SearchCriterion sc, int precision){
				
		String query = "SELECT m FROM Measure m join m.sssFile f WHERE m.positionQuality != 4 AND f.id = :sssFileId " +
				"AND m.measureDate BETWEEN :startDate AND :endDate ";

		if (!sc.isSalinityEmpty()){
			query += "AND (m.salinity IS NULL OR m.salinity BETWEEN " + sc.getSalinityMin() + " AND " + sc.getSalinityMax() + ")";
		}

		if (!sc.isTemperatureEmpty()){
			query += "AND (m.temperature IS NULL OR m.temperature BETWEEN " + sc.getTemperatureMin() + " AND "+ sc.getTemperatureMax() + ") ";
		}

		if (!sc.isDataQualityEmpty()){
			String flags = "";
			if (sc.isBadData()){
				flags +=  " OR m.salinityQuality = " + GosudConstants.BAD_DATA;
			}
			if (sc.isGoodData()){
				flags +=  " OR m.salinityQuality = " + GosudConstants.GOOD_DATA;
			}
			if (sc.isHarbourData()){
				flags +=  " OR m.salinityQuality = " + GosudConstants.HARBOUR_DATA;
			}
			if (sc.isUncontrolledData()){
				flags +=  " OR m.salinityQuality = " + GosudConstants.UNCONTROLLED_DATA;
			}
			if (sc.isProbablyBadData()){
				flags +=  " OR m.salinityQuality = " + GosudConstants.PROBABLY_BAD_DATA;
			}
			if (sc.isProbablyGoodData()){
				flags +=  " OR m.salinityQuality = " + GosudConstants.PROBABLY_GOOD_DATA;
			}

			query += "AND (" + flags.substring(3) + ")";
		}
		
		String whereBbox = "";
		for (Boundings b: sc.getBoundings()){
			whereBbox += "OR (m.latitude BETWEEN " + b.getSouthBoundLatitude() + " AND " + b.getNorthBoundLatitude() + 
					" AND m.longitude BETWEEN " + b.getWestBoundLongitude() + " AND " + b.getEastBoundLongitude() + ")";
		}
		query += "AND (" + whereBbox.substring(3) + ") ";
				
		if (precision > 0){
			query += "AND (m.measureIndex = 0 OR m.measureIndex = :trajSize OR MOD(m.measureIndex, :precision ) = 0 )";
		}
						
		query += "ORDER BY m.measureDate ";
				 
		Query q = em.createQuery(query);
		q.setParameter("startDate", sc.getBeginDate(), TemporalType.DATE);
		q.setParameter("endDate", sc.getEndDate(), TemporalType.DATE);
		
		return q;
	}
	
	/*
	@Override
	@Transactional
	public HashMap<Boat, List<Trajectory>> searchTrajectories(SearchCriterion sc)
	{
		HashMap<Boat, List<Trajectory>> trajectories = new HashMap<Boat, List<Trajectory>>();
		
		Query q = buildQuery(sc);
		
		initDao();
		for (Boat b: sc.getBoatListSelection()){
			//System.out.println("Boat " + b.getName());
			List<SssFile> files = sssFileDao.getFilesByBoat(b.getName(), sc);
			if (files != null){
				List<Trajectory> boatTrajectories = new ArrayList<Trajectory>();
				for (SssFile f: files){
					
					
					Trajectory t = measureDao.getMeasuresByFile(f, q, precision);
					//System.out.println("File " + f.getName() + ":" + t.getSize());
					if (t.getSize() > 0){
						boatTrajectories.add(t);
					}
				}
				if (!boatTrajectories.isEmpty()){
					trajectories.put(b, boatTrajectories);
				}
			}
		}
		return trajectories;
	}*/
/*
	private String getExclusiveCriterionRequest(SearchCriterion sc)
	{
		ArrayList<String> constraints = new ArrayList<String>();
		if (!sc.isTemperatureEmpty())
		{
			ArrayList<String> aux = new ArrayList<String>();
			if (sc.getTemperatureMax() != null)
			{
				StringBuilder sb = new StringBuilder();
				sb.append("("+SssFile.TABLE_NAME+"."+SssFile.MIN_TEMPERATURE_COLUMN_NAME+">"+sc.getTemperatureMax());
				aux.add(sb.toString());
			}
			if (sc.getTemperatureMin() != null)
			{
				StringBuilder sb = new StringBuilder();
				sb.append("("+SssFile.TABLE_NAME+"."+SssFile.MAX_TEMPERATURE_COLUMN_NAME+"<"+sc.getTemperatureMin());
				aux.add(sb.toString());
			}
			Iterator<String> iterator = aux.iterator();
			StringBuilder sb = new StringBuilder("(");
			while (iterator.hasNext()) 
			{
				String current = iterator.next();
				sb.append("("+current+")");
				if (iterator.hasNext())
				{
					sb.append(" or ");
				}
			}
			sb.append(")");
			constraints.add(sb.toString());
		}

		if (!sc.isSalinityEmpty())
		{
			ArrayList<String> aux = new ArrayList<String>();
			if (sc.getSalinityMax() != null)
			{
				StringBuilder sb = new StringBuilder();
				sb.append("("+SssFile.TABLE_NAME+"."+SssFile.MIN_SALINITY_COLUMN_NAME+">"+sc.getSalinityMax());
				aux.add(sb.toString());
			}
			if (sc.getTemperatureMin() != null)
			{
				StringBuilder sb = new StringBuilder();
				sb.append("("+SssFile.TABLE_NAME+"."+SssFile.MAX_SALINITY_COLUMN_NAME+"<"+sc.getSalinityMin());
				aux.add(sb.toString());
			}
			Iterator<String> iterator = aux.iterator();
			StringBuilder sb = new StringBuilder("(");
			while (iterator.hasNext()) 
			{
				String current = iterator.next();
				sb.append("("+current+")");
				if (iterator.hasNext())
				{
					sb.append(" or ");
				}
			}
			sb.append(")");
			constraints.add(sb.toString());
		}


		if (!sc.isDateEmpty())
		{
			ArrayList<String> aux = new ArrayList<String>();
			if (sc.getEndDate() != null)
			{
				StringBuilder sb = new StringBuilder();
				sb.append("("+SssFile.TABLE_NAME+"."+SssFile.DATE_START_COLUMN_NAME+">"+sc.getEndDate());
				aux.add(sb.toString());
			}
			if (sc.getBeginDate() != null)
			{
				StringBuilder sb = new StringBuilder();
				sb.append("("+SssFile.TABLE_NAME+"."+SssFile.DATE_END_COLUMN_NAME+"<"+sc.getBeginDate());
				aux.add(sb.toString());
			}
			Iterator<String> iterator = aux.iterator();
			StringBuilder sb = new StringBuilder("(");
			while (iterator.hasNext()) 
			{
				String current = iterator.next();
				sb.append("("+current+")");
				if (iterator.hasNext())
				{
					sb.append(" or ");
				}
			}
			sb.append(")");
			constraints.add(sb.toString());
		}

		StringBuilder sb = new StringBuilder();
		sb.append("select "+SssFile.TABLE_NAME+"."+SssFile.ID_COLUMN_NAME+" from "+SssFile.TABLE_NAME);
		if (!constraints.isEmpty())
		{
			sb.append(" where ");
			Iterator<String> iterator = constraints.iterator();
			while (iterator.hasNext()) 
			{
				String current = iterator.next();
				sb.append("("+current+")");
				if (iterator.hasNext())
				{
					sb.append(" or ");
				}
			}
		}

		return sb.toString();
	}

	private String getInclusiveCriterionRequest(SearchCriterion sc) 
	{
		ArrayList<String> constraints = new ArrayList<String>();
		if (!sc.getBoatListSelection().isEmpty())
		{
			StringBuilder aux = new StringBuilder();
			aux.append("(");
			Iterator<Boat> iterator = sc.getBoatListSelection().iterator();
			while (iterator.hasNext()) {
				Boat boat = iterator.next();
				aux.append("("+Boat.TABLE_NAME+"."+Boat.NAME_COLUMN_NAME+" = '"+boat.getName()+"')");
				if (iterator.hasNext())
				{
					aux.append(" and ");
				}
			}
			aux.append(")");
			constraints.add(aux.toString());
		}

		if (!sc.isDataQualityEmpty())
		{
			ArrayList<String> aux = new ArrayList<String>();

			if (sc.isUncontrolledData())
			{
				aux.add(SssFile.QUALITY_FLAGS_COLUMN_NAME+" like '%"+GosudConstants.UNCONTROLLED_DATA+"%'");
			}
			if (sc.isBadData())
			{
				aux.add(SssFile.QUALITY_FLAGS_COLUMN_NAME+" like '%"+GosudConstants.BAD_DATA+"%'");
			}
			if (sc.isProbablyBadData())
			{
				aux.add(SssFile.QUALITY_FLAGS_COLUMN_NAME+" like '%"+GosudConstants.PROBABLY_BAD_DATA+"%'");
			}
			if (sc.isProbablyGoodData())
			{
				aux.add(SssFile.QUALITY_FLAGS_COLUMN_NAME+" like '%"+GosudConstants.PROBABLY_GOOD_DATA+"%'");
			}
			if (sc.isGoodData())
			{
				aux.add(SssFile.QUALITY_FLAGS_COLUMN_NAME+" like '%"+GosudConstants.GOOD_DATA+"%'");
			}
			if (sc.isHarbourData())
			{
				aux.add(SssFile.QUALITY_FLAGS_COLUMN_NAME+" like '%"+GosudConstants.HARBOUR_DATA+"%'");
			}

			Iterator<String> iterator = aux.iterator();
			StringBuilder sb = new StringBuilder("(");
			while (iterator.hasNext()) 
			{

				String current = iterator.next();
				sb.append("("+current+")");
				if (iterator.hasNext())
				{
					sb.append(" or ");
				}
			}
			sb.append(")");
			constraints.add(sb.toString());
		}

		StringBuilder sb = new StringBuilder();
		sb.append("select "+SssFile.TABLE_NAME+"."+SssFile.ID_COLUMN_NAME+" from "+SssFile.TABLE_NAME+","+Boat.TABLE_NAME);
		sb.append(" where "+Boat.TABLE_NAME+"."+Boat.ID_COLUMN_NAME+"="+SssFile.BOAT_ID_COLUMN_NAME);
		if (!constraints.isEmpty())
		{
			sb.append(" and ");
			Iterator<String> iterator = constraints.iterator();
			while (iterator.hasNext()) 
			{
				String current = iterator.next();
				sb.append("("+current+")");
				if (iterator.hasNext())
				{
					sb.append(" and ");
				}
			}
		}

		return sb.toString();
	}*/

}
