package fr.sedoo.sssdata.server.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="DOWNLOAD")
public class Download {
	
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@Column(name="DATE_DL")
	private Date dateDL;
	
	@Column(name="LABO")
	private String labo;
	
	@Column(name="GOAL")
	@Lob
	private String goal;
			
	@Column(name="DATA_FORMAT", length=10)
	private String dataFormat;
	
	@Column(name="DATA_COMPRESSION", length=10)
	private String dataCompression;
	
	@ManyToOne(fetch = FetchType.LAZY)
	//@JoinColumn(name="USER_FK", insertable=false, updatable=false)
	@JoinColumn(name="USER_FK")
	private User user;
	
	/*@ManyToMany(
	        targetEntity=fr.sedoo.sssdata.shared.SssFile.class,
	        cascade={CascadeType.PERSIST, CascadeType.MERGE}
	    )
	    @JoinTable(
	        name="DOWNLOAD_SSSFILE",
	        joinColumns=@JoinColumn(name="DOWNLOAD_ID"),
	        inverseJoinColumns=@JoinColumn(name="FILE_ID")
	    )*/
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "download_sssfile",
				joinColumns={@JoinColumn(name="download_id", nullable = false, updatable = false)},
				inverseJoinColumns={@JoinColumn(name="sssfile_id", nullable = false, updatable = false)})
	private List<SssFile> files;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Archive archive;
	
	@Column(name="VOLUME")
	private Long volume;
	
	public Long getVolume() {
		return volume;
	}
	public void setVolume(Long volume) {
		this.volume = volume;
	}
	
	public Date getDateDL() {
		return dateDL;
	}

	public void setDateDL(Date dateDL) {
		this.dateDL = dateDL;
	}

	public String getLabo() {
		return labo;
	}

	public void setLabo(String labo) {
		this.labo = labo;
	}

	public String getGoal() {
		return goal;
	}

	public void setGoal(String goal) {
		this.goal = goal;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	public Archive getArchive() {
		return archive;
	}
	public void setArchive(Archive archive) {
		this.archive = archive;
	}
	public List<SssFile> getFiles() {
		return files;
	}

	public void setFiles(List<SssFile> files) {
		this.files = files;
	}

	public String getDataFormat() {
		return dataFormat;
	}
	public void setDataFormat(String dataFormat) {
		this.dataFormat = dataFormat;
	}

	public String getDataCompression() {
		return dataCompression;
	}

	public void setDataCompression(String dataCompression) {
		this.dataCompression = dataCompression;
	}
	
}
