package fr.sedoo.sssdata.server.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="METADATA")
public class Metadata {

	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(name="DOI")
	private String doi;
	
	@Column(name="XML", columnDefinition="TEXT")
	private String xml;
		
	@OneToMany(cascade=CascadeType.ALL, mappedBy="metadata", fetch=FetchType.LAZY)
	private List<MetadataHistory> history;
	
	public Metadata() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDoi() {
		return doi;
	}

	public void setDoi(String doi) {
		this.doi = doi;
	}

	public String getXml() {
		return xml;
	}

	public void setXml(String xml) {
		this.xml = xml;
	}

	public List<MetadataHistory> getHistory() {
		return history;
	}

	public void setHistory(List<MetadataHistory> history) {
		this.history = history;
	}
			
}
