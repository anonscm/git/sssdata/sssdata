package fr.sedoo.sssdata.server.dao.api;

import java.util.ArrayList;

import fr.sedoo.sssdata.shared.Administrator;

public interface AdministratorDAO {
	
	public final static String BEAN_NAME="administratorDAO";
	ArrayList<Administrator> findAll();
	void delete(Administrator administrator);
	Administrator save(Administrator administrator);
	
	Administrator login(String login, String password);

}
