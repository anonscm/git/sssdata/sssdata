package fr.sedoo.sssdata.server.extract;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.apache.log4j.Logger;

import fr.sedoo.commons.server.compress.Archive;
import fr.sedoo.commons.server.compress.ArchiveTar;
import fr.sedoo.commons.server.compress.ArchiveZip;
import fr.sedoo.sssdata.shared.FileDTO;

public class ArchiveSortie {

	private static Logger logger = Logger.getLogger(ArchiveSortie.class);
	
	private Archive archive;
	private String compression;
	
	private File file;
	private Collection<FileDTO> fichiers;
	
	public ArchiveSortie(String compression, String requestId) {
		//UUID uuid = UUID.randomUUID();
		Date now = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		sdf.setLenient(false);
		
		
		this.compression = compression;
		this.file = new File(FileDTO.EXTRACT_PATH +  "/" + FileDTO.FILENAME_PREFIX + sdf.format(now) + "_" + requestId + getExtension());
		this.fichiers = new ArrayList<FileDTO>();
	}

	public void open() throws IOException {
		logger.debug("open()");
		if (FileDTO.COMPRESSION_GZ.equals(compression)){
			archive = new ArchiveTar(getFile(),true);
		}else{
			//Zip par défaut
			archive = new ArchiveZip(getFile());
		}
		archive.openWrite();
	}

	public void write() throws IOException{
		logger.debug("write()");
		File extractPath = new File(FileDTO.EXTRACT_PATH);
		for (FileDTO fichier: fichiers){
			logger.debug(" - " + fichier.getGeneratedFileName());
			archive.addEntry(new File(fichier.getGeneratedFileName()), extractPath);
		}
	}
	
	public void close() throws IOException {
		logger.debug("close()");
		archive.close();
	}
	
	protected String getExtension() {
		if (FileDTO.COMPRESSION_GZ.equals(compression)){
			return ".tar.gz";
		}else{
			//Zip par défaut
			return ".zip";
		}
	}
	
	public void setFichiers(Collection<FileDTO> fichiers) {
		this.fichiers = fichiers;
	}

	public File getFile() {
		return file;
	}
	
}
