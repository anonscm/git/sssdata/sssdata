package fr.sedoo.sssdata.server.utils;

import java.util.Comparator;

import org.apache.commons.collections.ComparatorUtils;
import org.apache.log4j.Logger;

import fr.sedoo.sssdata.shared.UpdateDTO;

public class UpdateDTOComparatorByFilename implements Comparator<UpdateDTO> {

	private static Logger logger = Logger.getLogger(UpdateDTOComparatorByFilename.class);
	
	public static Comparator getComparator(){
		return ComparatorUtils.nullLowComparator(new UpdateDTOComparatorByFilename());
	}
	
	private UpdateDTOComparatorByFilename() {
		super();
	}
	
	@Override
	public int compare(UpdateDTO o1, UpdateDTO o2) {
		return o1.getFileName().compareTo(o2.getFileName());
	}

}
