package fr.sedoo.sssdata.server.dao.api;

import java.util.Date;
import java.util.List;
import java.util.Map;

import fr.sedoo.sssdata.server.model.User;

public interface UserDAO {
	public final static String BEAN_NAME="userDAO";
	User getUser(String email);
	
	User save(User u);

	List<User> getAll();
			
	Map<String,Integer> getNbUsersByCountry();
	List<Date> getRegistrationDates();

	User getUser(String email, String name, String country);
	
	//void insertDataFromUserInformation(UserInformations userinfo);
}
