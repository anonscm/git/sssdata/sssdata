package fr.sedoo.sssdata.server.metadata;

public class MetadataConfig {
	public static final String BEAN_NAME = "metadataConfig";

	private String exportPath;

	public MetadataConfig() {
		super();
	}

	public String getExportPath() {
		return exportPath;
	}
	public void setExportPath(String exportPath) {
		this.exportPath = exportPath;
	}
}
