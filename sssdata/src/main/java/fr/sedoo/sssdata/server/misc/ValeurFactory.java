package fr.sedoo.sssdata.server.misc;

/**
 * 
 * @author brissebr
 *
 */
public final class ValeurFactory {

	private ValeurFactory() {}
	
	/**
	 * Erreur sur SSPS.
	 */
	public static final double SSPS_ERROR = 1;
	/**
	 * Erreur sur SSTP.
	 */
	public static final double SSTP_ERROR = 0.2;
	/**
	 * Erreur sur SSJT.
	 */
	public static final double SSJT_ERROR = 0.5;
	
	
	
	/**
	 * Pour SST, on prend dans l'ordre : SSTP_ADJUSTED, SSTP, SSJT_ADJUSTED, SSJT
	 * @param m
	 * @return valeur + qc + err
	 */
	public static Valeur getSST(Mesure m){
		Valeur v = new Valeur();
		if (m.getTemperatureAdjusted() != null){
			v.setValeur(m.getTemperatureAdjusted());
			v.setQuality(m.getTemperatureAdjustedQuality());
			v.setError((m.getTemperatureAdjustedError() == null) ? SSTP_ERROR
					: m.getTemperatureAdjustedError());
		}else if (m.getTemperature() != null){
			v.setValeur(m.getTemperature());
			v.setQuality(m.getTemperatureQuality());
			v.setError(SSTP_ERROR);
		}else if (m.getJacketTemperatureAdjusted() != null){
			v.setValeur(m.getJacketTemperatureAdjusted());
			v.setQuality(m.getJacketTemperatureAdjustedQuality());
			v.setError((m.getJacketTemperatureAdjustedError() == null) ? SSJT_ERROR
					: m.getJacketTemperatureAdjustedError());
		}else if (m.getJacketTemperature() != null){
			v.setValeur(m.getJacketTemperature());
			v.setQuality(m.getJacketTemperatureQuality());
			v.setError(SSJT_ERROR);
		}
		return v;	
	}
	
	/**
	 * Pour SSS, on prend dans l'ordre : SSPS_ADJUSTED, SSPS
	 * @param m
	 * @return valeur + qc + err, null si pas de valeur
	 */
	public static Valeur getSSS(Mesure m){
		Valeur v = new Valeur();
		if (m.getSalinityAdjusted() != null){
			v.setValeur(m.getSalinityAdjusted());
			v.setQuality(m.getSalinityAdjustedQuality());
			v.setError((m.getSalinityAdjustedError() == null) ? SSPS_ERROR : m
					.getSalinityAdjustedError());
		}else if (m.getSalinity() != null){
			v.setValeur(m.getSalinity());
			v.setQuality(m.getSalinityQuality());
			v.setError(SSPS_ERROR);
		}		
		return v;
	}
	
}
