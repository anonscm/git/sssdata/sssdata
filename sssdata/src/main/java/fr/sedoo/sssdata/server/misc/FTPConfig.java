package fr.sedoo.sssdata.server.misc;

public class FTPConfig {
	
	private String adress;
	
	/**
	 * Répertoires à parcourir (séparés par des ',').
	 */
	private String dirs;
	private String login;
	private String password;
	private String rootDir;
	private String filter;
	
	public String getAdress() {
		return adress;
	}
	public void setAdress(String adress) {
		this.adress = adress;
	}
	
	public String getDirs() {
		return dirs;
	}	
	public void setDirs(String dirs) {
		this.dirs = dirs;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFilter() {
		return filter;
	}
	public void setFilter(String filter) {
		this.filter = filter;
	}
	public String getRootDir() {
		return rootDir;
	}
	public void setRootDir(String rootDir) {
		this.rootDir = rootDir;
	}

}
