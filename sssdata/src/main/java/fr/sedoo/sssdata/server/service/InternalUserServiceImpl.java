package fr.sedoo.sssdata.server.service;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.sedoo.commons.client.user.AuthenticatedUser;
import fr.sedoo.commons.client.user.DefaultAuthenticatedUser;
import fr.sedoo.commons.client.user.InternalUser;
import fr.sedoo.commons.client.util.ServiceException;
import fr.sedoo.sssdata.client.service.InternalUserService;
import fr.sedoo.sssdata.server.SSSDataApplication;
import fr.sedoo.sssdata.server.dao.api.AdministratorDAO;
import fr.sedoo.sssdata.shared.Administrator;

public class InternalUserServiceImpl extends RemoteServiceServlet implements InternalUserService {

	private AdministratorDAO dao;
	private void initDao(){
		if (dao == null){
			dao = (AdministratorDAO) SSSDataApplication.getSpringBeanFactory().getBeanByName(AdministratorDAO.BEAN_NAME);
		}
	}
	
	@Override
	public AuthenticatedUser internalLogin(String login, String password)
			throws ServiceException {

		InternalUser superAdmin = SSSDataApplication.getSuperAdmin();
		
		if (superAdmin != null && superAdmin.getLogin().equals(login) && superAdmin.getPassword().equals(password)){
			return superAdmin;
		}else{
			initDao();
			Administrator a = dao.login(login, password);
			
			if (a == null){
				return null;
			}else{
				DefaultAuthenticatedUser user = new DefaultAuthenticatedUser();
				user.setAdmin(true);
				user.setName(a.getName());
				return user;		
			}
			
		}
		
		
	}

}
