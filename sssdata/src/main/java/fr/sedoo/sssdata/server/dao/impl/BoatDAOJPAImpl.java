package fr.sedoo.sssdata.server.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

import fr.sedoo.sssdata.server.dao.api.BoatDAO;
import fr.sedoo.sssdata.server.model.Boat;
import fr.sedoo.sssdata.server.model.SssFile;

@Repository
@ContextConfiguration(locations={"classpath:/META-INF/spring/contextProvider.xml","classpath:/META-INF/spring/BeanLocations.xml"})
public class BoatDAOJPAImpl implements BoatDAO {

	protected EntityManager em;
	public EntityManager getEntityManager() {
		return em;
	}

	@PersistenceContext
	public void setEntityManager(EntityManager entityManager) {
		this.em = entityManager;
	}
	
	@Override
	@Transactional
	public List<Boat> findAll() {
		Query query = em.createQuery("SELECT boat FROM Boat boat ORDER BY boat.name");
		try{			
			return (List<Boat>) query.getResultList();
		}
		catch (NoResultException e){
			return new ArrayList<Boat>();
		}
	}

	@Override
	@Transactional
	public Boat save(Boat boat) {
		if (boat.getId() == null)
		{
			getEntityManager().persist(boat);
			return boat;
		}
		else
		{
			getEntityManager().merge(boat);
			return boat;
		}
		
	}

	@Override
	@Transactional
	public Boat findByName(String name) {
		Query query = em.createQuery("Select boat from Boat boat where boat.name=:arg1");
		query.setParameter("arg1", name);
		Boat boat = null;
		
		try{
			boat = (Boat) query.getSingleResult();
			//On force le chargement des files
			boat.getFiles().size();
		}
		catch (NoResultException e){
			return null;
		}
		
		boat.getFiles();
		return boat;
	}

	@Override
	@Transactional
	public Boat findByCode(String code) {
		Query query = em.createQuery("Select boat from Boat boat where boat.code=:arg1");
		query.setParameter("arg1", code);
		Boat boat = null;
		
		try{
			boat = (Boat) query.getSingleResult();
			//On force le chargement des files
			boat.getFiles().size();
		}
		catch (NoResultException e){
			return null;
		}
		
		boat.getFiles();
		return boat;
	}
	
	@Override
	@Transactional
	public Boat findAllFiles(String name) {
		Query query = em.createQuery("Select boat from Boat boat where boat.name=:arg1");
		query.setParameter("arg1", name);
		Boat boat = null;
		
		try{
			boat = (Boat) query.getSingleResult();
			//On force le chargement des files
			boat.getFiles().size();
			
			// chargement de toutes les measure
			for (SssFile f : boat.getFiles()){
				f.getMeasures().size();
			}
		}
		catch (NoResultException e){
			return null;
		}
		
		boat.getFiles();
		return boat;
	}

	@Override
	public Long getSize() {
		Query q = em.createQuery("SELECT count(boat.name) FROM Boat boat");
		
		try{
			return (Long) q.getSingleResult ();
		}
		catch (NoResultException e){
			return new Long(0);
		}
	}

	
	

}
