package fr.sedoo.sssdata.server.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.io.FileUtils;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.sedoo.commons.client.util.ServiceException;
import fr.sedoo.sssdata.client.service.JournalService;
import fr.sedoo.sssdata.server.SSSDataApplication;
import fr.sedoo.sssdata.server.dao.api.DownloadDAO;
import fr.sedoo.sssdata.server.dao.api.MeasureDAO;
import fr.sedoo.sssdata.server.dao.api.SssFileDAO;
import fr.sedoo.sssdata.server.model.Download;
import fr.sedoo.sssdata.server.model.SssFile;
import fr.sedoo.sssdata.server.model.SssFileJournal;
import fr.sedoo.sssdata.server.model.UpdateInfos;
import fr.sedoo.sssdata.server.utils.UpdateDTOComparatorByFilename;
import fr.sedoo.sssdata.shared.BoatDTO;
import fr.sedoo.sssdata.shared.DBStatusDTO;
import fr.sedoo.sssdata.shared.DownloadDTO;
import fr.sedoo.sssdata.shared.FileSearchInformations;
import fr.sedoo.sssdata.shared.GriddedProduct;
import fr.sedoo.sssdata.shared.UpdateDTO;

public class JournalServiceImpl extends RemoteServiceServlet implements JournalService {

	private DownloadDAO dlDao;
	private MeasureDAO mesureDAO;
	private SssFileDAO fileDAO;

	SimpleDateFormat welcomeDateFormat;
	SimpleDateFormat longDateFormat;
	SimpleDateFormat shortDateFormat;

	public JournalServiceImpl() {
		super();
		welcomeDateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH );
		shortDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		longDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		welcomeDateFormat.setLenient(false);
		longDateFormat.setLenient(false);
		shortDateFormat.setLenient(false);
	}

	private void initDao(){
		if (dlDao == null){
			dlDao = (DownloadDAO) SSSDataApplication.getSpringBeanFactory().getBeanByName(DownloadDAO.BEAN_NAME);
		}
		if (fileDAO == null){
			fileDAO = (SssFileDAO) SSSDataApplication.getSpringBeanFactory().getBeanByName(SssFileDAO.BEAN_NAME);
		}
		if (mesureDAO == null){
			mesureDAO = (MeasureDAO) SSSDataApplication.getSpringBeanFactory().getBeanByName(MeasureDAO.BEAN_NAME);
		}
	}

	@Override
	public List<DownloadDTO> getAllDownloads() throws ServiceException {
		initDao();
		List<DownloadDTO> result = new ArrayList<DownloadDTO>();
		for (Download dl : dlDao.getAllDownloads(true)) {
			result.add(buildDownloadDTO(dl));
		}
		return result;
	}

	private DownloadDTO buildDownloadDTO(Download dl){
		if (dl == null){
			return null;
		}
		DownloadDTO d = new DownloadDTO();
		d.setDate(longDateFormat.format(dl.getDateDL()));
		d.setUser(dl.getUser().getName());
		d.setUserEmail(dl.getUser().getEmail());
		d.setFormat(dl.getDataFormat());
		d.setCountry(dl.getUser().getCountry());
		d.setObjectives(dl.getGoal());
		if (dl.getVolume() != null){
			d.setVolume(FileUtils.byteCountToDisplaySize(dl.getVolume()));
		}else{
			d.setVolume("NA");
		}
		
		if (dl.getArchive() != null ){
			if (dl.getArchive().getSize() == GriddedProduct.GRID_SIZE){
				d.getFiles().add(dl.getArchive().getName());
			}else{
				d.getFiles().add(dl.getArchive().getName() + " (" + dl.getArchive().getSize() + " data files)");
			}
			d.setSize(dl.getArchive().getSize());
		}else{
			for (SssFile file: dl.getFiles()){
				d.getFiles().add(file.getName());
			}
			d.setSize(d.getFiles().size());
		}
		d.setLabo(dl.getLabo());
		d.setId(dl.getId().intValue());
		return d;
	}

	@Override
	public DownloadDTO getDownload(int id) throws ServiceException {
		initDao();
		Download dl = dlDao.getDownload(new Long(id), true);
		return buildDownloadDTO(dl);
	}

	@Override
	public List<UpdateDTO> getLastUpdates(int nb) throws ServiceException {
		initDao();		
		List<UpdateDTO> updates = new ArrayList<UpdateDTO>();
		List<SssFile> files = fileDAO.getLastUpdatedFiles(nb, true);
		for (SssFile f: files){
			updates.add(buildUpdateDTO(f));
		}
		return updates;
	}

	@Override
	public List<UpdateDTO> getAllUpdates() throws ServiceException {
		return getLastUpdates(0);
	}

	@Override
	public Integer getUpdateHits() throws ServiceException {
		initDao();
		Long result = fileDAO.getSize();
		return result.intValue();
	}

	@Override
	public List<UpdateDTO> getUpdatesByPage(int first, int pageSize) throws ServiceException {
		initDao();
		List<SssFile> files = fileDAO.getUpdatedFilesByPage(first, pageSize,
				true);
		List<UpdateDTO> result = new ArrayList<UpdateDTO>();
		for (SssFile f : files) {
			result.add(buildUpdateDTO(f));
		}
		return result;
	}

	private UpdateDTO buildUpdateDTO(SssFile f){
		UpdateDTO u = new UpdateDTO();
		u.setFileName(f.getName());
		u.setBoatName(f.getBoat().getName());
		if (f.getDateModification() != null){
			u.setDate(welcomeDateFormat.format(f.getDateModification()));
		}
		if (f.getDateStart() != null ){
			u.setStartDate(shortDateFormat.format(f.getDateStart()));
		}
		if (f.getDateEnd() != null){
			u.setEndDate(shortDateFormat.format(f.getDateEnd()));
		}

		for (SssFileJournal j: f.getFileJournal()){
			u.getJournal().add(longDateFormat.format(j.getDate()) + " - " + j.getType());
		}
		
		Collections.sort(u.getJournal());
		return u;
	}

	@Override
	public List<UpdateDTO> searchFiles(FileSearchInformations infos) throws ServiceException {
		initDao();
		List<UpdateDTO> result = new ArrayList<UpdateDTO>();
		for(BoatDTO boat: infos.getBoatListSelection()){
			List<SssFile> files = fileDAO.getFilesByBoat(boat.getName(),
					infos.getBeginDate(), infos.getEndDate(), true);
			for (SssFile f: files){
				result.add(buildUpdateDTO(f));
			}

		}

		result.sort(UpdateDTOComparatorByFilename.getComparator());
		
		return result;
	}

	@Override
	public void deleteFile(UpdateDTO u) throws ServiceException{
		initDao();
		SssFile file = fileDAO.getFileByNameAndBoat(u.getFileName(),
				u.getBoatName(), true);
		mesureDAO.deleteByFile(file);

		fileDAO.markAsDeleted(file);
		/*SssFileJournal j = new SssFileJournal();
		j.setDate(new Date());
		j.setType(SssFileJournal.TYPE_DELETION);
		j.setFile(file);

		file.setMeasuresNb(0);
		file.setDateModification(null);
		file.setDateStart(null);
		file.setDateEnd(null);
		file.setEastLongitude(null);
		file.setWestLongitude(null);
		file.setNorthLatitude(null);
		file.setSouthLatitude(null);
		file.getFileJournal().add(j);
		fileDAO.save(file);*/
	}

	@Override
	public DBStatusDTO getDBStatus() throws ServiceException {
		initDao();

		DBStatusDTO s = new DBStatusDTO();
		Date lastDbUpdate = fileDAO.getLastDbUpdate();
		if (lastDbUpdate != null){
			s.setLastDbUpdateDate(welcomeDateFormat.format(lastDbUpdate));
		}
		s.setShipNb(fileDAO.getBoatNb());
		s.setInsertedFilesNb(fileDAO.getSize().intValue());

		try{
			UpdateInfos infos = fileDAO.getLastUpdateInfos();
			if (infos.getDateModification() != null){
				s.setLastModifiedFileDate(welcomeDateFormat.format(infos.getDateModification()));
			}
			if (infos.getDateMax() != null){
				s.setEndDate(shortDateFormat.format(infos.getDateMax()));
			}
			if (infos.getDateMin() != null){
				s.setStartDate(shortDateFormat.format(infos.getDateMin()));
			}
			if (infos.getDateCreation() != null){
				s.setLastCreatedFileDate(welcomeDateFormat.format(infos.getDateCreation()));
			}
			if (infos.getDateUpdate() != null){
				s.setLastUpdatedFileDate(welcomeDateFormat.format(infos.getDateUpdate()));
			}
		}catch(Exception e){
			throw new ServiceException("unable to retrieve update infos in the database");
		}
		return s;
	}

	@Override
	public void delete(int id) throws ServiceException {
		initDao();
		dlDao.delete(new Long(id));
	}

}
