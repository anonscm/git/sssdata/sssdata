import ucar.nc2.NetcdfFileWriter;


public class Test {
	public static void main(String[] args) {
		 try {
	            NetcdfFileWriter writer = NetcdfFileWriter.openExisting("/home/brissebr/ProfilTotauxtra_PANHAM_VOCs_anthro_benzene_2010_flux_0.1x0.1.nc");

	            //Passer en mode redefine
	            writer.setRedefineMode(true);

	            //Modifs
	            /*Variable var = writer.findVariable("SST");
	            Attribute nvAttr = new Attribute("TUT", "bla bla");
	            var.addAttribute(nvAttr);*/
	            
	            writer.renameVariable("emiss_tra", "emiss");

	            //Sortir du mode redefine (il faut valider les modifs avec flush() avant sinon ça plante)
	            writer.flush();
	            writer.setRedefineMode(false);

	            //Fermer le fichier
	            writer.close();
	        } catch (Exception e) {
	            e.printStackTrace();
	        }


	}
	
}
